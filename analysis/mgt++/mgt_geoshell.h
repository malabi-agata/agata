
extern double  rsh_int;              // inner radius of shell
extern double  rsh_int2;             // inner radius of shell squared


extern int     nsegsP;               // number of segments around detector axis
extern int     nsegsR;               // number of segments along  detector radius
extern double  ssegsP;               // size of segment around crystal axis
extern double  ssegsR;               // size of segment along  crystal radius

