extern int     nbshit;               // number of detector bounding spheres hit by the ray 1-2
extern int    *ibshit;               // which bounding spheres are hit

extern double  r_germ;               // radius       of ge crystal
extern double  h_germ;               // half-length  of ge_crystal
extern double  a_germ;               // inner radius of ge crystal
extern double  a_germ2;              // inner radius of ge crystal squared 
extern double  r_germ2;              // outer radius of ge crystal squared
extern double  r_germh;              // outer radius of ge crystal halved 
extern double  r_bsphere;            // radius of bounding sphere
extern double  r_bsphere2;           // radius of bounding sphere squared

extern double  dist12;

extern int     nsegsZ;               // number of segments along  detector axis
extern int     nsegsP;               // number of segments around detector axis
extern int     nsegsR;               // number of segments along  detector radius
extern double  ssegsZ;               // size of segment along  crystal axis
extern double  ssegsP;               // size of segment around crystal axis
extern double  ssegsR;               // size of segment along  crystal radius

