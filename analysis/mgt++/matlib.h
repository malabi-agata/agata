#if !defined(MAT_LIB__INCLUDED_)
#    define  MAT_LIB__INCLUDED_

#define MMATRICES    256     // Max number of 2D matrices

int     matDefine (int lenX, int lenY, const char *name, const char *info);         // define a new matrix
short  *matPointer(int nn);                                             // get pointer to matrix
int     matHowMany();                                                   // returns number of matrices defined
void    matIncr   (int x, int y, int nmat);                             // increment nmat at (x,y)
void    matSave   (int skipEmpty);                                      // save all defined matrices
void    matWrite  (short *mm, int nx, int ny, char *name, char *info);  // save matrix as binary W

#endif // !defined(SPEC_LIB__INCLUDED_)
