
int     nbshit;               // number of detector bounding spheres hit by the ray 1-2
int    *ibshit;               // which bounding spheres are hit

double  r_germ;               // radius       of ge crystal
double  h_germ;               // half-length  of ge_crystal
double  a_germ;               // inner radius of ge crystal
double  a_germ2;              // inner radius of ge crystal squared 
double  r_germ2;              // outer radius of ge crystal squared
double  r_germh;              // outer radius of ge crystal halved 
double  r_bsphere;            // radius of bounding sphere
double  r_bsphere2;           // radius of bounding sphere squared
double  dist12;
int     nsegsZ;               // number of segments along  detector axis
int     nsegsP;               // number of segments around detector axis
int     nsegsR;               // number of segments along  detector radius
double  ssegsZ;               // size of segment along  crystal axis
double  ssegsP;               // size of segment around crystal axis
double  ssegsR;               // size of segment along  crystal radius

