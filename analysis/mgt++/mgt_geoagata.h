
extern double  rsh_int;              // inner radius of shell
extern double  rsh_ext;              // inner radius of shell squared
extern double  rsh_int2;             // outer radius of shell
extern double  rsh_ext2;             // outer radius of shell squared
extern double  rsh_thick;            // thickness    of shell
   
extern double  shell_dd, shell_dp, shell_d1;   // distanza12^2, p1*dir, p1^2 (A, B, C+rr^2 dell'equazione dell'intercetta)
extern double  shell_t1, shell_t2;             // le due soluzioni dell'equazione di secondo grado

extern int     nsegsZ;               // number of segments along  detector axis
extern int     nsegsP;                // number of segments around detector axis
extern int     nsegsR;               // number of segments along  detector radius
