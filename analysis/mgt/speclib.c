
#include <stdlib.h>
#include <string.h> 
#include <stdio.h>

#include "speclib.h"

double  specGain = 1.;

int     howManySpecs;               // numero di spettri multipli definiti
int    *specData[MSPECTRA];         // puntatori agli spettri multipli
int     specLength[MSPECTRA];       // numero di canali degli spettri
int     specCount[MSPECTRA];        // quanti spettri per i vari tipi
int     specEmpty[MSPECTRA];        // flag spettro non ancora incrementato
char    specName[MSPECTRA][100];    // nome degli spettri
char    specInfo[MSPECTRA][100];    // descrizione degli spettri
int     specBadNumber;              // specincr con errore in specNumber
int     specBadCount;               // specincr con errore in specCount


int
specDefine(int nchan, int nspe, char *name, char *info)
{
  if(howManySpecs == MSPECTRA) {
    printf("Too many spectra: Max is %d\n", MSPECTRA);
    printf("Increase MSPECTRA in speclib.h");
    exit(-1);
  }

  specLength[howManySpecs] = nchan;
  specCount[howManySpecs]  = nspe;
  strcpy(specName[howManySpecs], name); 
  strcpy(specInfo[howManySpecs], info); 
  specData[howManySpecs]  = calloc(specLength[howManySpecs]*specCount[howManySpecs], sizeof(int));
  specEmpty[howManySpecs] = 1;

  return howManySpecs++;
}

void
specSetGain(double gain)
{
  specGain = gain;
}

int *
specPointer(int count, int number)
{
  if(number < 0 || number >= howManySpecs)       return NULL;
  if(count  < 0 || count  >= specCount[number]) return NULL;
  return specData[number] + count*specLength[number];
}

int
specHowMany(){
  return howManySpecs;
}

void
specIncr(int channel, int count, int number)
{
  int speclen;

  if(number < 0 || number >= howManySpecs) {
    specBadNumber++;
    return;
  }
  if(count < 0 || count >= specCount[number]) {
    specBadCount++; 
    return;
  }

  speclen = specLength[number];

  if(channel >= speclen) channel = speclen - 1;
  else if(channel < 0)   channel = 0;
  specData[number][count*speclen + channel]++;
  specEmpty[number] = 0;

}

void
specIncrValue(int channel, int count, int number, int val)
{
  int speclen;

  if(number < 0 || number >= howManySpecs) {
    specBadNumber++;
    return;
  }
  if(count < 0 || count >= specCount[number]) {
    specBadCount++; 
    return;
  }

  speclen = specLength[number];

  if(channel >= speclen) channel = speclen - 1;
  else if(channel < 0)   channel = 0;
  specData[number][count*speclen + channel] += val;
  specEmpty[number] = 0;

}

void
specIncrGain(double energy, int count, int number)
{
  int speclen, channel;

  if(number < 0 || number >= howManySpecs) {
    specBadNumber++;
    return;
  }
  if(count < 0 || count >= specCount[number]) {
    specBadCount++; 
    return;
  }

  speclen = specLength[number];

  channel = (int)(energy*specGain);

  if(channel >= speclen) channel = speclen - 1;
  else if(channel < 0)   channel = 0;
  specData[number][count*speclen + channel]++;
  specEmpty[number] = 0;

}

void
specSave(int skipEmpty)
{
  int nn;

  for(nn = 0; nn < howManySpecs; nn++) {
    if(skipEmpty && specEmpty[nn]) 
       printf("Empty %s (%s)\n", specName[nn], specInfo[nn]);
    else
      specWrite(specData[nn], specLength[nn], specCount[nn], specName[nn], specInfo[nn]);
  }
    
}

void
specWrite(int *spec, int nchan, int count, char *name, char *linfo)
{
  FILE *ofp;
  int nn, totchan;
  
  totchan = nchan * count;
  
  if( !(ofp = fopen(name,"wb")) ) {
    printf("Error opening file %s\n", name);
    return;
  }

  nn = fwrite(spec, sizeof(int), totchan, ofp);
  if(nn ==  totchan ) {
    printf("File  %s  %4d L:%d spectrum (%s)\n", name, count, nchan/1024, linfo);
  }
  else {
    printf("Error writing spectrum %s\n", name);
  }

  fclose(ofp);
}

void
specWriteASCII(int *spec, int chan1, int chan2, char *name)
{
  FILE *ofpt;
  int   nn;
  
  if( !(ofpt = fopen(name,"w")) ) {
    printf("Error opening file %s\n", name);
    return;
  }

//for(nn = chan1; nn < chan2; nn++) fprintf(ofpt, "%d %d\n", nn, *(spec+nn));
  for(nn = chan1; nn < chan2; nn++) fprintf(ofpt, "%d\n", *(spec+nn));
  printf("File %s\\A %d channels\n", name, chan2-chan1+1);

  fclose(ofpt);
}

int
specRead(int *spec, int nchan, int count, char *name)
{
  FILE *ifp;
  int nn;
  
  if( !(ifp = fopen(name,"rb")) ) {
    printf("Error opening file %s\n", name);
    return 0;
  }

  if(count > 0) {
    nn = fseek(ifp, nchan*count*sizeof(long), SEEK_SET);
    if(nn) {
      printf("Error seeking spectrum %s\n", name);
      return 0;
    }
  }
  nn = fread(spec, sizeof(int), nchan, ifp);
  if(nn ==  nchan ) {
    printf("File  %s  %4d ", name, count);
    fclose(ifp);
    return 1;
  }
  else {
    printf("Error reading spectrum %s\n", name);
    fclose(ifp);
    return 0;
  }

}
