#if !defined(VECT_LIB__INCLUDED_)
#    define  VECT_LIB__INCLUDED_

// vettore 3D per punti e direzioni
typedef struct {
  double xx;
  double yy;
  double zz;
} V3D;

// vettore 3D in coordinate polari
typedef struct {
  double rr;
  double th;
  double ph;
} P3D;

// angoli di eulero (di solito in gradi)
typedef struct {
  double ps;
  double th;
  double ph;
} Euler;

// vettore 4D per equazioni di piani
typedef struct {
  double xx;
  double yy;
  double zz;
  double tt;
} V4D;

double  dotprod     (V3D* a, V3D* b);                     // prodotto scalare a.b
double  dotprodNorm (V3D* a, V3D* b);                     // coseno dell'angolo tra a e b
double  acosdotprod (V3D* a, V3D* b);                     // angolo tra i due vettori
void    crossprod   (V3D* a, V3D* b, V3D* c);             // prodotto esterno c = a x b
void    normalize   (V3D* v);                             // normalizza vettore v
double  modulo      (V3D* a);                             // ritorna la lunghezza del vettore
double  modulo2     (V3D* a);                             // ritorna la lunghezza del vettore
double  distance    (V3D* a, V3D* b);                     // distanza tra i 2 punti
double  distance2   (V3D* a, V3D* b);                     // (distanza)^2 tra i 2 punti
double  point2line  (V3D* p, V3D* a, V3D* b);             // distanza del punto p dalla linea ab
double  point2plane (V3D* p, V4D* vmat);                  // distanza del punto p dal piano vmat
void    cart2pol    (V3D* c, P3D* p);                     // trova le coordinate polari di c
void    pol2cart    (P3D* p, V3D* c);                     // trova le coordinate cartesiane di p
void    swap3D      (V3D* a, V3D* b);                     // swap dei 2 punti
void    copy3D      (V3D* a, V3D* b);                     // copia a in b
void    add3D       (V3D* a, V3D* b, V3D* c);             // c = a + b
void    sub3D       (V3D* a, V3D* b, V3D* c);             // c = a - b, cioe' il vettore da b ad a
void    line3D      (V3D* a, V3D* b, V3D* c);             // c = b - a, cioe' il vettore da a ad b
void    midpoint3D  (V3D* a, V3D* b, V3D* c);             // c = (a+b)/2
void    rot3D1x     (double deg, V3D* v);                 // rotazione ccw attorno a Ox, risultato in v
void    rot3D1y     (double deg, V3D* v);                 // rotazione ccw attorno a Oy, risultato in v
void    rot3D1z     (double deg, V3D* v);                 // rotazione ccw attorno a Oz, risultato in v
void    rot3D1d     (double deg, V3D* d, V3D* v);         // rotazione ccw attorno a Od, risultato in v
void    rot3D1E     (double ps, double th, double ph, V3D* v);  // ruotazione v = Rz(ph)*Ry(th)*Rz(ps)*v
void    rot3D1EE    (Euler *eu, V3D* v);                  // ruota usando eu.ps eu.th eu.ph
void    rot3D1EEI   (Euler *eu, V3D* v);                  // v = Rz(-ps)Ry(-th)Rz(-ph)v = Rz''(ph)Ry'(th)Rz(ps)v
void    rot3D2x     (double deg, V3D* v, V3D* r);         // rotazione ccw attorno a Ox, risultato in r
void    rot3D2y     (double deg, V3D* v, V3D* r);         // rotazione ccw attorno a Oy, risultato in r
void    rot3D2z     (double deg, V3D* v, V3D* r);         // rotazione ccw attorno a Oz, risultato in r
void    rot3D2d     (double deg, V3D* d, V3D* v, V3D* r); // rotazione ccw attorno a Od, risultato in r
void    rot3D2E     (double ps, double th, double ph, V3D* v, V3D* r);  // r = Rz(ph)*Ry(th)*Rz(ps)*v
void    rot3D2EE    (Euler *eu, V3D* v, V3D* r);          // ruota usando eu.ps eu.th eu.ph
void    rot3D2EEI   (Euler *eu, V3D* v, V3D* r);          // v = Rz(-eu.ps)Ry(-eu.th)Rz(-eu.ph)v
void    plane3P     (V3D* p1, V3D* p2, V3D* p3, V4D* vv); // plane defined by 3 points
int     X3Planes    (V4D* v0, V4D* v1, V4D* v2, V3D* pX); // point intersection of 3 planes
int     XPlaneLine  (V4D* vv, V3D* pA, V3D* pB, V3D* pX); // point intersection of line AB with Plane vv
int     X2Lines     (V3D* a1, V3D* a2, V3D* b1, V3D* b2, V3D* p1, V3D* p2); // p1 e p2 punti di minima distanza delle rette a1a1 e b1b2
#endif // !defined(VECT_LIB__INCLUDED_)
