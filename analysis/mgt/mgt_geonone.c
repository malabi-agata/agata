////////////////////////////////////////////////////
/////////// GEOMETRY: NONE /////////////////////////
////////////////////////////////////////////////////

#include "mgt.h"

int     insidegedet_none    (point *);
void    findsegment_none    (point *);
void    setsegcenter_none   (point *);
double  gedistance_none     (point *, point *);

void
geominit_none(int ir)
{
  int ii;
  char line[100];

  insidegedet  = insidegedet_none;
  findsegment  = findsegment_none;
  setsegcenter = setsegcenter_none;
  gedistance   = gedistance_none;
  if(ir) {
    if(fscanf(ifp, "%d", &ngedets) != 1) {
      printf("Error reading %s\n", ifname);
      errexit("geominit_none");
    }
    for(ii=0; ii < ngedets; ii++)
      fgets(line, 99, ifp);
  }
  else {
    ngedets = 1;
  }
  nsegdet  = 1;
  nsegtot  = 1;
  CDet     = (coors *)calloc(ngedets, sizeof(coors));
  GEOMETRY = NONE;
  return;
}

int
insidegedet_none(point *pp)
{
  return 1;
}

void
findsegment_none(point *pp)
{
  pp->ns = 0;
  pp->seg = pp->nd;
}

void
setsegcenter_none(point *pp)
{
  return;
}

double
gedistance_none(point *p1, point *p2)
{
  double xx, yy, zz;

  xx = p2->xx - p1->xx;
  yy = p2->yy - p1->yy;
  zz = p2->zz - p1->zz;
  return sqrt(xx*xx + yy*yy + zz*zz);
}
