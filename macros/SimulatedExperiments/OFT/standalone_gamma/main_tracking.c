#include <stdlib.h>
#include <stdio.h>
#include "tracking_events.h"
#include "tracking_pack_smear.h"
#include <math.h>


struct ancillary{
  char AncillaryName[256];
  int det_id[1000]; /*not more than 1000 hits in one event*/
  int seg_id[1000];
  double e[1000];
  int numberofhits;
};

void MakeCoreEnergies(const CntlStruct *cntlstruct, double ce[]);
 
int main(int argc, char * argv[]){
  FILE *in_file, *out_file;
  char fn[50];
  char buffer[200],dummy_str[50];
  double coreenergies[180];
  int dummy_i;
  double dummy_f;
  int stop_condition=0;
  int multiplicity;
  int tot_number_of_gammas;
  unsigned int error_code;
  int i,j_in,j,k,l,number_of_gammas=0,number_of_events=0,number_of_interacting_events=0;
  int particle_code;
  int crystal_numb,segment_numb;
  double energy,x,y,z,xsource,ysource,zsource,radius,radius_out;
  char last_char='$';
  int nbarycenter,npack,ncenter,nsmearp,nsmeare;
  int nintu=1000,read_seg_pos_flag=0; /* initialisation for random number vs uncertainty procedure */
  double u2,ddu=0.005; /* step in random number vs uncertainty procedure */
  double ee1u=0; /* initial value for random number vs uncertainty procedure */
  double facu,erfcu[1000];
  double segx[180][60],segy[180][60],segz[180][60]; /* positions of centers of segments - are generally written to Agata output if verbose level > 1 */
  CntlStruct *carac_process;
  int  EmissionCode=-1;
  double corrected;
  double dirg[3];
  double mdirg; 
  double gamma;
  double costheta;
  double beta;
  double vxnew,vynew,vznew,pxnew,pynew,pznew,betanew;
  struct ancillary Ancillaries[10]; /*max 10, should be ok*/
  int ancillaryLUT[10];
  int numberofancillaries = 0;
  int atancillary;
  int addto;
  int whichanc;
  int readthisline;
  
  if(getenv("tracking_EmissionCode")) EmissionCode = atoi(getenv("tracking_EmissionCode"));
  /*******************************************************************/
  /* mapping random number vs gaussian uncertainty in units of sigma */
  /*******************************************************************/
  u2=ddu;
  for(j=0;j<nintu;j++) {   
    ee1u=ee1u+exp(-(u2*u2/2.));   
    erfcu[j]=ee1u;
    u2=u2+ddu;
  }
  facu=ee1u;
  for(j=0;j<nintu;j++) {
    erfcu[j]=erfcu[j]/facu; /* random number  => (ddu * j)  in units of sigma  */  
  }
	
  /*********************************/
  /********* info *****************/
  /********************************/
	
  if(argc ==1) {
    printf("The program uses a source position of (0,0,0) unless the output of the Agata code specifies otherwise \n");
    printf("The geometry is supposed to be of Agata type with inner radius 23.5 cm and outer radius 32.9 cm \n");
    printf("but it can function with a 4 pi shell-type geometry as well and read the appropriate radii from file \n");
    printf("A default energy threshold of 5 keV on the interaction point energies is set in the tracjing_define.h \n");
    printf("The required arguments to run the program are the following: \n");
    printf("    argument 1 = Agata output file name \n");
    printf("    argument 2 = required incident photon multiplicity \n");
    printf("    argument 3 = required number of incident photons \n");
    printf("    argument 4 = 1 if packing points within 5 mm (defined in tracking_efine.h), 0 otherwise \n");
    printf("    argument 5 = 1 if packing points in segments at the energy weighted barycenter, 0 otherwise \n");
    printf("    argument 6 = 1 if packing points at the center of segments, 0 otherwise  ! need verbose level > 1 \n");
    printf("    argument 7 = 1 if positions are smeared with an energy-dependent formula, 0 otherwise \n");
    printf("    argument 8 = 1 if energies are semared, 0 otherwise \n");
    printf("    arguemnt 9 = emission code (optional default=-1)\n");
    return error_code=3;
  }
  if(!(argc == 9 || argc == 10)){
    error_code = 2;
    printf(" Insufficient number of arguments ! \n");
    printf("    argument 1 = Agata output file name \n");
    printf("    argument 2 = required incident photon multiplicity \n");
    printf("    argument 3 = required number of incident photons \n");
    printf("    argument 4 = 1 if packing points within 5 mm, 0 otherwise \n");
    printf("    argument 5 = 1 if packing points in segments at energy weighted barycenter, 0 otherwise \n");
    printf("    argument 6 = 1 if packing points at the center of segments, 0 otherwise  ! need verbose level > 1 \n");
    printf("    argument 7 = 1 if positions are smeared, 0 otherwise \n");
    printf("    argument 8 = 1 if energies are semared, 0 otherwise \n");
    printf("    arguemnt 9 = emission code (optional default=-1)\n");
    return error_code;
  }
  if(argc==10) EmissionCode = atoi(argv[9]);	

  /****************************************************/
  /********** initialising data containers  ***********/
  /****************************************************/
  for(i=0;i<180;i++){
    for(k=0;k<6;k++){
      for(l=0;l<6;l++){
	segx[i][k*10+l]=0; /* setting segment center positions to 0 */
	segy[i][k*10+l]=0;
	segz[i][k*10+l]=0;
      }
    }
  }
  carac_process = (CntlStruct *) malloc( sizeof(CntlStruct) );
  carac_process->etot = (double *)calloc(intmax, sizeof(double));
  carac_process->xpos = (double *)calloc(intmax, sizeof(double));
  carac_process->ypos = (double *)calloc(intmax, sizeof(double));
  carac_process->zpos = (double *)calloc(intmax, sizeof(double));
  carac_process->xfirst  = (double *)calloc(intmax, sizeof(double));
  carac_process->yfirst  = (double *)calloc(intmax, sizeof(double));
  carac_process->zfirst  = (double *)calloc(intmax, sizeof(double));
  carac_process->xsecond = (double *)calloc(intmax, sizeof(double));
  carac_process->ysecond = (double *)calloc(intmax, sizeof(double));
  carac_process->zsecond = (double *)calloc(intmax, sizeof(double));
  carac_process->probtot = (double *)calloc(intmax, sizeof(double));
  carac_process->flagu = (int *)calloc(intmax, sizeof(int));
  carac_process->nbtot = (int *)calloc(intmax, sizeof(int));
  carac_process->e    = (double *)calloc(intmax, sizeof(double));
  carac_process->et   = (double *)calloc(intmax, sizeof(double));
  carac_process->r    = (double *)calloc(intmax*intmax, sizeof(double));
  carac_process->r_ge = (double *)calloc(intmax*intmax, sizeof(double));
  carac_process->sn          = (int *)calloc(kmax*intmax, sizeof(int));
  carac_process->interaction = (int *)calloc(kmax*intmax, sizeof(int));
  carac_process->seg = (int *)calloc(intmax, sizeof(int));
  carac_process->det = (int *)calloc(intmax, sizeof(int));
  carac_process->angtheta = (double *)calloc(intmax, sizeof(double));
  carac_process->angphi   = (double *)calloc(intmax, sizeof(double));
  carac_process->angn     = (double *)calloc(intmax, sizeof(double));
  carac_process->numn = (int *)calloc(intmax, sizeof(int));
  carac_process->source = (double *)calloc(3, sizeof(double));
  carac_process->source[0] = 0; /* default values - can read them from file */
  carac_process->source[1] = 0;
  carac_process->source[2] = 0;
  carac_process->vel = (double *)calloc(3, sizeof(double));
  carac_process->vel[0] = 0; /* default values - can read them from file */
  carac_process->vel[1] = 0;
  carac_process->vel[2] = 0;
  carac_process->inner_r = 23.5;  /* default values : inner radius (cm) of AGATA or AGATA demonstrator THIS SHOULD NEVER CHANGE */
  carac_process->outer_r = 32.9;  /* default values: outer radius (cm) of AGATA or AGATA demonstrator THIS SHOULD NEVER CHANGE */
  carac_process->mult   = 0; /* tracked multiplicity */
  carac_process->nb_int = 0; /* number of interaction points in the event */

		

  /****************************************************/
  /********** read in data file name ******************/
  /********** use default file name *******************/
  /****************************************************/
  sprintf(fn, argv[1]);
  in_file = fopen(fn, "r");
  if (!in_file) {
    printf("could not open file %s\n", fn);
    error_code = 1;
    stop_condition=1;
  } else {
    printf("opened file %s\n", fn);
    error_code = 0;
  }
  sprintf(fn,"tracked_energies");
  out_file = fopen(fn,"w");
  /***********************************************************/
  /********** read in required multiplicity ******************/
  /********** and total number of incident gammas ***********/
  /**********************************************************/
  multiplicity = atoi(argv[2]);
  tot_number_of_gammas=atoi(argv[3]);
  printf("required incident multiplicity = %d \n",multiplicity);
  printf("required number of incident photons = %d \n",tot_number_of_gammas);
  if(multiplicity <=0 || multiplicity > 30){
    error_code=1;
    printf("inappropriate multiplicity \n");
    return error_code;
  }
  if(tot_number_of_gammas<=0){
    error_code=1;
    printf("inappropriate number of photons \n");
    return error_code;
  }
  npack=atoi(argv[4]);
  nbarycenter=atoi(argv[5]);
  ncenter=atoi(argv[6]);
  nsmearp=atoi(argv[7]);
  nsmeare=atoi(argv[8]);
  printf("npack = %d nbarycenter = %d ncenter = %d nsmearp = %d nsmeare = %d \n",npack,nbarycenter,ncenter,nsmearp,nsmeare);
	
  /****************************************************/
  /********** reading data file buffer ***************/
  /****************************************************/
  if(stop_condition)return error_code;
  while(!stop_condition) {
    if(fgets(buffer,sizeof(buffer),in_file)== NULL){
      printf("\nend of file \n");
      stop_condition=1;
      error_code = 1;
      printf("\nprocessed %d events with interaction \n",number_of_interacting_events);
      return error_code;
    } 
    if(strncmp(buffer,"SUMMARY",7)==0){
      sscanf(buffer,"%s %lf %lf",dummy_str,&radius,&radius_out);
      carac_process->inner_r= radius/10; /* in cm */
      carac_process->outer_r= radius_out/10; /* in cm */
      printf("inner and outer radius = %f %f \n",radius,radius_out);
    }
    if(strncmp(buffer,"SOURCE",6)==0){
      sscanf(buffer,"%s %d %d %lf %lf %lf",dummy_str,&dummy_i,&dummy_i,&xsource,&ysource,&zsource); 
      carac_process->source[0] = xsource/10.; /* convert to cm for tracking */
      carac_process->source[1] = ysource/10.;
      carac_process->source[2] = zsource/10.;
      printf("source position %f %f %f  \n",carac_process->source[0], carac_process->source[1],carac_process->source[2]); 
    }
    if (strncmp(buffer,"POSITION_SEGMENTS",17)==0) {
      read_seg_pos_flag=1;
      for(i=0;i<180;i++){
	for(k=0;k<6;k++){
	  for(l=0;l<6;l++){
	    fgets(buffer,sizeof(buffer),in_file);   
	    sscanf(buffer,"%d %d %d  %lf %lf %lf %lf ",&dummy_i,&dummy_i,&dummy_i,&segx[i][k*10+l],&segy[i][k*10+l],&segz[i][k*10+l],&dummy_f);				
	  }
	}
      }
    }
    if(strncmp(buffer,"ANCIL",5)==0){/* We got an ancillary*/
      sscanf(buffer,"%s %s %d %d",dummy_str, Ancillaries[numberofancillaries].AncillaryName,&dummy_i,&ancillaryLUT[numberofancillaries]);
      printf("Found ancillary %s with offset %d\n",Ancillaries[numberofancillaries].AncillaryName,ancillaryLUT[numberofancillaries]);
      numberofancillaries++;
    }
    if(strncmp(buffer,&last_char,1)==0){
      stop_condition=1;   
      error_code = 0;
    }        
  }
  if(read_seg_pos_flag==0 && ncenter==1){
    error_code=1;
    printf("cannot pack positions in the center of the segments as the file does not have sufficient verbose level \n");
    return error_code;
  }
  printf("\nend of buffer \n");

  /****************************************************/
  /************* reading data file ***** **************/
  /****************************************************/
  stop_condition = 0;
  j_in=0;
  pxnew=pynew=pznew=vxnew=vynew=vznew=betanew=0;
  while(!stop_condition){  
    carac_process->nb_int=0;
    carac_process->mult=0;
    /* Clear ancillary data */
    atancillary = 0;
    for(atancillary=0; atancillary<numberofancillaries; atancillary++){
      Ancillaries[atancillary].numberofhits=0;
    }
    readthisline = 1;
    while(j_in<=multiplicity && !stop_condition){
      if(readthisline){
	if( fgets(buffer,sizeof(buffer),in_file)== NULL){
	  printf("\nend of file \n");
	  stop_condition=1;
	  error_code = 1;
	  printf("\nprocessed %d events with interaction \n",number_of_interacting_events);
	  return error_code;
	} 
      }
      readthisline = 1;
      sscanf(buffer,"%d",&particle_code);
      /*
	Check for event token (-100), source velocity  (-101) and source position (-102)
      */
      if(particle_code==-100) {
	continue;
      }
      if(particle_code==-101) {
	/*sscanf(buffer,"%d %lf %lf %lf %lf",&particle_code,&beta,&carac_process->vel[0], &carac_process->vel[1],&carac_process->vel[2]);
	carac_process->vel[0]*=beta;
	carac_process->vel[1]*=beta;
	carac_process->vel[2]*=beta;*/
	carac_process->vel[0]=betanew*vxnew;
	carac_process->vel[1]=betanew*vynew;
	carac_process->vel[2]=betanew*vznew;
	sscanf(buffer,"%d %lf %lf %lf %lf",&particle_code,&betanew,&vxnew,&vynew,&vznew);
	continue;
      }
      if(particle_code==-102) {
	/*sscanf(buffer,"%d %lf %lf %lf",&particle_code,&carac_process->source[0], &carac_process->source[1],&carac_process->source[2]);*/
	carac_process->source[0]=pxnew;
	carac_process->source[1]=pynew;
	carac_process->source[2]=pznew;
	sscanf(buffer,"%d %lf %lf %lf",&particle_code,&pxnew,&pynew,&pznew);
	continue;
      }
      if(particle_code==-104){
	continue;
      }
      if(particle_code==EmissionCode){
	number_of_gammas ++;
	if(number_of_gammas>tot_number_of_gammas)stop_condition=1;
	j_in++;
	continue;
      }
      readthisline = 0;
      while(!(particle_code == EmissionCode || particle_code==-100 || 
	      particle_code==-101 || particle_code==-102 || 
	      particle_code==-104)){
	sscanf(buffer,"%d %lf %lf %lf %lf %d",&crystal_numb,&energy,&x,&y,&z,
	       &segment_numb);
	if(crystal_numb<1000){/*Skip ancillaries*/
	  carac_process->xpos[carac_process->nb_int]=x/10.; /* cm */
	  carac_process->ypos[carac_process->nb_int]=y/10.;
	  carac_process->zpos[carac_process->nb_int]=z/10.;
	  carac_process->e[carac_process->nb_int]=energy/1000.; /* MeV */
	  carac_process->det[carac_process->nb_int]=crystal_numb;
	  carac_process->seg[carac_process->nb_int]=segment_numb;
	  carac_process->nb_int++;
	} else {/*Here we have to do something with the ancillaries...*/
	  /* First find offset*/
	  int offset;offset = (crystal_numb/1000)*1000;
	  /* Get anc.*/
	  whichanc = 0;
	  while(ancillaryLUT[whichanc]!=offset && whichanc<10) whichanc++;
	  /* add data, however we will check if det_id already is in there and if so add*/
	  addto = -1;
	  for(i=0; i<Ancillaries[whichanc].numberofhits; i++){
	    if((Ancillaries[whichanc].det_id[i]==crystal_numb%1000) &&
	       (Ancillaries[whichanc].seg_id[i]==segment_numb)) addto = i;
	  }
	  if(addto>=0){
	    Ancillaries[whichanc].e[addto]+=energy/1000.;
	  } else {
	    Ancillaries[whichanc].det_id[Ancillaries[whichanc].numberofhits]=crystal_numb%1000; /* We get rid of offset */
	    Ancillaries[whichanc].seg_id[Ancillaries[whichanc].numberofhits]=segment_numb; /* We get rid of offset */
	    Ancillaries[whichanc].e[Ancillaries[whichanc].numberofhits]=energy/1000.;
	    Ancillaries[whichanc].numberofhits++;
	  }
	}
	if(fgets(buffer,sizeof(buffer),in_file)== NULL){
	  printf("\nend of file \n");
	  stop_condition=1; 
	  error_code = 1;
	  printf("\nprocessed %d events with interaction \n",number_of_interacting_events);
	  return error_code;
	}
	sscanf(buffer,"%d",&particle_code);
      }
    }

    /***************************************************/
    /************* packing and smearing ***************/
    /****************************************************/
    if(npack==1)carac_process->nb_int=packpoints(carac_process->nb_int,carac_process->xpos,carac_process->ypos,
						 carac_process->zpos,carac_process->e,carac_process->det,carac_process->seg);
    if(nbarycenter==1)carac_process->nb_int=packpointsbarycenter(carac_process->nb_int,carac_process->xpos,carac_process->ypos,
								 carac_process->zpos,carac_process->e,carac_process->det,carac_process->seg);
    if(ncenter==1)carac_process->nb_int=packpointscenter(carac_process->nb_int,carac_process->xpos,carac_process->ypos,
							 carac_process->zpos,carac_process->e,carac_process->det,carac_process->seg,segx,segy,segz);
    if(nsmearp==1)smearpoints(carac_process->nb_int,carac_process->e,carac_process->xpos,carac_process->ypos,carac_process->zpos,erfcu,ddu,nintu);
    if(nsmeare==1){
      smearenergies(carac_process->nb_int,carac_process->e,erfcu,ddu,nintu);
      for(i=0; i<numberofancillaries; i++){
	smearenergies(Ancillaries[i].numberofhits,Ancillaries[i].e,erfcu,ddu,nintu);
      }
    }

    /*****************************************************/
    /**************** make core signals ******************/
    /****************************************************/

    MakeCoreEnergies(carac_process,coreenergies);
		  
    /****************************************************/
    /************* tracking event ********************/
    /****************************************************/
    number_of_events++;
    if(carac_process->nb_int!=0)number_of_interacting_events++;
    if((number_of_events)%1000==0){
      printf("read in %d incident events  \r",number_of_events);
      fflush(stdout);
    }
    process_event (carac_process, &error_code);
	

	  
    /*******************************************************/
    /************* Analysis *******************************/
    /*****************************************************/
	  
    /* at this point one has all the information regarding the tracked gamma rays in the event */
	  
    /* writing tracked energies to file */

    fprintf(out_file,"# %f %f %f %f %f %f\n",carac_process->source[0],carac_process->source[1],carac_process->source[2],
	    carac_process->vel[0],carac_process->vel[1],carac_process->vel[2]);
    for(i=0;i<carac_process->mult;i++){
      corrected = carac_process->etot[i];
      dirg[0] = carac_process->xfirst[i]-carac_process->source[0];
      dirg[1] = carac_process->yfirst[i]-carac_process->source[1];
      dirg[2] = carac_process->zfirst[i]-carac_process->source[2];
      mdirg  = sqrt(dirg[0]*dirg[0]+dirg[1]*dirg[1]+dirg[2]*dirg[2]);
      beta = sqrt(carac_process->vel[0]*carac_process->vel[0]+carac_process->vel[1]*carac_process->vel[1]+carac_process->vel[2]*carac_process->vel[2]);
      if(beta>0 && mdirg>0){
	costheta = (dirg[0]*carac_process->vel[0]+dirg[1]*carac_process->vel[1]+dirg[2]*carac_process->vel[2])/mdirg/beta;
      } else costheta=1;
      gamma= 1./sqrt(1.-pow(beta,(double)2.));
      corrected *= gamma*(1-beta*costheta);
      fprintf(out_file,"%i %f %f %f %f AGATA\n",i,corrected,carac_process->xfirst[i],carac_process->yfirst[i],carac_process->zfirst[i]);
    }
    /* here we print out the energies for each core */
    for(i=0; i<180; i++){
      if(coreenergies[i]>0){
	fprintf(out_file,"%i %f CoreEAGATA\n",i,coreenergies[i]);
      }
    }
    /* Here we output the Ancillary data in raw format...*/
    for(atancillary = 0; atancillary<numberofancillaries; atancillary++){
      for(i=0; i<Ancillaries[atancillary].numberofhits; i++){
	fprintf(out_file,"%i %f %d %d %s\n",i,Ancillaries[atancillary].e[i],
		Ancillaries[atancillary].det_id[i],
		Ancillaries[atancillary].seg_id[i],
		Ancillaries[atancillary].AncillaryName);
      }
    }
    /* setting photon counter back to 1 before returning to read the file */
    j_in=1;
  }  
  printf("\nprocessed %d events with interaction \n",number_of_interacting_events);
  return error_code; 
}				/* end main */


void MakeCoreEnergies(const CntlStruct *cntlstruct, double ce[])

{
  int i,j;
  for(i=0; i<180; i++) ce[i]=0.;
  for(i=0; i<cntlstruct->nb_int; i++){
    ce[cntlstruct->det[i]]+=cntlstruct->e[i];
  }
}
