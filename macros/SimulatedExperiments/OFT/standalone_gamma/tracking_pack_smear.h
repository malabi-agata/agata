#ifndef DATA_PACKSMEAR_H

#define DATA_PACKSMEAR_H

int packpoints (int number,
		double posx[],
		double posy[],
		double posz[],
		double energy[],
		int detnumb[],
		int segnumb[]);

int packpointsbarycenter (int number,
				double posx[],
				double posy[],
				double posz[],
				double energy[],
				int detnumb[],
				int segnumb[]);

int packpointscenter (int number,
				double posx[],
				double posy[],
				double posz[],
				double energy[],
				int detnumb[],
				int segnumb[],
				double segx[180][60],
				double segy[180][60],
				double segz[180][60]);

void smearpoints (int number,
		  double enrgy[],
		  double posx[],
		  double posy[],
		  double posz[],
		  double errorfc[],
		  double step,
		  int nbsteps);

void smearenergies (int number,
				  double energy[],
				  double errorfc[],
				  double step,
				  int nbsteps);

int energy_thresh (int number,
		   double posx[],
		   double posy[],
		   double posz[],
		   double energy[],
		   int detnumb[],
		   int segnumb[]);

#endif
