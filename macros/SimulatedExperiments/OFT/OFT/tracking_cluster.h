#ifndef CLUSTER_H
#define CLUSTER_H

#include "tracking_define.h"

int cluster_search(int intnumber[intmax],
                   double anglecluster[intmax],
                   double ecluster[intmax],
                   int id[intmax*kmax],
                   int number,
                   double energy[intmax],
                   double thetaangle[intmax],
                   double phiangle[intmax]);

void cluster_evaluation(int number,
                        double ecluster[intmax],
                        int intnumber[intmax],
                        int id[intmax*kmax],
                        double probability[intmax],
                        int ordering[intmax*kmax],
                        double energy[intmax],
                        double posx[intmax],
                        double posy[intmax],
                        double posz[intmax],
                        double r[intmax*intmax],
                        double r_ge[intmax*intmax],
                        int clusterflag[intmax],
                        double source[3]); 

void cluster_sort_flag(int number,
                       double ecluster[intmax],
                       int intnumber[intmax],
                       int id[intmax*kmax],
                       double clusterang[intmax],
                       double probability[intmax],
                       int ordering[intmax*kmax],
                       int clusterflag[intmax]);

void single_interaction(int nbint,
                        double r[intmax*intmax],
                        double r_ge[intmax*intmax],
                        int number,
                        double ecluster[intmax],
                        int intnumber[intmax],
                        int id[intmax*kmax],
                        double probability[intmax],
                        int clusterflag[intmax]);

int cluster_validation(double etot[intmax],
                       int nbtot[intmax],
                       int number,
                       double probability[intmax],
                       int intnumber[intmax],
                       int ordering[intmax*kmax],
                       double ecluster[intmax],
                       int clusterflag[intmax],
                       double x[intmax],
                       double y[intmax],
                       double z[intmax],
                       double r[intmax*intmax],
                       double xfirst[intmax],
                       double yfirst[intmax],
                       double zfirst[intmax],
                       double xsecond[intmax],
                       double ysecond[intmax],
                       double zsecond[intmax]);

#endif
