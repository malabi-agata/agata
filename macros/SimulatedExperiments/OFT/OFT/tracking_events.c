#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
/*#include <unistd.h>*/

#include "tracking_define.h"
#include "tracking_utilitaires.h"
#include "tracking_physics.h"
#include "tracking_cluster.h"
#include "tracking_events.h"
#include "tracking_data_manip.h"

void process_event(CntlStruct *mydata, unsigned int *error_code) {

  int i,j;
  int n;
 
  if (mydata->nb_int == 0) return;

  /*********************************************************************/
  /**** energies in MeV and positions in cm ****************************/
  /*********************************************************************/

 // printf ("My data -> number of interactions : %d\n", mydata->nb_int);
  /*****************************************************/
  /******** compute distance source-int and ************/
  /*************theta and phi for each int *************/
  /*****************************************************/

  for (i = 0; i < mydata->nb_int; i++) {

    mydata->r[i*intmax + i] = sqrt(
        SQ(mydata->xpos[i] - mydata->source[0]) 
      + SQ(mydata->ypos[i] - mydata->source[1]) 
      + SQ(mydata->zpos[i] - mydata->source[2]));

    mydata->angtheta[i] = acos((mydata->zpos[i] - mydata->source[2]) / (mydata->r[i*intmax + i]));
    mydata->angphi[i] = atan2((mydata->ypos[i] - mydata->source[1]), (mydata->xpos[i] - mydata->source[0]));
    if (mydata->angphi[i] < 0)
      mydata->angphi[i] = 2 * PI + mydata->angphi[i];
  }

 // printf ("Out first loop\n");

  /*****************************************************/
  /********* sort according to increasing theta*********/
  /*****************************************************/

  for (i = 0; i < mydata->nb_int; i++) {
    for (j = i + 1; j < mydata->nb_int; j++) {
      if (mydata->angtheta[j] < mydata->angtheta[i]) {
        swap(mydata->e, i, j);
        swap(mydata->xpos, i, j);
        swap(mydata->ypos, i, j);
        swap(mydata->zpos, i, j);
        swap(mydata->angtheta, i, j);
        swap(mydata->angphi, i, j);
      }
    }
  }
 // printf ("Out second loop\n");

  /*****************************************************************/
  /******** computing all distances between interactions   *********/
  /*****************************************************************/

 /* for(i=0; i<mydata->nb_int; i++) {
   printf("hit#%3d  E x y z th = %6.2f %6.2f %6.2f %7.4f %7.2f \n",
      i, mydata->e[i]*1000.,
      mydata->xpos[i], mydata->ypos[i], mydata->zpos[i], mydata->angtheta[i]*(180/PI));
  } */

  distances(mydata->r, mydata->r_ge, mydata->nb_int, mydata->xpos, mydata->ypos, mydata->zpos, mydata->inner_r, mydata->outer_r, mydata->source);

  /* computes r and r_ge (real distances and effective distances in Ge)
  // given position vectors x,y and z and the number of interaction points nb_int
  // NB: distances in Ge are calculated assuming a 4pi ge sphere */

  /****************************************************/
  /*************** find clusters ***********************/
  /****************************************************/
//  printf ("Distances calculated !\n");

  n = cluster_search(mydata->numn, mydata->angn, mydata->et, mydata->sn, mydata->nb_int, mydata->e, mydata->angtheta, mydata->angphi);

 //   printf("number of clusters found = %d \n",n);

  /* numn = number of interactions in cluster
  // angn = angle of cluster 
  // et =  total energy of cluster
  // sn[1][2] = identity of 2nd point of cluster 1
  // e = energy vector of interaction points
  // angtheta and angphi = theta and phi vector of interaction points
  // routine returns number of clusters found */

  /*******************************************************/
  /******** compute figure of merit of clusters **********/
  /*******************************************************/

  cluster_evaluation(n, mydata->et, mydata->numn, mydata->sn, mydata->probtot, mydata->interaction, mydata->e, mydata->xpos, mydata->ypos, mydata->zpos, mydata->r, mydata->r_ge, mydata->flagu, mydata->source);

  /* n = number of clusters
  // probtot = figure of merit of cluster
  // interaction = ordering of interactions in cluster which gives best 
  // figure of merit (set to minprobtrack for singles temporarily)
  // x,y,z,r and e vectors of x,y,z and energy of interaction points
  // r and r_ge = real and effective distances in Ge
  // flagu - is set to 0 for all clusters */

  /***********************************************************************/
  /************ sort clusters according to figure of merit ***************/
  /************* flag worst clusters with same interactions **************/
  /***********************************************************************/

  cluster_sort_flag(n, mydata->et, mydata->numn, mydata->sn, mydata->angn, mydata->probtot, mydata->interaction, mydata->flagu);


  /* flagu is set to 1 if a cluster with a worst figure of merit than another one shares at least one common interaction point */

  /***********************************/
  /* test of direct photopeak events */
  /***********************************/

  single_interaction(mydata->nb_int, mydata->r, mydata->r_ge, n, mydata->et, mydata->numn, mydata->sn, mydata->probtot, mydata->flagu);

  /* routines computes figure of merit for single interaction points */

  /**************************************************************/
  /*********************** cluster validation ******************/
  /*************************************************************/

  mydata->mult =
    cluster_validation(mydata->etot, 
    mydata->nbtot, 
    n, 
    mydata->probtot, 
    mydata->numn, 
    mydata->interaction, 
    mydata->et, 
    mydata->flagu,
    mydata->xpos, 
    mydata->ypos, 
    mydata->zpos, 
    mydata->r, 
    mydata->xfirst, 
    mydata->yfirst, 
    mydata->zfirst,
    mydata->xsecond, 
    mydata->ysecond,
    mydata->zsecond);

  *error_code = 0;
}


