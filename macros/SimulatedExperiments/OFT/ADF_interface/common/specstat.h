#include <cstring>
#include <iostream>
#include <iomanip>

using namespace std;

class specstat 
{
public:

  FILE  *fp;
  string fname;
  bool   status;
  unsigned int    count;
  unsigned int    length;
  unsigned int    totcount;
  int   *spec;

  specstat(string fn, int len = 1024) :
   fp(NULL), length(0), count(0), totcount(0), spec(NULL), status(false)
  {
    fname = fn;
    length = (len > 0) ? len : 1024;
    spec = new int[length];
    clear();
    totcount = 0;
    fp = fopen(fname.c_str(), "wb");
    status = (fp != NULL);
  }
  ~specstat()
  {
    flush();
    fclose(fp);
    cout << fname << " --> " << totcount << " counts " 
         << " ( " << (totcount + length-1)/length << "*" << length << " )" << endl; 
  }
  void clear()
  {
    memset(spec, 0, sizeof(int)*length);
    count = 0;
  }
  void flush()
  {
    if(count) {
      totcount += count;
      size_t nn = fwrite(spec, sizeof(int), length, fp);
      status = (nn == length);
      clear();
    }
  }
  void add(int val)
  {
    if(count == length)
      flush();
    spec[count++] = val;
  }

};
