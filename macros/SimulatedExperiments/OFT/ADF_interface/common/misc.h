#ifndef MISC_H_INCLUDED
#define MISC_H_INCLUDED

#include <string>

void  stringTrim(std::string &str);
bool  stringSplit(std::string &line, const std::string sepa, std::string &keyw);
bool  stringSplit(std::string &line, const std::string sepa, std::string &keyw, std::string &data); 
bool  stringIncrement(std::string &fn, int count = 1);
bool  stringIncrement(std::string &fn, const std::string sp, int count = 1);

template<class T>
class median
{
public:
  median() : a1(0), a2(0), an(0) {}
  void reset(const T &a3) {a1=a3; a2=a3; an=a3;}
  T exec(const T &a3) {
    if(a1 < a2) {
      if     (a3 > a2) an = a2;
      else if(a3 < a1) an = a1;
      else             an = a3;
    }
    else {
      if     (a3 > a1) an = a1;
      else if(a3 < a2) an = a2;
      else             an = a3;
    }
    a1 = a2;
    a2 = a3;
    return an;
  }
  int myID;
  T   a1;
  T   a2;
  T   an;
};

#endif
