#ifndef _BASE_CLASS_H_
#define _BASE_CLASS_H_

class base_class;
class base_class
{
 private:
  unsigned int id;
 public:
  base_class() {};
  base_class *process_register (unsigned int *error_code);
  void process_initialise (unsigned int *error_code);
  void process_reset (unsigned int *error_code);
  void process_start (unsigned int *error_code);
  void process_stop (unsigned int *error_code);
  void process_pause (unsigned int *error_code);
  void process_resume (unsigned int *error_code);
  void process_unload (unsigned int *error_code);
  void set_id (unsigned int new_id);
};

class producer : public base_class
{
 public:
  producer () {};
  void process_block (void *output_buffer,
		      unsigned int size_of_output_buffer,
		      unsigned int *used_size_of_output_buffer,
		      unsigned int *error_code);
};

class intermediary : public base_class
{
 public:
  intermediary () {};
  void process_block (void *input_buffer,
		      unsigned int size_of_input_buffer,
		      void *output_buffer,
		      unsigned int size_of_output_buffer,
		      unsigned int *used_size_of_output_buffer,
		      unsigned int *error_code);
};

class consumer : public base_class
{
 public:
  consumer () {};
  void process_block (void *input_buffer,
		      unsigned int size_of_input_buffer,
		      unsigned int *error_code);
};

#endif
