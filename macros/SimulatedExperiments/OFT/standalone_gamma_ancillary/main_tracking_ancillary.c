#include <stdlib.h>
#include "tracking_data.h"
#include "tracking_events.h"



int main(int argc, char * argv[])
{
  char fn[50],buffer[200],dummy_str[50];
  FILE *fp;
  int dummy_i,int_source,int_velocity,int_particle,int_gamma,int_event;
  double xsource,ysource,zsource;
  void *input_buffer;
  unsigned int size_of_input_buffer;
  void *output_buffer;
  unsigned int size_of_output_buffer;
  unsigned int *used_size_of_output_buffer;
  int i,first_event=0;
  unsigned int error_code;
  char last_char='$';
  char zero = 0L;
  CntlStruct *carac_process;
  int crystal_numb,segment_numb;
  double energy,x,y,z,time;
  P_input temp;
  Event_List temp2;
  Event_List temp3;

  /****************************************************/
  /******* open configuraion file *********************/
  /******* check for smearing/packing *****************/
  /******* and energy resolution flags ****************/
  /****************************************************/


  process_config (".", &error_code);

  /***************************************************/
  /******* allocate memories for local ***************/
  /******* control structure variables ***************/
  /******** and return local structure **************/
  /***************************************************/


  carac_process = process_register (&error_code);
  


  /****************************************************/
  /********** read in data file name ******************/
  /********** use default file name *******************/
  /****************************************************/


  if (argc > 1) 
    {sprintf(fn, argv[1]);}
  else
    {sprintf(fn, "inputfile");}
  carac_process->event_file = fopen(fn, "r");;
  if (!carac_process->event_file) {
    printf("could not open file for reading %s\n", fn);
    error_code = 1;
    
  } else {
    printf("opened file %s\n", fn);
    error_code = 0;
  }
  
  fp= fopen("outputfile","w");

 /****************************************************/
  /********** initialising variables ******************/
  /********** and computing  tables  ******************/
  /****************************************************/

  
  process_initialise (carac_process, &error_code);
  
  
 /****************************************************/
  /********** reading data file buffer ***************/
  /****************************************************/
  
  while(1) {
    memset(buffer,zero,sizeof(buffer));
    fgets(buffer,sizeof(buffer),carac_process->event_file);
     if(strncmp(buffer,"SUMMARY",7)==0){
      sscanf(buffer,"%s %lf %lf",dummy_str,carac_process->inner_r,carac_process->outer_r);
      *carac_process->inner_r = *carac_process->inner_r/10.; /* cm */
      *carac_process->outer_r = *carac_process->outer_r/10.;
      printf("inner and outer radius = %f %f \n",*carac_process->inner_r,*carac_process->outer_r);
    }

  if(strncmp(buffer,"SOURCE",6)==0){
    sscanf(buffer,"%s %d %d %lf %lf %lf",dummy_str,&dummy_i,&dummy_i,&xsource,&ysource,&zsource); 
    carac_process->source[0] = xsource/10.; /* cm */
      carac_process->source[1] = ysource/10.;
      carac_process->source[2] = zsource/10.;
      printf("source position %f %f %f  \n",carac_process->source[0], carac_process->source[1],carac_process->source[2]); 
    }
    fputs(buffer,fp);
    if(strncmp(buffer,&last_char,1)==0) break;           
  }


  /*********************************************************/
  /********** reading and writing data file  ***************/
  /*********************************************************/
  
  carac_process->nshoot =0;
  
  
  /* read 1st event line */
  
  fgets(buffer,sizeof(buffer),carac_process->event_file);
  
  
  while(1){  
    
  read_next_event: ;
    
    carac_process->nb_int =0;
    
  read_next_line: ;
    
    int_event = strncmp(buffer,"-100",4);
    int_velocity = strncmp(buffer," -101",5);
    int_source = strncmp(buffer," -102",5);
    int_gamma = strncmp(buffer,"   -1",5);
    
        
    if(int_event ==0 || int_velocity ==0 || int_source ==0){
      fputs(buffer,fp); // write line 
      memset(buffer,zero,sizeof(buffer));
      if(fgets(buffer,sizeof(buffer),carac_process->event_file)== NULL) break; // get next line
      
      goto read_next_line;
    }
    
    if(int_gamma != 0) { // not gamma type (proton, neutron, ion ...
      
      fputs(buffer,fp); // write emitted data 
      memset(buffer,zero,sizeof(buffer));
      if(fgets(buffer,sizeof(buffer),carac_process->event_file)== NULL) break; // get next line
      if(strncmp(buffer," -101",5)==0) goto read_next_line; // no detected data for type
      if(strncmp(buffer,"-100",4)==0) goto track_event; // no detected data for type and end of event
      fputs(buffer,fp); // write detected data of type
      
      memset(buffer,zero,sizeof(buffer));
      if(fgets(buffer,sizeof(buffer),carac_process->event_file)== NULL) break; // get next line
      
      
      if(strncmp(buffer,"-100",4)==0) goto track_event;
      goto read_next_line;
    }
    
    if(int_gamma ==0){ 
      
      fputs(buffer,fp); // write emitted data 
      if(fgets(buffer,sizeof(buffer),carac_process->event_file)== NULL) break; // get next line
      
      
      int_event = strncmp(buffer,"-100",4);
      int_velocity = strncmp(buffer," -101",5);
      int_gamma = strncmp(buffer,"   -1",5);
      
      if(int_velocity==0) goto read_next_line; // no detected data for type
      if(int_event==0) goto track_event; // no detected data for type and end of event
      
    read_next_int: ;
      
      
      sscanf(buffer,"%d %lf %lf %lf %lf %d %lf",&crystal_numb,&energy,&x,&y,&z,&segment_numb,&time);
      
      carac_process->xpos[carac_process->nb_int]=x/10.; /* cm */
      carac_process->ypos[carac_process->nb_int]=y/10.;
      carac_process->zpos[carac_process->nb_int]=z/10.;
      carac_process->e[carac_process->nb_int]=energy/1000.; /* MeV */
      carac_process->t[carac_process->nb_int]=time;
      carac_process->det[carac_process->nb_int]=crystal_numb;
    
      
      carac_process->nb_int++;
      fputs(buffer,fp); // write detected data for type
      if(fgets(buffer,sizeof(buffer),carac_process->event_file)== NULL) break;
      
      int_event = strncmp(buffer,"-100",4);
      int_velocity = strncmp(buffer," -101",5);
      int_gamma = strncmp(buffer,"   -1",5);
      
      if(int_event ==0) goto track_event;
      if(int_velocity ==0)goto read_next_line;
      
      
      
      goto read_next_int;
      
      
    }

  /*******************************************************/
  /********** formatting interaction points  *************/
  /**************** data and tracking ********************/
  /*******************************************************/
    
  track_event: ;

    if(carac_process->nb_int ==0) goto read_next_event;
  
    size_of_input_buffer = sizeof(Header) + sizeof( Event) * carac_process->nb_int;
    
    temp = (void * ) malloc(size_of_input_buffer); 
    
    
    input_buffer = temp;
    
    temp->head.Length = carac_process->nb_int;
    temp->head.Event_Number = carac_process->nshoot;
    
    
    temp2=&temp->event;
    
    for(i=0;i<carac_process->nb_int;i++){
      
      temp2->Event_Number= carac_process->numinter[i];      
      temp2->Crystal_Number= carac_process->det[i];      
      temp2->Pos_X= carac_process->xpos[i];     
      temp2->Pos_Y= carac_process->ypos[i];      
      temp2->Pos_Z= carac_process->zpos[i];     
      temp2->Delta_X= 0.0;      
      temp2->Delta_Y= 0.0;      
      temp2->Delta_Z= 0.0;           
      temp2->Energy= carac_process->e[i];      
      temp2->Time= carac_process->t[i];
      temp3 = temp2;
      temp2++;
      temp3->Next_Event= temp2;
      
    } 
    
    temp3->Next_Event = NULL;
    
    process_block (carac_process, 
		   input_buffer,
		   size_of_input_buffer,
		   output_buffer,
		   size_of_output_buffer,
		   used_size_of_output_buffer,
		   &error_code);

  /*******************************************************/
  /********** writing tracked photon data  **************/
  /*******************************************************/
    
    
    for(i=0 ; i< carac_process->mult;i++){
   
      fprintf(fp,"%d %f %f %f %f %f %f %f \n",-202,1000*carac_process->etot[i],10*carac_process->xfirst[i],10*carac_process->yfirst[i],10*carac_process->zfirst[i],10*carac_process->xsecond[i],10*carac_process->ysecond[i],10*carac_process->zsecond[i]);
      
    }
    
 
  } 
  
  
  
  /*******************************************************/
  /********** formatting last interaction points  ********/
  /******* data tracking and writing tracked data ********/
  /*******************************************************/ 
  
 
  size_of_input_buffer = sizeof(Header) + sizeof( Event) * carac_process->nb_int;
  
  temp = (void * ) malloc(size_of_input_buffer); 
  
  
  input_buffer = temp;
  
  temp->head.Length = carac_process->nb_int;
  temp->head.Event_Number = carac_process->nshoot;
  
  
  temp2=&temp->event;
  
  for(i=0;i<carac_process->nb_int;i++){
    
    temp2->Event_Number= carac_process->numinter[i];      
    temp2->Crystal_Number= carac_process->det[i];      
    temp2->Pos_X= carac_process->xpos[i];     
    temp2->Pos_Y= carac_process->ypos[i];      
    temp2->Pos_Z= carac_process->zpos[i];     
    temp2->Delta_X= 0.0;      
    temp2->Delta_Y= 0.0;      
    temp2->Delta_Z= 0.0;           
    temp2->Energy= carac_process->e[i];      
    temp2->Time= carac_process->t[i];
    temp3 = temp2;
    temp2++;
    temp3->Next_Event= temp2;
    
  } 
  
  temp3->Next_Event = NULL;
  
  process_block (carac_process, 
		 input_buffer,
		 size_of_input_buffer,
		 output_buffer,
		 size_of_output_buffer,
		 used_size_of_output_buffer,
		 &error_code);
  
  
  for(i=0 ; i< carac_process->mult;i++){
    fprintf(fp,"%d %f %f %f %f %f %f %f \n",-202,carac_process->etot[i],carac_process->xfirst[i],carac_process->yfirst[i],carac_process->zfirst[i],carac_process->xsecond[i],carac_process->ysecond[i],carac_process->zsecond[i]);
  }
 
 /*******************************************************/
  /********** closing files and freeing memory * ********/
  /*******************************************************/ 

  fclose(fp);
  
  process_finalize (carac_process, &error_code);
  
  return error_code;
}				/* end main */
