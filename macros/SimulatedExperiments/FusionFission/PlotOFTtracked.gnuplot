reset
n=12000#number of intervals
min=-.25#max value
max=5999.75#min value
width=(max-min)/n#interval width
set xrange [0:6000]
set xlabel "Gamma-ray energy [keV]"
set ylabel "Counts/0.5 keV"
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0
plot "< grep ' AGATA' tracked_energies" using (hist(1000*$2,width)):(1.0) smooth freq w boxes
set terminal x11 enhanced
replot