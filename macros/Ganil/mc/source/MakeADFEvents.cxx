/*
  Code to make adf files from AGATA geant4 simulation output.
  It works either based on abosulte time (if that information is aviable)
  or on event by event basis (event start token in data file)

  Event are created either by taking data in time windows or in each event.
  These event are stored in temporary structures, and handled in "groups"
  corresponding to "events" in the real world.

  This data is then packed, smeared etc and stored in adf frames.
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <set>
#include <cmath>
#include <algorithm>

#include "TVector3.h"
#include "TMath.h"
#include "TGeoMatrix.h"
#include "TRandom.h"

#ifndef __CINT__
//ADF
//#include "./include/gw0.9/ConfAgent.h"
//#include "./include/gw0.9/DFAgent.h"
//#include "./include/gw0.9/AgataKeyFactory.h"
//#include "./include/gw0.9/AgataFrameFactory.h"
//#include "./include/gw0.9/BufferIO.h"
//#include "./include/gw0.9/FrameIO.h"
//#include "./include/gw0.9/ConfigurationFrame.h"
#include "ConfAgent.h"
#include "DFAgent.h"
#include "AgataKeyFactory.h"
#include "AgataFrameFactory.h"
#include "BufferIO.h"
#include "FrameIO.h"
#include "ConfigurationFrame.h"


#ifndef ADF_CrystalFrame
//#include "./include/gw0.9/CrystalFrame.h"
#include "CrystalFrame.h"
#endif
#ifndef ADF_PSAFrame
//#include "./include/gw0.9/PSAFrame.h"
#include "PSAFrame.h"
#endif
#ifndef ADF_TrackedFrame
//#include "./include/gw0.9/TrackedFrame.h"
#include "TrackedFrame.h"
#endif
#ifndef ADF_AgataCompositeFrame
//#include "./include/gw0.9/AgataCompositeFrame.h"
#include "AgataCompositeFrame.h"
#endif
//#include "./include/gw0.9/Trigger.h"
#include "Trigger.h"

#endif

struct agatacluster
{
  //euler angles of det in cluster
  double psi[3], theta[3], phi[3];

  //Postistion of det. in cluster
  TVector3 d[3];
  
  //Which crystals in this cluster
  int crystalnumber[3];
  //What type of crystals they are
  int crystaltype[3];

  // Which cluster
  int clusternumber;
};

struct clusterpos

{
  //Eulerangles of cluster
  double psi, theta, phi;

  // Position of cluster
  TVector3 d;

  //Which crystals that are in this cluster
  int crystals[3];

  // In case there is more than one typ of cluster
  int clusternumber;
};


struct crystaltransformationinfo

{
  TGeoHMatrix *transformation;
  int crystaltype;
};




class Interactions {
public:
  Interactions(){;}
  ~Interactions(){;}
  void push_back(Double_t x, Double_t y, Double_t z, Double_t e,
		 Int_t det, Int_t seg) 
  {
    posx.push_back(x);
    posy.push_back(y);
    posz.push_back(z);
    energy.push_back(e);
    detnumber.push_back(det);
    segnumber.push_back(seg);
  }
  void clear()
  {
    posx.clear();
    posy.clear();
    posz.clear();
    energy.clear();
    detnumber.clear();
    segnumber.clear();
  }
  std::vector<Double_t> posx,posy,posz,energy;
  std::vector<Int_t> detnumber, segnumber;
  unsigned int size() {return posx.size();}
};


class TimeInfo {
public:
  TimeInfo(){;}
  TimeInfo(Int_t d,Int_t h,Int_t m,Double_t t) : detT(t),day(d),hour(h),
						 min(m) {;}
  ~TimeInfo(){;}
  Double_t detT;
  Int_t day,hour,min;
  void Set(Int_t d,Int_t h,Int_t m,Double_t t){
    day=d; hour=h; min=m;
    detT=t;
    while(detT>=6e10){
      detT-=6e10;
      min++;
    }
    while(min>=60){
      min-=60;
      hour++;
    }
    while(hour>=24){
      hour-=24;
      day++;
    }
  }
  Double_t operator-(const TimeInfo &S) const {//Return the difference in ns
    Double_t dday=(day-S.day);
    Double_t dhour=(hour-S.hour);
    Double_t dmin=(min-S.min);
    Double_t dT=detT-S.detT;
    return (1e9*(dday*24.*3600.+dhour*3600.+dmin*60.)+dT);
  }
};

class Interaction {
public:
  Interaction(){
    time.Set(0,0,0,0);
  }
  Interaction(Double_t x, Double_t y, Double_t z, Double_t e,
	      Int_t det, Int_t seg, Int_t day, Int_t hour, Int_t min,
	      Double_t detT) 
  {
    posx=x;
    posy=y;
    posz=z;
    energy=e;
    detnumber=det;
    segnumber=seg;
    time.Set(day,hour,min,detT);
  }
  ~Interaction(){;}
  void Set(Double_t x, Double_t y, Double_t z, Double_t e,
	   Int_t det, Int_t seg, Int_t day, Int_t hour, Int_t min,
	   Double_t detT) 
  {
    posx=x;
    posy=y;
    posz=z;
    energy=e;
    detnumber=det;
    segnumber=seg;
    time.Set(day,hour,min,detT);
  }
  bool operator()(const Interaction &lhs, const Interaction &rhs)
  {
    return (lhs.time-rhs.time)<0;
  }
  Double_t posx,posy,posz,energy;
  Int_t detnumber, segnumber;
  TimeInfo time;
  static void Print(const Interaction &I, std::ostream&str=std::cout) {
    str << I.posx << " " << I.posy << " " << I.posz << " "
	<< I.energy  << " " << I.detnumber << " "
	<< I.segnumber << " " << I.time.day << " " << I.time.hour 
	<< " " << I.time.min << " " << I.time.detT << std::endl;
  }
};

template <class T> class event {
public:
  event(){;}
  ~event(){;}
  event(const T &data,Int_t da,Int_t h,Int_t m,Double_t t)
  {detData=data; day=da;hour=h;min=m;detT=t;}
  std::vector<T> detData;
  Double_t detT;
  Int_t day,hour,min;
  ULong64_t GetTimeStamp() const {
    return 1e8*(((day*24+hour)*60+min)*60)+detT/10;
  }
  bool operator()(const event &lhs,const event &rhs)
  {
    Int_t rhsday=rhs.day;
    Int_t rhshour=rhs.hour;
    Int_t rhsmin=rhs.min;
    Double_t rhst=rhs.detT;
    Int_t lhsday=lhs.day;
    Int_t lhshour=lhs.hour;
    Int_t lhsmin=lhs.min;
    Double_t lhst=lhs.detT;
    while(rhst>=6e10){
      rhst-=6e10;
      rhsmin++;
    }
    while(rhsmin>=60){
      rhsmin-=60;
      rhshour++;
    }
    while(rhshour>=24){
      rhshour-=24;
      rhsday++;
    }
    while(lhst>=6e10){
      lhst-=6e10;
      lhsmin++;
    }
    while(lhsmin>=60){
      lhsmin-=60;
      lhshour++;
    }
    while(lhshour>=24){
      lhshour-=24;
      lhsday++;
    }
    if(lhsday!=rhsday) return lhsday<rhsday; 
    if(lhshour!=rhshour) return lhshour<rhshour; 
    if(lhsmin!=rhsmin) return lhsmin<rhsmin;
    return lhst<rhst;
  }
  void Print() const {
    std::cout << day << " " << hour << " " << min << " " << detT 
	      << " ";
  }
};


class SantasLittleHelper {
public:
  SantasLittleHelper(){;}
  ~SantasLittleHelper(){;}
  static Interactions *InteractionsInEvent;
  static std::ostringstream *buffer;
  static void AddData(const Interaction &data){
    InteractionsInEvent->push_back(data.posx,data.posy,data.posz,data.energy,
		 data.detnumber,data.segnumber);    
  }
  static void StreamToBuffer(const Interaction &data){
    Interaction::Print(data,*buffer);
  }
};

Interactions *SantasLittleHelper::InteractionsInEvent=0;
std::ostringstream *SantasLittleHelper::buffer=0;

int packpoints(int number, double posx[], double posy[], double posz[], 
	       double energy[], int detnumb[], int segnumb[]);

int packpointsbarycenter(int number, double posx[], double posy[], 
			 double posz[], double energy[], int detnumb[], 
			 int segnumb[]);


void MakeDetectorTransformations(std::vector<clusterpos> 
				 &theclusterpostions,
				 std::vector<agatacluster> theclusters,
				 std::vector<crystaltransformationinfo> 
				 &thecrystaltransformations)

{
  std::vector<clusterpos>::iterator ittheclusterpostions;
  for (ittheclusterpostions = theclusterpostions.begin(); 
       ittheclusterpostions!= theclusterpostions.end();
       ittheclusterpostions++){
    clusterpos *cp = &(*(ittheclusterpostions));
    // Here pick the correct cluster number
    std::vector<agatacluster>::iterator ittheclusters;
    for (ittheclusters = theclusters.begin(); 
	 ittheclusters!=theclusters.end();
	 ittheclusters++){
      agatacluster lacluster = *(ittheclusters);
      if (cp->clusternumber == lacluster.clusternumber){
	// First the cluster transformation
	// All rotation matricies are made by hand due too different
	// convention used for the euler angles between ROOT (z x' z'')
	// and geom. input files (z y' z'') :(
	double matrix[9];
	double cosphi,sinphi,costheta,sintheta,cospsi,sinpsi;
	// Rotation of cluster rel. lab
	cosphi =   cos(cp->phi);
	sinphi =   sin(cp->phi);
	costheta = cos(cp->theta);
	sintheta = sin(cp->theta);
	cospsi =   cos(cp->psi);
	sinpsi =   sin(cp->psi);
	matrix[0] = -sinpsi*sinphi + costheta*cosphi*cospsi;
	matrix[3] = sinpsi*cosphi + costheta*sinphi*cospsi;
	matrix[6] = -cospsi*sintheta;
	matrix[1] = -cospsi*sinphi - costheta*cosphi*sinpsi;
	matrix[4] = cospsi*cosphi - costheta*sinphi*sinpsi;
	matrix[7] = sinpsi*sintheta;
	matrix[2] = sintheta*cosphi;
	matrix[5] = sintheta*sinphi;
	matrix[8] = costheta;
	TGeoRotation clusterrotation;
	clusterrotation.SetMatrix(matrix);
	//Translation of cluster rel lab
	TGeoTranslation clustertranslation(cp->d[0],cp->d[1],cp->d[2]);
	TGeoCombiTrans clustercombination(clustertranslation,
					  clusterrotation);
	TGeoRotation crystalrotation;
	TGeoTranslation crystaltranslation;
	TGeoCombiTrans crystalcombination;
	TGeoHMatrix m;
	for (int i=0; i<3; i++){
	  // Here we actually put the crystals in position
	  // Calculate rotation matrix for crystal rel. cluster...
	  cosphi   = cos(lacluster.phi[i]);
	  sinphi   = sin(lacluster.phi[i]);
	  costheta = cos(lacluster.theta[i]);
	  sintheta = sin(lacluster.theta[i]);
	  cospsi   = cos(lacluster.psi[i]);
	  sinpsi   = sin(lacluster.psi[i]);
	  matrix[0] = -sinpsi*sinphi + costheta*cosphi*cospsi;
	  matrix[3] = sinpsi*cosphi + costheta*sinphi*cospsi;
	  matrix[6] = -cospsi*sintheta;
	  matrix[1] = -cospsi*sinphi - costheta*cosphi*sinpsi;
	  matrix[4] = cospsi*cosphi - costheta*sinphi*sinpsi;
	  matrix[7] = sinpsi*sintheta;
	  matrix[2] = sintheta*cosphi;
	  matrix[5] = sintheta*sinphi;
	  matrix[8] = costheta;
	  crystalrotation.SetMatrix(matrix);
	  // Translation of crystal rel cluster
	  crystaltranslation.SetTranslation((lacluster.d[i])[0],
					    (lacluster.d[i])[1],
					    (lacluster.d[i])[2]);
	  crystalcombination.SetTranslation(crystaltranslation);
	  crystalcombination.SetRotation(crystalrotation);
	  // Make total transformation for a crystal
	  m = clustercombination * crystalcombination;
	  TGeoHMatrix *real = new TGeoHMatrix(m);
	  crystaltransformationinfo temp;
	  temp.transformation = real;
	  temp.crystaltype = lacluster.crystaltype[i];
	  // Keep the transformation
	  thecrystaltransformations.push_back(temp);
	  cp->crystals[i] = thecrystaltransformations.size()-1;
	}
	break;
      }
    }
  }
}


int MakeADFEvents(std::string baseoutput="PSA_",
		  bool transformtocrystal=true,bool addres=true,
		  Double_t timegate = 10,
		  /*micros, summing in a det, if Dt larger, considered as new
		    event*/
		  std::string particleid = "-1"
		  /* particle to look for when checking event time*/)

{
  //  sleep(5000);
  /**
     Here we will define ADF things...
  */
  // To customise
  // the factory for everything
#ifndef __CINT__
  gRandom->SetSeed(time(0));
  ADF::ConfAgent::theGlobalAgent();
  const char factory[] = "Agata";
  // the two types of embedded frames, the first one should stay data:psa
  const char frame_type1[] = "data:psa";
  // the version of the primary key and the frames to be embedded
  ADF::Version key_v(4,0), frame_v1(65000,1);
  // end to customize ... works without any changes after
  ADF::FactoryItem prikeydef1(factory,frame_type1,key_v);
  ADF::FactoryItem aframedef1(factory,frame_type1,frame_v1);
  // Ask global agent to fill a Configuration frame which is dumped
  ADF::Frame *cframe = NULL;
  ADF::ConfigurationFrame *gconfframe = NULL;
  ADF::APSAFrame     *fFramePSA; 
  ADF::AgataFrameTrigger fTrigger("data:psa");
  if(!ADF::MainFrameFactory::theMainFactory().
     IsKnown(aframedef1.GetFactoryName().data()) ){
    std::cout << " Agata Factory not properly loaded " << std::endl;
    return 1;
  }
  fFramePSA=dynamic_cast<ADF::APSAFrame*> 
    (ADF::MainFrameFactory::theMainFactory().New(prikeydef1,aframedef1));
  cframe=ADF::MainFrameFactory::theMainFactory().
    New(ADF::FactoryItem("Agata","conf:global",key_v),
	ADF::FactoryItem("Agata","conf:global",frame_v1));
  if (cframe) gconfframe = dynamic_cast<ADF::ConfigurationFrame *>(cframe);
  // change the agent to take into account this configuration
  ADF::ConfAgent::theGlobalAgent()->GetDFAgent()->
    SetComment("from geant4 AGATA sim");
  ADF::ConfAgent::theGlobalAgent()->GetDFAgent()->
    AddKnownFrame(fFramePSA,'+');
  ADF::ConfAgent::theGlobalAgent()->GetDFAgent()->
    Configure(gconfframe,"out");
  UShort_t crystal_id,crystal_status;
  Float_t CoreE0,CoreE1,CoreT0,CoreT1;
  ADF::GObject *glob = fFramePSA->Data()->Global();
  glob->LinkItem("CrystalID",     &crystal_id); 
  glob->LinkItem("CrystalStatus", &crystal_status); 
  glob->LinkItem("CoreE0",        &CoreE0); 
  glob->LinkItem("CoreE1",        &CoreE1);
  glob->LinkItem("CoreT0",        &CoreT0);
  glob->LinkItem("CoreT1",        &CoreT1);
  //Anc frame
  const char frame_type_anc[] = "data:ranc0";
  ADF::Version key_anc(4,0), frame_anc(65000,0);
  // end to customize ... works without any changes after
  ADF::FactoryItem prikeyanc(factory,frame_type_anc,key_anc);
  ADF::FactoryItem aframeanc(factory,frame_type_anc,frame_anc);
  ADF::RawFrame *fFrameAnc=dynamic_cast<ADF::RawFrame*> 
    (ADF::MainFrameFactory::theMainFactory().New(prikeyanc,aframeanc));
#endif
  std::vector<crystaltransformationinfo> thecrystaltransformations;
  std::vector<agatacluster> theclusters;
  std::vector<clusterpos>   theclusterpostions;
  std::map<int,std::vector<int> > listofinteractions;
  std::map<int,int> listofinteractionsperdetector;
  std::map<int,int> listofframesperdetector;
  agatacluster *acluster = 0;
  clusterpos *apos = 0; 
  std::map<int,std::ofstream *> outputfiles; 
  std::ofstream *ancoutput = 0;
  //Check OUTPUT_MASK
  std::string keyword,value;
  char oneline[1024];
  bool UseTimeForTimestamps = false;
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> value;
  }while(keyword!="OUTPUT_MASK");
  std::cout << keyword << " " << value << "\n";
  if(value[2]!='1'){
    std::cout << "We use abosult positions!!!";
    //    return 1;
  }
  if(value[6]=='1'){
    UseTimeForTimestamps=true;
    std::cout << "We use event time for timestamps!!!\n";
  }
  //Get number of detectors
  int numberofdet = 0;
  double rinner,router;
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> rinner >> router >> numberofdet;
  }while(keyword!="SUMMARY");
  std::cout << keyword << " " << numberofdet << "\n";
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> value;
  }while(keyword!="CLUSTER");
  std::cout << keyword << " " << value << "\n"; 
  int i1,i2,i3;
  double ps,th,ph,x,y,z;
  int oclust = -1;
  int lline;
  do{//Here we get the clusters
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> value;
    if(keyword!="ENDCLUSTER"){
      lline = strlen(oneline);
      if(lline < 2) continue;
      if(oneline[0] == '#') continue;
      if(sscanf(oneline,"%d %d %d %lf %lf %lf %lf %lf %lf", 
		&i1, &i2, &i3, &ps, &th, &ph, &x, &y, &z) != 9){
	break;
      }
      if(oclust != i1){
	oclust = i1;
	theclusters.push_back( agatacluster());
	acluster = &theclusters.back();
	acluster -> clusternumber = i1;
      } 
      acluster -> crystaltype[i3] = i2;
      acluster -> crystalnumber[i3] = i3;
      acluster -> psi[i3] =  ps * TMath::DegToRad();
      acluster -> theta[i3] = th * TMath::DegToRad();
      acluster -> phi[i3] = ph * TMath::DegToRad();
      (acluster -> d[i3])[0] = x;
      (acluster -> d[i3])[1] = y;
      (acluster -> d[i3])[2] = z;      
    }
  }while(keyword!="ENDCLUSTER");
  std::cout << keyword << " " << value << "\n";
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> value;
  }while(keyword!="EULER");
  std::cout << keyword << " " << value << "\n";
  int nEuler=0;
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword >> value;
    if(keyword!="ENDEULER"){
      lline = strlen(oneline);
      if(lline < 2) continue;
      if(oneline[0] == '#') continue;
      if(sscanf(oneline,"%d %d %lf %lf %lf %lf %lf %lf", 
		&i1, &i2, &ps, &th, &ph, &x, &y, &z) != 8)
	break;
      theclusterpostions.push_back(clusterpos());
      apos = &theclusterpostions[nEuler];
      apos -> clusternumber = i2;
      apos -> psi = ps*TMath::DegToRad();
      apos -> theta = th*TMath::DegToRad();
      apos -> phi = ph*TMath::DegToRad();
      (apos -> d[0]) = x;
      (apos -> d[1]) = y;
      (apos -> d[2]) = z;
      nEuler++;
    }
  }while(keyword!="ENDEULER");
  std::cout << keyword << " " << value << "\n";
  int ctr = 0;
  do{
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    ss >> keyword;
  }while(keyword!="$");
  //Make detectortransformations
  MakeDetectorTransformations(theclusterpostions,theclusters,
			      thecrystaltransformations);
  std::multiset<event<Interaction>,event<Interaction> > EventQue;
  event<Interaction> AnEvent;
  event<Interaction> StopEvent;
  std::map<int, std::multiset<Interaction,Interaction> > theInteractions;
  Interaction oneinteraction,testinteraction;
  Interactions InteractionsInEvent;
  unsigned long int evtnb=0,readevtnb=0;
  std::string codewords;
  float e;double t,t_=0;short detnb,segnb;
  int day_=0,hour_=0,min_=0;
  while(!std::cin.eof()){
    std::cin.getline(oneline,1024);
    std::istringstream ss(oneline);
    if(ss>>codewords){
      if(codewords=="-100"){//new event
	//Track down the particle id
	do{
	  std::cin.getline(oneline,1024);
	  ss.str(oneline);
	  ss.clear();
	  ss>>codewords;
	}while(codewords!=particleid);
	if(ss >> e){
	  if(ss >> x){
	    if(ss >> y){
	      if(ss >> z){
		if(ss >> readevtnb){
		  if(UseTimeForTimestamps){
		    if(ss >> day_){
		      if(ss >> hour_){
			if(ss >> min_){
			  if(ss >> t) {;} else exit(-1);
			}
		      }  else exit(-1);
		    }  else exit(-1);
		  } else {;} 
		}  else exit(-1);
	      }  else exit(-1);
	    }  else exit(-1);
	  } else exit(-1);
	} else exit(-1);
	evtnb++;
	if(UseTimeForTimestamps){//Here we treat data with times...
	  StopEvent.detT=t;
	  StopEvent.day=day_;
	  StopEvent.hour=hour_;
	  StopEvent.min=min_;
	  std::multiset<event<Interaction>,event<Interaction> >::iterator 
	    itStartEventQue=EventQue.begin();
	  std::multiset<event<Interaction>,event<Interaction> >::iterator 
	    itStopEventQue=EventQue.upper_bound(StopEvent);
	  for(;itStartEventQue!=itStopEventQue; ++itStartEventQue){
	    //It is here I should make the adf frames,
	    //We start to do it only for AGATA crystals
	    if(itStartEventQue->detData.begin()->detnumber<180){
	      //Start with packing points
	      InteractionsInEvent.clear();
	      SantasLittleHelper::InteractionsInEvent = &InteractionsInEvent;
	      std::for_each(itStartEventQue->detData.begin(),
			    itStartEventQue->detData.end(),
			    SantasLittleHelper::AddData);
	      //Pack points...
	      Int_t numberofinteractions = 
		packpointsbarycenter(InteractionsInEvent.size(),
				     &InteractionsInEvent.posx[0],
				     &InteractionsInEvent.posy[0],
				     &InteractionsInEvent.posz[0],
				     &InteractionsInEvent.energy[0],
				     &InteractionsInEvent.detnumber[0],
				     &InteractionsInEvent.segnumber[0]);
	      //Smear energy and pos res...
	      for(Int_t i=0; i<numberofinteractions; i++){
		/*error_p = 0.5*sqrt(0.1/energy[i])/2.35; 
		  fwhm/2.35 in cm 
		  this is from NIM A 638 PA Söderström et al.*/
		double energy=InteractionsInEvent.energy[i];
		if(addres){
		  Double_t resfactor=0.62/2.35/sqrt(3);
		  InteractionsInEvent.posx[i]+=
		    gRandom->Gaus(0,10.*(.27+resfactor*sqrt(0.1/energy))); 
		  InteractionsInEvent.posy[i]+=
		    gRandom->Gaus(0,10.*(.27+resfactor*sqrt(0.1/energy))); 
		  InteractionsInEvent.posz[i]+=
		    gRandom->Gaus(0,10.*(.27+resfactor*sqrt(0.1/energy))); 
		}
		InteractionsInEvent.energy[i]+=
		  gRandom->Gaus(0,sqrt(5305./5326+energy*21./5326)/2.35);
		//We also tranform to crystal refernce if asked for
		if(transformtocrystal){
		  double r[3],rp[3];
		  rp[0]=InteractionsInEvent.posx[i];
		  rp[1]=InteractionsInEvent.posy[i];
		  rp[2]=InteractionsInEvent.posz[i];
		  thecrystaltransformations[InteractionsInEvent.detnumber[i]].
		    transformation->MasterToLocal(rp,r);	    
		  InteractionsInEvent.posx[i]=r[0];
		  InteractionsInEvent.posy[i]=r[1];
		  InteractionsInEvent.posz[i]=r[2];
		}
	      }
	      //Here we fill the event frame with PSA frames...
	      //We need to fill crystal per crystal...
	      listofinteractions.clear();
	      for(Int_t i=0; i<numberofinteractions; i++){
		if(outputfiles.find(InteractionsInEvent.detnumber[i])==
		   outputfiles.end()){//open output file if needed...
		  std::string det[3]={"R","G","B"};
		  std::ostringstream filename;
		  filename << "Data/" << (InteractionsInEvent.detnumber[i]/3)+1
			   << det[(InteractionsInEvent.detnumber[i]%3)]
			   << "/" << baseoutput 
			   << (InteractionsInEvent.detnumber[i]/3)+1
			   << det[(InteractionsInEvent.detnumber[i]%3)]
			   << "_" << std::setw(4) 
			   << std::setfill('0') 
			   << 0
			   << ".adf";
		  outputfiles[InteractionsInEvent.detnumber[i]]=
		    new std::ofstream(filename.str().c_str());
		  //We also dump a config frame on the stream...
		  outputfiles[InteractionsInEvent.detnumber[i]]->
		    write((char*)gconfframe->GetKey()->GetRealBuffer()->
			  GetAddress(),
			  gconfframe->GetKey()->GetKeyLength());
		  outputfiles[InteractionsInEvent.detnumber[i]]->
		    write((char*)gconfframe->GetRealBuffer()->GetAddress(),
			  gconfframe->GetKey()->GetDataLength());
		}
		listofinteractions[InteractionsInEvent.detnumber[i]].
		  push_back(i);
	      }
	      std::map<int,std::vector<int> >::iterator itlist 
		= listofinteractions.begin();
	      for(;itlist!=listofinteractions.end(); ++itlist){
		listofinteractionsperdetector[itlist->first]+=
		  itlist->second.size();
		listofframesperdetector[itlist->first]++;
		fFramePSA->Reset();
		crystal_id=itlist->first;
		crystal_status=0;
		CoreE0 = 0;
		CoreE1 = 0;
		CoreT0 = 0;
		CoreT1 = 0;
		((ADF::AgataKey*)fFramePSA->GetKey())->SetEventNumber(evtnb);
		((ADF::AgataKey*)fFramePSA->GetKey())->
		  SetTimeStamp(itStartEventQue->GetTimeStamp());  
		std::vector<int>::iterator iti = itlist->second.begin();
		for(; iti!=itlist->second.end(); ++iti){
		  CoreE0+=InteractionsInEvent.energy[*iti];
		  CoreE1+=InteractionsInEvent.energy[*iti];
		  ADF::PSAHit *hit = (ADF::PSAHit*)fFramePSA->Data()->NewHit();
		  hit->Reset();
		  hit->SetE(InteractionsInEvent.energy[*iti]);
		  hit->SetXYZ(InteractionsInEvent.posx[*iti], 
			      InteractionsInEvent.posy[*iti], 
			      InteractionsInEvent.posz[*iti]);
		  hit->SetID(InteractionsInEvent.segnumber[*iti]);
		  hit->SetT(0);
		  hit->SetDT(1.);  // chi2 temporarily stored in 
		}
		fFramePSA->Write();
		//Write this to the correct file
		std::ofstream *fpointer=outputfiles[itlist->first]; 
		fpointer->write((char*)fFramePSA->GetKey()->GetRealBuffer()->
				GetAddress(),
				fFramePSA->GetKey()->GetKeyLength());
		fpointer->write((char*)fFramePSA->GetRealBuffer()->
				GetAddress(),
				fFramePSA->GetKey()->GetDataLength());
 
	      } 
	    } else {//So this is an ancillary... 
	      if(ancoutput==0){
		std::ostringstream filename;
		filename << "Data/Ancillary/Anc" 
			 << "_" << std::setw(4) 
			 << std::setfill('0') 
			 << 0
			 << ".adf";
		ancoutput = new std::ofstream(filename.str().c_str());
	      }
	      ((ADF::AgataKey*)fFrameAnc->GetKey())->SetEventNumber(evtnb);
	      ((ADF::AgataKey*)fFrameAnc->GetKey())->
		SetTimeStamp(itStartEventQue->GetTimeStamp());  
	      std::ostringstream tmpbf;
	      SantasLittleHelper::buffer=&tmpbf;
	      std::for_each(itStartEventQue->detData.begin(),
			    itStartEventQue->detData.end(),
			    SantasLittleHelper::StreamToBuffer);
	      ((ADF::AgataKey*)fFrameAnc->GetKey())->
		SetDataLength((UInt_t)SantasLittleHelper::buffer->str().size());
	      ancoutput->write((char*)fFramePSA->GetKey()->GetRealBuffer()->
			       GetAddress(),
			       fFramePSA->GetKey()->GetKeyLength());
	      ancoutput->write((char*)SantasLittleHelper::buffer->str().c_str(),
			       fFramePSA->GetKey()->GetDataLength());
	    }
#ifdef      _DEBUG_	    
	    itStartEventQue->Print();
	    std::cout << std::endl;
	    std::for_each(itStartEventQue->detData.begin(),
			  itStartEventQue->detData.end(),
			  Interaction::Print);
	    std::cout << "<->\n";
#endif
	  }
#ifdef      _DEBUG_	    
	  std::cout << ">-------<\n";
#endif
	  EventQue.erase(EventQue.begin(),itStopEventQue);
	  StopEvent.detT=0;
	  StopEvent.day=100000000;
	  StopEvent.hour=100000000;
	  StopEvent.min=100000000;
	  AnEvent.day=day_;
	  AnEvent.hour=hour_;
	  AnEvent.min=min_;
	  AnEvent.detT=t;
	  t_=t;
	} else {//Here if we only got events
	  std::multiset<event<Interaction>,event<Interaction> >::iterator 
	    itStartEventQue=EventQue.begin();
	  for(;itStartEventQue!=EventQue.end(); ++itStartEventQue){
	  }
	  EventQue.erase(EventQue.begin(),EventQue.end());
	}
      }
      detnb = atoi(codewords.c_str());
      if(detnb>=0){
	ss >> e >> x >> y >> z >> segnb;
	if(UseTimeForTimestamps) ss >> t; else {
	  std::cout << "Only event number files not yet implemented\n";
	  exit(-1);
	}
	oneinteraction.Set(x,y,z,e,detnb,segnb,day_,hour_,min_,t_+t); 
	//Put to the correct detector
	theInteractions[detnb].insert(oneinteraction);
	//Check if we should make an event in this detector
	testinteraction.time.Set(theInteractions[detnb].begin()->time.day,
				 theInteractions[detnb].begin()->time.hour,
				 theInteractions[detnb].begin()->time.min,
				 theInteractions[detnb].begin()->time.detT+
				 timegate*1000);
	std::multiset<Interaction,Interaction>::iterator itstart=
	  theInteractions[detnb].begin();
	std::multiset<Interaction,Interaction>::iterator itstop=
	  theInteractions[detnb].lower_bound(testinteraction);
	if(itstop!=theInteractions[detnb].end()){
	  //Make new event 
	  AnEvent.detData.clear();
	  AnEvent.day=theInteractions[detnb].begin()->time.day;
	  AnEvent.hour=theInteractions[detnb].begin()->time.hour;
	  AnEvent.min=theInteractions[detnb].begin()->time.min;
	  AnEvent.detT=theInteractions[detnb].begin()->time.detT;
	  for(;itstart!=itstop;++itstart){
	    AnEvent.detData.push_back(*itstart);
	  }
	  EventQue.insert(AnEvent);
	  //Now get rid of the entries already used
	  theInteractions[detnb].erase(theInteractions[detnb].begin(),
				       itstop);
	}
      }
    }
    ctr++;
    if (ctr%10000==0){
      std::cout << "At line " << std::setw(10) << ctr 
		<< " and event " << std::setw(10) << evtnb << "\r";
      std::cout.flush();
    }
  }
  std::map<int,std::ofstream*>::iterator itfiles = outputfiles.begin();
  for(; itfiles!=outputfiles.end(); ++itfiles){
    itfiles->second->close();
    delete itfiles->second;
  }
  if(ancoutput){
    ancoutput->close();
    delete ancoutput;
    ancoutput=0;
  }
  std::cout << "\n\n\n";  
  std::ofstream intperdet("interactionsperdetector.txt");
  intperdet << std::setw(4) << "#Det" << std::setw(10) << "Nb Frames"
	    << std::setw(20) << " Nb Interactions" << "\n";
  std::map<int,int>::iterator itipd = listofinteractionsperdetector.begin();
  for(; itipd!=listofinteractionsperdetector.end(); ++itipd){
    intperdet << std::setw(4) << itipd->first << " " 
	      << std::setw(10) << listofframesperdetector[itipd->first]  
	      << std::setw(14) << itipd->second << "\n";
  }
  return 0;
}










#define SQ(x) ((x)*(x))
#define resolution 5.0        
/* distance below which one cannot distinguish the presence of more than 1 interaction */

void swap(double v[], int m, int l)
{
  register double temp;
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
} 
void swapi(int v[], int m, int l)
{
  register int temp;
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
}

int packpoints(int number, double posx[], double posy[], double posz[], 
	       double energy[], int detnumb[], int segnumb[])
{
  int i, j, n, l, jp[5000], jjp[5000], ip[5000], iip[5000];
  double rpack, esum;
  l = 0;
  /* check which interactions are within precision of each other in 
     the same segment */
  for (i = 0; i < number; i++) {
    for (j = i + 1; j < number; j++) {
      rpack = sqrt(SQ(posx[i] - posx[j]) + SQ(posy[i] - posy[j]) + 
		   SQ(posz[i] - posz[j]));
      if (rpack < resolution && segnumb[i]==segnumb[j] && 
	  detnumb[i]==detnumb[j]) {
	l++;
	jp[l] = j;
	jjp[l] = j;
	ip[l] = i;
	iip[l] = i;
      }
    }
  }
  /* check if couples have already been packed by previous couples */
  for (i = 1; i <= l; i++) {
    for (j = i + 1; j <= l; j++) {
      if (ip[i] == ip[j]) {	/*&& ip[i] != -1) { */
	for (n = j + 1; n <= l; n++) {
	  if (ip[n] == jp[i] && jp[n] == jp[j]) {
	    iip[n] = -1;
	    jjp[n] = -1;
	  }
	  if (ip[n] == jp[j] && jp[n] == jp[i]) {
	    iip[n] = -1;
	    jjp[n] = -1;
	  }
	}
      }
    }
  }

  for (n = 1; n <= l; n++) {
    if (iip[n] == -1)
      ip[n] = -1;
    if (jjp[n] == -1)
      jp[n] = -1;
  }
    
  for (j = 1; j <= l; j++) {
    for (i = 0; i < number; i++) {
      if (ip[j] == i && jp[j] != i) {
	esum = energy[i] + energy[jp[j]];
	/* new position pondered by energie */
	posx[i] = ((posx[i] * energy[i]) + 
		   (posx[jp[j]] * energy[jp[j]])) / esum;
	posy[i] = ((posy[i] * energy[i]) + 
		   (posy[jp[j]] * energy[jp[j]])) / esum;
	posz[i] = ((posz[i] * energy[i]) + 
		   (posz[jp[j]] * energy[jp[j]])) / esum;
	energy[i] = esum;	/* put 2 energies into 1 */
	swap(energy, number - 1, jp[j]);	
	/* put unused energy at the end of the list */
	swap(posx, number - 1, jp[j]);
	swap(posy, number - 1, jp[j]);
	swap(posz, number - 1, jp[j]);
	swapi(detnumb, number - 1, jp[j]);
	swapi(segnumb, number - 1, jp[j]);
	for (n = j + 1; n <= l; n++) {
	  if (ip[n] == jp[j]) {	/* if the one just packed needs to be packed */
	    ip[n] = i;
	  }
	  if (jp[n] == jp[j]) {	/* if the one just packed needs to be packed */
	    jp[n] = i;
	  }
	  if (ip[n] == number - 1) {	/* if end of list needs to be packed */
	    ip[n] = jp[j];
	  }
	  if (jp[n] == number - 1) {	/* if end of list needs to be packed */
	    jp[n] = jp[j];
	  }
	}
	number -= 1;	/* decrement the number of interactions */
      }
    }
  }
  return number;		/* return new number of interactions */
}



int packpointsbarycenter(int number, double posx[], double posy[], 
			 double posz[], double energy[], int detnumb[], 
			 int segnumb[])
{
  int i,j;
  double en[800];
  for(i=0;i<number;i++){
    for(j=i+1;j<number;j++){
      if(energy[i]!=0 && detnumb[i]==detnumb[j] && segnumb[i]==segnumb[j]){
	en[i]=energy[i]+energy[j];
	posx[i]=((energy[i]*posx[i])+(energy[j]*posx[j]))/en[i];
	posy[i]=((energy[i]*posy[i])+(energy[j]*posy[j]))/en[i];
	posz[i]=((energy[i]*posz[i])+(energy[j]*posz[j]))/en[i];
	energy[j]=0;
	energy[i]=en[i];
      }
    }
  }
  j=0;
  for(i=0;i<number;i++){
    if(energy[i]!=0){
      energy[j]=energy[i];
      posx[j]=posx[i];
      posy[j]=posy[i];
      posz[j]=posz[i];
      detnumb[j]=detnumb[i];
      segnumb[j]=segnumb[i];
      j++;
    }
  }
  number = j;
  return number;		/* return new number of interactions */
}
