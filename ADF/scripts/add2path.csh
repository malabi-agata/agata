# source this script to add path to library/binary path
#
# Author : O. Stezowski
#
if ( $#argv < 2 ) then
   echo "Usage: source add2path.csh path [lib][bin][/lib][/bin]"
   echo " [lib][bin]   : add the given path as it is to library_path/path"
   echo " [/lib][/bin] : add first lib/bin to the given path and add them to library_path/path"
   exit
endif

if ( ! -d $1 ) then
   echo "Error: Directory $1 does not exist"
   exit
endif

#
# loop on all arguments to set up the options 
#
set DO_LIB_PATH = 0	; set DO_BIN_PATH = 0
#
foreach var ( $argv )
  if ( $var == "lib" ) then
   set ADD_TO_PATH_LIB = $1	; set DO_LIB_PATH = 1	
  endif
  if ( $var == "/lib" ) then
   set ADD_TO_PATH_LIB = $1/lib	; set DO_LIB_PATH = 2	   
  endif
  if ( $var == "bin" ) then
   set ADD_TO_PATH_BIN = $1	; set DO_BIN_PATH = 1	
  endif
  if ( $var == "/bin" ) then
   set ADD_TO_PATH_BIN = $1/bin	; set DO_BIN_PATH = 2	   
  endif  
end

#
# Add to library path
#
if ( $DO_LIB_PATH == 0 ) then
   echo "Nothing to add to library_path"
else
   	echo "Add to LIBRARY_PATH" $ADD_TO_PATH_LIB 
   	# linux-like
	if ($?LD_LIBRARY_PATH) then
  		setenv LD_LIBRARY_PATH ${ADD_TO_PATH_LIB}:${LD_LIBRARY_PATH}  
	else
  		setenv LD_LIBRARY_PATH ${ADD_TO_PATH_LIB}
  	endif
  	# macosx
	if ($?DYLD_LIBRARY_PATH) then
  		setenv DYLD_LIBRARY_PATH ${ADD_TO_PATH_LIB}:${DYLD_LIBRARY_PATH}
	else
  		setenv DYLD_LIBRARY_PATH ${ADD_TO_PATH_LIB}  		
	endif  		
	#legacy HP-UX
	if ($?SHLIB_PATH) then
  		setenv SHLIB_PATH ${ADD_TO_PATH_LIB}:${SHLIB_PATH}
	else
  		setenv SHLIB_PATH ${ADD_TO_PATH_LIB}  		
	endif  			
	# AIX
	if ($?LIBPATH) then
  		setenv LIBPATH ${ADD_TO_PATH_LIB}:${LIBPATH}
	else
  		setenv LIBPATH ${ADD_TO_PATH_LIB}  		
	endif 	
endif

#
# Add to path
#
if ( $DO_BIN_PATH == 0 ) then
   echo "Nothing to add to path"
else
   	echo "Add to PATH" $ADD_TO_PATH_BIN 
	if ($?PATH) then
  		setenv PATH ${ADD_TO_PATH_BIN}:${PATH}  
	else
  		setenv PATH ${ADD_TO_PATH_BIN}
  	endif   	
endif




