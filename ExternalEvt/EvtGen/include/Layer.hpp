#ifndef LAYER_HPP
#define LAYER_HPP

#include <iostream>

class Layer
{
	public:
		int A, Z;
		double density; // unit for density is g/cm^3
		
		// unit of length is micrometer
		double thickness; // thickness of this Layer
		double integrated_thickness; 
		
		
		double fragmentation_prob; // relative probability for fragmentation reaction in this Layer
		char reaction_type; // c=coulex, C=inverse coulex, f=fusion(evaporation), k=knockout
		
		double angular_straggling; // number in   mrad/um   i.e. in mrad per target thickness
		
		double integrated_fragmentation_prob; // integral of the fragmentation probability in the multilayer target
		
		Layer(std::istream &in);
		Layer(); // creates a 100um thick vacuum layer
};


#endif

