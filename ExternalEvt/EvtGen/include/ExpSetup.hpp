#ifndef EXP_SETUP_HPP
#define EXP_SETUP_HPP

#include <string>
#include <vector>

#include "Layer.hpp"

class ExpSetup
{
	public:
		ExpSetup(const char* filename);
		int layer_number(double z);
		
		// number of projectiles that will be shot onto the target
		int NumberProjectiles; 
		
		// Projectile characteristics
		int Ap, Zp;
                double Xsp;  // Mass excess
		double E0, deltaE; // in units of MeV / A
		double x0, deltax; // mean position and variance of incomming beam in x-direction (in um)
		double y0, deltay; // mean position and variance of incomming beam in y-direction (in um)
		
		// Target nucleus characteristics
		int Atg, Ztg;
                double Xstg; // Mass excess [MeV]

		// Heavy Ejectile characteristics
		int Ae, Ze;
		double Bn; // binding energy per nucleon
		double Ex;     // excitation energy  [MeV]
		double Xse; // Mass excess  [MeV]

		// light ejectile characteristics
		int Ale, Zle;
		double Xsle; // Mass excess [MeV]

		// Detector resolution:
		int AddRes;
		double AngRes, NrjRes;

		bool fragmented;

		// For Prisma events (adding ionic Z)
		int Prisma;
		int MeanZeq, deltaZeq;
		
		std::string decay_filename;

		std::string Xsec_filename;

		std::vector<Layer> layers;
		
};

#endif

