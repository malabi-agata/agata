NumberProjectiles: 20000

#######################################
# definition of a target layer:
#######################################
# Layer: A Z density(in g/cm^3)   thickness(in um)  relative_excitation_probability angular_straggling(in mrad/um) reaction_type
# all length in micometers
# A = Z = 1 and density = 0 means vacuum
# available reaction types: C   inverse coulex  ( do nothing : ejectile flys with velocity of projectile )
#                           k   knock out (or fragmentation) : Relative change of velocity (Borrel et al.)
#			    f   fusion evaporation     	     : ejectile flys with center of mass velocity
#			    T   transfer in inverse kinematics: ejectile flys with binary reaction kinematics solution
#########################################
Layer: 2 1  0.002	 50.0 	 90.0  0.0  T
#########################################
# characteristics of projectile and beam
#########################################
Ap:  132		# projectile mass
Zp:   50		# projectile charge number
Xsp: -76.61		# Mass excess [MeV]

E0:		10.     # initial projectile energy (in MeV/A)
deltaE:	  0.005 # variance of initial projectile energy (in MeV/A)

x0: 0.0			# primary beam position and variance in x-direction (in um)
deltax: 25000  
y0: 0.0			# primary beam position and variance in y-direction (in um)
deltay: 17000  


#######################################
# characteristics of target nucleus (for transfer reaction)
# (note: target layer could be different that the target nucleus of interest: CD2 layer - d=target nucleus) 
#######################################
Atg:	2		# target ejectile mass
Ztg:	1		# target z
Xstg:	13.136		# Mass excess [MeV]

#######################################
# characteristics of heavy ejectile 
#######################################
Ae:  133		# ejectile mass
Ze:   50		# ejectile charge number
Bn:   8.0   		# binding energy in MeV/u
Xse:  -71.9   		# Mass excess [MeV]
Ex:   4.416 		# Excited state populated after transfer reaction (Ex used only for the kinematics,
                        # the decay cascade should be described in the decay_filename below)

# gamma decay file  
decay_filename: exampleTransfer.decay

# Cross section file for transfer (cross section vs thetaCM) 
Xsec_filename: CrossSection/flat.txt
#Xsec_filename: CrossSection/sn132dp_gs_10AMeV.txt

#######################################
# characteristics of light ejectile (for transfer reaction)
#######################################
Ale: 1			# light ejectile mass
Zle: 1			# light ejectile z
Xsle:	7.289		# Mass excess [MeV]

#######################################
## AddRes is Optional( 0 is off 1 is on): Adding detection resolution in input event file to take into account gamma doppler.
## - If heavy ejectile is detecte on can add the angle (deg) and energy (keV) resolution here. 
## - If only the light ejectile is detected on can add angle (deg) and energy (keV) detection resolution instead.
#######################################
AddRes: 0
AngRes: 3.525
NrjRes: 70.5

#######################################
# if heavy ion spectrometer (Prisma) is used
#######################################
Prisma:	0		# set to 1 if PRISMA
MeanZeq:	0	# effective charge state (could be Ze of lower value)	
deltaZeq:	0	# variation
