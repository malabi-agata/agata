#include "ExpSetup.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

ExpSetup::ExpSetup(const char *filename)
	: Ap(-1), Zp(-1), E0(-1.0), deltaE(-1.0), x0(0.0), deltax(10.0), y0(0.0), deltay(10.0),
	  Atg(-1), Ztg(-1), Xstg(0.0), 
	  Ae(-1), Ze(-1), Bn(-1), Xsp(0.0), Ex(0.0), Ale(-1), Zle(-1), Xsle(0.0), fragmented(false),
          Prisma(0), MeanZeq(0), deltaZeq(0), AddRes(0), AngRes(0), NrjRes(0)
{
	std::ifstream in(filename);
	
	if (!in)
	{
		std::cerr << "couldn't open file: " << filename << std::endl;
		return;
	}
	
	for (;;)
	{
		std::string line;
		std::getline(in, line);
		if (!in)
			break;
			
		std::istringstream lin(line);
		
		char comment;
		lin >> comment;
		lin.putback(comment);
		if (!lin || comment == '#') // omit empty lines or commented lines
			continue;
		
		
		std::string key;
		lin >> key;
		
		if (key == "Layer:")
		{
			Layer layer(lin);
			layers.push_back(layer);
		}
		else if (key == "NumberProjectiles:")
		{
			lin >> NumberProjectiles;
		}
		else if (key == "Ap:")
		{
			lin >> Ap;
		}
		else if (key == "Zp:")
		{
			lin >> Zp;
		}
		else if (key == "Xsp:")
		{
			lin >> Xsp;
		}
		else if (key == "Atg:")
		{
			lin >> Atg;
		}
		else if (key == "Ztg:")
		{
			lin >> Ztg;
		}
		else if (key == "Xstg:")
		{
			lin >> Xstg;
		}
		else if (key == "Ae:")
		{
			lin >> Ae;
		}
		else if (key == "Ze:")
		{
			lin >> Ze;
		}
		else if (key == "Bn:")
		{
			lin >> Bn;
		}
		else if (key == "Xse:")
		{
			lin >> Xse;
		}
		else if (key == "Ex:")
		{
			lin >> Ex;
		}
		else if (key == "E0:")
		{
			lin >> E0;
		}
		else if (key == "deltaE:")
		{
			lin >> deltaE;
		}
		else if (key == "x0:")
		{
			lin >> x0;
		}
		else if (key == "deltax:")
		{
			lin >> deltax;
		}
		else if (key == "y0:")
		{
			lin >> y0;
		}
		else if (key == "deltay:")
		{
			lin >> deltay;
		}
		else if (key == "decay_filename:")
		{
			lin >> decay_filename;
		}
		else if (key == "Xsec_filename:")
		{
			lin >> Xsec_filename;
		}
		else if (key == "Ale:")
		{
			lin >> Ale;
		}
		else if (key == "Zle:")
		{
			lin >> Zle;
		}
		else if (key == "Xsle:")
		{
			lin >> Xsle;
		}
		else if (key == "Prisma:")
		{
			lin >> Prisma;
		}
		else if (key == "MeanZeq:")
		{
			lin >> MeanZeq;
		}
		else if (key == "deltaZeq:")
		{
			lin >> deltaZeq ;
		}
		else if (key == "AddRes:")
		{
			lin >> AddRes ;
		}
		else if (key == "AngRes:")
		{
			lin >> AngRes ;
		}
		else if (key == "NrjRes:")
		{
			lin >> NrjRes ;
		}
		else
		{
			std::cerr << "unknown key: " << key << std::endl;
		}
		
	}
	
	if (layers.size() < 1)
	{
		std::cerr << "no target layer defined" << std::endl;
	}
	if (Ap < 0)
	{
		std::cerr << "no projectile atomic mass defined" << std::endl;
	}
	if (Zp < 0)
	{
		std::cerr << "no projectile charge number defined" << std::endl;
	}
	if (Ae < 0)
	{
		std::cerr << "no fragment atomic mass defined" << std::endl;
	}
	if (Ze < 0)
	{
		std::cerr << "no fragment charge number defined" << std::endl;
	}
	if (Bn < 0)
	{
		std::cerr << "no binding energy per nucleon defined" << std::endl;
	}
	if (Ex < 0)
	{
		std::cerr << "no binding energy per nucleon defined" << std::endl;
	}
	if (E0 < 0)
	{
		std::cerr << "no projectile energy (E0) given" << std::endl;
	}
	if (deltaE < 0)
	{
		std::cerr << "no projectile energy variance (deltaE) given" << std::endl;
	}
	if (MeanZeq < 0 || MeanZeq> Ze)
	{
		std::cerr << " incorrect charge state value (MeanZeq) given" << std::endl;
	}

	// normalization of total fragmentation probability to 1.0
	double total_fragmentation_prob = 0;
	double total_thickness = 0;
	for (unsigned i = 0; i < layers.size(); ++i)
	{
		total_fragmentation_prob += layers[i].fragmentation_prob * layers[i].thickness;
		layers[i].integrated_fragmentation_prob = total_fragmentation_prob;
		
		total_thickness += layers[i].thickness;
		layers[i].integrated_thickness = total_thickness;
	}	
	for (unsigned i = 0; i < layers.size(); ++i)
	{
		layers[i].fragmentation_prob /= total_fragmentation_prob;
		layers[i].integrated_fragmentation_prob /= total_fragmentation_prob;
	}	
}

int ExpSetup::layer_number(double z)
{
	if (layers.size() == 1)
		return 1;
		
	if (layers.size() == 0)
		std::cerr << "warning, no layers defined" << std::endl;
		
	for (unsigned i = 0; i < layers.size(); ++i)
		if (z < layers[i].integrated_thickness)
			return i;
		
	return -1;	
}


