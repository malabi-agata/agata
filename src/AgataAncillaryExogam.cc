#ifdef ANCIL

//#define SMALL

#include "AgataAncillaryExogam.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "G4Material.hh"

#include "G4MaterialTable.hh" //?
#include "G4Element.hh" //?
#include "G4ElementTable.hh" //?

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Trap.hh"
#include "G4Trd.hh"
#include "G4Para.hh"
#include "G4Polyhedra.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"

#include "G4PVReplica.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4SDManager.hh"

#include <math.h> //?

#include "G4UserLimits.hh" //?

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ios.hh"

//#include "Exogam2TrackerSD.hh"


using namespace std;

AgataAncillaryExogam::AgataAncillaryExogam(G4String path, G4String name)
{

  //---------------------------------------------------------------
  //	Generic rotation matrices
  //---------------------------------------------------------------
  rm90.rotateZ(90.*deg);
  rm90m.rotateZ(-90.*deg);
  rm180.rotateZ(180.*deg);
  rm270.rotateZ(270.*deg);


  G4String iniPath     = path;
  dirName = name;
  //booleans to set geometry
  EXOGAMinput = "EXOGAM_Input";

  //doubles which can be changed

  //materials

  //name:
  nameVacuum    = "Vacuum";
  //material:
  Vacuum        = NULL;

  nameAluminum  = "Aluminum";
  Aluminum      = NULL;
  nameCopper    = "Copper";
  Copper        = NULL;
  nameGermanium = "Germanium";
  Germanium     = NULL;
  nameBGO       = "BGO";
  BGO           = NULL;
  nameCsI       = "CsI";
  CsI           = NULL;
  nameHeavymet  = "Heavymet";
  Heavymet      = NULL;

  nameMatTarget = "Silicon28";
  matTarget     = NULL;
  nameMatLayer  = "Zirconium90";
  matLayer      = NULL;

  nameMatElDet   = "Silicon28";
  matElDet       = NULL;
  

  ancSD = NULL;

  ancName   = G4String("EXOGAM");
  ancOffset = 9000;

  numAncSd = 0;

  myMessenger = new AgataAncillaryExogamMessenger(this,name);
}


AgataAncillaryExogam::~AgataAncillaryExogam()
{
  delete myMessenger;
}

G4int AgataAncillaryExogam::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(nameVacuum);
  if (ptMaterial){
    Vacuum = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Vacuum->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << nameVacuum << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   

  ptMaterial = G4Material::GetMaterial(nameAluminum);
  if (ptMaterial){
    Aluminum = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Aluminum->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameAluminum << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameCopper);
  if (ptMaterial){
    Copper = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Copper->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << nameCopper << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameGermanium);
  if (ptMaterial){
    Germanium = ptMaterial;
    G4cout<<"\n----> The ancillary detector material is "<<Germanium->GetName()<<G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameGermanium << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameBGO);
  if (ptMaterial){
    BGO = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<BGO->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameBGO << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameCsI);
  if (ptMaterial){
    CsI = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<CsI->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameCsI << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameHeavymet);
  if (ptMaterial){
    Heavymet = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<Heavymet->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameHeavymet << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  //target and layer:
  ptMaterial = G4Material::GetMaterial(nameMatTarget);
  if (ptMaterial){
    matTarget = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matTarget->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatTarget << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameMatLayer);
  if (ptMaterial){
    matLayer = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matLayer->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatLayer << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  //material for electron detectors
  ptMaterial = G4Material::GetMaterial(nameMatElDet);
  if (ptMaterial){
    matElDet = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matElDet->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatElDet << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  
  //find all used materials
  

  return 0;
}

void AgataAncillaryExogam::WriteHeader(std::ofstream &, G4double)
{;}
void AgataAncillaryExogam::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}

void AgataAncillaryExogam::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryExogam::InitSensitiveDetector()
{
  G4int offset = ancOffset;
  G4cout << "offset="<< offset<< G4endl;

#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4cout << "offset=" << offset<< G4endl;

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    //    ancSD = new AgataSensitiveDetector( "/anc/Exo", "AncCollection", offset, depth, menu );
    //    ancSD = new AgataSensitiveDetector( "/anc/all", "AncCollection", offset, depth, menu );
    //gj 2010/02/05
    //Enrico cos mowil o tym, ze kazda kolekcja winna byc z osobna okreslona
    ancSD = new AgataSensitiveDetector(dirName, "/anc/Exo", "ExoCollection", offset, depth, menu );

    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

G4int AgataAncillaryExogam::GetSegmentNumber(G4int, G4int detCode, G4ThreeVector position)
{
  if(detCode<72){
    //G4double newz=position.z()/mm+45.*mm;
    G4double xyLim;
    //if ( (newz/mm) <= 30.*mm) xyLim=3.96*mm*(1.-(newz/mm)/(30.*mm));
    //else                      xyLim=0.;
    xyLim=0;  
    G4int iSeg=99; //gj =99

    //cout << "PosX= "<< position.x()/mm << " PosY= " << position.y()/mm << " PosZ= "<< position.z()/mm << endl;
     
    if ( (position.x()/mm >= xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSeg=0;    // Segment 0  
    else if ( (position.x()/mm <  xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSeg=1;    // Segment 1
    else if ( (position.x()/mm <  xyLim/mm) && (position.y()/mm <  xyLim/mm)) iSeg=2;  //Segment 2
    else if ( (position.x()/mm >= xyLim/mm) && (position.y()/mm <  xyLim/mm)) iSeg=3;    // Segment 3

    //else iSeg=9;//checking for errors //gj
  
    return iSeg;
  }
  return 0;
}

void AgataAncillaryExogam::PlaceEverything()
{


  //---------------------------------------------------------------
  //	 Read in EXOGAM-Geometry file ... 
  //---------------------------------------------------------------

  //gj changing from 18 to 22 - putting catcher inside
  G4double d[22], TargetCanDistance[22];
  G4double Theta[22],Phi[22];//,PositionZ[22];
  
  
  // Table of Clover IDs in array

  //  G4int CloverInArray[22]; //gj 08/02/2010 przenosze do hh

  FILE *fi;
  char line[256];
  char CloverType='L';	// "L" for large (=EXOGAM) Clovers ; "S" for small ones
#ifdef SMALL
  char CloverTypeTable[22];
#endif
  if( (fi = fopen(EXOGAMinput.c_str(), "r")) == NULL) {
    G4cout << "\nError opening " << EXOGAMinput << " file " << G4endl;
    exit(-1);
  } else {
    G4cout << "\n" << EXOGAMinput << "file open..." << G4endl;
  }
  
  NumberOfClover=0;
  while(fgets(line, 255, fi) != NULL) {
    G4int n, j, CloverIn;
    G4double Distance, Thet, Ph;
    G4double Pos;
    char str[14], str1[6];
    if(line[0] != '#') {
      if ( (strstr(line,"REALISTIC") != NULL) || (strstr(line,"SIMPLE") != NULL) ) {
        n=sscanf(line,"%s",(char*)&GammaGenerator);
        G4cout << " Construction::GammaGenerator = " << GammaGenerator << G4endl;
      } else if ( strstr(line,"TRACKING") != NULL ) {
        n=sscanf(line,"%s %s",(char*)&str, (char*)&str1);
	Tracking=true;					// make tracking by default...
	if (strstr(str1,"true") == NULL) Tracking=false;
        G4cout << " Tracking = " << Tracking << G4endl;
	if (Tracking) {
	  G4cout << " Tracking will be performed for this run..." << G4endl;
	} else {
	  G4cout << " Tracking will not be performed for this run..." << G4endl;
	} 
      } else if ( strstr(line,"CONFIGURATION") != NULL ) {
        n=sscanf(line,"%s %c",(char*)&str, (char*)&Config);
	//       G4cout << " str = " << str << " ; Config = " << Config << G4endl;
        G4cout << " AC Shield configuration = " << Config << G4endl;
	  } 
      else if ( (n=sscanf(line,"%d %c %d %lf %lf %lf %lf",&j,&CloverType,&CloverIn,&Distance,&Thet,&Ph,&Pos)) == 7 ) {
#ifdef SMALL
	CloverTypeTable[j]=CloverType;
#endif
        CloverInArray[j]=CloverIn;
        TargetCanDistance[j]=Distance*cm;
	PositionZ[j]=Pos*cm;
        Theta[j]=Thet*deg;
        Phi[j]=Ph*deg;

        d[j]=0.;
        if (CloverInArray[j]) {
          IDCloverInArray[NumberOfClover] = j;		// Identify the Clovers in array
          NumberOfClover++;				// Number of Clovers in array
        }//gj stop;
      }else if ( (n=sscanf(line,"%d %c %d %lf %lf %lf",&j,&CloverType,&CloverIn,&Distance,&Thet,&Ph)) == 6 ) {
#ifdef SMALL
	CloverTypeTable[j]=CloverType;
#endif
        CloverInArray[j]=CloverIn;
        TargetCanDistance[j]=Distance*cm;

        Theta[j]=Thet*deg;
        Phi[j]=Ph*deg;

	PositionZ[j]=0.;//gj 

        d[j]=0.;
        if (CloverInArray[j]) {
          IDCloverInArray[NumberOfClover] = j;		// Identify the Clovers in array
          NumberOfClover++;				// Number of Clovers in array
        }
      } else if ( (n=sscanf(line,"%s ",(char*)&InputFilename)) == 1 ) {
        G4cout << " Input filename is " << InputFilename << G4endl;//gj start:
      } 
      else {break;}
    } 
  }
  fclose(fi);

  if (NumberOfClover==0) {
    G4cerr << " No detector in EXOGAM: # of clovers = " << NumberOfClover << " !" << G4endl;
    exit(-1);
  }
  
  if (NumberOfClover>22) {// gj changed from 18 - place for catchers.
    G4cerr << " Too many detectors: # of clovers = " << NumberOfClover << " !" << G4endl;
    exit(-1);
  }
  G4cout << "-------------------------------" << G4endl 
	 << "Number of Clover in array: " << NumberOfClover << G4endl << G4endl;

  G4cout << "Clovers in array and distances: " << G4endl 
	 << "-------------------------------" << G4endl;

  for (G4int i=0 ; i<NumberOfClover ; i++ ) {
    G4int ID=IDCloverInArray[i];
    G4cout << " >>> Clover # " << IDCloverInArray[i] 
	   << " is in array; distance target to Ge can = " << TargetCanDistance[ID]/cm << " cm" << G4endl;
  }
  G4cout << "-------------------------------" << G4endl << G4endl; 

  //---------------------------------------------------------------
  //	Main distances
  //
  // For the BGO Side Shield and the collimator, we consider the 
  // distance given by the projection on the z axis of the closest
  // part of them to the target.
  //---------------------------------------------------------------

  G4double distBGOSShieldToGeCan;	 	// distance from the front face of the 
						// BGO Side Shield to the front face of the 
						// Ge can (theory: 3.2*cm)
  
  G4double distCollimatorToGeCan=0;		// distance from front face of the collimator
						// to the front face of the Ge can
  
  if (Config == 'B') {
    distCollimatorToBGOSShield = 2.95*cm;
    distBGOSShieldToGeCan = 3.3*cm; 
    distCollimatorToGeCan = distCollimatorToBGOSShield+distBGOSShieldToGeCan;	
  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  // The enveloppe of the whole Clover (i.e. suppressed Clover) called 'SupClover' including: 
  // - the cryostat 
  // - the dewar 
  // - the side shield
  // - the back catcher
  // - the collimator
  ///////////////////////////////////////////////////////////////////////////////////////////

  //  G4double dzEnv;
  G4double dx1Env;
  G4double dy1Env;

  if (Config == 'B') {
    dzEnv = 40.472*cm;
    dx1Env = 3.17*cm;
    dy1Env = 3.17*cm;
  } else {
    dzEnv = 34.7*cm;
    dx1Env = 4.4085*cm;
    dy1Env = 4.4085*cm;
  }

  G4double dx2Env = 2.*dzEnv*tan(22.5*deg)+dx1Env;
  G4double dy2Env = 2.*dzEnv*tan(22.5*deg)+dy1Env;

  G4Trd* solidSupClover = new G4Trd("SupClover",dx1Env,dx2Env,dy1Env,dy2Env,dzEnv);

  //  G4LogicalVolume* 
  logicSupClover = new G4LogicalVolume(solidSupClover,Vacuum,"SupClover");
  // for the small Clovers:

#ifdef SMALL
  G4double SdzEnv = 37.5*cm;
  G4double Sdx1Env = 4.50*cm;
  G4double Sdy1Env = 4.50*cm;

  G4double Sdx2Env = 2.*SdzEnv*tan(13.8*deg)+Sdx1Env;
  G4double Sdy2Env = 2.*SdzEnv*tan(13.8*deg)+Sdy1Env;

  G4Trd* solidSmallSupClover = new G4Trd("SmallSupClover",Sdx1Env,Sdx2Env,Sdy1Env,Sdy2Env,SdzEnv);

  G4LogicalVolume* logicSmallSupClover = new G4LogicalVolume(solidSmallSupClover,Vacuum,"SmallSupClover");
#endif
  
  //
  // ... ant its position(s) in the Experimental Hall...
  //

  G4double xClover[22]; 
  G4double yClover[22]; 
  G4double zClover[22]; 

  G4RotationMatrix rm[22];

  //  G4PVPlacement *physiSupClover;
  G4Transform3D positionClover;
  
  if (Config == 'B') {
    Offset=dzEnv-distCollimatorToGeCan;
  } else {
    Offset=dzEnv;
  }

#ifdef SMALL
  SOffset=SdzEnv; 	// Offset for the small Clovers
#endif
  
#ifdef SMALL
  SOffset=SdzEnv; 	// Offset for the small Clovers
#endif

  G4int NumberOfSmallClover=0;
  for (G4int j=0 ; j<NumberOfClover ; j++) {
    G4int i=IDCloverInArray[j];

    rm[i].rotateY(Theta[i]).rotateZ(Phi[i]);

#ifdef SMALL
    if (CloverTypeTable[i]=='S' || CloverTypeTable[i]=='s') {	// Position of the small Clovers
      G4cout << " >>> Clover in position " << i << " is a small Clover..." << G4endl;

      d[i]=TargetCanDistance[i]+SOffset;	// distance for Clover placement
      xClover[i]= d[i]*sin(Theta[i])*cos(Phi[i]); 
      yClover[i]= d[i]*sin(Theta[i])*sin(Phi[i]);
      zClover[i]= PositionZ[i]+d[i]*cos(Theta[i]);

      physiSmallSupClover = new G4PVPlacement(G4Transform3D(rm[i],
							    G4ThreeVector(xClover[i],yClover[i],zClover[i])),
					      "SmallClover",logicSmallSupClover,theDetector->HallPhys(),false,i);

      NumberOfSmallClover++;

    } else {
      G4cout << " >>> Clover in position " << i << " is an EXOGAM Clover..." << G4endl;
      d[i]=TargetCanDistance[i]+Offset;		// distance for Clover placement
      xClover[i]= d[i]*sin(Theta[i])*cos(Phi[i]); 
      yClover[i]= d[i]*sin(Theta[i])*sin(Phi[i]);
      zClover[i]= PositionZ[i]+d[i]*cos(Theta[i]);

      physiSupClover = new G4PVPlacement(G4Transform3D(rm[i],
						       G4ThreeVector(xClover[i],yClover[i],zClover[i])),
					 "Clover",logicSupClover,theDetector->HallPhys(),false,i);
    }
#endif
#ifndef SMALL
    G4cout << " >>> Clover in position " << i << " is an EXOGAM Clover..." << G4endl;
    d[i]=TargetCanDistance[i]+Offset;		// distance for Clover placement

    xClover[i]= d[i]*sin(Theta[i])*cos(Phi[i]); 
    yClover[i]= d[i]*sin(Theta[i])*sin(Phi[i]);
    zClover[i]= PositionZ[i]+d[i]*cos(Theta[i]);

    physiSupClover = new G4PVPlacement(G4Transform3D(rm[i],
						     G4ThreeVector(xClover[i],yClover[i],zClover[i])),
				       "Clover",logicSupClover,theDetector->HallPhys(),false,i);
#endif

  }

  //
  //  The big EXOGAM Clovers...
  //

  ConstructClover();
  G4cout << "The Clover built... " << G4endl;

  //
  //  The small Clovers...
  //

#ifdef SMALL
  ConstructSmallClover();
  G4cout << "The small Clover built... " << G4endl;
#endif



  //dotad ok.
  



  /////////////////////////////////////////////////////////////////////////////////////
  //
  // The shield
  //
  /////////////////////////////////////////////////////////////////////////////////////

  if ( (Config == 'A') || (Config == 'B')) {

    //
    // The Side Catcher
    //
    ConstructSideCatcher();
    G4cout << "The Side Catcher built... " << G4endl;
    //
    // The Back Catcher
    //
    ConstructBackCatcher();
    G4cout << "The Back Catcher built... " << G4endl;

  }	

  if (Config == 'B') {

    //
    // The Side Shield
    //
    ConstructSideShield();
    G4cout << "The Side Shield built... " << G4endl;
    //
    // The Collimator 
    //
    ConstructCollimator();
    G4cout << "The Collimator built... " << G4endl;

  }

  //
  // End of detector construction
  //





  //---------------------------------------------------------------
  //
  // Visualization attributes
  //
  //---------------------------------------------------------------

  G4VisAttributes* CanVisAtt= new G4VisAttributes(G4Colour(0.5,0.5,0.5));  // Grey
  G4VisAttributes* DewarVisAtt= new G4VisAttributes(G4Colour(0.8,0.8,0.0));  // ~Yellow
  //gj 2010-03-09:2
    CanVisAtt ->SetForceWireframe(true);
  //CanVisAtt ->SetForceSolid(true);
  

  DewarVisAtt ->SetForceSolid(true);
  
  G4VisAttributes* GeAVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
    GeAVisAtt ->SetForceSolid(true);
    //GeAVisAtt->SetVisibility(false);
  G4VisAttributes* GeAVacVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red

  G4VisAttributes* GeBVisAtt= new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
  GeBVisAtt ->SetForceSolid(true);
  //GeBVisAtt->SetVisibility(false);
  G4VisAttributes* GeBVacVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green

  G4VisAttributes* GeCVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue
  GeCVisAtt ->SetForceSolid(true);
  //GeCVisAtt->SetVisibility(false);
  G4VisAttributes* GeCVacVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue

  G4VisAttributes* GeDVisAtt= new G4VisAttributes(G4Colour()); //White 
  GeDVisAtt ->SetForceSolid(true);
  //GeDVisAtt->SetVisibility(false);
  G4VisAttributes* GeDVacVisAtt= new G4VisAttributes(G4Colour()); //White 
  
  if (NumberOfSmallClover != NumberOfClover) {
    
    //  The SupClover
    
    //    logicSupClover->SetVisAttributes(SupCloVisAtt);
    logicSupClover->SetVisAttributes (G4VisAttributes::Invisible);  
    
    
    //  The Clover can and the dewar
    
    logicCloverCan ->SetVisAttributes(CanVisAtt);
    logicEnvColdFinger ->SetVisAttributes(CanVisAtt);
    //    logicIntEnvColdFinger ->SetVisAttributes(CanVisAtt);
    logicDewar ->SetVisAttributes(DewarVisAtt);
    //    logicVac ->SetVisAttributes(CanVisAtt);
    
    //logicVac ->SetVisAttributes(G4VisAttributes::Invisible);
    logicVac ->SetVisAttributes(CanVisAtt);
    //    logicCloverCan ->SetVisAttributes(G4VisAttributes::Invisible);
    //    logicDewar ->SetVisAttributes(G4VisAttributes::Invisible);
    
    // The cold finger
    
    //    logicCFPlate ->SetVisAttributes(CFVisAtt);
    //    logicIntCF ->SetVisAttributes(CFVisAtt);
    //    logicExtCF ->SetVisAttributes(CFVisAtt);
    
    // The holes (anode) of the diodes:
    
    G4VisAttributes* HoleVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,0.0));  // Black
    G4VisAttributes* AGeVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0));  // Yellow
    G4VisAttributes* AbsorbVisAtt= new G4VisAttributes(G4Colour(0.5,0.0,0.5));  // purple
    
    AGeVisAtt ->SetForceSolid(true);
    HoleVisAtt ->SetForceSolid(true);
    //    AbsorbVisAtt ->SetForceSolid(true);
    AbsorbVisAtt ->SetForceWireframe(true);
    
    logicAGe1 ->SetVisAttributes(AGeVisAtt);
    logicHole1 ->SetVisAttributes(HoleVisAtt);
    logicAbsorb ->SetVisAttributes(AbsorbVisAtt);
#ifdef SMALL
    logicSAbsorb ->SetVisAttributes(AbsorbVisAtt);
#endif
    // Visu diode A (Red):
    
    logicGeA ->SetVisAttributes(GeAVisAtt);
    //    logicGeA ->SetVisAttributes(G4VisAttributes::Invisible);
    GeAVacVisAtt ->SetForceWireframe(true);
    
    // Visu diode B (Green):
    
    logicGeB ->SetVisAttributes(GeBVisAtt);
    //    logicGeB ->SetVisAttributes(G4VisAttributes::Invisible);
    GeBVacVisAtt ->SetForceWireframe(true);
    
    // Visu diode C (Blue):
    
    logicGeC ->SetVisAttributes(GeCVisAtt);
    //    logicGeC ->SetVisAttributes(G4VisAttributes::Invisible);
    GeCVacVisAtt ->SetForceWireframe(true);
    
    // Visu diode D (White):

    logicGeD ->SetVisAttributes(GeDVisAtt);
    //    logicGeD ->SetVisAttributes(G4VisAttributes::Invisible);
    GeDVacVisAtt ->SetForceWireframe(true);
  }
  
  // The small Clovers(s) if any
#ifdef SMALL
  //  logicSmallSupClover->SetVisAttributes (G4VisAttributes::Invisible);  
  
  logicSmallDewar ->SetVisAttributes(DewarVisAtt);
  //    logicSmallVac ->SetVisAttributes(CanVisAtt);
  logicSmallCloverCan ->SetVisAttributes(CanVisAtt);
  //gj:1:
  logicSmallSupClover->SetVisAttributes (G4VisAttributes::Invisible);  

  
  //    logicSmallDewar ->SetVisAttributes(G4VisAttributes::Invisible);
  logicSmallVac ->SetVisAttributes(G4VisAttributes::Invisible);
  //    logicSmallCloverCan ->SetVisAttributes(G4VisAttributes::Invisible);
  
  logicSmallGeA ->SetVisAttributes(GeAVisAtt);
  logicSmallGeB ->SetVisAttributes(GeBVisAtt);
  logicSmallGeC ->SetVisAttributes(GeCVisAtt);
  logicSmallGeD ->SetVisAttributes(GeDVisAtt);
#endif
  
  if ( (Config == 'A') || (Config == 'B') ) {
    
    //  The BGO Side Catcher
    
    G4VisAttributes* SCAhieldVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,0.8));  // Blue 2
    G4VisAttributes* SCBhieldVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,0.6));  // Blue 3
    G4VisAttributes* SCCanVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0));  // Yellow 
    
    SCCanVisAtt ->SetForceWireframe(true);
    //      SCCanVisAtt ->SetForceSolid(true);
    SCAhieldVisAtt ->SetForceSolid(true);
    SCBhieldVisAtt ->SetForceSolid(true);
    
    logicAlSCatcher->SetVisAttributes(SCCanVisAtt);
    logicBGOSCatcherA->SetVisAttributes(SCAhieldVisAtt);
    logicBGOSCatcherB->SetVisAttributes(SCBhieldVisAtt);
    
    //    logicAlSCatcher->SetVisAttributes(G4VisAttributes::Invisible);
    //logicBGOSCatcherA->SetVisAttributes(G4VisAttributes::Invisible);
    //    logicBGOSCatcherB->SetVisAttributes(G4VisAttributes::Invisible);
    
    //  The CsI backcatcher
    
    G4VisAttributes* BackVisAtt= new G4VisAttributes(G4Colour(0.8,0.8,0.0));  // Yellow 2
    BackVisAtt->SetForceWireframe(true);
    //      BackVisAtt->SetForceSolid(true);
    logicBackCsI->SetVisAttributes(BackVisAtt);
    //    logicBackCsI->SetVisAttributes(G4VisAttributes::Invisible);
  }
  
  if (Config == 'B') {
    
    //  The BGO Side Shield
    G4VisAttributes* SShieldVisAtt= new G4VisAttributes(G4Colour(0.6,0.6,0.6));  // Magenta
    G4VisAttributes* SSAhieldVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,1.0));  // Magenta
    G4VisAttributes* SSBhieldVisAtt= new G4VisAttributes(G4Colour(0.8,0.0,0.8));  // Magenta 2
    
    SShieldVisAtt ->SetForceSolid(true);
    SShieldVisAtt ->SetForceWireframe(true);
    SSAhieldVisAtt ->SetForceSolid(true);
    SSBhieldVisAtt ->SetForceSolid(true);
    
    logicSShield->SetVisAttributes(SShieldVisAtt);
    logicBGOSShieldA->SetVisAttributes(SSAhieldVisAtt);
    logicBGOSShieldB->SetVisAttributes(SSBhieldVisAtt);
    
    //    logicSShield->SetVisAttributes(G4VisAttributes::Invisible);
    //    logicBGOSShieldA->SetVisAttributes(G4VisAttributes::Invisible);
    //    logicBGOSShieldB->SetVisAttributes(G4VisAttributes::Invisible);
    
    
    //  The collimator
    
    G4VisAttributes* CollVisAtt= new G4VisAttributes(G4Colour(0.4,0.4,0.4));  // Grey
    CollVisAtt ->SetForceSolid(true);
    //      CollVisAtt ->SetForceWireframe(true);
    logicColl->SetVisAttributes(CollVisAtt);
    //    logicColl->SetVisAttributes(G4VisAttributes::Invisible);
  }
  
}


/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 			Construction of the Clover				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

void AgataAncillaryExogam::ConstructClover( )
{

  ////////////////
  //
  // The Cryostat
  //
  ////////////////
  //
  // The Aluminum Clover can ( "CloverCan" )...
  //

  G4double PhiStartCan = 45.*deg;
  G4double PhiTotCan = 360.*deg;
  HalfLengthCan = 7.35*cm;
  TaperLengthCan = 4.325*cm;

  G4double zPlaneCan[3];
  G4double rInnerCan[3];

  G4double zPlaneVac[3];
  G4double rInnerVac[3];
  G4double rOuterVac[3];

  zPlaneCan[0] = -HalfLengthCan;
  zPlaneCan[1] = -HalfLengthCan+TaperLengthCan;
  zPlaneCan[2] =  HalfLengthCan;

  rOuterCan[0] = 4.4085*cm;
  rOuterCan[1] = 6.2*cm;
  rOuterCan[2] = 6.2*cm;

  rInnerCan[0] = rInnerCan[1] = rInnerCan[2] = 0.*cm;

  G4Polyhedra* solidCloverCan = new G4Polyhedra("CloverCan",PhiStartCan,PhiTotCan,4,3,
						zPlaneCan,rInnerCan,rOuterCan);

  logicCloverCan = new G4LogicalVolume(solidCloverCan,Aluminum,"CloverCan");

  // The position of the Clover can in the SupClover:

  G4ThreeVector posClover(0.*cm,0.*cm,-Offset+HalfLengthCan+0.001*mm); //+0.001mm to avoid roundoff errors

  G4VPhysicalVolume* physiCloverCan = new G4PVPlacement(0,posClover,"CloverCan",
							logicCloverCan,physiSupClover,false,0,true);

  //
  // The vacuum clover ( "Vac" ) ...
  //

  HalfLengthVac = 7.175*cm;
  TaperLengthVac = 4.0842*cm;

  zPlaneVac[0] = -HalfLengthVac;
  zPlaneVac[1] = -HalfLengthVac+TaperLengthVac;
  zPlaneVac[2] =  HalfLengthVac;

  rOuterVac[0] = 4.3083*cm;
  rOuterVac[1] = 6.0*cm;
  rOuterVac[2] = 6.0*cm;

  rInnerVac[0] = rInnerVac[1] = rInnerVac[2] = 0.*cm;

  G4Polyhedra* solidVac = new G4Polyhedra("Vac",PhiStartCan,PhiTotCan,4,3,
					  zPlaneVac,rInnerVac,rOuterVac);
  logicVac = new G4LogicalVolume(solidVac,Vacuum,"Vac");

  //
  // ... ant its position in CloverCan
  //

  G4ThreeVector positionVac = G4ThreeVector(0.*cm,0.*cm,-0.25*mm);

  physiVac = new G4PVPlacement(0,positionVac,"Vac",
			       logicVac,physiCloverCan,false,0,true);

  //
  // The enveloppe of the cold finger from the back side of the can to the Dewar
  //

  G4double zPlaneEnvColdFinger[6];
  G4double rInnerEnvColdFinger[6];
  G4double rOuterEnvColdFinger[6];

  G4double PhiStart = 0.*deg;
  G4double PhiTot = 360.*deg;
  G4double EnvColdFingerHalfLength = 7.24*cm;
  
  zPlaneEnvColdFinger[0] = -EnvColdFingerHalfLength;
  zPlaneEnvColdFinger[1] = -EnvColdFingerHalfLength+4.1*cm;
  zPlaneEnvColdFinger[2] = -EnvColdFingerHalfLength+4.1*cm;
  zPlaneEnvColdFinger[3] = -EnvColdFingerHalfLength+4.9*cm;
  zPlaneEnvColdFinger[4] = -EnvColdFingerHalfLength+4.9*cm;
  zPlaneEnvColdFinger[5] =  EnvColdFingerHalfLength;

  rInnerEnvColdFinger[0]=rInnerEnvColdFinger[1]=rInnerEnvColdFinger[2]=0.*cm;
  rInnerEnvColdFinger[3]=rInnerEnvColdFinger[4]=rInnerEnvColdFinger[5]=0.*cm;

  rOuterEnvColdFinger[0]=2.225*cm;
  rOuterEnvColdFinger[1]=2.225*cm;
  rOuterEnvColdFinger[2]=3.1*cm;
  rOuterEnvColdFinger[3]=3.1*cm;
  rOuterEnvColdFinger[4]=2.225*cm;
  rOuterEnvColdFinger[5]=2.225*cm;

  G4Polycone* solidEnvColdFinger = new G4Polycone("EnvColdFinger",PhiStart,PhiTot,6,
						  zPlaneEnvColdFinger,rInnerEnvColdFinger,rOuterEnvColdFinger);

  logicEnvColdFinger = new G4LogicalVolume(solidEnvColdFinger,Aluminum,"EnvColdFinger");

  // The position of the cold finger enveloppe in the SupClover:

  G4ThreeVector posEnvColdFinger = 
    G4ThreeVector(0.*cm,0.*cm,-Offset+2.*HalfLengthCan+EnvColdFingerHalfLength+0.005*mm);

  /*G4VPhysicalVolume* physiEnvColdFinger = */
  new G4PVPlacement(0,posEnvColdFinger,"EnvColdFinger",logicEnvColdFinger,physiSupClover,false,0,true);
  // Its internal vacuum...

  G4double minRadiusIntEnvColdFinger = 0.*cm;
  G4double maxRadiusIntEnvColdFinger = 2.025*cm;
  G4double HalfLengthIntEnvColdFinger = 7.24*cm;
  G4double startPhiIntEnvColdFinger = 0.*deg;
  G4double deltaPhiIntEnvColdFinger = 360.*deg;

  G4Tubs* solidIntEnvColdFinger = new G4Tubs("IntDewar",minRadiusIntEnvColdFinger,maxRadiusIntEnvColdFinger,
					     HalfLengthIntEnvColdFinger,startPhiIntEnvColdFinger,deltaPhiIntEnvColdFinger);

  G4LogicalVolume* logicIntEnvColdFinger = 
    new G4LogicalVolume(solidIntEnvColdFinger,Vacuum,"IntEnvColdFinger");

  // and its position in the cold finger enveloppe.

  G4VPhysicalVolume* physiIntEnvColdFinger = 
    new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicIntEnvColdFinger,"IntEnvColdFinger",logicEnvColdFinger,false,0,true);
  //	new G4PVPlacement(0,0,"IntEnvColdFinger",logicIntEnvColdFinger,physiEnvColdFinger,false,0,true);

  //
  // The cold finger and the associated plate
  //

  G4double xHalfLengthCFPlate = 5.04*cm;
  G4double yHalfLengthCFPlate = 5.04*cm;
  G4double zHalfLengthCFPlate = 1.*mm;

  G4Box* solidCFPlate = new G4Box("CFPlate",xHalfLengthCFPlate,yHalfLengthCFPlate,
				  zHalfLengthCFPlate);

  G4LogicalVolume* logicCFPlate = new G4LogicalVolume(solidCFPlate,Copper,"CFPlate");

  G4ThreeVector posCFPlate(0.*cm,0.*cm,-HalfLengthVac+9.65*cm); // 0.55(d(IntCan-Ge)
  // +9.(Ge length)+0.1(half length plate)

  //  G4VPhysicalVolume* physiCFPlate = 
  new G4PVPlacement(0,posCFPlate,"CFPlate",logicCFPlate,physiVac,false,0,true);
  //
  // The cold finger (internal part)
  //

  G4double minRadiusIntCF = 0.*cm;
  G4double maxRadiusIntCF = 1.5*cm;
  G4double HalfLengthIntCF = 2.30*cm;
  G4double startPhiIntCF = 0.*deg;
  G4double deltaPhiIntCF = 360.*deg;

  G4Tubs* solidIntCF = new G4Tubs("IntCF",minRadiusIntCF,maxRadiusIntCF,
				  HalfLengthIntCF,startPhiIntCF,deltaPhiIntCF);

  G4LogicalVolume* logicIntCF = 
    new G4LogicalVolume(solidIntCF,Copper,"IntCF");

  // its position vs CloverCan...

  G4ThreeVector posIntCF(0.*cm,0.*cm,4.875*cm); // -7.175 (halflengthcan internal)
						// +0.55 (ext Can - Ge)
						// +9.0 (Ge length)
						// +0.2 (CF plate)
						// +2.3 (IntCF length)

  //  G4VPhysicalVolume* physiIntCF = 
  new G4PVPlacement(0,posIntCF,"IntCF",logicIntCF,physiVac,false,0,true);

  //
  // The cold finger (external part)
  //

  G4double minRadiusExtCF = 0.*cm;
  G4double maxRadiusExtCF = 2.0*cm;
  G4double HalfLengthExtCF = 7.2*cm;
  G4double startPhiExtCF = 0.*deg;
  G4double deltaPhiExtCF = 360.*deg;

  G4Tubs* solidExtCF = new G4Tubs("IntCF",minRadiusExtCF,maxRadiusExtCF,
				  HalfLengthExtCF,startPhiExtCF,deltaPhiExtCF);

  G4LogicalVolume* logicExtCF = 
    new G4LogicalVolume(solidExtCF,Copper,"ExtCF");

  // its position vs EnvColdFinger...

  G4ThreeVector posExtCF(0.*cm,0.*cm,0.*cm); 

  //  G4VPhysicalVolume* physiExtCF = 
  new G4PVPlacement(0,posExtCF,"ExtCF",logicExtCF,physiIntEnvColdFinger,false,0,true);

  //
  // The Dewar
  //

  G4double minRadiusDewar = 0.*cm;
  G4double maxRadiusDewar = 10.9*cm;
  G4double HalfLengthDewar = 15.2*cm;
  G4double startPhiDewar = 0.*deg;
  G4double deltaPhiDewar = 360.*deg;

  G4Tubs* solidDewar = new G4Tubs("Dewar",minRadiusDewar,maxRadiusDewar,
				  HalfLengthDewar,startPhiDewar,deltaPhiDewar);

  logicDewar = new G4LogicalVolume(solidDewar,Aluminum,"Dewar");

  G4double distFrontToMidDewar = 
    -Offset+2.*(HalfLengthCan+EnvColdFingerHalfLength)+HalfLengthDewar+0.01*mm; 
  //+0.01mm to avoid roundoff errors

  G4ThreeVector posDewar = G4ThreeVector(0.*cm,0.*cm,distFrontToMidDewar);

  // ... and its position in the SupClover

  /*G4VPhysicalVolume* physiDewar =*/
  new G4PVPlacement(0,posDewar,"Dewar",logicDewar,physiSupClover,false,0,true);

  // Its internal vacuum...

  G4double minRadiusIntDewar = 0.*cm;
  G4double maxRadiusIntDewar = 10.4*cm;
  G4double HalfLengthIntDewar = 14.7*cm;
  G4double startPhiIntDewar = 0.*deg;
  G4double deltaPhiIntDewar = 360.*deg;

  G4Tubs* solidIntDewar = new G4Tubs("IntDewar",minRadiusIntDewar,maxRadiusIntDewar,
				     HalfLengthIntDewar,startPhiIntDewar,deltaPhiIntDewar);

  G4LogicalVolume* logicIntDewar = new G4LogicalVolume(solidIntDewar,Vacuum,"IntDewar");

  //  G4VPhysicalVolume* physiIntDewar = 
  //new G4PVPlacement(0,0,"IntDewar",logicIntDewar,physiDewar,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicIntDewar,"IntDewar",logicDewar,false,0,true);

  G4cout << "The Cryostat built... " << G4endl;

  /////////////////////////////////////////
  //
  //  Construction of the active Ge volume:
  //
  /////////////////////////////////////////
  //
  //  A: Ge diode built from cuts subtracted from a cylinder (the "GeDiode")
  //

  G4double minRadiusGeDiode = 0.*cm;
  G4double maxRadiusGeDiode = 3.0*cm;
  G4double HalfLengthGeDiode = 4.5*cm;
  G4double startPhiGeDiode = 0.*deg;
  G4double deltaPhiGeDiode = 360.*deg;

  G4Tubs* solidGeDiode = new G4Tubs("GeDiode",minRadiusGeDiode,maxRadiusGeDiode,
				    HalfLengthGeDiode,startPhiGeDiode,deltaPhiGeDiode);
  //
  // External Tapered volume all along the diode ( "Cut1&2" )
  //
  //
  // Cut 1 :
  //
  G4double dummy = acos(2.9/3.0);
  G4double xHalfLengthCut1 = 0.5*mm;
  G4double yHalfLengthCut1 = 2.9*tan(dummy)*cm;
  G4double zHalfLengthCut1 = 4.55*cm;

  G4Box* solidCut1 = new G4Box("Cut1",xHalfLengthCut1,yHalfLengthCut1,
			       zHalfLengthCut1);

  //
  //... and its position vs GeDiode
  //

  G4ThreeVector transCut1(2.95*cm,0.*cm,0.*cm);
  G4SubtractionSolid* solidGeMinusCut1 = 
    new G4SubtractionSolid("GeMinusCut1",solidGeDiode,solidCut1,0,transCut1);

  //
  // Cut 2 :
  //
  //... and its position vs GeMinusCut1

  G4ThreeVector transCut2(0.,2.95*cm,0.);
  G4Transform3D positionCut2(rm90,transCut2);

  G4SubtractionSolid* solidGeMinusCut12 = 
    new G4SubtractionSolid("GeMinusCut12",solidGeMinusCut1,solidCut1,positionCut2);
  //
  // External Tapered volume at the front face ( "Cut3&4" )
  //
  // Cut 3 :
  //

  G4double cosTap = cos(22.5*deg);  
  G4double sinTap = sin(22.5*deg);
  G4double tanTap = tan(22.5*deg);

  G4double xHalfLengthCut3 = 3.0*cm;
  G4double yHalfLengthCut3 = 1.5*cm*sinTap;
  G4double zHalfLengthCut3 = 1.5*cm/cosTap;

  G4Box* solidCut3 = new G4Box("Cut3",xHalfLengthCut3,yHalfLengthCut3,
			       zHalfLengthCut3+0.5*cm);
  //
  // ... and its position vs GeMinusCut12
  //

  G4double yCut3 = 2.9*cm-1.5*cm*tanTap+yHalfLengthCut3*cosTap;

  G4double temp = zHalfLengthCut3*cosTap-yHalfLengthCut3*sinTap;
  G4double zCut3 = -HalfLengthGeDiode+temp;

  G4RotationMatrix rmCut3;
  rmCut3.rotateX(-22.5*deg);

  G4ThreeVector transCut3(0.,yCut3,zCut3);
  G4Transform3D positionCut3(rmCut3,transCut3);

  G4SubtractionSolid* solidGeMinusCut123 = 
    new G4SubtractionSolid("GeMinusCut123",solidGeMinusCut12,solidCut3,positionCut3);

  //
  // Cut 4 :
  //

  G4Box* solidCut4 = new G4Box("Cut4",yHalfLengthCut3,xHalfLengthCut3,
			       zHalfLengthCut3);

  //
  // ... and its position vs GeMinusCut123
  //

  G4RotationMatrix rmCut4;
  rmCut4.rotateY(22.5*deg);

  G4ThreeVector transCut4(yCut3,0.,zCut3);
  G4Transform3D positionCut4(rmCut4,transCut4);

  G4SubtractionSolid* solidGeMinusCut1234 = 
    new G4SubtractionSolid("GeMinusCut1234",solidGeMinusCut123,solidCut4,positionCut4);

  //
  // Internal Tapered volume all along the diode ( "Cut5&6" )
  //
  //
  // Cut 5 :
  //
  dummy = acos(2.45/3.0);
  G4double xHalfLengthCut5 = 5.5*mm;
  G4double yHalfLengthCut5 = 2.45*tan(dummy)*cm;
  G4double zHalfLengthCut5 = 4.55*cm;

  G4Box* solidCut5 = new G4Box("Cut5",xHalfLengthCut5,yHalfLengthCut5,
			       zHalfLengthCut5);

  //
  //... and its position vs GeMinusCut1234
  //

  G4ThreeVector transCut5(-3.0*cm,0.*cm,0.*cm);

  G4SubtractionSolid* solidGeMinusCut12345 = 
    new G4SubtractionSolid("GeMinusCut12345",solidGeMinusCut1234,solidCut5,0,transCut5);

  //
  // Cut 6 :
  //
  //... and its position vs GeMinusCut12345 to get the final diode

  G4ThreeVector transCut6(0.,-3.0*cm,0.);
  G4Transform3D positionCut6(rm90,transCut6);

  G4SubtractionSolid* solidGe = 
    new G4SubtractionSolid("Ge",solidGeMinusCut12345,solidCut5,positionCut6);

  // Now the individual diode is built; create logical volumes for each of
  // the four individual diodes A, B, C and D:

  logicGeA = new G4LogicalVolume(solidGe,Germanium,"Ge1A");
  logicGeB = new G4LogicalVolume(solidGe,Germanium,"Ge1B");
  logicGeC = new G4LogicalVolume(solidGe,Germanium,"Ge1C");
  logicGeD = new G4LogicalVolume(solidGe,Germanium,"Ge1D");

  //gj - making ge sensitive detector:
  logicGeA -> SetSensitiveDetector(ancSD);
  logicGeB -> SetSensitiveDetector(ancSD);
  logicGeC -> SetSensitiveDetector(ancSD);
  logicGeD -> SetSensitiveDetector(ancSD);



  // positioning the tapered partial diodes (A to D)
  // into the real vacuum of the can

  G4double HalfDistanceBetweenDiodes = 0.5*mm;

  G4double xDumVac = 2.45*cm+HalfDistanceBetweenDiodes; 
  G4double yDumVac = 2.45*cm+HalfDistanceBetweenDiodes; 
  G4double zDumVac = -HalfLengthVac+5.05*cm; 	// 5.05 = 0.55 d(int can to Ge) +4.5(half length Ge) 

  G4ThreeVector positionVacA(xDumVac,yDumVac,zDumVac);

  G4ThreeVector posDumVacB(xDumVac,-yDumVac,zDumVac);
  G4Transform3D positionVacB(rm270,posDumVacB);

  G4ThreeVector posDumVacC(-xDumVac,-yDumVac,zDumVac);
  G4Transform3D positionVacC(rm180,posDumVacC);

  G4ThreeVector posDumVacD(-xDumVac,yDumVac,zDumVac);
  G4Transform3D positionVacD(rm90,posDumVacD);


  G4VPhysicalVolume* physiGeA = 
    new G4PVPlacement(0,positionVacA,"GeA",logicGeA,physiVac,false,0,true);  // Red in gj geometry
  G4VPhysicalVolume* physiGeB = 
    new G4PVPlacement(positionVacB,"GeB",logicGeB,physiVac,false,1,true);    // Green in gj geometry
  G4VPhysicalVolume* physiGeC = 
    new G4PVPlacement(positionVacC,"GeC",logicGeC,physiVac,false,2,true);    // Blue in gj geometry
  G4VPhysicalVolume* physiGeD = 
    new G4PVPlacement(positionVacD,"GeD",logicGeD,physiVac,false,3,true);    // White in gj geometry

  //
  // some material between the diodes to reproduce the experimental addback factor ...
  //

  G4double xAbsorb1 = 4.16*cm;
  G4double yAbsorb1 = 200.*um; // max = HalfDistanceBetweenDiodes = 0.5*mm;
  G4double zAbsorb1 = 4.5*cm;

  G4Box* solidAbsorb1 = new G4Box("Absorb1",xAbsorb1,yAbsorb1,zAbsorb1);

  G4double xAbsorb2 = 200*um; // max = HalfDistanceBetweenDiodes = 0.5*mm;
  G4double yAbsorb2 = 4.16*cm;
  G4double zAbsorb2 = 4.5*cm;

  G4Box* solidAbsorb2 = new G4Box("Absorb2",xAbsorb2,yAbsorb2,zAbsorb2);

  //G4UnionSolid* solidAbsorb = 
  //new G4UnionSolid("Absorb",solidAbsorb1,solidAbsorb2,0,0);
  G4UnionSolid* solidAbsorb = 
    new G4UnionSolid("Absorb",solidAbsorb1,solidAbsorb2);

  logicAbsorb = new G4LogicalVolume(solidAbsorb,Copper,"Absorb");

  G4ThreeVector positionAbsorb(0.,0.,zDumVac);

  /*G4VPhysicalVolume* physiAbsorb = */
  new G4PVPlacement(0,positionAbsorb,"Absorb",logicAbsorb,physiVac,false,0,true);

  //
  // Now: takes care of the holes and amorphous Ge in each diode:
  // Central hole with amorphous Ge for each diode. 
  //

  G4double minRadiusAGe1 = 0.*cm;
  G4double maxRadiusAGe1 = 0.52*cm;
  G4double HalfLengthAGe1 = 3.75*cm;
  G4double startPhiAGe1 = 0.*deg;
  G4double deltaPhiAGe1 = 360.*deg;

  G4Tubs* solidAGe1 = new G4Tubs("AGe1",minRadiusAGe1,maxRadiusAGe1,
				 HalfLengthAGe1,startPhiAGe1,deltaPhiAGe1);

  logicAGe1 = new G4LogicalVolume(solidAGe1,Germanium,"AGe1");
  //gj - making sensitive
  //  logicAGe1 -> SetSensitiveDetector(ancSD);
  

  // ... and second the hole in it:

  G4Tubs* solidHole1 = new G4Tubs("Hole1",minRadiusAGe1,maxRadiusAGe1-2.*mm,
				  HalfLengthAGe1,startPhiAGe1,deltaPhiAGe1);

  logicHole1 = new G4LogicalVolume(solidHole1,Vacuum,"Hole1");

  //
  // positioning the amorphous Ge in the diode
  //

  G4ThreeVector transAGe(0.*cm,0.*cm,0.75*cm);

  /*  G4VPhysicalVolume* physiAGe1A = */
  new G4PVPlacement(0,transAGe,"AGe1A",logicAGe1,physiGeA,false,0,true);
  new G4PVPlacement(0,transAGe,"AGe1B",logicAGe1,physiGeB,false,1,true);
  new G4PVPlacement(0,transAGe,"AGe1C",logicAGe1,physiGeC,false,2,true);
  new G4PVPlacement(0,transAGe,"AGe1D",logicAGe1,physiGeD,false,3,true);
  
  //
  // positioning the hole in the amorphous Ge
  //

  //new G4PVPlacement(0,0,"Hole1A",logicHole1,physiAGe1A,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicHole1,"Hole1A",logicAGe1,false,0,true);

  //
  // the Ge diodes for the EXOGAM Clover done
  //

}

/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 		Construction of the small Clover				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

#ifdef SMALL
void AgataAncillaryExogam::ConstructSmallClover( )
{

  ////////////////
  //
  // The Cryostat
  //
  ////////////////
  //
  // The Aluminum Clover can ( "CloverCan" )...
  //

  G4double SPhiStartCan = 45.*deg;
  G4double SPhiTotCan = 360.*deg;
  SHalfLengthCan = 22.25*cm;
  G4double STaperLengthCan = 5.7*cm;

  G4double SzPlaneCan[3];
  G4double SrInnerCan[3];
  G4double SrOuterCan[3];

  G4double SzPlaneVac[3];
  G4double SrInnerVac[3];
  G4double SrOuterVac[3];

  SzPlaneCan[0] = -SHalfLengthCan;
  SzPlaneCan[1] = -SHalfLengthCan+STaperLengthCan;
  SzPlaneCan[2] =  SHalfLengthCan;

  SrOuterCan[0] = 4.45*cm;
  SrOuterCan[1] = 5.045*cm;
  SrOuterCan[2] = 5.045*cm;

  SrInnerCan[0] = SrInnerCan[1] = SrInnerCan[2] = 0.*cm;

  G4Polyhedra* solidSmallCloverCan = new G4Polyhedra("SmallCloverCan",SPhiStartCan,SPhiTotCan,4,3,
						     SzPlaneCan,SrInnerCan,SrOuterCan);

  logicSmallCloverCan = new G4LogicalVolume(solidSmallCloverCan,Aluminum,"SmallCloverCan");

  // The position of the Clover can in the SmallSupClover:

  G4ThreeVector SposClover(0.*cm,0.*cm,-SOffset+SHalfLengthCan+0.001*mm); //+0.001mm to avoid roundoff errors

  G4VPhysicalVolume* physiSmallCloverCan = new G4PVPlacement(0,SposClover,"SmallCloverCan",
							     logicSmallCloverCan,physiSmallSupClover,false,0,true);

  //
  // The vacuum clover ( "Vac" ) ...
  //

  G4double SHalfLengthVac = 22.10*cm;
  G4double STaperLengthVac = 5.7*cm;

  SzPlaneVac[0] = -SHalfLengthVac;
  SzPlaneVac[1] = -SHalfLengthVac+STaperLengthVac;
  SzPlaneVac[2] =  SHalfLengthVac;

  SrOuterVac[0] = 4.3*cm;
  SrOuterVac[1] = 4.895*cm;
  SrOuterVac[2] = 4.895*cm;

  SrInnerVac[0] = SrInnerVac[1] = SrInnerVac[2] = 0.*cm;

  G4Polyhedra* solidSmallVac = new G4Polyhedra("SmallVac",SPhiStartCan,SPhiTotCan,4,3,
					       SzPlaneVac,SrInnerVac,SrOuterVac);
  logicSmallVac = new G4LogicalVolume(solidSmallVac,Vacuum,"SmallVac");

  //
  // ... ant its position in CloverCan
  //

  G4ThreeVector SpositionVac = G4ThreeVector(0.*cm,0.*cm,-0.375*mm);

  G4VPhysicalVolume* physiSmallVac = new G4PVPlacement(0,SpositionVac,"SmallVac",
						       logicSmallVac,physiSmallCloverCan,false,0,true);

  //
  // The Dewar
  //

  G4double SminRadiusDewar = 0.*cm;
  G4double SmaxRadiusDewar = 10.9*cm;
  G4double SHalfLengthDewar = 15.2*cm;
  G4double SstartPhiDewar = 0.*deg;
  G4double SdeltaPhiDewar = 360.*deg;

  G4Tubs* solidSmallDewar = new G4Tubs("SmallDewar",SminRadiusDewar,SmaxRadiusDewar,
				       SHalfLengthDewar,SstartPhiDewar,SdeltaPhiDewar);

  logicSmallDewar = new G4LogicalVolume(solidSmallDewar,Aluminum,"SmallDewar");

  G4double SdistFrontToMidDewar = 
    -SOffset+2.*SHalfLengthCan+SHalfLengthDewar+0.01*mm; 
  //+0.01mm to avoid roundoff errors

  G4ThreeVector SposDewar = G4ThreeVector(0.*cm,0.*cm,SdistFrontToMidDewar);

  // the position of the Dewar (identical to the big Clover) in the SmallSupClover

  G4VPhysicalVolume* physiSmallDewar = new G4PVPlacement(0,SposDewar,"SmallDewar",
							 logicSmallDewar,physiSmallSupClover,false,0,true);

  // Its internal vacuum...

  G4double SminRadiusIntDewar = 0.*cm;
  G4double SmaxRadiusIntDewar = 10.4*cm;
  G4double SHalfLengthIntDewar = 14.7*cm;
  G4double SstartPhiIntDewar = 0.*deg;
  G4double SdeltaPhiIntDewar = 360.*deg;

  G4Tubs* solidSmallIntDewar = new G4Tubs("SmallIntDewar",SminRadiusIntDewar,SmaxRadiusIntDewar,
					  SHalfLengthIntDewar,SstartPhiIntDewar,SdeltaPhiIntDewar);

  G4LogicalVolume* logicSmallIntDewar = new G4LogicalVolume(solidSmallIntDewar,Vacuum,"SmallIntDewar");

  // new G4PVPlacement(0,0,"SmallIntDewar",logicSmallIntDewar,physiSmallDewar,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicSmallIntDewar,"SmallIntDewar",logicSmallDewar,false,0,true);

  G4cout << "The Cryostat for the small Clover built... " << G4endl;

  /////////////////////////////////////////
  //
  //  Construction of the active Ge volume:
  //
  /////////////////////////////////////////
  //
  //  A: Ge diode built from cuts subtracted from a cylinder (the "GeDiode")
  //

  G4double SminRadiusGeDiode = 0.*cm;
  G4double SmaxRadiusGeDiode = 2.5*cm;
  G4double SHalfLengthGeDiode = 3.5*cm;
  G4double SstartPhiGeDiode = 0.*deg;
  G4double SdeltaPhiGeDiode = 360.*deg;

  G4Tubs* solidSmallGeDiode = new G4Tubs("SmallGeDiode",SminRadiusGeDiode,SmaxRadiusGeDiode,
					 SHalfLengthGeDiode,SstartPhiGeDiode,SdeltaPhiGeDiode);
  //
  // External Tapered volume all along the diode ( "Cut1&2" )
  //
  //
  // SmallCut 1 :
  //
  G4double Sdummy = acos(2.35/2.5);
  G4double SxHalfLengthCut1 = 1*mm;
  G4double SyHalfLengthCut1 = 2.35*tan(Sdummy)*cm;
  G4double SzHalfLengthCut1 = 3.55*cm;

  G4Box* solidSmallCut1 = new G4Box("SCut1",SxHalfLengthCut1,SyHalfLengthCut1,
				    SzHalfLengthCut1);

  //
  //... and its position vs SmallGeDiode
  //

  G4ThreeVector StransCut1(2.45*cm,0.*cm,0.*cm);
  G4SubtractionSolid* solidSmallGeMinusCut1 = 
    new G4SubtractionSolid("SmallGeMinusCut1",solidSmallGeDiode,solidSmallCut1,0,StransCut1);

  //
  // SmallCut 2 :
  //
  //... and its position vs SmallGeMinusCut1

  G4ThreeVector StransCut2(0.,2.45*cm,0.);
  G4Transform3D SpositionCut2(rm90,StransCut2);

  G4SubtractionSolid* solidSmallGeMinusCut12 = 
    new G4SubtractionSolid("SmallGeMinusCut12",solidSmallGeMinusCut1,solidSmallCut1,SpositionCut2);
  //
  // External Tapered volume at the front face ( "Cut3&4" )
  //
  // SmallCut 3 :
  //

  G4double ScosTap = cos(7.1*deg);  
  G4double SsinTap = sin(7.1*deg);
  G4double StanTap = tan(7.1*deg);

  G4double SxHalfLengthCut3 = 2.5*cm;
  G4double SyHalfLengthCut3 = 1.6*cm*SsinTap;	// tapered length = 3.2 cm (remove 4 mm at the front face)
  G4double SzHalfLengthCut3 = 1.6*cm/ScosTap;

  G4Box* solidSmallCut3 = new G4Box("SmallCut3",SxHalfLengthCut3,SyHalfLengthCut3,
				    SzHalfLengthCut3+0.5*cm);
  //
  // ... and its position vs SmallGeMinusCut12
  //

  G4double SyCut3 = 2.35*cm-1.6*cm*StanTap+SyHalfLengthCut3*ScosTap;

  G4double Stemp = SzHalfLengthCut3*ScosTap-SyHalfLengthCut3*SsinTap;
  G4double SzCut3 = -SHalfLengthGeDiode+Stemp;

  G4RotationMatrix rmSCut3;
  rmSCut3.rotateX(-7.1*deg);

  G4ThreeVector StransCut3(0.,SyCut3,SzCut3);
  G4Transform3D SpositionCut3(rmSCut3,StransCut3);

  G4SubtractionSolid* solidSmallGeMinusCut123 = 
    new G4SubtractionSolid("SmallGeMinusCut123",solidSmallGeMinusCut12,solidSmallCut3,SpositionCut3);

  //
  // SmallCut 4 :
  //

  G4Box* solidSmallCut4 = new G4Box("SmallCut4",SyHalfLengthCut3,SxHalfLengthCut3,
				    SzHalfLengthCut3);

  //
  // ... and its position vs SmallGeMinusCut123
  //

  G4RotationMatrix rmSCut4;
  rmSCut4.rotateY(7.1*deg);

  G4ThreeVector StransCut4(SyCut3,0.,SzCut3);
  G4Transform3D SpositionCut4(rmSCut4,StransCut4);

  G4SubtractionSolid* solidSmallGeMinusCut1234 = 
    new G4SubtractionSolid("SmallGeMinusCut1234",solidSmallGeMinusCut123,solidSmallCut4,SpositionCut4);

  //
  // Internal Tapered volume all along the diode ( "Cut5&6" )
  //
  //
  // SmallCut 5 :
  //
  Sdummy = acos(2.15/2.5);
  G4double SxHalfLengthCut5 = 4.0*mm;
  G4double SyHalfLengthCut5 = 2.15*tan(Sdummy)*cm;
  G4double SzHalfLengthCut5 = 3.55*cm;

  G4Box* solidSmallCut5 = new G4Box("SmallCut5",SxHalfLengthCut5,SyHalfLengthCut5,
				    SzHalfLengthCut5);

  //
  //... and its position vs SmallGeMinusCut1234
  //

  G4ThreeVector StransCut5(-2.55*cm,0.*cm,0.*cm);

  G4SubtractionSolid* solidSmallGeMinusCut12345 = 
    new G4SubtractionSolid("SmallGeMinusCut12345",solidSmallGeMinusCut1234,solidSmallCut5,0,StransCut5);

  //
  // SmallCut 6 :
  //
  //... and its position vs SmallGeMinusCut12345 to get the final diode

  G4ThreeVector StransCut6(0.,-2.55*cm,0.);
  G4Transform3D SpositionCut6(rm90,StransCut6);

  G4SubtractionSolid* solidSmallGe = 
    new G4SubtractionSolid("SmallGe",solidSmallGeMinusCut12345,solidSmallCut5,SpositionCut6);

  // Now the individual diode is built; create logical volumes for each of
  // the four individual diodes A, B, C and D:

  logicSmallGeA = new G4LogicalVolume(solidSmallGe,Germanium,"SmallGe1A");
  logicSmallGeB = new G4LogicalVolume(solidSmallGe,Germanium,"SmallGe1B");
  logicSmallGeC = new G4LogicalVolume(solidSmallGe,Germanium,"SmallGe1C");
  logicSmallGeD = new G4LogicalVolume(solidSmallGe,Germanium,"SmallGe1D");

  logicSmallGeA -> SetSensitiveDetector(ancSD);
  logicSmallGeB -> SetSensitiveDetector(ancSD);
  logicSmallGeC -> SetSensitiveDetector(ancSD);
  logicSmallGeD -> SetSensitiveDetector(ancSD);

  // positioning the tapered partial diodes (A to D)
  // into the real vacuum of the can

  G4double SHalfDistanceBetweenDiodes = 0.5*mm;

  G4double SxDumVac = 2.15*cm+SHalfDistanceBetweenDiodes; 
  G4double SyDumVac = 2.15*cm+SHalfDistanceBetweenDiodes; 
  G4double SzDumVac = -SHalfLengthVac+5.5*cm; 	// 5.5 = 2.1 d(int can to Ge) +3.5(half length Ge) 

  G4ThreeVector SpositionVacA(SxDumVac,SyDumVac,SzDumVac);

  G4ThreeVector SposDumVacB(SxDumVac,-SyDumVac,SzDumVac);
  G4Transform3D SpositionVacB(rm270,SposDumVacB);

  G4ThreeVector SposDumVacC(-SxDumVac,-SyDumVac,SzDumVac);
  G4Transform3D SpositionVacC(rm180,SposDumVacC);

  G4ThreeVector SposDumVacD(-SxDumVac,SyDumVac,SzDumVac);
  G4Transform3D SpositionVacD(rm90,SposDumVacD);

  G4VPhysicalVolume* physiSmallGeA = 
    new G4PVPlacement(0,SpositionVacA,"SmallGeA",logicSmallGeA,physiSmallVac,false,0,true);
  G4VPhysicalVolume* physiSmallGeB = 
    new G4PVPlacement(SpositionVacB,"SmallGeB",logicSmallGeB,physiSmallVac,false,1,true);
  G4VPhysicalVolume* physiSmallGeC = 
    new G4PVPlacement(SpositionVacC,"SmallGeC",logicSmallGeC,physiSmallVac,false,2,true);
  G4VPhysicalVolume* physiSmallGeD = 
    new G4PVPlacement(SpositionVacD,"SmallGeD",logicSmallGeD,physiSmallVac,false,3,true);

  //
  // some material between the diodes to reproduce the experimental addback factor ...
  //

  G4double xSAbsorb1 = 4.1*cm;
  G4double ySAbsorb1 = 0.2*mm; // max = SHalfDistanceBetweenDiodes = 0.5*mm;
  G4double zSAbsorb1 = 3.5*cm;

  G4Box* solidSAbsorb1 = new G4Box("SAbsorb1",xSAbsorb1,ySAbsorb1,zSAbsorb1);

  G4double xSAbsorb2 = 0.2*mm; // max = SHalfDistanceBetweenDiodes = 0.5*mm;
  G4double ySAbsorb2 = 4.1*cm;
  G4double zSAbsorb2 = 3.5*cm;

  G4Box* solidSAbsorb2 = new G4Box("SAbsorb2",xSAbsorb2,ySAbsorb2,zSAbsorb2);

  //G4UnionSolid* solidSAbsorb = 
  //new G4UnionSolid("SAbsorb",solidSAbsorb1,solidSAbsorb2,0,0);
  G4UnionSolid* solidSAbsorb = 
    new G4UnionSolid("SAbsorb",solidSAbsorb1,solidSAbsorb2);

  logicSAbsorb = new G4LogicalVolume(solidSAbsorb,Copper,"SAbsorb");

  G4ThreeVector SpositionSAbsorb(0.,0.,SzDumVac);

  G4VPhysicalVolume* physiSAbsorb = 
    new G4PVPlacement(0,SpositionSAbsorb,"SAbsorb",logicSAbsorb,physiSmallVac,false,0,true);
  //
  // Now: takes care of the holes and amorphous Ge in each diode:
  // Central hole with amorphous Ge for each diode. 
  //

  G4double SminRadiusAGe1 = 0.*cm;
  G4double SmaxRadiusAGe1 = 0.52*cm;
  G4double SHalfLengthAGe1 = 3.0*cm;
  G4double SstartPhiAGe1 = 0.*deg;
  G4double SdeltaPhiAGe1 = 360.*deg;

  G4Tubs* solidSmallAGe1 = new G4Tubs("SmallAGe1",SminRadiusAGe1,SmaxRadiusAGe1,
				      SHalfLengthAGe1,SstartPhiAGe1,SdeltaPhiAGe1);

  logicSmallAGe1 = new G4LogicalVolume(solidSmallAGe1,Germanium,"SmallAGe1");
  //  logicSmallAGe1 -> SetSensitiveDetector(ancSD);
  //  G4VisAttributes* red= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
  //  logicSmallAGe1->SetVisAttributes(red);
  
  // ... and second the hole in it:

  G4Tubs* solidSmallHole1 = new G4Tubs("SmallHole1",SminRadiusAGe1,SmaxRadiusAGe1-2.*mm,
				       SHalfLengthAGe1,SstartPhiAGe1,SdeltaPhiAGe1);

  logicSmallHole1 = new G4LogicalVolume(solidSmallHole1,Vacuum,"SmallHole1");

  //
  // positioning the amorphous Ge in the diode
  //

  G4ThreeVector StransAGe(0.*cm,0.*cm,0.5*cm);

  G4VPhysicalVolume* physiSmallAGe1A = 
    new G4PVPlacement(0,StransAGe,"SmallAGe1A",logicSmallAGe1,physiSmallGeA,false,0,true);
  new G4PVPlacement(0,StransAGe,"SmallAGe1B",logicSmallAGe1,physiSmallGeB,false,1,true);
  new G4PVPlacement(0,StransAGe,"SmallAGe1C",logicSmallAGe1,physiSmallGeC,false,2,true);
  new G4PVPlacement(0,StransAGe,"SmallAGe1D",logicSmallAGe1,physiSmallGeD,false,3,true);

  //
  // positioning the hole in the amorphous Ge
  //

  //  new G4PVPlacement(0,0,"SmallHole1A",logicSmallHole1,physiSmallAGe1A,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicSmallHole1,"SmallHole1A",logicSmallAGe1,false,0,true);

  //
  // The Ge diodes for the small Clover done
  //

}
#endif


/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 		Construction of the BGO side catcher				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

void AgataAncillaryExogam::ConstructSideCatcher( )
{

  G4double zHalfLengthSCFront = 2.5349*cm;

  G4double ThetaSCFront = 16.325*deg;
  G4double PhiSCFront = -45.*deg;

  G4double dy1SCFront = 6.4125*cm;
  G4double dx1SCFront = 0.2*cm;
  G4double dx2SCFront = 0.2*cm;
  G4double dy2SCFront = 7.4625*cm;
  G4double dx3SCFront = 1.25*cm;
  G4double dx4SCFront = 1.25*cm;

  G4Trap* solidSCFront = new G4Trap("SCFront",zHalfLengthSCFront,ThetaSCFront,PhiSCFront,
				    dy1SCFront, dx1SCFront,dx2SCFront,0.,dy2SCFront, dx3SCFront,dx4SCFront,0.);

  //
  // ... (ii) secondly the rear part ...
  //

  G4double xHalfLengthSCRear = 1.25*cm;
  G4double yHalfLengthSCRear = 7.4625*cm;
  G4double zHalfLengthSCRear = 3.19*cm;

  G4Box* solidSCRear = new G4Box("SCRear",xHalfLengthSCRear,yHalfLengthSCRear,
				 zHalfLengthSCRear);

  //
  // ... (iii) finally the union which is the Al can of the BGO side shield elements.
  //

  G4double zSCRearFront = -zHalfLengthSCFront-zHalfLengthSCRear;
  G4ThreeVector posSCRearFront(-0.525*cm,0.525*cm,zSCRearFront);

  G4UnionSolid* solidAlSCatcher = 
    new G4UnionSolid("AlSCatcher",solidSCRear,solidSCFront,0,posSCRearFront);

  logicAlSCatcher = new G4LogicalVolume(solidAlSCatcher,Aluminum,"AlSCatcher");

  //
  // positioning the Side Catcher elements in the Clover total enveloppe 
  //

  G4double xSC0 = rOuterCan[1]+xHalfLengthSCRear+0.05*cm;
  G4double ySC0 = -1.25*cm;
  G4double zSC0 = -Offset+TaperLengthCan+0.45*cm/tan(22.5*deg)
    +2.*zHalfLengthSCFront+zHalfLengthSCRear+0.5*mm;

  G4ThreeVector posSC0(xSC0,ySC0,zSC0);

  G4ThreeVector transSC1(-ySC0,xSC0,zSC0);
  G4Transform3D posSC1(rm90,transSC1);

  G4ThreeVector transSC2(-xSC0,-ySC0,zSC0);
  G4Transform3D posSC2(rm180,transSC2);

  G4ThreeVector transSC3(ySC0,-xSC0,zSC0);
  G4Transform3D posSC3(rm270,transSC3);

  G4VPhysicalVolume* physiAlSCatcher;
  physiAlSCatcher = new G4PVPlacement(0,posSC0,"AlSCatcherA",logicAlSCatcher,physiSupClover,false,0,true);
  physiAlSCatcher = new G4PVPlacement(posSC1,"AlSCatcherB",logicAlSCatcher,physiSupClover,false,1,true);
  physiAlSCatcher = new G4PVPlacement(posSC2,"AlSCatcherC",logicAlSCatcher,physiSupClover,false,2,true);
  physiAlSCatcher = new G4PVPlacement(posSC3,"AlSCatcherD",logicAlSCatcher,physiSupClover,false,3,true);

  //
  // Now the active BGO elements of the side shields: they are made from 
  // 2 distinct pieces: a tapered (right, labelled 'A') and a straight (left, 'B')  
  // each of them being also built from a front part and a rear part 
  // with the same logic as for the can:
  //   
  // (A-i) the tapered front part ...
  //

  G4double zHalfLengthSCBGOFrontA = 2.1728*cm;

  G4double ThetaSCBGOFrontA = 16.343*deg;
  G4double PhiSCBGOFrontA = -45.*deg;

  G4double dy1SCBGOFrontA = 3.27625*cm;
  G4double dx1SCBGOFrontA = 0.15*cm;
  G4double dx2SCBGOFrontA = 0.15*cm;
  G4double dy2SCBGOFrontA = 4.17625*cm;
  G4double dx3SCBGOFrontA = 1.05*cm;
  G4double dx4SCBGOFrontA = 1.05*cm;

  G4Trap* solidSCBGOFrontA = new G4Trap("SCBGOFrontA",
					zHalfLengthSCBGOFrontA,ThetaSCBGOFrontA,PhiSCBGOFrontA,
					dy1SCBGOFrontA,dx1SCBGOFrontA,dx2SCBGOFrontA,0.,
					dy2SCBGOFrontA, dx3SCBGOFrontA,dx4SCBGOFrontA,0.);

  //
  // ... (A-ii) the box rear part ...
  //

  G4double xHalfLengthSCBGORearA = 1.05*cm;
  G4double yHalfLengthSCBGORearA = 4.17625*cm;
  G4double zHalfLengthSCBGORearA = 3.5012*cm;

  G4Box* solidSCBGORearA = new G4Box("SCBGORearA",xHalfLengthSCBGORearA,yHalfLengthSCBGORearA,
				     zHalfLengthSCBGORearA);

  //
  // ... (A-iii) finally the union to build part 'A' of the BGO side shield elements.
  //

  G4double zBGOSCFrontRearA = -zHalfLengthSCBGOFrontA-zHalfLengthSCBGORearA;
  G4ThreeVector posBGOSCFrontRearA(-0.45*cm,0.45*cm,zBGOSCFrontRearA);

  G4UnionSolid* solidBGOSCatcherA = 
    new G4UnionSolid("SCBGOA",solidSCBGORearA,solidSCBGOFrontA,0,posBGOSCFrontRearA);

  logicBGOSCatcherA = new G4LogicalVolume(solidBGOSCatcherA,BGO,"SCBGOA");
  //gj
  logicBGOSCatcherA -> SetSensitiveDetector(ancSD);

  //   
  // (B-i) the "single tapered" front part ...
  //

  G4double zHalfLengthSCBGOFrontB = 2.1728*cm;

  G4double ThetaSCBGOFrontB = 11.701*deg;
  G4double PhiSCBGOFrontB = 0.*deg;

  G4double dy1SCBGOFrontB = 3.04*cm;
  G4double dx1SCBGOFrontB = 0.15*cm;
  G4double dx2SCBGOFrontB = 0.15*cm;
  G4double dy2SCBGOFrontB = 3.04*cm;
  G4double dx3SCBGOFrontB = 1.05*cm;
  G4double dx4SCBGOFrontB = 1.05*cm;

  G4Trap* solidSCBGOFrontB = new G4Trap("SCBGOFrontA",
					zHalfLengthSCBGOFrontB,ThetaSCBGOFrontB,PhiSCBGOFrontB,
					dy1SCBGOFrontB,dx1SCBGOFrontB,dx2SCBGOFrontB,0.,
					dy2SCBGOFrontB,dx3SCBGOFrontB,dx4SCBGOFrontB,0.);

  //   
  // ... (B-ii) the box rear part ...
  //

  G4double xHalfLengthSCBGORearB = 1.05*cm;
  G4double yHalfLengthSCBGORearB = 3.04*cm;
  G4double zHalfLengthSCBGORearB = 3.5012*cm;

  G4Box* solidSCBGORearB = new G4Box("SCBGORearB",xHalfLengthSCBGORearB,yHalfLengthSCBGORearB,
				     zHalfLengthSCBGORearB);

  //   
  // ... (B-iii) finally the union
  //

  G4double zBGOSCFrontRearB = -zHalfLengthSCBGOFrontB-zHalfLengthSCBGORearB;
  G4ThreeVector posBGOSCFrontRearB(-0.45*cm,0.*cm,zBGOSCFrontRearB);

  G4UnionSolid* solidBGOSCatcherB = 
    new G4UnionSolid("SCBGOB",solidSCBGORearB,solidSCBGOFrontB,0,posBGOSCFrontRearB);

  logicBGOSCatcherB = new G4LogicalVolume(solidBGOSCatcherB,BGO,"SCBGOB");
  //gj
  logicBGOSCatcherB -> SetSensitiveDetector(ancSD);

  //
  // positioning the unions 'A' and 'B' in the can
  //

  G4double xSCA = -0.15*cm;
  G4double ySCA = -2.88625*cm;
  G4double zSCA = -0.3621*cm;

  G4double xSCB = -0.15*cm;
  G4double ySCB = 4.3425*cm;
  G4double zSCB = -0.3621*cm;

  G4ThreeVector posSCA(xSCA,ySCA,zSCA);
  G4ThreeVector posSCB(xSCB,ySCB,zSCB);

  // G4VPhysicalVolume* physiSCatcherA;
  // G4VPhysicalVolume* physiSCatcherB;

  // physiSCatcherA =
    new G4PVPlacement(0,posSCA,"SCatcherA",logicBGOSCatcherA,physiAlSCatcher,false,110,true);
  // physiSCatcherB =
    new G4PVPlacement(0,posSCB,"SCatcherB",logicBGOSCatcherB,physiAlSCatcher,false,120,true);

  // 
  // Side catcher done!
  //

}

/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 		Construction of the CsI backcatcher				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////


void AgataAncillaryExogam::ConstructBackCatcher( )
{

  // Now building the CsI backcatcher ...
  //
  // The Al enveloppe...
  //

  G4double xHalfLengthBackAl = 3.10*cm;
  G4double yHalfLengthBackAl = 3.10*cm;
  G4double zHalfLengthBackAl = 1.99*cm;

  G4Box* solidBackAl = new G4Box("BackAl",xHalfLengthBackAl,yHalfLengthBackAl,
				 zHalfLengthBackAl);

  G4double minRadiusBackAl = 0.*cm;
  G4double maxRadiusBackAl = 2.25*cm;
  G4double HalfLengthBackAl = 1.995*cm;
  G4double startPhiBackAl = 0.*deg;
  G4double deltaPhiBackAl = 90.*deg;

  G4Tubs* solidBackTub = new G4Tubs("BackTub",minRadiusBackAl,maxRadiusBackAl,
				    HalfLengthBackAl,startPhiBackAl,deltaPhiBackAl);

  G4ThreeVector transTub(-3.1*cm,-3.1*cm,0.*cm);

  G4SubtractionSolid* solidBack = 
    new G4SubtractionSolid("Back",solidBackAl,solidBackTub,0,transTub);

  logicBack = new G4LogicalVolume(solidBack,Aluminum,"Back");

  // the position of the backcatcher in the enveloppe

  G4double xBack = 3.11*cm;
  G4double yBack = 3.11*cm;
  G4double zBack = -Offset+2.*HalfLengthCan+2.04*cm;

  G4ThreeVector transBackA(xBack,yBack,zBack);

  G4ThreeVector transBackB(-xBack,yBack,zBack);
  G4Transform3D posBackB(rm90,transBackB);

  G4ThreeVector transBackC(-xBack,-yBack,zBack);
  G4Transform3D posBackC(rm180,transBackC);

  G4ThreeVector transBackD(xBack,-yBack,zBack);
  G4Transform3D posBackD(rm270,transBackD);

  G4VPhysicalVolume* physiBack;
  physiBack = new G4PVPlacement(0,transBackA,"BackA",logicBack,physiSupClover,false,0,true);
  physiBack = new G4PVPlacement(posBackB,"BackB",logicBack,physiSupClover,false,1,true);
  physiBack = new G4PVPlacement(posBackC,"BackC",logicBack,physiSupClover,false,2,true);
  physiBack = new G4PVPlacement(posBackD,"BackD",logicBack,physiSupClover,false,3,true);

  //
  // The CsI crystals...
  //

  G4double xHalfLengthBackCsI = 3.05*cm;
  G4double yHalfLengthBackCsI = 3.05*cm;
  G4double zHalfLengthBackCsI = 1.94*cm;

  G4Box* solidCsI = new G4Box("sCsI",xHalfLengthBackCsI,yHalfLengthBackCsI,
			      zHalfLengthBackCsI);

  G4double minRadiusBackCsI = 0.*cm;
  G4double maxRadiusBackCsI = 2.3*cm;
  G4double HalfLengthBackCsI = 1.945*cm;
  G4double startPhiBackCsI = 0.*deg;
  G4double deltaPhiBackCsI = 90.*deg;

  G4Tubs* solidCsITub = new G4Tubs("CsITub",minRadiusBackCsI,maxRadiusBackCsI,
				   HalfLengthBackCsI,startPhiBackCsI,deltaPhiBackCsI);

  G4ThreeVector transCsITub(-3.05*cm,-3.05*cm,0.*cm);
  G4SubtractionSolid* solidBackCsI = 
    new G4SubtractionSolid("BackCsI",solidCsI,solidCsITub,0,transCsITub);

  logicBackCsI = new G4LogicalVolume(solidBackCsI,CsI,"BackCsI");
  logicBackCsI -> SetSensitiveDetector(ancSD);
  
  
  // the position of the CsI in the Al enveloppe

  G4ThreeVector posBackCsI(0.*cm,0.*cm,0.*cm);

  //  G4VPhysicalVolume* physiBackCsI;
  // physiBackCsI =
    new G4PVPlacement(0,posBackCsI,"BackCsI",logicBackCsI,physiBack,false,
		      150,true);

  // 
  // Backcatcher done!
  //

}

/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 		Construction of the Side Shield 				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

void AgataAncillaryExogam::ConstructSideShield( )
{

  //
  // add the Side Shield elements to build the pulled back configuration B
  //
  //
  // The Al enveloppe...
  //

  G4double dx1SShield = 0.57*cm;
  G4double dx2SShield = 0.57*cm;
  G4double dy1SShield = 3.7*cm;
  G4double dy2SShield = 9.93*cm;
  G4double zHalfLengthSShield = 8.1435*cm;

  G4Trd* solidSShieldTmp = new G4Trd("SShieldTmp",dx1SShield, dx2SShield,
				     dy1SShield,dy2SShield,zHalfLengthSShield);

  // building a box to subract from the above Trd the back side corner 
  // to avoid a clash between Side Shield elements.

  G4double xHalfLengthBoxTmp= 0.575*cm;
  G4double yHalfLengthBoxTmp = 0.4*cm;
  G4double zHalfLengthBoxTmp = 0.9*cm;

  G4Box* solidBoxTmp = new G4Box("BoxTmp",xHalfLengthBoxTmp,yHalfLengthBoxTmp,
				 zHalfLengthBoxTmp);

  G4ThreeVector transBox(0.*cm,-9.68*cm,7.37*cm);

  G4SubtractionSolid* solidSShield = 
    new G4SubtractionSolid("SShield",solidSShieldTmp,solidBoxTmp,0,transBox);

  logicSShield = new G4LogicalVolume(solidSShield,Aluminum,"SShield");

  // the position of the Al enveloppe in the enveloppe containing the whole Clover

  G4double xSShield = 6.897*cm;
  G4double ySShield = 0.62*cm;
  G4double zSShield = -29.78*cm;

  G4RotationMatrix rmSShield0;
  rmSShield0.rotateY(22.5*deg);

  G4ThreeVector transSShield0(xSShield,ySShield,zSShield);
  G4Transform3D posSShield0(rmSShield0,transSShield0);

  G4RotationMatrix rmSShield1;
  rmSShield1.rotateY(22.5*deg).rotateZ(90.*deg);

  G4ThreeVector transSShield1(-ySShield,xSShield,zSShield);
  G4Transform3D posSShield1(rmSShield1,transSShield1);

  G4RotationMatrix rmSShield2;
  rmSShield2.rotateY(22.5*deg).rotateZ(180.*deg);

  G4ThreeVector transSShield2(-xSShield,-ySShield,zSShield);
  G4Transform3D posSShield2(rmSShield2,transSShield2);

  G4RotationMatrix rmSShield3;
  rmSShield3.rotateY(22.5*deg).rotateZ(270.*deg);

  G4ThreeVector transSShield3(ySShield,-xSShield,zSShield);
  G4Transform3D posSShield3(rmSShield3,transSShield3);

  G4PVPlacement *physiSShield;
  physiSShield = new G4PVPlacement(posSShield0,"SShield",logicSShield,physiSupClover,false,0,true);
  physiSShield = new G4PVPlacement(posSShield1,"SShield",logicSShield,physiSupClover,false,1,true);
  physiSShield = new G4PVPlacement(posSShield2,"SShield",logicSShield,physiSupClover,false,2,true);
  physiSShield = new G4PVPlacement(posSShield3,"SShield",logicSShield,physiSupClover,false,3,true);

  //
  // The BGO is built from 2 distinct pieces to match the optical isolation... 
  //

  G4double zHalfLengthSSBGOA = 8.085*cm;

  G4double ThetaSSBGOA = 10.83*deg;
  G4double PhiSSBGOA = -90.*deg;

  G4double dy1SSBGOA = 1.546*cm;
  G4double dx1SSBGOA = 0.5*cm;
  G4double dx2SSBGOA = 0.5*cm;
  G4double dy2SSBGOA = 4.64*cm;
  G4double dx3SSBGOA = 0.5*cm;
  G4double dx4SSBGOA = 0.5*cm;

  G4Trap* solidSShieldATmp = new G4Trap("SShieldATmp",zHalfLengthSSBGOA,ThetaSSBGOA,PhiSSBGOA,
					dy1SSBGOA,dx1SSBGOA,dx2SSBGOA,0.,dy2SSBGOA, dx3SSBGOA,dx4SSBGOA,0.);

  // Using the same box as before to subtract from the above Trap the back side corner 
  // to avoid a clash between Side Shield elements. This applies only for elements 'A'

  G4ThreeVector transBoxSSA(0.*cm,-5.95*cm,7.315*cm);

  G4SubtractionSolid* solidSShieldA = 
    new G4SubtractionSolid("SShieldA",solidSShieldATmp,solidBoxTmp,0,transBoxSSA);

  G4double zHalfLengthSSBGOB = 8.085*cm;

  G4double ThetaSSBGOB = 10.83*deg;
  G4double PhiSSBGOB = 90.*deg;

  G4double dy1SSBGOB = 2.080*cm;
  G4double dx1SSBGOB = 0.5*cm;
  G4double dx2SSBGOB = 0.5*cm;
  G4double dy2SSBGOB = 5.174*cm;
  G4double dx3SSBGOB = 0.5*cm;
  G4double dx4SSBGOB = 0.5*cm;

  G4Trap* solidSShieldB = new G4Trap("SShieldB",zHalfLengthSSBGOB,ThetaSSBGOB,PhiSSBGOB,
				     dy1SSBGOB,dx1SSBGOB,dx2SSBGOB,0.,dy2SSBGOB, dx3SSBGOB,dx4SSBGOB,0.);

  logicBGOSShieldA = new G4LogicalVolume(solidSShieldA,BGO,"SShieldA");
  logicBGOSShieldB = new G4LogicalVolume(solidSShieldB,BGO,"SShieldB");
  //gj
  logicBGOSShieldA -> SetSensitiveDetector(ancSD);
  logicBGOSShieldB -> SetSensitiveDetector(ancSD);

  //
  // positioning the two parts 'A' and 'B' in the Al can
  //

  G4double xSSA = 0.*cm;
  G4double ySSA = -3.681*cm;
  G4double zSSA = 0.*cm;

  G4double xSSB = 0.*cm;
  G4double ySSB = 3.133*cm;
  G4double zSSB = 0.*cm;

  G4ThreeVector posSSA(xSSA,ySSA,zSSA);
  G4ThreeVector posSSB(xSSB,ySSB,zSSB);

  // G4VPhysicalVolume* physiSShieldA;
  // G4VPhysicalVolume* physiSShieldB;

  // physiSShieldA =
    new G4PVPlacement(0,posSSA,"SShieldA",logicBGOSShieldA,physiSShield,false,130,true);
  // physiSShieldB =
    new G4PVPlacement(0,posSSB,"SShieldB",logicBGOSShieldB,physiSShield,false,140,true);

  // 
  // Side Shield done!
  //

}

/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 		 Construction of the Collimator 				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

void AgataAncillaryExogam::ConstructCollimator( )
{
  //
  // Now build the Heavymet collimator in front of the Side Shield...
  //

  G4double PhiStartColl = 45.*deg;
  G4double PhiTotColl = 360.*deg;
  G4double HalfLengthColl = distCollimatorToBGOSShield/2.;

  G4double zPlaneColl[2];
  G4double rInnerColl[2];
  G4double rOuterColl[2];

  zPlaneColl[0] = -HalfLengthColl;
  zPlaneColl[1] =  HalfLengthColl;

  rOuterColl[0] = 3.17*cm;
  rOuterColl[1] = 4.37*cm;

  rInnerColl[0] = 2.224*cm;
  rInnerColl[1] = 3.21*cm;

  G4Polyhedra* solidColl = new G4Polyhedra("Collimator",PhiStartColl,PhiTotColl,4,2,
					   zPlaneColl,rInnerColl,rOuterColl);

  logicColl = new G4LogicalVolume(solidColl,Heavymet,"Collimator");
  //  logicColl = new G4LogicalVolume(solidColl,Vacuum,"Collimator");

  G4ThreeVector transColl0(0.*cm,0.*cm,-dzEnv+HalfLengthColl);

  new G4PVPlacement(0,transColl0,"Collimator",logicColl,physiSupClover,false,0,true);

  // 
  // Collimator done!
  //

}


////kuniec konstruowania

void AgataAncillaryExogam::PlaceTarget()
{
  //  tarcza wlasciwa
  G4double Target_Rmin        =    .5*1.*cm;//0.  * mm;
  G4double Target_Rmax        =    .5*7.*cm;//0.5 * 10 * mm;
  G4double Target_Thickness   =    2.1468 * um;
  G4double Target_PosX        =    0.  * mm;
  G4double Target_PosY        =    0.  * mm;
  G4double Target_PosZ        =    1.*m;//0.  * mm;
  G4double Target_StAngle     =    0.  * deg;
  G4double Target_SegAngle    =  360.  * deg;

  G4double Layer_Thickness    =   12.296 * um;
  /*  G4double Layer_PosZ         =   Target_PosZ+.5*(Target_Thickness+Layer_Thickness)+0.01*um;*/

  G4RotationMatrix rm0;
  rm0.set(0, 0, 0);
  G4Tubs* solidTarget = new G4Tubs("Target",Target_Rmin,Target_Rmax,
				   .5*Target_Thickness,Target_StAngle,Target_SegAngle);

  G4LogicalVolume* logicTarget = new G4LogicalVolume(solidTarget,matTarget,
						     "Target",0,0,0);

  // G4VPhysicalVolume* physTarget;
  // physTarget =
    new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(Target_PosX,
								 Target_PosY,
								 Target_PosZ)),
				 "Target", logicTarget,
				 theDetector->HallPhys(), true, 800);
  
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  czerwony->SetVisibility(true);
  logicTarget -> SetVisAttributes(czerwony);
  //podkladka
  G4Tubs* solidLayer = new G4Tubs("Layer",Target_Rmin,Target_Rmax,
				  .5*Layer_Thickness,Target_StAngle,Target_SegAngle);

  G4LogicalVolume* logicLayer = new G4LogicalVolume(solidLayer,matLayer,
						    "Layer",0,0,0);
  
  //  G4VPhysicalVolume* physLayer;
  //  physLayer = new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(Target_PosX,
  //								Target_PosY,
  //								Layer_PosZ)),
  //				"Layer", logicLayer,
  //				 theDetector->HallPhys(), true, 900);
  G4VisAttributes* bialy = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  bialy->SetVisibility(true);
  logicLayer -> SetVisAttributes(bialy);

}

void AgataAncillaryExogam::PlaceCatcherIonGuide()
{
  if(true||CloverInArray[18]||CloverInArray[19]||CloverInArray[20]||CloverInArray[21]){
    G4double Rmax        =    30 * mm;
    G4double Thickness   =     2 * mm;
    G4double Rmin        =  Rmax-Thickness;
    G4double PosX        =    0.  * mm;
    G4double PosY        =    0.  * mm;
    G4double StAngle     =    0.  * deg;
    G4double SegAngle    =  360.  * deg;
    G4double Length      =    1.  *  m;
    G4double PosZ        =    0.  * mm;
    G4int clov =0, ii;
    G4RotationMatrix rm0;rm0.set(0, 0, 0);
    for(ii=18;ii<22;ii++)
      if(CloverInArray[ii]){PosZ+=PositionZ[ii];clov++;}
    PosZ/=clov;
    //for a while:
    PosZ=0;
    G4Tubs* sCatcherIonGuide = new G4Tubs("CatcherIonGuide",Rmin,Rmax,.5*Length,StAngle,SegAngle);
    G4LogicalVolume* lCatcherIonGuide = new G4LogicalVolume(sCatcherIonGuide,Aluminum,"CatcherIonGuide",0,0,0);
    // G4VPhysicalVolume* physCathcerIonGuide;
    // physCathcerIonGuide =
      new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(PosX,PosY,PosZ)),
					    "CatcherIonGuide",lCatcherIonGuide,theDetector->HallPhys(),true,0);
    G4VisAttributes* CanVisAtt= new G4VisAttributes(G4Colour(0.5,0.5,0.5));  // Grey
    CanVisAtt->SetVisibility(true);
    lCatcherIonGuide->SetVisAttributes(CanVisAtt);
  }
}

void AgataAncillaryExogam::PlaceElectronDetectors(){
  G4double ElDetSideLength = 1.  * cm;
  G4double ElDetThickness  =  .1 * mm;
  G4Box* solidElDet = new G4Box("ElectronDetector",.5*ElDetSideLength,.5*ElDetSideLength,.5*ElDetThickness);
  G4LogicalVolume* logicElDet = new G4LogicalVolume(solidElDet,matElDet,"ElectronDetector");
  G4VisAttributes* white= new G4VisAttributes(G4Colour());white->SetVisibility(true);
  logicElDet->SetVisAttributes(white);

  const G4int NumberOfDets = 20;
  G4RotationMatrix rm[NumberOfDets];
  G4double x[NumberOfDets],y[NumberOfDets],z;
  G4double z_shift=3.*cm;
  z=100.*cm-z_shift;
  G4double radius=4.*cm;
  G4double AngleStep = (360./NumberOfDets)*deg;
  /*  G4double rotation_angle = atan(radius/z_shift);*/
  
  for(G4int i=0;i<NumberOfDets;i++){
    x[i]=radius*cos(i*AngleStep);
    y[i]=radius*sin(i*AngleStep);
    // rm[i].set(rotation_angle*sin(i*AngleStep),rotation_angle*cos(i*AngleStep),0);
    rm[i].set(0,0,0);

    /*    G4VPhysicalVolume* physiSiDet =*/
    new G4PVPlacement(G4Transform3D(rm[i],G4ThreeVector(x[i],y[i],z)),
		      "ElectronDetector",logicElDet,theDetector->HallPhys(),true,900+i);
  }	    

}


void AgataAncillaryExogam::Placement()
{
  PlaceEverything();
  //if()place
  // uncomment line below to place secondary Target/IonGuide/ElectronDetector
  //PlaceTarget();
  //  PlaceCatcherIonGuide();
  //PlaceElectronDetectors();
  return;
}

void AgataAncillaryExogam::ShowStatus()
{
  //G4cout
  //if()G4cout
}

//rozne klasy pt setSth

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"

AgataAncillaryExogamMessenger::AgataAncillaryExogamMessenger(AgataAncillaryExogam* pTarget, G4String name)
  :myTarget(pTarget)
{ 
  //  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/ancillary/Exogam/";
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of EXOGAM construction.");
  
  commandName = directoryName+"SetEXOGAMinput";
  cmdSetEXOGAMinput = new G4UIcmdWithAString(commandName.c_str(),this);
  cmdSetEXOGAMinput->SetGuidance("Set name of EXOGAM input");
  cmdSetEXOGAMinput->AvailableForStates(G4State_PreInit,G4State_Idle);
  //poszczegolne komendy
}

AgataAncillaryExogamMessenger::~AgataAncillaryExogamMessenger()
{
  //kasacja poszczegolnych komend
}

void AgataAncillaryExogamMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command==cmdSetEXOGAMinput){
    myTarget->SetEXOGAMInput(newValue);
  }
  //if(command=cos)procedura
}

#endif
