#ifdef ANCIL

//#define SMALL

#include "AgataAncillarySigma.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"

#include "G4MaterialTable.hh" //?
#include "G4Element.hh" //?
#include "G4ElementTable.hh" //?

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Trap.hh"
#include "G4Trd.hh"
#include "G4Para.hh"
#include "G4Polyhedra.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"

#include "G4PVReplica.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4IntersectionSolid.hh"

#include "G4SDManager.hh"

#include <math.h> //?

#include "G4UserLimits.hh" //?

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ios.hh"

//#include "Sigma2TrackerSD.hh"


using namespace std;

AgataAncillarySigma::AgataAncillarySigma(G4String path, G4String name)
{

  //---------------------------------------------------------------
  //	Generic rotation matrices
  //---------------------------------------------------------------
  rm90.rotateZ(90.*deg);
  rm90m.rotateZ(-90.*deg);
  rm180.rotateZ(180.*deg);
  rm270.rotateZ(270.*deg);


  G4String iniPath     = path;
  dirName = name;

  cout <<" ##### dirName= " << dirName << endl;


  //booleans to set geometry
  SIGMAinput = "SIGMA_Input";

  //doubles which can be changed

  //materials

  //name:
  nameVacuum    = "Vacuum";
  //material:
  Vacuum        = NULL;

  nameAluminum  = "Aluminum";
  Aluminum      = NULL;
  nameCopper    = "Copper";
  Copper        = NULL;
  nameGermanium = "Germanium";
  Germanium     = NULL;
  nameBGO       = "BGO";
  BGO           = NULL;
  nameCsI       = "CsI";
  CsI           = NULL;
  nameHeavymet  = "Heavymet";
  Heavymet      = NULL;

  nameMatTarget = "Silicon28";
  matTarget     = NULL;
  nameMatLayer  = "Zirconium90";
  matLayer      = NULL;

  nameMatElDet   = "Silicon28";
  matElDet       = NULL;
  

  ancSD = NULL;

  ancName   = G4String("SIGMA");
  ancOffset = 30000;

  numAncSd = 0;

  myMessenger = new AgataAncillarySigmaMessenger(this,name);
}


AgataAncillarySigma::~AgataAncillarySigma()
{
  delete myMessenger;
}

G4int AgataAncillarySigma::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(nameVacuum);
  if (ptMaterial){
    Vacuum = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Vacuum->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << nameVacuum << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   

  ptMaterial = G4Material::GetMaterial(nameAluminum);
  if (ptMaterial){
    Aluminum = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Aluminum->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameAluminum << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameCopper);
  if (ptMaterial){
    Copper = ptMaterial;
    G4cout << "\n----> The ancillary detector material is " << Copper->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << nameCopper << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameGermanium);
  if (ptMaterial){
    Germanium = ptMaterial;
    G4cout<<"\n----> The ancillary detector material is "<<Germanium->GetName()<<G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameGermanium << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameBGO);
  if (ptMaterial){
    BGO = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<BGO->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameBGO << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameCsI);
  if (ptMaterial){
    CsI = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<CsI->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameCsI << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameHeavymet);
  if (ptMaterial){
    Heavymet = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<Heavymet->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameHeavymet << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  //target and layer:
  ptMaterial = G4Material::GetMaterial(nameMatTarget);
  if (ptMaterial){
    matTarget = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matTarget->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatTarget << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameMatLayer);
  if (ptMaterial){
    matLayer = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matLayer->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatLayer << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  //material for electron detectors
  ptMaterial = G4Material::GetMaterial(nameMatElDet);
  if (ptMaterial){
    matElDet = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "<<matElDet->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameMatElDet << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  
  //find all used materials
  

  return 0;
}

void AgataAncillarySigma::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillarySigma::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}


void AgataAncillarySigma::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillarySigma::InitSensitiveDetector()
{
  G4int offset = ancOffset;
  G4cout << "offset="<< offset<< G4endl;

#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4cout << "offset=" << offset<< G4endl;

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    //    ancSD = new AgataSensitiveDetector( "/anc/Exo", "AncCollection", offset, depth, menu );
    //    ancSD = new AgataSensitiveDetector( "/anc/all", "AncCollection", offset, depth, menu );
    //gj 2010/02/05
    //Enrico cos mowil o tym, ze kazda kolekcja winna byc z osobna okreslona
    //ancSD = new AgataSensitiveDetector(dirName, "/anc/Sig", "SigCollection", offset, depth, menu );
     ancSD = new AgataSensitiveDetector(dirName, "/anc/Sig", "SigCollection", offset, depth, menu );

     cout << "%%%% dirName=" << dirName << endl;

    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

G4int AgataAncillarySigma::GetSegmentNumber(G4int offset, G4int detCode, G4ThreeVector position)
{
  if(detCode<72){
    //G4double newz=position.z()/mm+45.*mm;
    G4double xyLim,Ztrans ;
    G4double Phi;
    G4int iSect, iSlice;
    xyLim=0;
    Phi= atan(abs(position.y())/ abs(position.x()));

	//cout << "x=" << position.x()/mm << " y=" << position.y()/mm << " z=" << position.z()/mm << endl;

	Ztrans= position.z()/mm + 272.2*mm;  // Translation along Z of 272.2mm to get: -40mm < Ztrans < +40 mm 
									   // 272.2= 350 	(=half length of Det. Envelope)
									   //	    -25.7	(= shift of GeA in Vac)
									   //	    -11.8    (= Dist front face can to front face Ge)
									   //	    -80/2    (= half the length of the Ge Crystal)
	                                                                   //	    -0.3     (?) 
									   
	//cout << "Ztrans= " << Ztrans << endl;

	//cout << "Ztrans= " << Ztrans << endl;

	if( (Ztrans <= -30) && (Ztrans>=-40.) ) iSlice=0;
	else if( (Ztrans <= -20) && (Ztrans>-30) ) iSlice=1;
	else if( (Ztrans <= -10) && (Ztrans>-20) ) iSlice=2;
	else if( (Ztrans <= 0)  && (Ztrans>-10) ) iSlice=3;
	else if( (Ztrans <= 10)  && (Ztrans>0) ) iSlice=4;
	else if( (Ztrans <= 20)  && (Ztrans>10) ) iSlice=5;
	else if( (Ztrans <= 30)  && (Ztrans>20) ) iSlice=6;
	else if( (Ztrans <= 40.01)  && (Ztrans>30) ) iSlice=7;
	else iSlice=9;
	//cout << "iSlice= " << iSlice << endl;
        if (Phi<=(M_PI/4) && Phi>(0) 	      && (position.x()/mm >= xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSect=0;	//top right lower half
	else if (Phi<=(M_PI/2) && Phi>(M_PI/4) && (position.x()/mm >= xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSect=1;  //top right upper half
	else if (Phi<=(M_PI/4) && Phi>(0)      && (position.x()/mm >= xyLim/mm) && (position.y()/mm <= xyLim/mm)) iSect=7;	//bottom right upper half
	else if (Phi<=(M_PI/2) && Phi>(M_PI/4) && (position.x()/mm >= xyLim/mm) && (position.y()/mm <= xyLim/mm)) iSect=6;  //bottom right lower half

        else if (Phi<=(M_PI/4) && Phi>(0)      && (position.x()/mm <= xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSect=3;	//top left lower half
	else if (Phi<=(M_PI/2) && Phi>(M_PI/4) && (position.x()/mm <= xyLim/mm) && (position.y()/mm >= xyLim/mm)) iSect=2;  //top left upper half
	else if (Phi<=(M_PI/4) && Phi>(0)      && (position.x()/mm <= xyLim/mm) && (position.y()/mm <= xyLim/mm)) iSect=4;	//bottom left upper half
	else if (Phi<=(M_PI/2) && Phi>(M_PI/4) && (position.x()/mm <= xyLim/mm) && (position.y()/mm <= xyLim/mm)) iSect=5;  //bottom left lower half
	else iSect=9;
    //if ( (newz/mm) <= 30.*mm) xyLim=3.96*mm*(1.-(newz/mm)/(30.*mm));
    //else                      xyLim=0.;
	/*
	if(iSlice==9){
	 cout << "x=" << position.x()/mm << " y=" << position.y()/mm << " z=" << position.z()/mm << endl;
	 cout << "Ztrans= " << Ztrans << endl;
	 cout << "iSect= " << iSect << endl;
	}*/


    G4int iSeg=88; //gj =99
/*    if ( (position.x()/mm >= xyLim/mm) && (position.y()/mm >= xyLim/mm) ) iSeg=0;	// Segment 0
    else if ( (position.x()/mm <  xyLim/mm) && (position.y()/mm >= xyLim/mm) ) iSeg=1;	// Segment 1
    else if ( (position.x()/mm <  xyLim/mm) && (position.y()/mm <  xyLim/mm) ) iSeg=2;	// Segment 2
    else if ( (position.x()/mm >= xyLim/mm) && (position.y()/mm <  xyLim/mm) ) iSeg=3;	// Segment 3
    //else iSeg=9;//checking for errors //gj
*/
 
 	//iSeg=10*iSlice+iSect;

    G4int iRing;
    G4double Circle;
    Circle = sqrt(pow((position.x()),2)+ pow((position.y()),2)); 
	if (Circle<15.) iRing = 1;
	//else if (Circle >15) iRing = 2;
	else if (Circle >15. && Circle < 28.) iRing = 2;
	else if (Circle > 28.) iRing = 2;  //iRing = 3;

	iSeg=100*iRing+10*iSlice+iSect;
  
    return iSeg;
  }
  return 0;
}

void AgataAncillarySigma::PlaceEverything()
{
  G4int depth = 0;
  ancSD->SetDepth(depth);


  //---------------------------------------------------------------
  //	 Read in SIGMA-Geometry file ... 
  //---------------------------------------------------------------

  //gj changing from 18 to 22 - putting catcher inside
  G4int detlim = DETLIM;              //Limit on number of detectors to be used - was set at 22 - DETLIM set in AgataAncillarySigma.hh
  G4double d[detlim], TargetCanDistance[detlim];
  G4double Theta[detlim],Phi[detlim];//,PositionZ[22];
  G4double U[detlim],V[detlim], W[detlim]; // ToDo: increase size if more than 22 Sigma crystals
  G4double PosX[detlim],PosY[detlim], PosZ[detlim]; // ToDo: increase size if more than 22 Sigma crystals
  
  
  // Table of Clover IDs in array

  //  G4int CloverInArray[22]; //gj 08/02/2010 przenosze do hh

  FILE *fi;
  char line[256];
  char CloverType='L';	// "L" for large (=SIGMA) Clovers ; "S" for small ones
  char CloverTypeTable[detlim];

  if( (fi = fopen(SIGMAinput.c_str(), "r")) == NULL) {
    G4cout << "\nError opening " << SIGMAinput << " file " << G4endl;
    exit(-1);
  } else {
    G4cout << "\n" << SIGMAinput << "file open..." << G4endl;
  }
  
  NumberOfClover=0;
  while(fgets(line, 255, fi) != NULL) {
    G4int n, j, CloverIn;
    G4double Distance, Thet, Ph, Px, Py, Pz, Ux, Uy, Uz;
    G4double Pos;
    char str[14], str1[6];
    if(line[0] != '#') {
      if ( (strstr(line,"REALISTIC") != NULL) || (strstr(line,"SIMPLE") != NULL) ) {
        n=sscanf(line,"%s",(char*)&GammaGenerator);
        G4cout << " Construction::GammaGenerator = " << GammaGenerator << G4endl;
      } else if ( strstr(line,"TRACKING") != NULL ) {
        n=sscanf(line,"%s %s",(char*)&str, (char*)&str1);
	Tracking=true;					// make tracking by default...
	if (strstr(str1,"true") == NULL) Tracking=false;
        G4cout << " Tracking = " << Tracking << G4endl;
	if (Tracking) {
	  G4cout << " Tracking will be performed for this run..." << G4endl;
	} else {
	  G4cout << " Tracking will not be performed for this run..." << G4endl;
	} 
      } else if ( strstr(line,"CONFIGURATION") != NULL ) {
        n=sscanf(line,"%s %c",(char*)&str, (char*)&Config);
	//       G4cout << " str = " << str << " ; Config = " << Config << G4endl;

        G4cout << " Configuration = " << Config << G4endl;
        G4cout << " Configuration X <=> spherical configuration " << G4endl;
        G4cout << " Configuration B <=> cubical configuration " << G4endl;


	  } 
      else if ( (n=sscanf(line,"%d %c %d %lf %lf %lf %lf %lf %lf",&j,&CloverType,&CloverIn,&Px,&Py,&Pz,&Ux,&Uy,&Uz)) == 9 ) {  // for cubic config
	CloverTypeTable[j]=CloverType;
        CloverInArray[j]=CloverIn;
	//        TargetCanDistance[j]=Distance*cm;
	//	Theta[j]=Thet*deg;
	//	Phi[j]=Ph*deg;
	PosX[j]=Px*cm;
        PosY[j]=Py*cm;
        PosZ[j]=Pz*cm;
	U[j]=Ux*deg;
        V[j]=Uy*deg;
        W[j]=Uz*deg;

        d[j]=0.;
        if (CloverInArray[j]) {
          IDCloverInArray[NumberOfClover] = j;		// Identify the Clovers in array
          NumberOfClover++;				// Number of Clovers in array
        }//gj stop;
      }else if ( (n=sscanf(line,"%d %c %d %lf %lf %lf",&j,&CloverType,&CloverIn,&Distance,&Thet,&Ph)) == 6 ) {  // for spherical config
	CloverTypeTable[j]=CloverType;
        CloverInArray[j]=CloverIn;
        TargetCanDistance[j]=Distance*cm;

	Theta[j]=Thet*deg;
	Phi[j]=Ph*deg;
	PositionZ[j]=0.;//gj 

	
        d[j]=0.;
        if (CloverInArray[j]) {
          IDCloverInArray[NumberOfClover] = j;		// Identify the Clovers in array
          NumberOfClover++;				// Number of Clovers in array
        }
      } else if ( (n=sscanf(line,"%s ",(char*)&InputFilename)) == 1 ) {
        G4cout << " Input filename is " << InputFilename << G4endl;//gj start:
      } 
      else {break;}
    } 
  }
  fclose(fi);

  if (NumberOfClover==0) {
    G4cerr << " No detector in SIGMA: # of clovers = " << NumberOfClover << " !" << G4endl;
    exit(-1);
  }
  
  if (NumberOfClover>detlim) {// gj changed from 18 - place for catchers.
    G4cerr << " Too many detectors: # of clovers = " << NumberOfClover << " !" << G4endl;
    exit(-1);
  }
  G4cout << "-------------------------------" << G4endl 
	 << "Number of Clover in array: " << NumberOfClover << G4endl << G4endl;

  G4cout << "Clovers in array and distances: " << G4endl 
	 << "-------------------------------" << G4endl;

  for (G4int i=0 ; i<NumberOfClover ; i++ ) {
    G4int ID=IDCloverInArray[i];
    G4cout << " >>> Clover # " << IDCloverInArray[i] 
	   << " is in array; distance target to Ge can = " << TargetCanDistance[ID]/cm << " cm" << G4endl;
  }
  G4cout << "-------------------------------" << G4endl << G4endl; 

  //---------------------------------------------------------------
  //	Main distances
  //
  // For the BGO Side Shield and the collimator, we consider the 
  // distance given by the projection on the z axis of the closest
  // part of them to the target.
  //---------------------------------------------------------------

 
  G4double distBGOSShieldToGeCan;	 	// distance from the front face of the 
						// BGO Side Shield to the front face of the 
						// Ge can (theory: 3.2*cm)
  
  G4double distCollimatorToGeCan=0;		// distance from front face of the collimator
						// to the front face of the Ge can
  
  /*
  if (Config == 'B') {
    distCollimatorToBGOSShield = 2.95*cm;
    distBGOSShieldToGeCan = 3.3*cm; 
    distCollimatorToGeCan = distCollimatorToBGOSShield+distBGOSShieldToGeCan;	
  }
  */
  ///////////////////////////////////////////////////////////////////////////////////////////
  // The enveloppe of the whole Clover (i.e. suppressed Clover) called 'SupClover' including: 
  // - the cryostat 
  // - the dewar 
  // - the side shield
  // - the back catcher
  // - the collimator
  ///////////////////////////////////////////////////////////////////////////////////////////

  //  G4double dzEnv;
  G4double dx1Env;
  G4double dy1Env;

  /*  if (Config == 'B') {
    dzEnv = 40.472*cm;
    dx1Env = 3.17*cm;
    dy1Env = 3.17*cm;
    } else { */
    dzEnv = 35.*cm;
    dx1Env = 2.7*cm;		//6.1*cm;
    dy1Env = 2.7*cm;		//6.1*cm;
    //dzEnv = 34.7*cm;
    //dx1Env = 4.4085*cm;
    //dy1Env = 4.4085*cm;
    //}

  G4double dx2Env = 2.*dzEnv*tan(20.*deg)+dx1Env;
  G4double dy2Env = 2.*dzEnv*tan(20.*deg)+dy1Env;

  G4Trd* solidSupCloverA = new G4Trd("SupCloverA",dx1Env,dx2Env,dy1Env,dy2Env,dzEnv);
  G4Tubs* solidSupCloverB = new G4Tubs("SupCloverB",0.*cm,7.6/*12.505*/*cm,dzEnv, 0.*deg, 360.*deg);
  G4IntersectionSolid* solidSupClover = new G4IntersectionSolid("SupClover", solidSupCloverA, solidSupCloverB );

  //  G4LogicalVolume* 
  logicSupClover = new G4LogicalVolume(solidSupClover,Vacuum,"SupClover");

 
  //
  // ... ant its position(s) in the Experimental Hall...
  //

  G4double xClover[detlim]; 
  G4double yClover[detlim]; 
  G4double zClover[detlim]; 

  G4RotationMatrix rm[detlim];

  //  G4PVPlacement *physiSupClover;
  G4Transform3D positionClover;
  
  /*  if (Config == 'B') {
    Offset=dzEnv-distCollimatorToGeCan;
    } else {*/
    GeoOffset=dzEnv;
    //}


	
	G4int NumberOfSmallClover=0;
    if(Config == 'X')
      {
	for (G4int j=0 ; j<NumberOfClover ; j++) {
	  G4int i=IDCloverInArray[j];
	  
	  rm[i].rotateY(Theta[i]).rotateZ(Phi[i]);
	  
	  
	  //#ifndef SMALL
	    G4cout << " >>> Clover in position " << i << " is a SIGMA Clover..." << G4endl;
	    d[i]=TargetCanDistance[i]+GeoOffset;		// distance for Clover placement
	    
	    xClover[i]= d[i]*sin(Theta[i])*cos(Phi[i]); 
	    yClover[i]= d[i]*sin(Theta[i])*sin(Phi[i]);
	    zClover[i]= PositionZ[i]+d[i]*cos(Theta[i]);
	    
	    
	    
	    cout << "PositionZ=" << PositionZ[i] << " d[i]=" << d[i] << endl;
	    
	    
	    physiSupClover = new G4PVPlacement(G4Transform3D(rm[i],
							     G4ThreeVector(xClover[i],yClover[i],zClover[i])),
					       "Clover",logicSupClover,theDetector->HallPhys(),false,i);
	    //#endif
	}
      }else // Cubic config.
      {
	for (G4int j=0 ; j<NumberOfClover ; j++) {
	  G4int i=IDCloverInArray[j];
	  
	  rm[i].rotateX(W[i]).rotateY(V[i]).rotateZ(U[i]);
	  
	  
	  //#ifndef SMALL
	    G4cout << " >>> Clover in position " << i << " is a SIGMA Clover..." << G4endl;
	    d[i]=TargetCanDistance[i]+GeoOffset;		// distance for Clover placement
	    
	    xClover[i]= PosX[i]; 
	    yClover[i]= PosY[i];
	    zClover[i]= PosZ[i]; //+GeoOffset;
	    
	    
	    
	    cout << "PositionZ=" << PositionZ [i] << " d[i]=" << d[i] << endl;
	    
	    
	    physiSupClover = new G4PVPlacement(G4Transform3D(rm[i],
							     G4ThreeVector(xClover[i],yClover[i],zClover[i])),
					       "Clover",logicSupClover,theDetector->HallPhys(),false,i);

	}
      }


  //
  //  The SIGMA Modules...
  //

  ConstructClover();
  G4cout << "The Clover built... " << G4endl;

   





  //---------------------------------------------------------------
  //
  // Visualization attributes
  //
  //---------------------------------------------------------------

  G4VisAttributes* CanVisAtt= new G4VisAttributes(G4Colour(0.5,0.5,0.5));  // Grey
  G4VisAttributes* DewarVisAtt= new G4VisAttributes(G4Colour(0.8,0.8,0.0));  // ~Yellow
  //gj 2010-03-09:2
  //  CanVisAtt ->SetForceWireframe(true);
  CanVisAtt ->SetForceSolid(true);

  DewarVisAtt ->SetForceSolid(true);
  
  G4VisAttributes* GeAVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
    GeAVisAtt ->SetForceSolid(true);
    //GeAVisAtt->SetVisibility(false);
  G4VisAttributes* GeAVacVisAtt= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red

  G4VisAttributes* GeBVisAtt= new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green
  GeBVisAtt ->SetForceSolid(true);
  //GeBVisAtt->SetVisibility(false);

  G4VisAttributes* GeCVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue
  GeCVisAtt ->SetForceSolid(true);
  //GeCVisAtt->SetVisibility(false);
  G4VisAttributes* GeCVacVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,1.0)); //Blue

  G4VisAttributes* GeDVisAtt= new G4VisAttributes(G4Colour()); //White 
  GeDVisAtt ->SetForceSolid(true);
  
  if (NumberOfSmallClover != NumberOfClover) {
    
    //  The SupClover
    
        logicSupClover->SetVisAttributes(DewarVisAtt);
     //logicSupClover->SetVisAttributes (G4VisAttributes::Invisible);  
    
    
    //  The Clover can and the dewar
    
    logicCan ->SetVisAttributes(CanVisAtt);
    logicEnvColdFinger ->SetVisAttributes(CanVisAtt);
    //    logicIntEnvColdFinger ->SetVisAttributes(CanVisAtt);
    logicDewar ->SetVisAttributes(DewarVisAtt);
    logicVac ->SetVisAttributes(CanVisAtt);
    
    //logicVac ->SetVisAttributes(G4VisAttributes::Invisible);
    //logicVac ->SetVisAttributes(GeCVacVisAtt);
     
    G4VisAttributes* HoleVisAtt= new G4VisAttributes(G4Colour(0.0,0.0,0.0));  // Black
    G4VisAttributes* AGeVisAtt= new G4VisAttributes(G4Colour(1.0,1.0,0.0));  // Yellow
    G4VisAttributes* AbsorbVisAtt= new G4VisAttributes(G4Colour(0.5,0.0,0.5));  // purple
    
    AGeVisAtt ->SetForceSolid(true);
    HoleVisAtt ->SetForceSolid(true);
    //    AbsorbVisAtt ->SetForceSolid(true);
    AbsorbVisAtt ->SetForceWireframe(true);
    
    logicAGe1 ->SetVisAttributes(AGeVisAtt);
    logicHole1 ->SetVisAttributes(HoleVisAtt);
    logicAbsorb ->SetVisAttributes(AbsorbVisAtt);

    
    logicGeA ->SetVisAttributes(GeAVisAtt);
    //    logicGeA ->SetVisAttributes(G4VisAttributes::Invisible);
    GeAVacVisAtt ->SetForceWireframe(true);
 }
    
}


/////////////////////////////////////////////////////////////////////////////////////
//										   //		
// 			Construction of the Clover				   //
//										   //		
/////////////////////////////////////////////////////////////////////////////////////

void AgataAncillarySigma::ConstructClover( )
{
  
  ////////////////
  //
  // The Cryostat
  //
  ////////////////
  //
  // The Aluminum can ( "Can" )...
  //

  G4double PhiStartCan = 0.*deg; //45
  G4double PhiTotCan = 360.*deg;
  HalfLengthCan = 7.*cm;
  //TaperLengthCan = 4.325*cm;

  G4double zPlaneCan[2];
  G4double rInnerCan[2];

  G4double zPlaneVac[2];
  G4double rInnerVac[2];
  G4double rOuterVac[2];

  zPlaneCan[0] = -HalfLengthCan;
  zPlaneCan[1] =  HalfLengthCan;


  rOuterCan[0] = 2.6*cm;		//5.1*cm;
  rOuterCan[1] = 5.1*cm;		//5.1*cm;
  // rOuterCan[2] = 6.2*cm;

  rInnerCan[0] = rInnerCan[1] = 0.*cm;

  G4Polycone* solidCan = new G4Polycone("Can", PhiStartCan, PhiTotCan, 2, zPlaneCan,rInnerCan, rOuterCan  );

  logicCan = new G4LogicalVolume(solidCan,Aluminum,"Can");

  // The position of the Clover can in the SupClover:

  G4ThreeVector posCryst(0.*cm,0.*cm,-GeoOffset+HalfLengthCan+0.05*mm);   // added 50um offset for nicer visualization  

  G4VPhysicalVolume* physiCan = new G4PVPlacement(0,posCryst,"Can",
							logicCan,physiSupClover,false,0,true);

  //
  // The vacuum clover ( "Vac" ) ...
  //

  HalfLengthVac = 6.9*cm;
  //TaperLengthVac = 4.0842*cm;

  zPlaneVac[0] = -HalfLengthVac;
  zPlaneVac[1] = HalfLengthVac;
  //zPlaneVac[2] =  HalfLengthVac;

  rOuterVac[0] = 2.5*cm;		//4.9*cm;
  rOuterVac[1] = 5.0*cm;		//4.9*cm;
  //rOuterVac[2] = 6.0*cm;

  rInnerVac[0] = rInnerVac[1] = 0.*cm;

  //G4Polyhedra* solidVac = new G4Polyhedra("Vac",PhiStartCan,PhiTotCan,4,3,
					 //zPlaneVac,rInnerVac,rOuterVac);

  G4Polycone* solidVac = new G4Polycone("Vac",PhiStartCan,PhiTotCan,2,
					 zPlaneVac,rInnerVac,rOuterVac);

  logicVac = new G4LogicalVolume(solidVac,Vacuum,"Vac");

  //
  // ... and its position in Can
  //

  //G4ThreeVector positionVac = G4ThreeVector(0.*cm,0.*cm,0.5*mm);
  G4ThreeVector positionVac = G4ThreeVector(0.*cm,0.*cm, (HalfLengthCan-HalfLengthVac)/2);  // 

  physiVac = new G4PVPlacement(0,positionVac,"Vac",
			       logicVac,physiCan,false,0,true);

 
  //
  // The envelope of the cold finger from the back side of the can to the Dewar
  //

  G4double zPlaneEnvColdFinger[8];
  G4double rInnerEnvColdFinger[8];
  G4double rOuterEnvColdFinger[8];

  G4double PhiStart = 0.*deg;
  G4double PhiTot = 360.*deg;
  G4double EnvColdFingerHalfLength = 11.70*cm;
  
  zPlaneEnvColdFinger[0] = -EnvColdFingerHalfLength;
  zPlaneEnvColdFinger[1] = -EnvColdFingerHalfLength+11.*cm;
  zPlaneEnvColdFinger[2] = -EnvColdFingerHalfLength+11.*cm;
  zPlaneEnvColdFinger[3] = -EnvColdFingerHalfLength+18.*cm; 
  zPlaneEnvColdFinger[4] = -EnvColdFingerHalfLength+18.*cm; 
  zPlaneEnvColdFinger[5] = -EnvColdFingerHalfLength+20.*cm;  
  zPlaneEnvColdFinger[6] = -EnvColdFingerHalfLength+20.*cm; 
  zPlaneEnvColdFinger[7] =  EnvColdFingerHalfLength;

  rInnerEnvColdFinger[0] = 0.*cm; 
  rInnerEnvColdFinger[1] = 0.*cm;
  rInnerEnvColdFinger[2] = 0.*cm;
  rInnerEnvColdFinger[3] = 0.*cm;
  rInnerEnvColdFinger[4] = 0.*cm;
  rInnerEnvColdFinger[5] = 0.*cm;
  rInnerEnvColdFinger[6] = 0.*cm;
  rInnerEnvColdFinger[7] = 0.*cm;

  rOuterEnvColdFinger[0]=7.5*cm;
  rOuterEnvColdFinger[1]=7.5*cm;;
  rOuterEnvColdFinger[2]=3.75*cm;
  rOuterEnvColdFinger[3]=3.75*cm;
  rOuterEnvColdFinger[4]=6.*cm;
  rOuterEnvColdFinger[5]=6.*cm;
  rOuterEnvColdFinger[6]=3.75*cm;
  rOuterEnvColdFinger[7]=3.75*cm;

  G4Polycone* solidEnvColdFinger = new G4Polycone("EnvColdFinger",PhiStart,PhiTot,8,
						  zPlaneEnvColdFinger,rInnerEnvColdFinger,rOuterEnvColdFinger);

  logicEnvColdFinger = new G4LogicalVolume(solidEnvColdFinger,Aluminum,"EnvColdFinger");

  // The position of the cold finger enveloppe in the SupClover:

  G4ThreeVector posEnvColdFinger = 
    G4ThreeVector(0.*cm,0.*cm,-GeoOffset+2.*HalfLengthCan+EnvColdFingerHalfLength+0.05*mm);

  /*G4VPhysicalVolume* physiEnvColdFinger = */
  G4VPhysicalVolume* physiEnvColdFinger = new G4PVPlacement(0,posEnvColdFinger,"EnvColdFinger",logicEnvColdFinger,physiSupClover,false,0,true);            //TUUUUUUUUUUBE



  // Its internal vacuum...

  G4double minRadiusIntEnvColdFinger = 0.*cm;
  G4double maxRadiusIntEnvColdFinger = 2.025*cm;
  G4double HalfLengthIntEnvColdFinger = 11.7*cm; //if it needs to extend throughout the cold finger envelope, just turn this to 11.9 and reduce the G4ThreeVector for physiIntEnvColdFinger to 0.0 for all values
  G4double startPhiIntEnvColdFinger = 0.*deg;
  G4double deltaPhiIntEnvColdFinger = 360.*deg;

  G4Tubs* solidIntEnvColdFinger = new G4Tubs("IntEnvColdFinger",minRadiusIntEnvColdFinger,maxRadiusIntEnvColdFinger,
					     HalfLengthIntEnvColdFinger,startPhiIntEnvColdFinger,deltaPhiIntEnvColdFinger);

  G4LogicalVolume* logicIntEnvColdFinger = 
    new G4LogicalVolume(solidIntEnvColdFinger,Vacuum,"IntEnvColdFinger");

  // and its position in the cold finger enveloppe.

  G4VPhysicalVolume* physiIntEnvColdFinger = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicIntEnvColdFinger,"IntEnvColdFinger",logicEnvColdFinger,false,0,true);
  //G4VPhysicalVolume* physiIntEnvColdFinger = new G4PVPlacement(0,G4ThreeVector(0.,0.,0.1),logicIntEnvColdFinger,"IntEnvColdFinger",logicCFVac,false,0,true);
  //	new G4PVPlacement(0,0,"IntEnvColdFinger",logicIntEnvColdFinger,physiEnvColdFinger,false,0,true);

  //
  // The cold finger and the associated plate
  //

  G4double xHalfLengthCFPlate = 2.5*cm;
  G4double yHalfLengthCFPlate = 2.5*cm;
  G4double zHalfLengthCFPlate = 0.1*cm;

  G4Box* solidCFPlate = new G4Box("CFPlate",xHalfLengthCFPlate,yHalfLengthCFPlate,
				  zHalfLengthCFPlate);

  G4LogicalVolume* logicCFPlate = new G4LogicalVolume(solidCFPlate,Copper,"CFPlate");

  G4ThreeVector posCFPlate(0.*cm,0.*cm,1.53*cm); // 0.55(d(IntCan-Ge)
  // +9.(Ge length)+0.1(half length plate)

  //  G4VPhysicalVolume* physiCFPlate = 
  new G4PVPlacement(0,posCFPlate,"CFPlate",logicCFPlate,physiVac,false,0,true);
  //
  // The cold finger (internal part)
  //

  G4double minRadiusIntCF = 0.*cm;
  G4double maxRadiusIntCF = 1.5*cm;
//  G4double HalfLengthIntCF = 3.06*cm;
  G4double HalfLengthIntCF = 3.03*cm;
  G4double startPhiIntCF = 0.*deg;
  G4double deltaPhiIntCF = 360.*deg;

  G4Tubs* solidIntCF = new G4Tubs("IntCF",minRadiusIntCF,maxRadiusIntCF,
				  HalfLengthIntCF,startPhiIntCF,deltaPhiIntCF);

  G4LogicalVolume* logicIntCF = 
    new G4LogicalVolume(solidIntCF,Copper,"IntCF");

  // its position vs CloverCan...

  G4ThreeVector posIntCF(0.*cm,0.*cm,4.66*cm); // -7.175 (halflengthcan internal)
						// +0.55 (ext Can - Ge)
						// +9.0 (Ge length)
						// +0.2 (CF plate)
						// +2.3 (IntCF length)

  //  G4VPhysicalVolume* physiIntCF = 
  new G4PVPlacement(0,posIntCF,"IntCF",logicIntCF,physiVac,false,0,true);

    G4VisAttributes* red= new G4VisAttributes(G4Colour(1.0,0.0,0.0)); //Red
    red->SetForceSolid(true);
    logicIntCF->SetVisAttributes(red);

 
  //
  // The cold finger (external part)
  //

  G4double minRadiusExtCF = 0.*cm;
  G4double maxRadiusExtCF = 2.0*cm;
  G4double HalfLengthExtCF = 7.2*cm;
  G4double startPhiExtCF = 0.*deg;
  G4double deltaPhiExtCF = 360.*deg;

  G4Tubs* solidExtCF = new G4Tubs("ExtCF",minRadiusExtCF,maxRadiusExtCF,
				  HalfLengthExtCF,startPhiExtCF,deltaPhiExtCF);

  G4LogicalVolume* logicExtCF = 
    new G4LogicalVolume(solidExtCF,Copper,"ExtCF");

  // its position vs EnvColdFinger...

  G4ThreeVector posExtCF(0.*cm,0.*cm,0.*cm); 

  //  G4VPhysicalVolume* physiExtCF = 
  new G4PVPlacement(0,posExtCF,"ExtCF",logicExtCF,physiIntEnvColdFinger,false,0,true);

  //
  // The Dewar
  //

  G4double minRadiusDewar = 0.*cm;
  G4double maxRadiusDewar = 7.5*cm;
  G4double HalfLengthDewar = 15.2*cm;
  G4double startPhiDewar = 0.*deg;
  G4double deltaPhiDewar = 360.*deg;

  G4Tubs* solidDewar = new G4Tubs("Dewar",minRadiusDewar,maxRadiusDewar,
				  HalfLengthDewar,startPhiDewar,deltaPhiDewar);

  logicDewar = new G4LogicalVolume(solidDewar,Aluminum,"Dewar");

  G4double distFrontToMidDewar = 
    -GeoOffset+2.*(HalfLengthCan+EnvColdFingerHalfLength)+HalfLengthDewar+0.05*mm; 
  //+0.01mm to avoid roundoff errors

  G4ThreeVector posDewar = G4ThreeVector(0.*cm,0.*cm,distFrontToMidDewar);

  // ... and its position in the SupClover

  // G4VPhysicalVolume* physiDewar =
  new G4PVPlacement(0,posDewar,"Dewar",logicDewar,physiSupClover,false,0,true);

  // Its internal vacuum...

  G4double minRadiusIntDewar = 0.*cm;
  G4double maxRadiusIntDewar = 7.4*cm;
  G4double HalfLengthIntDewar = 14.7*cm;
  G4double startPhiIntDewar = 0.*deg;
  G4double deltaPhiIntDewar = 360.*deg;

  G4Tubs* solidIntDewar = new G4Tubs("IntDewar",minRadiusIntDewar,maxRadiusIntDewar,
				     HalfLengthIntDewar,startPhiIntDewar,deltaPhiIntDewar);

  G4LogicalVolume* logicIntDewar = new G4LogicalVolume(solidIntDewar,Vacuum,"IntDewar");

  //  G4VPhysicalVolume* physiIntDewar = 
  //new G4PVPlacement(0,0,"IntDewar",logicIntDewar,physiDewar,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicIntDewar,"IntDewar",logicDewar,false,0,true);

  G4cout << "The Cryostat built... " << G4endl;

  /////////////////////////////////////////
  //
  //  Construction of the active Ge volume:
  //
  /////////////////////////////////////////

  G4double zPlanesolidGeDiode[3];
  G4double rInnersolidGeDiode[3];
  G4double rOutersolidGeDiode[3];

  zPlanesolidGeDiode[0] = 4.*cm;
  zPlanesolidGeDiode[1] = 2.*cm;
  zPlanesolidGeDiode[2] = -4.*cm;

  rInnersolidGeDiode[0] = 0.*cm;
  rInnersolidGeDiode[1] = 0.*cm;
  rInnersolidGeDiode[2] = 0.*cm;

  rOutersolidGeDiode[0] = 3.5*cm;
  rOutersolidGeDiode[1] = 3.5*cm;
  rOutersolidGeDiode[2] = 2.44*cm;

  G4double GePhiStart = 0.*deg;
  G4double GePhiTot = 360.*deg;


  G4Polycone* solidGeDiode = new G4Polycone("GeDiode",GePhiStart,GePhiTot,3,zPlanesolidGeDiode,rInnersolidGeDiode,rOutersolidGeDiode);         

  G4ThreeVector possolidGeDiode = G4ThreeVector(0.*cm,0.*cm,-2.57*cm);

  logicGeA = new G4LogicalVolume(solidGeDiode,Germanium,"GeA");

  logicGeA->SetSensitiveDetector(ancSD);  

  cout <<  "#############  Sigma Sensitive detector built ###############" << endl;


 
  G4double zDumVac = -HalfLengthVac+5.05*cm; 	// 5.05 = 0.55 d(int can to Ge) +4.5(half length Ge) 

  G4ThreeVector positionVacA(0,0,zDumVac);


  G4VPhysicalVolume* physiGeA = new G4PVPlacement(0,possolidGeDiode,"GeA",logicGeA,physiVac,false,0,true);
   //
  // some material between the diodes to reproduce the experimental addback factor ...
  //

  G4double xAbsorb1 = 4.16*cm;
  G4double yAbsorb1 = 200.*um; // max = HalfDistanceBetweenDiodes = 0.5*mm;
  G4double zAbsorb1 = 4.5*cm;

  G4Box* solidAbsorb1 = new G4Box("Absorb1",xAbsorb1,yAbsorb1,zAbsorb1);

  G4double xAbsorb2 = 200*um; // max = HalfDistanceBetweenDiodes = 0.5*mm;
  G4double yAbsorb2 = 4.16*cm;
  G4double zAbsorb2 = 4.5*cm;

  G4Box* solidAbsorb2 = new G4Box("Absorb2",xAbsorb2,yAbsorb2,zAbsorb2);

  //G4UnionSolid* solidAbsorb = 
  //new G4UnionSolid("Absorb",solidAbsorb1,solidAbsorb2,0,0);
  G4UnionSolid* solidAbsorb = 
    new G4UnionSolid("Absorb",solidAbsorb1,solidAbsorb2);

  logicAbsorb = new G4LogicalVolume(solidAbsorb,Copper,"Absorb");

  G4ThreeVector positionAbsorb(0.,0.,zDumVac);

  //G4VPhysicalVolume* physiAbsorb = 
  //new G4PVPlacement(0,positionAbsorb,"Absorb",logicAbsorb,physiVac,false,0,true);

  //
  // Now: takes care of the holes and amorphous Ge in each diode:
  // Central hole with amorphous Ge for each diode. 
  //

  G4double minRadiusAGe1 = 0.*cm;								// Central tube
  G4double maxRadiusAGe1 = 0.5*cm;
  G4double HalfLengthAGe1 = 2.75*cm;
  G4double startPhiAGe1 = 0.*deg;
  G4double deltaPhiAGe1 = 360.*deg;

  G4Tubs* solidAGe1 = new G4Tubs("AGe1",minRadiusAGe1,maxRadiusAGe1,
				 HalfLengthAGe1,startPhiAGe1,deltaPhiAGe1);                      //Yellow bit

  logicAGe1 = new G4LogicalVolume(solidAGe1,Germanium,"AGe1");
  //gj - making sensitive
  //  logicAGe1 -> SetSensitiveDetector(ancSD);
  

  // ... and second the hole in it:

  G4Tubs* solidHole1 = new G4Tubs("Hole1",minRadiusAGe1,maxRadiusAGe1-2.*mm,
				  HalfLengthAGe1,startPhiAGe1,deltaPhiAGe1);

  logicHole1 = new G4LogicalVolume(solidHole1,Vacuum,"Hole1");                                       //HOLE (VACUUM)

  //
  // positioning the amorphous Ge in the diode
  //

  G4ThreeVector transAGe(0.*cm,0.*cm,-1.25*cm);

  //  G4VPhysicalVolume* physiAGe1A = 
  new G4PVPlacement(0,transAGe,"AGe1A",logicAGe1,physiGeA,false,0,true);                                     //putting hole in red bit
 /* new G4PVPlacement(0,transAGe,"AGe1B",logicAGe1,physiGeB,false,1,true);
  new G4PVPlacement(0,transAGe,"AGe1C",logicAGe1,physiGeC,false,2,true);
  new G4PVPlacement(0,transAGe,"AGe1D",logicAGe1,physiGeD,false,3,true);
*/  
  //
  // positioning the hole in the amorphous Ge
  //

  //new G4PVPlacement(0,0,"Hole1A",logicHole1,physiAGe1A,false,0,true);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),logicHole1,"Hole1A",logicAGe1,false,0,true);         //hole in yellow bit

  //
  // the Ge diodes for the SIGMA Clover done
  //

  //
  // the vacuum in the cold finger (maybe?)
  //

  G4double zPlaneCFVac[8];
  G4double rInnerCFVac[8];
  G4double rOuterCFVac[8];

  G4double CFVacPhiStart = 0.*deg;
  G4double CFVacPhiTot = 360.*deg;
  G4double CFVacHalfLength = 11.90*cm;
  
  zPlaneCFVac[0] = -CFVacHalfLength+0.2*cm;
  zPlaneCFVac[1] = -CFVacHalfLength+10.8*cm;
  zPlaneCFVac[2] = -CFVacHalfLength+10.8*cm;
  zPlaneCFVac[3] = -CFVacHalfLength+18.2*cm; 
  zPlaneCFVac[4] = -CFVacHalfLength+18.2*cm; 
  zPlaneCFVac[5] = -CFVacHalfLength+19.8*cm;  
  zPlaneCFVac[6] = -CFVacHalfLength+19.8*cm; 
  zPlaneCFVac[7] =  CFVacHalfLength;

  rInnerCFVac[0] = 0.*cm; 
  rInnerCFVac[1] = 0.*cm;
  rInnerCFVac[2] = 0.*cm;
  rInnerCFVac[3] = 0.*cm;
  rInnerCFVac[4] = 0.*cm;
  rInnerCFVac[5] = 0.*cm;
  rInnerCFVac[6] = 0.*cm;
  rInnerCFVac[7] = 0.*cm;

  rOuterCFVac[0]=12.3*cm;
  rOuterCFVac[1]=12.3*cm;;
  rOuterCFVac[2]=3.55*cm;
  rOuterCFVac[3]=3.55*cm;
  rOuterCFVac[4]=5.8*cm;
  rOuterCFVac[5]=5.8*cm;
  rOuterCFVac[6]=3.55*cm;
  rOuterCFVac[7]=3.55*cm;

  G4Polycone* solidCFVac = new G4Polycone("CFVac",CFVacPhiStart,CFVacPhiTot,8,zPlaneCFVac,rInnerCFVac,rOuterCFVac);         

  G4ThreeVector posCFVac = G4ThreeVector(0.*cm,0.*cm,0.*cm);

  logicCFVac = new G4LogicalVolume(solidCFVac,Vacuum,"CFVac");

  G4VisAttributes* CFVacVisAtt= new G4VisAttributes(G4Colour(0.0,1.0,0.0)); 
  CFVacVisAtt->SetForceSolid(true);
  logicCFVac ->SetVisAttributes(CFVacVisAtt);

  //new G4PVPlacement(0,posCFVac,"CFVac",logicCFVac,physiEnvColdFinger,false,0,true); 
  
}


////kuniec konstruowania

void AgataAncillarySigma::PlaceTarget()
{
  //  tarcza wlasciwa
  G4double Target_Rmin        =    .5*1.*cm;//0.  * mm;
  G4double Target_Rmax        =    .5*7.*cm;//0.5 * 10 * mm;
  G4double Target_Thickness   =    2.1468 * um;
  G4double Target_PosX        =    0.  * mm;
  G4double Target_PosY        =    0.  * mm;
  G4double Target_PosZ        =    1.*m;//0.  * mm;
  G4double Target_StAngle     =    0.  * deg;
  G4double Target_SegAngle    =  360.  * deg;

  G4double Layer_Thickness    =   12.296 * um;
  /*  G4double Layer_PosZ         =   Target_PosZ+.5*(Target_Thickness+Layer_Thickness)+0.01*um;*/

  G4RotationMatrix rm0;
  rm0.set(0, 0, 0);
  G4Tubs* solidTarget = new G4Tubs("Target",Target_Rmin,Target_Rmax,
				   .5*Target_Thickness,Target_StAngle,Target_SegAngle);

  G4LogicalVolume* logicTarget = new G4LogicalVolume(solidTarget,matTarget,
						     "Target",0,0,0);

  G4VPhysicalVolume* physTarget;
  physTarget = new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(Target_PosX,
								 Target_PosY,
								 Target_PosZ)),
				 "Target", logicTarget,
				 theDetector->HallPhys(), true, 800);
  
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  czerwony->SetVisibility(true);
  logicTarget -> SetVisAttributes(czerwony);
  //podkladka
  G4Tubs* solidLayer = new G4Tubs("Layer",Target_Rmin,Target_Rmax,
				  .5*Layer_Thickness,Target_StAngle,Target_SegAngle);

  G4LogicalVolume* logicLayer = new G4LogicalVolume(solidLayer,matLayer,
						    "Layer",0,0,0);
  
  //  G4VPhysicalVolume* physLayer;
  //  physLayer = new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(Target_PosX,
  //								Target_PosY,
  //								Layer_PosZ)),
  //				"Layer", logicLayer,
  //				 theDetector->HallPhys(), true, 900);
  G4VisAttributes* bialy = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  bialy->SetVisibility(true);
  logicLayer -> SetVisAttributes(bialy);

}

void AgataAncillarySigma::PlaceCatcherIonGuide()
{
  if(true||CloverInArray[18]||CloverInArray[19]||CloverInArray[20]||CloverInArray[21]){
    G4double Rmax        =    30 * mm;
    G4double Thickness   =     2 * mm;
    G4double Rmin        =  Rmax-Thickness;
    G4double PosX        =    0.  * mm;
    G4double PosY        =    0.  * mm;
    G4double StAngle     =    0.  * deg;
    G4double SegAngle    =  360.  * deg;
    G4double Length      =    1.  *  m;
    G4double PosZ        =    0.  * mm;
    G4int clov =0, ii;
    G4RotationMatrix rm0;rm0.set(0, 0, 0);
    for(ii=18;ii<22;ii++)
      if(CloverInArray[ii]){PosZ+=PositionZ[ii];clov++;}
    PosZ/=clov;
    //for a while:
    PosZ=0;
    G4Tubs* sCatcherIonGuide = new G4Tubs("CatcherIonGuide",Rmin,Rmax,.5*Length,StAngle,SegAngle);
    G4LogicalVolume* lCatcherIonGuide = new G4LogicalVolume(sCatcherIonGuide,Aluminum,"CatcherIonGuide",0,0,0);
    G4VPhysicalVolume* physCathcerIonGuide;
    physCathcerIonGuide = new G4PVPlacement(G4Transform3D(rm0,G4ThreeVector(PosX,PosY,PosZ)),
					    "CatcherIonGuide",lCatcherIonGuide,theDetector->HallPhys(),true,0);
    G4VisAttributes* CanVisAtt= new G4VisAttributes(G4Colour(0.5,0.5,0.5));  // Grey
    CanVisAtt->SetVisibility(true);
    lCatcherIonGuide->SetVisAttributes(CanVisAtt);
  }
  }

void AgataAncillarySigma::PlaceElectronDetectors(){
  G4double ElDetSideLength = 1.  * cm;
  G4double ElDetThickness  =  .1 * mm;
  G4Box* solidElDet = new G4Box("ElectronDetector",.5*ElDetSideLength,.5*ElDetSideLength,.5*ElDetThickness);
  G4LogicalVolume* logicElDet = new G4LogicalVolume(solidElDet,matElDet,"ElectronDetector");
  G4VisAttributes* white= new G4VisAttributes(G4Colour());white->SetVisibility(true);
  logicElDet->SetVisAttributes(white);

  const G4int NumberOfDets = 20;
  G4RotationMatrix rm[NumberOfDets];
  G4double x[NumberOfDets],y[NumberOfDets],z;
  G4double z_shift=3.*cm;
  z=100.*cm-z_shift;
  G4double radius=4.*cm;
  G4double AngleStep = (360./NumberOfDets)*deg;
  /*  G4double rotation_angle = atan(radius/z_shift);*/
  
  for(G4int i=0;i<NumberOfDets;i++){
    x[i]=radius*cos(i*AngleStep);
    y[i]=radius*sin(i*AngleStep);
    // rm[i].set(rotation_angle*sin(i*AngleStep),rotation_angle*cos(i*AngleStep),0);
    rm[i].set(0,0,0);

    /*    G4VPhysicalVolume* physiSiDet =*/
    new G4PVPlacement(G4Transform3D(rm[i],G4ThreeVector(x[i],y[i],z)),
		      "ElectronDetector",logicElDet,theDetector->HallPhys(),true,900+i);
  }	    

}


void AgataAncillarySigma::Placement()
{
  PlaceEverything();
  //if()place
  // uncomment line below to place secondary Target/IonGuide/ElectronDetector
  //PlaceTarget();
  //  PlaceCatcherIonGuide();
  //PlaceElectronDetectors();
  return;
}

void AgataAncillarySigma::ShowStatus()
{
  //G4cout
  //if()G4cout
}

//rozne klasy pt setSth

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAString.hh"

AgataAncillarySigmaMessenger::AgataAncillarySigmaMessenger(AgataAncillarySigma* pTarget, G4String name)
  :myTarget(pTarget)
{ 
  //  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/ancillary/Sigma/";
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of SIGMA construction.");
  
  commandName = directoryName+"SetSIGMAinput";
  cmdSetSIGMAinput = new G4UIcmdWithAString(commandName.c_str(),this);
  cmdSetSIGMAinput->SetGuidance("Set name of SIGMA input");
  cmdSetSIGMAinput->AvailableForStates(G4State_PreInit,G4State_Idle);
  //poszczegolne komendy
}

AgataAncillarySigmaMessenger::~AgataAncillarySigmaMessenger()
{
  //kasacja poszczegolnych komend
}

void AgataAncillarySigmaMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if(command==cmdSetSIGMAinput){
    myTarget->SetSIGMAInput(newValue);
  }
  //if(command=cos)procedura
}

#endif
