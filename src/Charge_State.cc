#include "Charge_State.hh"

#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

Charge_State::Charge_State()
{
  Charge=0;
  UnReactedFraction=1.;
  ReactedFraction=1.;
  setKEu=30*MeV;
  useSetKEu=true;
}

Charge_State::~Charge_State()
{
  ;
}

