#ifdef ANCIL
#include "AgataAncillaryLycca.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryLycca::AgataAncillaryLycca( G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  dirName = name ;
  
  
  matLycca1    = NULL;
  matName1     = "Silicon";
  matLycca2    = NULL;
  matName2     = "CsI";
  //matLycca3    = NULL;
  //matName3     = "Diamond";
  matLycca4    = NULL;
  matName4     = "FastPlastic";
  
  ancSD       = NULL;
  ancName     = G4String("Lycca");
  ancOffset   = 22000;
  
  numAncSd = 0;

}

AgataAncillaryLycca::~AgataAncillaryLycca()
{
}

G4int AgataAncillaryLycca::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial1 = G4Material::GetMaterial(matName1);
  if (ptMaterial1) {
    matLycca1 = ptMaterial1;
    G4String name1 = matLycca1->GetName();
    G4cout << "\n----> The ancillary Lycca material1 is  "
          << name1 << G4endl;
                  }
  else {
    G4cout << " Could not find the material " << matName1 << G4endl;
    G4cout << " Could not build the ancillary shell! " << G4endl;
    return 1;
  }  

  G4Material* ptMaterial2 = G4Material::GetMaterial(matName2);
  if (ptMaterial2) {
    matLycca2 = ptMaterial2;
    G4String name2 = matLycca2->GetName();
    G4cout << "\n----> The ancillary Lycca material2 is  "
          << name2 << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName2 << G4endl;
    G4cout << " Could not build the ancillary shell! " << G4endl;
    return 1;
  }  

  /*G4Material* ptMaterial3 = G4Material::GetMaterial(matName3);
  if (ptMaterial3) {
    matLycca3 = ptMaterial3;
    G4String name3 = matLycca3->GetName();
    G4cout << "\n----> The ancillary Lycca material3 is  "
          << name3 << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName3 << G4endl;
    G4cout << " Could not build the ancillary Lycca! " << G4endl;
    return 1;
  }  */

  G4Material* ptMaterial4 = G4Material::GetMaterial(matName4);
  if (ptMaterial4) {
  
  matLycca4 = ptMaterial4;
    G4String name4 = matLycca4->GetName();
    G4cout << "\n----> The ancillary Lycca material4 is  "
          << name4 << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName4 << G4endl;
    G4cout << " Could not build the ancillary Lycca! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryLycca::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/all", "AncCollection", offset, depth, menu );
    G4cout << "\n----> The ancillary offset is " << offset << G4endl;
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
}


void AgataAncillaryLycca::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryLycca::Placement()

{

//From ELOG292 (expt.s377)
  defTgtDetDist = 3.645*m;
  //defTgtDetDist = 3.542*m;
  G4cout << "\n----> The  Lycca Placement follows : " << G4endl ;
  G4cout << "\n----> The  Target to Lycca distance (defTgtDetDist) is  =" << defTgtDetDist <<G4endl ;

//  DiamondTgtDet = new DiamondTgt();
//  DiamondTgtDet->createDetector(theDetector->HallPhys(), matLycca3);
//  DiamondTgtDet->DiamondTgt_log->SetSensitiveDetector( ancSD );

  FPDet = new FPDetector(defTgtDetDist);
  FPDet->createDetector(theDetector->HallPhys(),matLycca4);
  FPDet->FP_log->SetSensitiveDetector( ancSD );

//  DiamondLyccaDet = new DiamondLycca(defTgtDetDist);
//  DiamondLyccaDet->createDetector(theDetector->HallPhys(), matLycca3);
//  DiamondLyccaDet->DiamondLycca_log->SetSensitiveDetector( ancSD );

  SiDet = new SiDetector(defTgtDetDist);
  SiDet->createDetector(theDetector->HallPhys(),matLycca1);
  SiDet->Si_log->SetSensitiveDetector( ancSD );

  SiTgtDet = new SiTgtDetector(defTgtDetDist);
  SiTgtDet->createDetector(theDetector->HallPhys(),matLycca1);
  SiTgtDet->SiTgt_log->SetSensitiveDetector( ancSD );
  
  CsIDet = new CsIDetector(defTgtDetDist);
  CsIDet->createDetector(theDetector->HallPhys(),matLycca2);
  CsIDet->CsI_log->SetSensitiveDetector( ancSD );

  G4cout << "\n----> The Lycca Placement ends ! " << G4endl ;
}


void AgataAncillaryLycca::ShowStatus()
{
  G4cout << " ANCILLARY Lycca has been constructed." << G4endl;
  G4cout << " Ancillary Lycca material1 is " << matLycca1->GetName() << G4endl;
  G4cout << " Ancillary Lycca material2 is " << matLycca2->GetName() << G4endl;
  //G4cout << " Ancillary Lycca material3 is " << matLycca3->GetName() << G4endl;
  G4cout << " Ancillary Lycca material4 is " << matLycca4->GetName() << G4endl; 
  G4cout << "\n----> The ancillary Lycca offset is " << ancOffset << G4endl;
}

void AgataAncillaryLycca::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillaryLycca::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}


///////////////////
// The Messenger
///////////////////
#endif
