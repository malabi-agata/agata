#include "AgataAnalysis.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataGeneratorAction.hh"
#include "AgataEventAction.hh"
#include "AgataPhysicsList.hh"
#include "AgataHitDetector.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include <cstdio>
#include <cmath>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "CLHEP/Random/RandomEngine.h"
#include "Randomize.hh"

#include "CGaspBuffer.hh"
#include "CSpec1D.hh"
// uncomment if matrices are needed!
//#include "CSpec2D.hh"

using namespace std;



AgataAnalysis::AgataAnalysis(G4String name)
{
  //Beta=12./100.;  // TO BE modifiued by the USER accordingly and the recompile with make !!!
  Beta=0./100.;  // TO BE modifiued by the USER accordingly and the recompile with make !!!

  // buffers
  geAccu    = NULL;
  gtAccu    = NULL;
  anAccu    = NULL;
  anAccuSeg    = NULL;
  agAccu    = NULL;
  cluAccu   = NULL;
  det2clust = NULL;
  EXOGAM_AC_hit = NULL; // Anti-Compton  // added by Marc
  EXOGAM_Clover_hit = NULL; // Added by Marc
  
  aLine       = new char[100];
  
  position    = 0;
  runNumber   = 0;
  
  nDetectors  = 0;
  nClusters   = 0;
  nInstances  = 0;
  nAncil      = 0;

  EXOGAM_AC_hit=NULL;

  maxFileSize = 2136997888; // 95% of 2GB
  fileNumber  = 0;
  
  // Energy resolution modelled as FWHM = sqrt( A + B E )
  CalculateResolution( 122.*keV, 1.1*keV,       // preset standard values for A and B
		       1333.*keV, 1.9*keV,
		       false);
  FWHMk       = 1./(sqrt(8.*log(2.)));  // sigma = FWHM/2.355
  
  // buffer
  gaspBuffer    = NULL;
  gaspEvent     = NULL;
  // ge energy and time calibration
  gf1           = 0.25*keV;
  gf2           = 0.1*ns;
  fThreshold    = 1.0*keV;
  //fThreshold    = 1.0*keV;
  ancillaryOnly = 0;
  
  
  // spectra
  specLength        = 4096;        //> length of spectra
  offset            = 0.;          //> offset for spectra
  //  gain              = .25;          //> gain   for spectra
  gain              = 1.;          //> gain   for spectra

  // default: disable analysis; if enabled, writes file in GASP format
  enabledAnalysis       = false;
  packEvent             = false;       // don't pack multievent-gammas
  makeSpectra           = true;
  makeBuffer            = false;
  asciiSpectra          = false;       //> default: binary spectra
  enabledResolution     = true;       // energy resolution on/off AGATA only (Marc changed it from false to true)

  myMessenger = new AgataAnalysisMessenger(this, name);
}

AgataAnalysis::~AgataAnalysis()
{
  spec1.clear();
  // uncomment if matrices are needed!
  //spec2.clear();

  if( geAccu    ) delete [] geAccu;  // ge energy
  if( gtAccu    ) delete [] gtAccu;  // ge time
  if( cluAccu   ) delete [] cluAccu; // cluster energy
  if( det2clust ) delete [] det2clust;
  if( anAccu    ) delete [] anAccu;  // ancillary detectors
  if( anAccuSeg    ) delete [] anAccuSeg;  // ancillary detectors
  if( agAccu    ) delete [] agAccu;  // ancillary detectors --> good or not
  if( EXOGAM_AC_hit    ) delete [] EXOGAM_AC_hit;  // ancillary detectors --> good or not
  if( EXOGAM_Clover_hit    ) delete [] EXOGAM_Clover_hit;  // ancillary detectors --> good or not
  
  if( gaspEvent ) delete [] gaspEvent;
  
  delete [] aLine;
}

//////////////////////////////////////////////////////////////
//// General "service" methods ///////////////////////////////
//////////////////////////////////////////////////////////////
void AgataAnalysis::AnalysisStart(G4int run)
{
  if(!enabledAnalysis)
    return;
    
  if( (!makeSpectra) && (!makeBuffer) ) {
    cout << " ---> Warning! Nothing to do, disabling analysis ... " << endl;
    this->EnableAnalysis(false);
    return;
  } 

  G4RunManager * runManager = G4RunManager::GetRunManager();

  theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theEvent     = (AgataEventAction*)          runManager->GetUserEventAction();
  
  if( (theGenerator->GetBeginOfEventTag() != "") && (!packEvent) )
    SetPackEvent( true );

  if(packEvent && theGenerator->GetCascadeMult() > 1)
    scaleEtot = 1.; //0.1;
  else if(packEvent && (theGenerator->GetBeginOfEventTag() != "") ) 
    scaleEtot = 1.;//0.1;
  else
    scaleEtot = 1.;
  
  // gain factor for ancillary detectors
  if( packEvent )
    gf3 = 10.*keV; // writes out at 100 ch/MeV
  else
    gf3 =  5.*keV;

  runNumber   = run;
  
  nDetectors  = theDetector->GetMaxDetectorIndex() + 1;          // safe allocation: this number was already calculated leaving some margin
  nClusters   = theDetector->GetMaxClusterIndex()  + 1;    
  cout << " nDetectors: " <<  nDetectors << " nClusters: " << nClusters << endl;
 
  nDetectorsAncilIndex  = 4*4 + 8*4; // (Id of first clover in EXOGAM_INPUT x 4) + (X used clovers * 4 crystals for EXOGAM)  added by Marc
  cout << " nDetectorsAncilIndex: " <<  nDetectorsAncilIndex << endl;

    
  nAncil      = theDetector->HowManyOffset(); // Why nancil =10 for exogam ? Because EXOGAM ancillary index is 9 !!! 
  nInstances  = 0;                                               // how many different ancillary detectors
  cout << " nInstances: " <<  nInstances << " nAncil: " << nAncil << endl;
  
  
  /////////////////////
  // several buffers
  ////////////////////
  if( geAccu    ) delete [] geAccu;  // ge energy
  if( gtAccu    ) delete [] gtAccu;  // ge time
  if( cluAccu   ) delete [] cluAccu; // cluster energy
  if( det2clust ) delete [] det2clust;
  if( anAccu    ) delete [] anAccu;  // ancillary detectors
  if( anAccuSeg    ) delete [] anAccuSeg;  // ancillary detectors
  if( agAccu    ) delete [] agAccu;  // ancillary detectors --> good or not
  if( EXOGAM_AC_hit    ) delete [] EXOGAM_AC_hit;  // ancillary detectors --> good or not
  if( EXOGAM_Clover_hit    ) delete [] EXOGAM_Clover_hit;  // ancillary detectors --> good or not

  parLut.clear();
  ancLut.clear();
  minLut.clear();
  
  // germanium detectors
  geAccu    = new G4double [nDetectors];
  memset(geAccu,   0, nDetectors*sizeof(G4double));
  gtAccu    = new G4double [nDetectors];
  memset(gtAccu,   0, nDetectors*sizeof(G4double));
  
  det2clust = new G4int [nDetectors];
  memset(det2clust,0, nDetectors*sizeof(G4int));
  
  minGeAccu = nDetectors;
  maxGeAccu = 0;

  cluAccu   = new G4double [nClusters];
    
  // ancillary detectors: they have been created only when nAncil > 1
  // this will most likely crash with fixed offsets (which are not recommended anyway ...)
  if( nAncil > 1 ) {
    anAccu   = new G4double[nDetectorsAncilIndex*nAncil];               //  nDetectors replaced by ndetectorsAncilIndex  (Marc)
    memset(anAccu,   0, nDetectorsAncilIndex*nAncil*sizeof(G4double));  //  nDetectors replaced by ndetectorsAncilIndex  (Marc)
    minAnAccu = nDetectorsAncilIndex*nAncil;                //  nDetectors replaced by ndetectorsAncilIndex  (Marc)
    maxAnAccu = 0;
 
    anAccuSeg   = new G4double*[nDetectorsAncilIndex*nAncil];               // To deal with EXOGAM segments  (Marc)
    for(int i=0; i<nDetectorsAncilIndex*nAncil ;i++){
      anAccuSeg[i]= new G4double[4];
    }

    EXOGAM_AC_hit   = new G4int[8];               //  (Marc)
    memset(EXOGAM_AC_hit,   0, 8*sizeof(G4int));  //  (Marc)

    EXOGAM_Clover_hit   = new G4int[8];               //  (Marc)
    memset(EXOGAM_Clover_hit,   0, 8*sizeof(G4int));  //  (Marc)
   
    G4int ii;
    // easy task: lookup table offset ==> ancillary (copy what is stored into DetectorConstruction)
    ancLut.resize(nAncil);
    cout << " nAncil: " << nAncil << endl;

    for( ii=0; ii<8; ii++ ){
      EXOGAM_AC_hit[ii] = 0;
      EXOGAM_Clover_hit[ii] = 0;
      cout << " EXOGAM_AC_hit[ii]: " << ii << " " <<  EXOGAM_AC_hit[ii] << endl;
    }

    for( ii=1; ii<nAncil; ii++ ){
      ancLut[ii] = theDetector->GetOffset(ii);
      cout << " ancLut 1-9: " <<  ancLut[ii] << endl;
    }

    // lookup table ancillary ==> how many parameters?
    parLut.resize(nAncil);
    minLut.resize(nAncil);
    cout << " nAncil: " << nAncil << endl;
    for( ii=0; ii<nAncil; ii++ ) {
      parLut[ii] = 0;
      minLut[ii] = nAncil;
      //cout << " parLut [1-9]: " <<  parLut[ii] << endl;
      //cout << " minLut [1-9]: " <<  minLut[ii] << endl;
    }  
    
    for( ii=1; ii<nAncil; ii++ ) {
      parLut[theDetector->GetOffset(ii)]++;  // counts how many times the given ancillary is called (how many offsets ==> how many parameters)
      if( ii < minLut[theDetector->GetOffset(ii)] ){
        minLut[theDetector->GetOffset(ii)] = ii;    // first offset corresponding to a given ancillary
	//cout << "theDetector->GetOffset(ii):" << theDetector->GetOffset(ii) << " minLut [1-9]: " << minLut[theDetector->GetOffset(ii)]  << " ii:" <<  ii << endl;
      }
    }  

      
    for( ii=0; ii<nAncil; ii++ ) {
      if( parLut[ii] > 0 ) 
        nInstances++;    // counts how many ancillaries have at least a parameter
    }
    //cout << " nInstances: " <<  nInstances << " nAncil: " << nAncil << endl;
    
    // flag: ancillary fired or not
    agAccu   = new G4int[nDetectorsAncilIndex*nInstances]; // nDetectors refers to the number of AGATA crystals so why using it for fired Ancillaries ??? 
    memset(agAccu,   0, nDetectorsAncilIndex*nInstances*sizeof(G4int));  
    minAgAccu = nDetectorsAncilIndex*nInstances; 
    maxAgAccu = 0;
    //cout << " nInstances: " <<  nInstances << " nAncil: " << nAncil << endl;


  }  // end of if(nAncil>1)

   
  /////////////////////
  /// Create buffer
  ////////////////////
  fileNumber = 0;
  
  if( makeBuffer ) {
    if( !gaspBuffer )                               
      gaspBuffer = new CGaspBuffer();               
    gaspBuffer->Reset();                            
    gaspBuffer->BeginOfRun( runNumber );            
    gaspBuffer->SetMaxFileSize( maxFileSize );      
    if( !gaspEvent )                                
      gaspEvent = new unsigned short int[2048];      
  }

  /////////////////////
  /// Create spectra
  ////////////////////
  if( makeSpectra ) {
    spec1.clear();
    // uncomment if matrices are needed!
    //spec2.clear();

    spec1.push_back(new CSpec1D(specLength, 1));           //> total spectrum (AGATA+EXOGAM)
    spec1.push_back(new CSpec1D(specLength, nDetectors));  //> spectrum of detectors
    spec1.push_back(new CSpec1D(specLength, nClusters));   //> spectrum of clusters
    spec1.push_back(new CSpec1D(specLength, 1));           //> sum of spectrum of detectors
    spec1.push_back(new CSpec1D(specLength, 1));           //> Ancillary EXOGAM detectors only (with addback between crystals and clover !)
    spec1.push_back(new CSpec1D(specLength, 1));           //> same as above but vetoed by Anti Compton shield 
    spec1.push_back(new CSpec1D(specLength, 1));           //> as above + AGATA
    spec1.push_back(new CSpec1D(specLength, 1));           //> AGATA contribution



    // uncomment if matrices are needed!
    // spec2.push_back(new CSpec2D(256, 256, 1));
  }
}

void AgataAnalysis::AnalysisEnd( G4String workPath )
{
  if(!enabledAnalysis)
    return;
    
  if( makeBuffer )
    gaspBuffer->EndOfRun();
    
  if( makeSpectra ) 
    FlushSpectra( workPath );

}

void AgataAnalysis::EndOfEvent()
{
  if(!enabledAnalysis)
    return;

  if( packEvent && !theGenerator->IsEndOfEvent() )
    return;

  ApplyResolution();
  WriteGaspEvent();
  IncrementSpectra();
  ResetBuffers();
  return;
}

void AgataAnalysis::ResetBuffers()
{
  if( nDetectors ) {
    memset(geAccu,   0, nDetectors*sizeof(G4double));
    memset(gtAccu,   0, nDetectors*sizeof(G4double));
    memset(cluAccu,  0, nClusters *sizeof(G4double));

    minGeAccu = nDetectors;                           //> we have erased detAccu in the loop
    maxGeAccu = 0;                                    //> so we prepare it for the next event
  }
  if( nInstances ) {
    memset(anAccu,   0, nDetectorsAncilIndex*nAncil    *sizeof(G4double));// nDetectors replaced by ndetectorsAncilIndex  (Marc)
    memset(agAccu,   0, nDetectorsAncilIndex*nInstances*sizeof(G4int));// nDetectors replaced by ndetectorsAncilIndex  (Marc)

    //minAnAccu = nDetectorsAncilIndex*nInstances;// nDetectors replaced by ndetectorsAncilIndex  (Marc)
    minAnAccu = nDetectorsAncilIndex*nAncil;// nDetectors replaced by ndetectorsAncilIndex  (Marc)
    maxAnAccu = 0;
    //minAgAccu = nDetectorsAncilIndex*nInstances;// nDetectors replaced by ndetectorsAncilIndex  (Marc)
    //maxAgAccu = 0;

    memset(EXOGAM_AC_hit,   0, 8*sizeof(G4int));//  (Marc)
    memset(EXOGAM_Clover_hit,   0, 8*sizeof(G4int));//  (Marc)

  }
}

G4double AgataAnalysis::EnergyResolution(double value)
{
  // resolution modelled as FWHM = sqrt( A + B E )
  // FWHMk = 1/2.355
  G4double dummy = G4RandGauss::shoot( value, FWHMk*sqrt( FWHMa + FWHMb*value ) );
  if( dummy < 0. ) 
    dummy = 0.;
  return dummy;
}

void AgataAnalysis::ApplyResolution()
{
  if(!enabledAnalysis)
    return;
  
  if( !enabledResolution )
    return;

  if( minGeAccu > maxGeAccu )
    return;
  
  for( G4int ii=minGeAccu; ii<=maxGeAccu; ii++ ) {
    if( geAccu[ii] ) {
      geAccu[ii] = EnergyResolution(geAccu[ii]);
      if( geAccu[ii] < 0. )
        geAccu[ii] = 0.;
    }
  }  	
}



////////////////////////////////////////////////////////////
/////// Treatment of the hits  /////////////////////////////
//////  Get hit object and dispatch relevant information ///
////////////////////////////////////////////////////////////
void AgataAnalysis::AddHit( AgataHitDetector* theHit )
{
  if(!enabledAnalysis)
    return;
  AddEnergy   ( theHit->GetEdep(), theHit->GetDetNb(), theHit->GetCluNb(), theHit->GetSegNb());
  //AddEnergy   ( theHit->GetEdep(), theHit->GetDetNb(), theHit->GetCluNb() );
  AddTime     ( theHit->GetTime(), theHit->GetEdep(), theHit->GetDetNb() );
}

//void AgataAnalysis::AddEnergy( G4double energy, G4int detnum, G4int clunum )
void AgataAnalysis::AddEnergy( G4double energy, G4int detnum, G4int clunum, G4int segnum )
{
  if( detnum < 1000 ){
    AddGermaniumEnergy( energy, detnum, clunum );
  }else {
    //cout << "detnum: " << detnum <<  " energy: " << energy <<endl;
    //AddAncillaryEnergy( energy, detnum );
    AddAncillaryEnergy( energy, detnum, segnum );
  }
}

void AgataAnalysis::AddGermaniumEnergy( G4double energy, G4int detnum, G4int clunum )
{
  if(detnum >= 0 && detnum < nDetectors) {   
    geAccu[detnum] += energy;
    if(detnum < minGeAccu) minGeAccu = detnum;
    if(detnum > maxGeAccu) maxGeAccu = detnum;
    det2clust[detnum] = clunum;
  }
}

//void AgataAnalysis::AddAncillaryEnergy( G4double energy, G4int detcode ) // For EXOGAM without segment
void AgataAnalysis::AddAncillaryEnergy( G4double energy, G4int detcode, G4int iSeg ) // for EXOGAM with segment
{
  G4int detnum;
  detnum = detcode%1000;
  //cout << endl;
  //cout << "detnum:" << detnum << endl;
  //cout << endl;
  //cout << "nDetectorsAncilIndex:" << nDetectorsAncilIndex << endl;

  G4int Crystnum;
  Crystnum = detcode%100;
  //cout << "Crystnum:" << Crystnum << endl;

  G4int Segnum;
  Segnum=iSeg;
  //cout << "Segnum:" << Segnum << endl;


  G4int ClovIndex;
  ClovIndex = (Crystnum-(4*4))/4;
  //cout << "ClovIndex:" << ClovIndex << endl;

  if(detnum > nDetectorsAncilIndex) {  // an EXOGAM Anti-Compton detectors is hit

    EXOGAM_AC_hit[ClovIndex] = 1;
  }else
    {
      EXOGAM_Clover_hit[ClovIndex] = 1 ;
    }


  if(detnum >= 0 && detnum < nDetectorsAncilIndex) {   // nDetectors replaced by nDetectorAncil (Marc)
    G4int ofset = detcode/1000;
    G4int whichAnc = ancLut[ofset];

    //cout << "whichAnc= " << whichAnc << endl;

    anAccu[detnum + ofset*nDetectorsAncilIndex]  += energy;            // nDetectors replaced by nDetectorAncil (Marc)

    //cout << "offset= " << ofset << endl;
    //cout << "nDetectorsAncilIndex= " << nDetectorsAncilIndex << endl;
    anAccuSeg[detnum + ofset*nDetectorsAncilIndex][Segnum]  = energy; // Added to fin wich EXOGAM seg is hit first 

    agAccu[detnum + whichAnc*nDetectorsAncilIndex] = 1;  // it fired!   // nDetectors replaced by nDetectorAncil (Marc)

    //cout << "detnum:" << detnum << " offset:" << offset << " ndetectorsAncilIndex:" << nDetectorsAncilIndex << " anAccu[detnum + offset*nDetectorsAncilIndex]:" << anAccu[detnum + offset*nDetectorsAncilIndex] << " energy:" << energy << endl;

    if((detnum + ofset*nDetectorsAncilIndex) < minAnAccu) minAnAccu = detnum + ofset*nDetectorsAncilIndex;
    if((detnum + ofset*nDetectorsAncilIndex) > maxAnAccu) maxAnAccu = detnum + ofset*nDetectorsAncilIndex;

    //cout << "minAnAccu:" << minAnAccu  << " maxAnAccu:"  << maxAnAccu << endl;


  }
}

void AgataAnalysis::AddTime( G4double time, G4double ener, G4int detnum )
{
  if( detnum < 1000 )
    AddGermaniumTime ( time, ener, detnum );  
  else 
    return;    // no treatment for ancillary "time" provided!!!
}

void AgataAnalysis::AddGermaniumTime( G4double time, G4double ener, G4int detnum )
{
  if( ener == 0. ) return;
  
  if(detnum >= 0 && detnum < nDetectors) {
    if( !gtAccu[detnum] )
      gtAccu[detnum] = time;
    else
      gtAccu[detnum] = ( ener * time + ( geAccu[detnum] - ener ) * gtAccu[detnum] ) /  geAccu[detnum];

    if(detnum < minGeAccu) minGeAccu = detnum;
    if(detnum > maxGeAccu) maxGeAccu = detnum;
  }
}


///////////////////////////////////////////////
///// Management of spectra ///////////////////
///////////////////////////////////////////////
void AgataAnalysis::IncrementSpectra()
{
  if( !makeSpectra ) return;

  G4double edepTot      = 0.;
  G4int minClu = nClusters;
  G4int maxClu = 0;
  G4int clunum;
  G4int ii;
  G4int nd = 0;
  G4double edummy;
  for(ii=minGeAccu; ii<=maxGeAccu; ii++) {
    edummy = geAccu[ii]/keV;
    if( edummy > 0. ) {  // 0 keV Threshold
      clunum = det2clust[ii];
      edummy = offset + gain * edummy;
      if(edummy) {
        spec1[1]->incr((G4int)edummy, ii);          // the energy of the detectors
        spec1[3]->incr((G4int)edummy, 0);           // the energy of the detectors
        edepTot += edummy;
        nd++;
      }
      if(clunum >= 0 && clunum < nClusters) {
        cluAccu[clunum] += edummy;
        if(clunum < minClu) minClu = clunum;
        if(clunum > maxClu) maxClu = clunum;
      }
    }
  }

  if (edepTot) {
    spec1[0]->incr((G4int)(edepTot*scaleEtot), 0);   //> the total energy of the array (AGATA+EXOGAM)
    spec1[6]->incr((G4int)(edepTot*scaleEtot), 0);   //> the total energy of the array (AGATA+EXOGAM compton suppress)
    spec1[7]->incr((G4int)(edepTot*scaleEtot), 0);   //> AGATA contribution only
  }

 
  // Ancillary:
  if(nAncil>1)
    {

      //For EXOGAM (at least)
      G4double edepTotAn;
      edepTotAn = 0.;
      G4int iii, jjj;
      G4double kkk;
      G4int minAnClover/*, maxAnClover*/;
      G4int nd_An = 0;
      //G4double edummyAn;
      G4double edummyAn[8];  // for 8 EXOGAM clovers
      G4double eAnCryst[8][4];  // for 4 crystal of 8 EXOGAM clovers
      G4double nrjCrystFirst;
      G4int CrystFirst;
      G4double nrjSegFirst;
      G4int SegFirst;

      nrjCrystFirst=0.;
      CrystFirst=0;
      nrjSegFirst=0.;
      SegFirst=0;

      for(jjj=0;jjj<8;jjj++){
	edummyAn[jjj]=0.;

	for(iii=0;iii<4;iii++)
	  {
	    eAnCryst[jjj][iii]=-1.;
	  }
      }


      if(minAnAccu==maxAnAccu){  // only one crystal hit amonsgt all of them


	// For 8 detectors at 90 degrees	  
	jjj=((minAnAccu-9*48)-4*4)/4;  // jjj = clover index ; minAnAccu= crystal index; offset=9 ; nDetectorsAncilIndex=48;

	kkk=((minAnAccu-9.*48.)-4.*4.)/4.- jjj;  // minAnAccu= crystal index; offset=9 ; nDetectorsAncilIndex=48;
	// kkk=0 <=> crystal A (if clover at 90 degrees -> A is backward)
	// kkk=0.25 <=> crystal B (if clover at 90 degrees -> B is backward)
	// kkk=0.5 <=> crystal C (if clover at 90 degrees -> C is forward)
	// kk=0.75 <=> crystal D (if clover at 90 degrees -> D is forward)

	anAccu[minAnAccu] = anAccu[minAnAccu]/keV; // transform in keV
	anAccu[minAnAccu]= G4RandGauss::shoot( anAccu[minAnAccu], FWHMk*3.); // Exogam intrinsic energy resolution 3 keV
	edummyAn[jjj] = anAccu[minAnAccu];  


	//Finding the first segment hit (based on nrj deposited):
	for(int i=0;i<4;i++)
	  {
	    //cout << "i=" << i << " anAccuSeg= " << anAccuSeg[minAnAccu][i] << endl;
	    if(anAccuSeg[minAnAccu][i]>nrjSegFirst)
	      {
		nrjSegFirst=anAccuSeg[minAnAccu][i];
		SegFirst=i;
	      }
	  }
	//cout << "SegFirst=" << SegFirst << endl;
	//first segment hit found !!!


	    
	if( edummyAn[jjj] > 0. ) {  // 0 keV threshold
	  //edummyAn = offset + gain * edummyAn;
	  //cout << "kkk=" << kkk << " jjj=" << jjj << " edummyAn[jjj=" << edummyAn[jjj] << endl;
	  //cout << "EXOGAM_AC_hit[jjj]=" << EXOGAM_AC_hit[jjj] <<endl;
	  //cout << "offset=" << offset << " Gain=" << gain <<endl;
	  //edummyAn[jjj] = offset + gain * edummyAn[jjj];  // 

	  //if(edummyAn[jjj]) {    
	  //		edepTotAn += edummyAn[jjj];  // calorimeter mode
	  //		nd_An++;
	  //	      }

	  if(edummyAn[jjj] && (kkk==0.5 || kkk==0.75)) {    // forward detector (C&D)
	    //cout << "forward detector" << endl;
	    //

	    // Doppler correction for central contact
	    //edummyAn[jjj]=  edummyAn[jjj]*(1-Beta*cos(82.5*3.14159/180.))/sqrt(1-Beta*Beta); // spectroscopy mode with Doppler Correction (83 degrees is for clovers at 90 degrees) 
	    // Doppler correction for segment:
	    if(SegFirst==0 || SegFirst==3) edummyAn[jjj]=  edummyAn[jjj]*(1-Beta*cos((82.5+0)*3.14159/180.))/sqrt(1-Beta*Beta); // spectroscopy mode with Doppler Correction (83 degrees is for clovers at 90 degrees) 
	    if(SegFirst==1 || SegFirst==2) edummyAn[jjj]=  edummyAn[jjj]*(1-Beta*cos((82.5-0.)*3.14159/180.))/sqrt(1-Beta*Beta); // spectroscopy mode with Doppler Correction (83 degrees is for clovers at 90 degrees) 
	    edepTotAn += edummyAn[jjj]; // calorimeter mode with Doppler correction 


	    nd_An++;
	  }
	  if(edummyAn[jjj] && (kkk==0. || kkk==0.25)) {    // backward detector (A&B)
	    //cout << "Backward detector" << endl;

	    // Doppler correction for central contact
	    //edummyAn[jjj]= edummyAn[jjj]*(1+Beta*cos(83.1*3.14159/180.))/sqrt(1-Beta*Beta); // calorimeter mode with Doppler Correction (83 degrees is for clovers at 90 degrees)
	    // Doppler correction for segment:
	    if(SegFirst==0 || SegFirst==3)edummyAn[jjj]=  edummyAn[jjj]*(1+Beta*cos((82.5+0)*3.14159/180.))/sqrt(1-Beta*Beta); // spectroscopy mode with Doppler Correction (83 degrees is for clovers at 90 degree
	    if(SegFirst==1 || SegFirst==2)edummyAn[jjj]=  edummyAn[jjj]*(1+Beta*cos((82.5-0)*3.14159/180.))/sqrt(1-Beta*Beta); // spectroscopy mode with Doppler Correction (83 degrees is for
	    edepTotAn += edummyAn[jjj]; // calorimeter mode with Doppler Correction
	    nd_An++;
	  }

	  spec1[0]->incr((G4int)(edummyAn[jjj]*scaleEtot), 0);   // For only 1 hit crystal fill > the total energy of the array AGATA +EXOGAM
	  spec1[4]->incr((G4int)(edummyAn[jjj]*scaleEtot), 0);   // for only 1 hit crystal fill > the total energy spectrum of the array EXOGAM only 

	}

      }else  // more than 1 cristal have been hit over all the cristals

	if (minAnAccu<maxAnAccu){  	    

	  //cout << "minAnAccu" << minAnAccu << "maxAnAccu " << maxAnAccu << endl;

	  for(jjj=0; jjj<8; jjj++){

	    if(EXOGAM_Clover_hit[jjj]==1){

	      minAnClover= jjj*4 + 9*48 + 4*4;   // min crystal index in the clover hit
	      //	      maxAnClover= jjj*4 + 9*48 + 4*4 +3;// max crystal index in the same clover


	      //Finding first crystal hit in a Clover:
	      for(iii=0; iii<=3; iii++) { 

		if(anAccu[minAnClover+iii]>0.){

		  if(anAccu[minAnClover+iii]>nrjCrystFirst)
		    {
		      nrjCrystFirst=anAccu[minAnClover+iii];
		      CrystFirst=iii;
		      //cout << "CrystFirst=" << CrystFirst << " nrjCrystFirst" << nrjFirst << endl;
		    }
		}
	      }
	      //cout << "CrystFirstFinal =" << CrystFirst << endl;
	      // first crystal hit in a Clover found !!



	      //Finding the first segment in first crystal (based on nrj deposited):
	      for(int i=0;i<4;i++)
		{
		  //cout << "i=" << i << " anAccuSeg= " << anAccuSeg[minAnAccu][i] << endl;
		  if(anAccuSeg[minAnClover+CrystFirst][i]>nrjSegFirst)
		    {
		      nrjSegFirst=anAccuSeg[minAnClover+CrystFirst][i];
		      SegFirst=i;
		    }
		}
	      //cout << "SegFirst=" << SegFirst << endl;
	      //first segment in first crystal is found !!!




	      for(iii=0; iii<=3; iii++) { 

		if(anAccu[minAnClover+iii]>0.){


		  anAccu[minAnClover+iii] = anAccu[minAnClover+iii]/keV; // transform in keV
		  anAccu[minAnClover+iii]=G4RandGauss::shoot( anAccu[minAnClover+iii], FWHMk*3.); // Exogam intrinsic energy resolution 3 keV

		  edummyAn[jjj] += anAccu[minAnClover+iii];  // edummyAn contains total addback between all crystals hit whatever the multiplicity

		  eAnCryst[jjj][iii] = anAccu[minAnClover+iii]; // eAnCrystal contains the energy of the crystal only
		      
		}
	      }

	    }     
	  

	    //
	    //Now, we do the addback ONLY FOR ADJACENT crystal in a same clover. 
	    //When diagonal crystals are hit we consider there is no add done at all (for now),meaning where there only 2,3 or 4 crystal hit in a clover, 
	    //all are considered as 2, 3 or 4 different gammas:
	    //

	

	    // if (A & C) or (B & D) are hit no addback is done (including triple and quadruple hit, ie: M=3 and M=4):
	    // All are taken as individual hit and doppler corrected as such:
	    if((eAnCryst[jjj][0]>0. && eAnCryst[jjj][2]>0.)   // 20keV threshold for each crystal
	       || (eAnCryst[jjj][1]>0. && eAnCryst[jjj][3]>0.)) // no addback on any diagonal hits (including triple and quadruple hit)
	      {  	

		// Apply Doppler correction on individual crystal => without addback:
		// A & B (backward):
		if(eAnCryst[jjj][0]>0.)eAnCryst[jjj][0]=eAnCryst[jjj][0]*(1-Beta*cos(82.5*3.14159/180.))/sqrt(1-Beta*Beta);
		if(eAnCryst[jjj][1]>0.)eAnCryst[jjj][1]=eAnCryst[jjj][1]*(1-Beta*cos(82.5*3.14159/180.))/sqrt(1-Beta*Beta);
		// C & D (forward):
		if(eAnCryst[jjj][2]>0.)eAnCryst[jjj][2]=eAnCryst[jjj][2]*(1+Beta*cos(82.5*3.14159/180.))/sqrt(1-Beta*Beta);
		if(eAnCryst[jjj][3]>0.)eAnCryst[jjj][3]=eAnCryst[jjj][3]*(1+Beta*cos(82.5*3.14159/180.))/sqrt(1-Beta*Beta);


		//if((eAnCryst[jjj][0]>0. && eAnCryst[jjj][2]>0. && ( eAnCryst[jjj][1]<0.&& eAnCryst[jjj][3]<0.))) // no addback on double hit in diagonal cristals
		if((eAnCryst[jjj][0]>0. && eAnCryst[jjj][2]>0.)) // no addback on any diagonal hits (including triple and quadruple hit) 
		  {
		    spec1[0]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
		    spec1[0]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
		    //spec1[4]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		    //spec1[4]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		    // if in addition, a third crystal is hit (ie: M=3):
		    if(eAnCryst[jjj][1]>0.&& eAnCryst[jjj][3]==0.) 
		      {
			spec1[0]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			//spec1[4]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
			  
		      }
		    if(eAnCryst[jjj][3]>0. && eAnCryst[jjj][1]==0.)
		      {

			spec1[0]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			//spec1[4]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		      }   
		      


		    if(EXOGAM_AC_hit[jjj]!=1){
		      spec1[5]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
		      spec1[5]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
		      spec1[6]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
		      spec1[6]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
		      /*// if in addition, a third crystal is hit (ie: M=3):
			if(eAnCryst[jjj][1]>0.&& eAnCryst[jjj][3]==0.) 
			{
			spec1[5]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			spec1[6]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array EXOGAM only			  
			}
			if(eAnCryst[jjj][3]>0. && eAnCryst[jjj][1]==0.)
			{
			spec1[5]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			spec1[6]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
			}   
		      */
		    }

		  }

		//if((eAnCryst[jjj][1]>0. && eAnCryst[jjj][3]>0. && ( eAnCryst[jjj][0]<0.&& eAnCryst[jjj][2]<0.))) // no addback on double hit in diagonal cristals
		if((eAnCryst[jjj][1]>0. && eAnCryst[jjj][3]>0.)) // no addback on any diagonal hits (including triple and quadruple hit)
		  {
		    spec1[0]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
		    spec1[0]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
		    //spec1[4]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		    //spec1[4]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		    // if in addition, a third crystal is hit (ie: M=3):
		    if(eAnCryst[jjj][0]>0.&& eAnCryst[jjj][2]==0.) 
		      {
			spec1[0]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			//spec1[4]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
			  
		      }
		    if(eAnCryst[jjj][2]>0. && eAnCryst[jjj][0]==0.)
		      {

			spec1[0]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			//spec1[4]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
		      }   
		      

		    if(EXOGAM_AC_hit[jjj]!=1){
		      spec1[5]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
		      spec1[5]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
		      spec1[6]->incr((G4int)(eAnCryst[jjj][1]*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
		      spec1[6]->incr((G4int)(eAnCryst[jjj][3]*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
		      /*// if in addition, a third crystal is hit (ie: M=3):
			if(eAnCryst[jjj][0]>0.&& eAnCryst[jjj][2]==0.) 
			{
			spec1[5]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			spec1[6]->incr((G4int)(eAnCryst[jjj][0]*scaleEtot), 0);   //> the total energy of the array EXOGAM only			  
			}
			if(eAnCryst[jjj][2]>0. && eAnCryst[jjj][0]==0.)
			{
			spec1[5]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
			spec1[6]->incr((G4int)(eAnCryst[jjj][2]*scaleEtot), 0);   //> the total energy of the array EXOGAM only
			}
		      */   
		    }
		  }

		edepTotAn=edummyAn[jjj]=0.; // to avoid double counting
 
	      }else   // ie: event with crystal Multiplicity =2 in a same clover
	      {
		//cout << "edummyAn[jjj]: " << edummyAn[jjj] << endl;
		if( edummyAn[jjj] > 0. ) {  // 0 keV threshold
		  //edummyAn = offset + gain * edummyAn;
		  //cout << "edummyAn[jjj]=" << jjj << " " << edummyAn[jjj] << endl;
		  //cout << "EXOGAM_AC_hit[jjj]=" << EXOGAM_AC_hit[jjj] <<endl;

		  //edummyAn[jjj] = offset + gain * edummyAn[jjj];  //

		  // For Doppler correction from central contact only:
		  //if(CrystFirst==0 || CrystFirst==1)edummyAn[jjj]= edummyAn[jjj]*(1+Beta*cos(83.1*3.14159/180.))/sqrt(1-Beta*Beta); // A&B backward
		  //if(CrystFirst==2 || CrystFirst==3)edummyAn[jjj]= edummyAn[jjj]*(1-Beta*cos(83.1*3.14159/180.))/sqrt(1-Beta*Beta); // C&D forward
		  // For Doppler correction from segment:
		  if(CrystFirst==0 || CrystFirst==1)
		    {
		      if(SegFirst==0 || SegFirst==3 )edummyAn[jjj]= edummyAn[jjj]*(1+Beta*cos((82.5+0.)*3.14159/180.))/sqrt(1-Beta*Beta); // A&B backward
		      if(SegFirst==1 || SegFirst==2 )edummyAn[jjj]= edummyAn[jjj]*(1+Beta*cos((82.5-0)*3.14159/180.))/sqrt(1-Beta*Beta); // A&B backward
		    }
		  if(CrystFirst==2 || CrystFirst==3)
		    {
		      if(SegFirst==0 || SegFirst==3 )edummyAn[jjj]= edummyAn[jjj]*(1-Beta*cos((82.5-0)*3.14159/180.))/sqrt(1-Beta*Beta); // C&D forward
		      if(SegFirst==1 || SegFirst==2 )edummyAn[jjj]= edummyAn[jjj]*(1-Beta*cos((82.5+0)*3.14159/180.))/sqrt(1-Beta*Beta); // C&D forward
		    }
 
		  if(edummyAn[jjj]) {
		    edepTotAn += edummyAn[jjj];  // calorimeter mode
		    nd_An++;
		  }

		  spec1[0]->incr((G4int)(edummyAn[jjj]*scaleEtot), 0);   // For 2 adjacent hit crystals fill > the total energy of the array AGATA +EXOGAM
		  spec1[4]->incr((G4int)(edummyAn[jjj]*scaleEtot), 0);   // For 2 adjacent hit crystals fill > the total energy of the array EXOGAM only


		}
	      }
	  }  // end of loop on jjj
	}



      //cout << "edepTotAn: " << edepTotAn << endl;

      if (edepTotAn>0) {
	/*
	  spec1[0]->incr((G4int)(edepTotAn*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM
	  spec1[4]->incr((G4int)(edepTotAn*scaleEtot), 0);   //> the total energy of the array EXOGAM only
	  cout << EXOGAM_AC_hit << endl;
	  if(EXOGAM_AC_hit==false){
	  spec1[5]->incr((G4int)(edepTotAn*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
	  spec1[6]->incr((G4int)(edepTotAn*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
	  }
	*/

	for (G4int kk=0; kk<8; kk++){  
	  //spec1[0]->incr((G4int)(edummyAn[kk]*scaleEtot), 0);   //> the total energy of the array AGATA +EXOGAM // This is now filled above, for crystal Multiplicity 1 and 2
	  //spec1[4]->incr((G4int)(edummyAn[kk]*scaleEtot), 0);   //> the total energy of the array EXOGAM only  // This is now filled above, for crystal Multiplicity 1 and 2

	  if(EXOGAM_AC_hit[kk]!=1){
	    spec1[5]->incr((G4int)(edummyAn[kk]*scaleEtot), 0);   //> the total energy of the array EXOGAM with compton suppresion
	    spec1[6]->incr((G4int)(edummyAn[kk]*scaleEtot), 0);   //> AGATA + EXOGAM with compton suppresion
	  }
	}


      }       
    }

  // end of Ancillary 

  if(minClu > maxClu)
    return;

  G4int nc = 0;
  for(ii = minClu; ii <= maxClu; ii++) {
    edummy   = cluAccu[ii];
    if(edummy) {
      spec1[2]->incr((G4int)edummy, ii);             //> the energy of the clusters
      nc++;
    }  
  }

  //spec2[0]->incr((int)(edepTot*0.1), nd);

}

void AgataAnalysis::FlushSpectra( G4String workPath )
{
  if( !makeSpectra ) return;
  ////////////////////////////////////////////
  /// Write out spectra (packed GASP format)
  ////////////////////////////////////////////
  G4int size = spec1.size();
  G4int nn;
  for(nn = 0; nn <size; nn++) {
    if(spec1[nn]) {
      sprintf(aLine, "/mnt/hgfs/Echanges/4Agata/%sG%2.2d.%4.4d", workPath.c_str(), nn, runNumber);
      if( asciiSpectra )
        spec1[nn]->writeA(aLine);
      else  
        spec1[nn]->write(aLine);
    }
  }

  // uncomment if matrices are needed!
  /*
    size = spec2.size();
    for(nn = 0; nn < size; nn++) {
    if(spec2[nn]) {
    sprintf(aLine, "M%2.2d.%4.4d", nn, runNumber);
    spec2[nn]->write(aLine);
    }
    }
  */
}


/////////////////////////////////////////////////////////////
//// Management of the list-mode event (GASP format) ////////
/////////////////////////////////////////////////////////////
void AgataAnalysis::WriteGaspEvent()
{
  if( !makeBuffer ) return;

  memset( gaspEvent, 0, 2048*sizeof(short int) );
  
  unsigned short int nGd=0, nAncTot=0;
  
  unsigned short int* nAnc = new unsigned short int[nInstances];
  memset( nAnc, 0, nInstances*sizeof(unsigned short int));
  
  // Calculates event length
  G4int ii, jj, kk;
  G4int evLength = 1;       //   Ge  (2 par: energy, time) 
  if( nInstances ) 
    evLength += nInstances; // + ancillary detectors
  //cout << " WGE " << minAnAccu << " " << maxAnAccu << endl;
  //cout << " nInstances: " <<  nInstances << " minGeAccu: " << minAnAccu << " maxAnAccu: " << maxAnAccu << endl;
  //cout << " nDetectors: " <<  nDetectors << endl;
  //G4cout << " nInstances: " <<  nInstances << " minGeAccu: " << minAnAccu << " maxAnAccu: " << maxAnAccu << G4endl;
  //G4cout << " nDetectors: " <<  nDetectors << G4endl;

  if( nDetectors ) {
    for( ii=minGeAccu; ii<=maxGeAccu; ii++ ) {
      if( geAccu[ii] > 0. ) {
        evLength += 3;            // number, ge energy, ge time
        nGd++;
      }  
    }
  }
  if( nInstances ) {
    for( jj=0; jj<nInstances; jj++ ) {          // cycle on ancillary detectors
    
      
      //cout << " WGE " << minAnAccu << " " << maxAnAccu << endl;
      for( ii=minAnAccu; ii<=maxAnAccu; ii++ ) {  // cycle on detector number
        
	if( agAccu[ii+nDetectors*jj] > 0 ) {
	  evLength += (parLut[jj]+1);     // ndet + x parameters
	  nAncTot++;
	  nAnc[jj]++;
	}  
      }
    }
  }

  //cout << " EoE " << nInstances << " " << nGd << " " << nAncTot << endl;
  if( !(nGd+ancillaryOnly*nAncTot) )
    return;
  
  unsigned short int length = evLength;
  position = 0;
  
  if( evLength ) {  // begin of event tag
    gaspEvent[position++] = (unsigned short int)0xf000+length;
    gaspEvent[position++] = nGd;
    
    if( nAncTot ) {
      for( jj=0; jj<nInstances; jj++ )
        gaspEvent[position++] = nAnc[jj];
    }
    //if( nAncTot ) {
    //   for( jj=0; jj<nInstances; jj++ )
    //    cout << nAnc[jj] << endl;
    //}
  }
  
  
  if( nGd ) {
    for( G4int ng=minGeAccu; ng<=maxGeAccu; ng++ ) {
      if( geAccu[ng] > fThreshold  ) {
        gaspEvent[position++] = (unsigned short int)ng;
        gaspEvent[position++] = (unsigned short int)floor(geAccu[ng]/gf1);
        gaspEvent[position++] = (unsigned short int)floor(gtAccu[ng]/gf2);
      }  
    }
  }
  
  if( nAncTot ) {
    for( jj=0; jj<nInstances; jj++ ) {
      //cout << "   " << nAnc[jj] << " " << minAnAccu << " " << maxAnAccu << endl;
      if( nAnc[jj] > 0 ) {
        for( G4int na=minAnAccu; na<=maxAnAccu; na++ ) {
	  if( agAccu[na+nDetectors*jj] > 0 ) {
            gaspEvent[position++] = (unsigned short int)na;
	    for( kk=0; kk<parLut[jj]; kk++ ) {
	      gaspEvent[position++] = (unsigned short int)floor(anAccu[na+(kk+minLut[jj])*nDetectors]/gf3);
	    }
	  } 
	}
      }
    }
  }

  gaspBuffer->AddToBuffer( gaspEvent, position );
}



/////////////////////////////////////////////////////////////////////////
// Methods for the Messenger
/////////////////////////////////////////////////////////////////////////

void AgataAnalysis::EnableAnalysis( G4bool enable )
{
  enabledAnalysis = enable;

  if(enabledAnalysis)
    cout << " ----> On-line analysis enabled!" << endl;
  else
    cout << " ----> On-line analysis disabled!" << endl;
}

void AgataAnalysis::EnableSpectra( G4bool enable )
{
  makeSpectra = enable;

  if(makeSpectra)
    cout << " ----> On-line spectra enabled!" << endl;
  else
    cout << " ----> On-line spectra disabled!" << endl;
}

void AgataAnalysis::EnableBuffer( G4bool enable )
{
  makeBuffer = enable;

  if(makeBuffer)
    cout << " ----> Events will be written out in REDUCED GASP format!" << endl;
  else
    cout << " ----> Events will not be written out in REDUCED GASP format!" << endl;
}

void AgataAnalysis::EnableResolution(G4bool enable )
{
  enabledResolution = enable;

  if(enabledResolution)
    cout << " ----> Energy resolution will be considered!" << endl;
  else
    cout << " ----> Energy resolution will not be considered!" << endl;
}

void AgataAnalysis::CalculateResolution( G4double e1, G4double f1, G4double e2, G4double f2, G4bool print)
{
  if( e1 != e2 )
    {
      FWHMb = ( pow(f1, 2.)-pow(f2, 2.) )/( e1-e2 );
      FWHMa = pow(f1, 2.) - FWHMb*e1;
      if(!print) return;
      sprintf(aLine, " FWHM = %8.3f keV   at  %8.3f keV \n FWHM = %8.3f keV   at  %8.3f keV", f1, e1, f2, e2);
      cout << aLine << endl;
      sprintf(aLine, " a = %f\n b = %f", FWHMa, FWHMb);
      cout << aLine << endl;
    }
}

void AgataAnalysis::SetPackEvent( G4bool value )
{
  packEvent = value;
  if(packEvent)
    cout << " ----> Packing gammas of cascade is enabled" << endl;
  else
    cout << " ----> Packing gammas of cascade is disabled" << endl;
}

void AgataAnalysis::SetAncillaryOnly( G4bool value )
{
  if( value )
    ancillaryOnly = 1;
  else
    ancillaryOnly = 0;  
  
  if( value )
    cout << " ----> Writing out ancillary data also when no germanium is firing" << endl;
  else  
    cout << " ----> Writing out ancillary data only when at least a germanium is firing" << endl;
}

void AgataAnalysis::ShowStatus()
{
  if( !enabledAnalysis )
    return;
  
  cout << endl;
  if(makeSpectra) {
    cout << " On-line spectra enabled" << endl;
    if(asciiSpectra)
      cout << " Spectra written in ASCII format." << endl;
    else   
      cout << " Spectra written in BINARY format." << endl;
    
    cout << " Spectra with " << specLength << " channels"  << endl;    

    sprintf(aLine, " Offset = %f\n Gain   = %f",offset, gain);
    cout << aLine << endl;
  }
  else
    cout << " On-line spectra disabled" << endl; 

  if(makeBuffer) {
    cout << " Writing out events in reduced GASP format at " << gf1/keV << " keV/ch." << endl;
  }
  else {
    cout << " File in reduced GASP format will NOT be produced." << endl;
  }

  if(enabledResolution) { 
    cout << " Energy resolution will be applied" << endl;
    sprintf(aLine, " Energy resolution coefficients  a = %f   b = %f", FWHMa, FWHMb);
    cout << aLine << endl;
  }  
  else
    cout << " Energy resolution will not be considered" << endl; 

  if(packEvent)
    cout << " Packing gammas of cascade is enabled" << endl;
  else
    cout << " Packing gammas of cascade is disabled" << endl;

  if(enabledAnalysis)
    cout << " On-line analysis enabled" << endl;
  else
    cout << " On-line analysis disabled" << endl;

}

void AgataAnalysis::SetMaxFileSize( G4int size )
{
  if( size < 0 ) {
    cout << " ----> Invalid value, keeping previous limit ("
	 << maxFileSize/1048576 << " MB)" << endl;
  }
  else if( size > 2048 ) {  // 2GB
    cout << " ----> Value out of range, resetting to maximum value." << endl;
    maxFileSize = 2040109465; // 95% of 2GB
  }
  else {
    maxFileSize = (G4int)( 0.95 * size * 1048576 );
    cout << " ----> Maximum file size has been set to " << size << " MB." << endl;
  }
}

void AgataAnalysis::SetEnerGain( G4double value )
{
  if( value > 0. )
    gf1 = value * keV;
  else
    gf1 = 1.0 * keV;
  cout << " ----> Energy Gain = " << gf1/keV << " keV/ch" << endl;    
}

void AgataAnalysis::SetTimeGain( G4double value )
{
  if( value > 0. )
    gf2 = value * ns;
  else
    gf2 = 1.0 * ns;
  cout << " ----> Time Gain = " << gf2/ns << " ns/ch" << endl;    
}

void AgataAnalysis::SetFThreshold( G4double value )
{
  if( value > 1. )
    fThreshold = value * keV;
  else
    fThreshold = 1.0 * keV;
  cout << " ----> Threshold for output to file = " << fThreshold/keV << " keV" << endl;    
}

void AgataAnalysis::SetOffset( G4double value )
{
  offset = value;
  cout << " ----> Offset = " << offset << endl;
}

void AgataAnalysis::SetGain( G4double value )
{
  if( value > 0. )
    gain = value;
  else
    gain = 1.0;
  cout << " ----> Gain = " << gain << endl;    
}

void AgataAnalysis::SetAsciiSpectra( G4bool value )
{
  asciiSpectra = value;
  if(asciiSpectra)
    cout << " ----> Spectra will be written in ASCII format." << endl;
  else
    cout << " ----> Spectra will be written in BINARY format." << endl;
}

void AgataAnalysis::SetSpecLength( G4int value )
{
  if( value > 0 )
    specLength = value * 1024;
  else
    specLength = 1024;
  cout << " ----> Spectra with " << specLength << " channels"  << endl;    
}

/////////////////////////////////////////////////////////////////////////
// The Messenger

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"

#include "G4UIcmdWithoutParameter.hh"

AgataAnalysisMessenger::AgataAnalysisMessenger(AgataAnalysis* pTarget, G4String name)
  :myTarget(pTarget)
{
  const char *aLine;
  G4String commandName;
  G4String directoryName = name + "/analysis/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of on-line analysis.");
  ///////////////////////////////////////////////////////////////////////
  //// General commands /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enable";
  aLine = commandName.c_str();
  EnableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  EnableAnalysisCmd->SetGuidance("Activate the on-line analysis");
  EnableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  EnableAnalysisCmd->SetDefaultValue(true);
  EnableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disable";
  aLine = commandName.c_str();
  DisableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  DisableAnalysisCmd->SetGuidance("Deactivate the on-line analysis");
  DisableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  DisableAnalysisCmd->SetDefaultValue(false);
  DisableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionDisable";
  aLine = commandName.c_str();
  DisableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  DisableResolutionCmd->SetGuidance("Disable application of energy resolution");
  DisableResolutionCmd->SetParameterName("enabledResolution",true);
  DisableResolutionCmd->SetDefaultValue(false);
  DisableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionEnable";
  aLine = commandName.c_str();
  EnableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  EnableResolutionCmd->SetGuidance("Enable application of energy resolution");
  EnableResolutionCmd->SetParameterName("enabledResolution",true);
  EnableResolutionCmd->SetDefaultValue(true);
  EnableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "calculateResolution";
  aLine = commandName.c_str();
  CalculateResolutionCmd = new G4UIcmdWithAString(aLine, this);
  CalculateResolutionCmd->SetGuidance("Calculate parameters of the energy resolution");
  CalculateResolutionCmd->SetGuidance("starting from FWHM values at two energies");
  CalculateResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "packGammas";
  aLine = commandName.c_str();
  PackEventCmd = new G4UIcmdWithABool(aLine, this);
  PackEventCmd->SetGuidance("Activate packing the gammas of a cascade");
  PackEventCmd->SetParameterName("packEvent",true);
  PackEventCmd->SetDefaultValue(true);
  PackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "unpackGammas";
  aLine = commandName.c_str();
  UnpackEventCmd = new G4UIcmdWithABool(aLine, this);
  UnpackEventCmd->SetGuidance("Deactivate packing the gammas of a cascade");
  UnpackEventCmd->SetParameterName("packEvent",true);
  UnpackEventCmd->SetDefaultValue(false);
  UnpackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fileSize";
  aLine = commandName.c_str();
  SetFileSizeCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetFileSizeCmd->SetGuidance("Set maximum size for the list-mode file.");
  SetFileSizeCmd->SetGuidance("Required parameters: 1 integer (file size in MB).");
  SetFileSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ///////////////////////////////////////////////////////////////////////
  //// Spectra          /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enableSpectra";
  aLine = commandName.c_str();
  EnableSpectraCmd = new G4UIcmdWithABool(aLine, this);
  EnableSpectraCmd->SetGuidance("Activate the on-line spectra");
  EnableSpectraCmd->SetParameterName("makeSpectra",true);
  EnableSpectraCmd->SetDefaultValue(true);
  EnableSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableSpectra";
  aLine = commandName.c_str();
  DisableSpectraCmd = new G4UIcmdWithABool(aLine, this);
  DisableSpectraCmd->SetGuidance("Deactivate the on-line spectra");
  DisableSpectraCmd->SetParameterName("makeSpectra",true);
  DisableSpectraCmd->SetDefaultValue(false);
  DisableSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "asciiSpectra";
  aLine = commandName.c_str();
  SetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  SetAsciiSpectraCmd->SetGuidance("Enables ASCII writing in the on-line analysis.");
  SetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  SetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  SetAsciiSpectraCmd->SetDefaultValue(true);
  SetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "binarySpectra";
  aLine = commandName.c_str();
  UnsetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  UnsetAsciiSpectraCmd->SetGuidance("Enables BINARY writing in the on-line analysis.");
  UnsetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  UnsetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  UnsetAsciiSpectraCmd->SetDefaultValue(false);
  UnsetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "specLength";
  aLine = commandName.c_str();
  SetSpecLengthCmd = new G4UIcmdWithAnInteger(aLine, this);  
  SetSpecLengthCmd->SetGuidance("Define number of channels of spectra");
  SetSpecLengthCmd->SetGuidance("Required parameters: 1 integer (spectra length as a multiple of 1024 channels)");
  SetSpecLengthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "specOffset";
  aLine = commandName.c_str();
  SetOffsetCmd = new G4UIcmdWithADouble(aLine, this);  
  SetOffsetCmd->SetGuidance("Define offset of the energy calibration for the on-line spectra.");
  SetOffsetCmd->SetGuidance("Required parameters: 1 double.");
  SetOffsetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "specGain";
  aLine = commandName.c_str();
  SetGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetGainCmd->SetGuidance("Define gain of the energy calibration for the on-line spectra.");
  SetGainCmd->SetGuidance("Required parameters: 1 double.");
  SetGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ///////////////////////////////////////////////////////////////////////
  //// Buffer           /////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enableBuffer";
  aLine = commandName.c_str();
  EnableBufferCmd = new G4UIcmdWithABool(aLine, this);
  EnableBufferCmd->SetGuidance("Activate the on-line analysis");
  EnableBufferCmd->SetParameterName("makeBuffer",true);
  EnableBufferCmd->SetDefaultValue(true);
  EnableBufferCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableBuffer";
  aLine = commandName.c_str();
  DisableBufferCmd = new G4UIcmdWithABool(aLine, this);
  DisableBufferCmd->SetGuidance("Deactivate the on-line analysis");
  DisableBufferCmd->SetParameterName("makeBuffer",true);
  DisableBufferCmd->SetDefaultValue(false);
  DisableBufferCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ancillaryOnly";
  aLine = commandName.c_str();
  AncillaryOnlyCmd = new G4UIcmdWithABool(aLine, this);
  AncillaryOnlyCmd->SetGuidance("Activate writing out of ancillary data for events without germanium firing");
  AncillaryOnlyCmd->SetParameterName("ancillaryOnly",true);
  AncillaryOnlyCmd->SetDefaultValue(true);
  AncillaryOnlyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ancillaryCoinc";
  aLine = commandName.c_str();
  AncillaryCoincCmd = new G4UIcmdWithABool(aLine, this);
  AncillaryCoincCmd->SetGuidance("Deactivate writing out of ancillary data for events without germanium firing");
  AncillaryCoincCmd->SetParameterName("ancillaryOnly",true);
  AncillaryCoincCmd->SetDefaultValue(false);
  AncillaryCoincCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "egain";
  aLine = commandName.c_str();
  SetEnerGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetEnerGainCmd->SetGuidance("Define gain of the energy calibration (keV/ch).");
  SetEnerGainCmd->SetGuidance("Required parameters: 1 double.");
  SetEnerGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "tgain";
  aLine = commandName.c_str();
  SetTimeGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetTimeGainCmd->SetGuidance("Define gain of the time calibration (ns/ch).");
  SetTimeGainCmd->SetGuidance("Required parameters: 1 double.");
  SetTimeGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fThreshold";
  aLine = commandName.c_str();
  SetFThresholdCmd = new G4UIcmdWithADouble(aLine, this);  
  SetFThresholdCmd->SetGuidance("Define threshold for output to file (keV).");
  SetFThresholdCmd->SetGuidance("Required parameters: 1 double.");
  SetFThresholdCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataAnalysisMessenger::~AgataAnalysisMessenger()
{
  delete myDirectory;
  delete EnableAnalysisCmd;
  delete DisableAnalysisCmd;
  delete EnableResolutionCmd;
  delete DisableResolutionCmd;
  delete CalculateResolutionCmd;
  delete PackEventCmd;
  delete UnpackEventCmd;
  delete SetFileSizeCmd;
  delete StatusCmd;

  delete EnableSpectraCmd;
  delete DisableSpectraCmd;
  delete SetAsciiSpectraCmd;
  delete UnsetAsciiSpectraCmd;
  delete SetSpecLengthCmd;
  delete SetOffsetCmd;
  delete SetGainCmd;
  
  delete EnableBufferCmd;
  delete DisableBufferCmd;
  delete AncillaryOnlyCmd;
  delete AncillaryCoincCmd;
  delete SetEnerGainCmd;
  delete SetTimeGainCmd;
  delete SetFThresholdCmd;
}

void AgataAnalysisMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{

  if( command == EnableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableResolutionCmd ) {
    myTarget->EnableResolution( EnableResolutionCmd->GetNewBoolValue(newValue)  );
  }
  if( command == DisableResolutionCmd ) {
    myTarget->EnableResolution( DisableResolutionCmd->GetNewBoolValue(newValue)  );
  }
  if( command == CalculateResolutionCmd ) { 
    G4double e1, e2, f1, f2;
    sscanf( newValue, " %lf %lf %lf %lf ", &e1, &f1, &e2, &f2 );
    myTarget->CalculateResolution(e1, f1, e2, f2 );
  }
  if( command == PackEventCmd ) {
    myTarget->SetPackEvent( PackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnpackEventCmd ) {
    myTarget->SetPackEvent( UnpackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetFileSizeCmd ) {
    myTarget->SetMaxFileSize( SetFileSizeCmd->GetNewIntValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus( );
  }
  
  if( command == EnableSpectraCmd ) {
    myTarget->EnableSpectra( EnableSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableSpectraCmd ) {
    myTarget->EnableSpectra( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( SetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnsetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( UnsetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetSpecLengthCmd ) {
    myTarget->SetSpecLength(SetSpecLengthCmd->GetNewIntValue(newValue));
  }
  if( command == SetOffsetCmd ) {
    myTarget->SetOffset(SetOffsetCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetGainCmd ) {
    myTarget->SetGain(SetGainCmd->GetNewDoubleValue(newValue));
  }

  if( command == EnableBufferCmd ) {
    myTarget->EnableBuffer( EnableBufferCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableBufferCmd ) {
    myTarget->EnableBuffer( EnableBufferCmd->GetNewBoolValue(newValue) );
  }
  if( command == AncillaryOnlyCmd ) {
    myTarget->SetAncillaryOnly( AncillaryOnlyCmd->GetNewBoolValue(newValue) );
  }
  if( command == AncillaryCoincCmd ) {
    myTarget->SetAncillaryOnly( AncillaryCoincCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetEnerGainCmd ) {
    myTarget->SetEnerGain(SetEnerGainCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetTimeGainCmd ) {
    myTarget->SetTimeGain(SetTimeGainCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetFThresholdCmd ) {
    myTarget->SetFThreshold(SetFThresholdCmd->GetNewDoubleValue(newValue));
  }

}
