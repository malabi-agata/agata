#ifdef ANCIL
// this code was created by grzegorz jaworski -> tatrofil@slcj.uw.edu.pl
#include "AgataAncillaryNordBallNDet.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4Polyhedra.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4Box.hh"

using namespace std;

AgataAncillaryNordBallNDet::AgataAncillaryNordBallNDet(G4String path, G4String name)
{
  G4String iniPath     = path;
  dirName = name;

  useAlCans = false;

  distanceToTheTarget =   5.   * cm;
  aluminiumThickness  =   2.   * mm;
  
  theSmallDistance      =   1.   * nm;

  rmZero.set(0,0,0);
  
  nameCan    = "Steel";
  matCan     =  NULL;
  nameScint  = "BC501A"; //sklad chemiczny BC501 ?!
  //nameScint  = "BC537";
  Scint      =  NULL;
  nameBrick  = "Lead";
  matBrick   = NULL;

  
  ancSD = NULL;

  ancName   = G4String("NordBallNDet");
  ancOffset = 23000;

  numAncSd = 0;

  myMessenger = new AgataAncillaryNordBallNDetMessenger(this,name);
}

AgataAncillaryNordBallNDet::~AgataAncillaryNordBallNDet()
{
  delete myMessenger;
}


G4int AgataAncillaryNordBallNDet::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(nameCan);
  if (ptMaterial){
    matCan = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
	   << matCan->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameCan << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameScint);
  if (ptMaterial){
    Scint = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
	   << Scint->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameScint << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameBrick);
  if (ptMaterial){
    matBrick = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
 	   << matBrick->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameBrick << G4endl;
     G4cout << " Could not build the ancillary! " << G4endl;
     return 1;
  }   
  return 0;
}


void AgataAncillaryNordBallNDet::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillaryNordBallNDet::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}


void AgataAncillaryNordBallNDet::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryNordBallNDet::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName,"/anc/NordBallNDet", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

void AgataAncillaryNordBallNDet::PlaceDetector(){
  G4double detector_thickness    = 170. *mm;
  G4double back_part_thickness   = 114. *mm;
  G4double front_face_diagonal   = 150. *mm; //75
  G4double middle_plane_diagonal = 196. *mm; //98
  G4double back_plane_diagonal   = 196. *mm; //98
  G4double r_inner[]={0,0,0,0,0,0};
  G4double r_outer[]={0,.5*sqrt(3)*.5*back_plane_diagonal,.5*sqrt(3)*.5*middle_plane_diagonal,.5*sqrt(3)*.5*front_face_diagonal};
  G4double z_planes[]={0,0,back_part_thickness,detector_thickness};

  /*  G4Polyhedra *polyh_big = new G4Polyhedra("",0.*deg,360.*deg,6,4,z_planes,r_inner,r_outer);*/
  r_outer[1]-=aluminiumThickness;r_outer[2]-=aluminiumThickness;r_outer[3]-=aluminiumThickness;
  z_planes[3]-=aluminiumThickness;
  G4Polyhedra *polyh_liq = new G4Polyhedra("",0.*deg,360.*deg,6,4,z_planes,r_inner,r_outer);
  /*G4SubtractionSolid *polyh_alu = 
    new G4SubtractionSolid("al_can",polyh_big,polyh_liq,0,G4ThreeVector(0,0,-.5*aluminiumThickness));*/

  /*G4LogicalVolume *log_can = new G4LogicalVolume(polyh_alu,matCan,"");*/
  G4LogicalVolume *log_det =  new G4LogicalVolume(polyh_liq,Scint,"");
  
  //  can = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,-(distanceToTheTarget+detector_thickness))),"can",log_can,theDetector->HallPhys(), false, 0);
  
  new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,-(distanceToTheTarget+detector_thickness))),
			  "det",log_det,theDetector->HallPhys(), false, 70);
  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));//zielony
  zielony->SetVisibility(true);log_det->SetVisAttributes(zielony);
  log_det->SetSensitiveDetector(ancSD);
}

void AgataAncillaryNordBallNDet::PlaceSwierkDetector(){
  G4double det_diameter  = 153. *mm;
  G4double det_thickness = 135. *mm;

  /*  G4Tubs *tube_big = new G4Tubs("",0.,.5*det_diameter,.5*det_thickness,0.*deg,360.*deg);*/
  G4Tubs *tube_liq = new G4Tubs("",0.,.5*det_diameter-aluminiumThickness,.5*(det_thickness-aluminiumThickness),0.*deg,360.*deg);
  /*G4SubtractionSolid *tube_alu = new G4SubtractionSolid("al_can",tube_big,tube_liq,0,G4ThreeVector(0,0,-aluminiumThickness));*/

  /*  G4LogicalVolume *log_can =  new G4LogicalVolume(tube_alu,matCan,"");*/
  G4LogicalVolume *log_det =  new G4LogicalVolume(tube_liq,Scint,"");
  
  //  can = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,-(distanceToTheTarget+.5*det_thickness))),"can",log_can,theDetector->HallPhys(), false, 0);
  new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,-(distanceToTheTarget+.5*det_thickness))),
			  "det",log_det,theDetector->HallPhys(), false, 80);
  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));//zielony
  zielony->SetVisibility(true);log_det->SetVisAttributes(zielony);
  log_det->SetSensitiveDetector(ancSD);  
}

void AgataAncillaryNordBallNDet::PlaceAlCans(){}

void AgataAncillaryNordBallNDet::PlaceGarbageConcrete(){
  //CONCRETE
  /*  G4double inch = 25.4*mm;*/
  G4double size_x = .5*m/2.;
  G4double size_y = 2.*m/2.;
  G4double size_z = 5.*m/2.;

  G4double pos_x = 0.5*m+size_x;
  G4double pos_y = 0.*cm;
  G4double pos_z = 0.*cm;//size_z+1.*mm;

  G4Box  *sBrick = new G4Box( "brick", size_x, size_y+40*cm, size_z );
  G4LogicalVolume *lBrick = new G4LogicalVolume( sBrick, matBrick, "Brick", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(pos_x,pos_y,pos_z), "Brick", lBrick, theDetector->HallPhys(), true, 0 );

  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(1.0, 1.0, 1.0) );
  lBrick->SetVisAttributes( pVA );
  
  pos_x= 0*cm;
  pos_y= 1.2*m+size_x;
  pos_z=-0.*mm;//2*size_z;
  
  G4Box  *sBrick2 = new G4Box( "brick", size_y, size_x, size_z );
  G4LogicalVolume *lBrick2 = new G4LogicalVolume( sBrick2, matBrick, "Brick", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(pos_x,pos_y,pos_z), "Brick", lBrick2, theDetector->HallPhys(), true, 0 );
  
}

void AgataAncillaryNordBallNDet::PlaceGarbageLead(){
  //lead/concrete brick nearby
  G4double inch = 25.4*mm;
  G4double size_x = 40.*inch/2.;
  G4double size_y = 80.*inch/2.;
  G4double size_z = 20.*inch/2.;

  G4double pos_x = 0.*cm;
  G4double pos_y = 0.*cm;
  G4double pos_z =size_z+1.*mm;

  G4Box  *sBrick = new G4Box( "brick", size_x, size_y, size_z );
  G4LogicalVolume *lBrick = new G4LogicalVolume( sBrick, matBrick, "Brick", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(pos_x,pos_y,pos_z), "Brick", lBrick, theDetector->HallPhys(), true, 0 );

  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(1.0, 1.0, 1.0) );
  lBrick->SetVisAttributes( pVA );
  
  pos_x= 0*cm;
  pos_y= size_z+20*cm;
  pos_z=-2*size_z;
  
  G4Box  *sBrick2 = new G4Box( "brick", size_x, size_z, size_y );
  G4LogicalVolume *lBrick2 = new G4LogicalVolume( sBrick2, matBrick, "Brick", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(pos_x,pos_y,pos_z), "Brick", lBrick2, theDetector->HallPhys(), true, 0 );
  

}

G4int AgataAncillaryNordBallNDet::GetSegmentNumber(G4int offset, G4int detCode, G4ThreeVector position)
{
  //just to avoid warnings:
  offset=detCode;position=G4ThreeVector(0,0,0);
  return 0*offset;
}

void AgataAncillaryNordBallNDet::Placement()
{
  //  PlaceDetector();
  //
  //  PlaceGarbageLead();
  PlaceSwierkDetector();
  //  if(useAlCans)
  //    PlaceAlCans();
}


void AgataAncillaryNordBallNDet::ShowStatus(){}

void AgataAncillaryNordBallNDet::SetUseAlCans( G4bool value )
{
  useAlCans = value;
  if( useAlCans )
    G4cout << " ----> Aluminium cans of scintillator will be used." << G4endl;
  else  
    G4cout << " ----> Aluminium cans of scintillator wont be generated." << G4endl;
}

void AgataAncillaryNordBallNDet::SetDistanceToTheTarget(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << distanceToTheTarget/cm << " cm)" << G4endl;
  else
    distanceToTheTarget = length;
}

void AgataAncillaryNordBallNDet::SetAluminiumThickness(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << aluminiumThickness/cm << " cm)" << G4endl;
  else
    aluminiumThickness = length;
}


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillaryNordBallNDetMessenger::AgataAncillaryNordBallNDetMessenger(AgataAncillaryNordBallNDet* pTarget, G4String name)
  :myTarget(pTarget)
{ 
  G4String commandName;
  G4String directoryName;
  

  directoryName = name + "/detector/ancillary/NordBallNDet/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of NordBall NDet construction.");  

  //al cans
  commandName = directoryName + "enableAlCans";
  enableAlCans = new G4UIcmdWithABool(commandName, this);
  enableAlCans->SetGuidance("Creating aluminium cans for scintillators.");
  enableAlCans->SetGuidance("Required parameters: none.");
  enableAlCans->SetParameterName("useAlCans",true);
  enableAlCans->SetDefaultValue(true);
  enableAlCans->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "disableAlCans";
  disableAlCans = new G4UIcmdWithABool(commandName, this);
  disableAlCans->SetGuidance("Aluminium cans for scintillators wont be generated.");
  disableAlCans->SetGuidance("Required parameters: none.");
  disableAlCans->SetParameterName("useAbsorbers",true);
  disableAlCans->SetDefaultValue(false);
  disableAlCans->AvailableForStates(G4State_PreInit,G4State_Idle);

  //changeable parameters
  commandName = directoryName + "DistanceToTheTarget";
  DistanceToTheTarget =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  DistanceToTheTarget -> SetGuidance("Sets distance between target and detector's face.");
  DistanceToTheTarget -> SetParameterName("Size",false);
  DistanceToTheTarget -> SetRange("Size>0.");
  DistanceToTheTarget -> SetUnitCategory("Length");
  DistanceToTheTarget -> AvailableForStates(G4State_Idle);
  
  commandName = directoryName + "AluminiumThickness";
  AluminiumThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  AluminiumThickness -> SetGuidance("Sets thickness of aluminium cans.");
  AluminiumThickness -> SetParameterName("Size",false);
  AluminiumThickness -> SetRange("Size>0.");
  AluminiumThickness -> SetUnitCategory("Length");
  AluminiumThickness -> AvailableForStates(G4State_Idle);
}

AgataAncillaryNordBallNDetMessenger::~AgataAncillaryNordBallNDetMessenger()
{
  delete  enableAlCans;
  delete disableAlCans;
  delete DistanceToTheTarget;
  delete AluminiumThickness;
}

void AgataAncillaryNordBallNDetMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == enableAlCans ) 
    myTarget->SetUseAlCans( enableAlCans->GetNewBoolValue(newValue) );
  if( command == disableAlCans ) 
    myTarget->SetUseAlCans( disableAlCans->GetNewBoolValue(newValue) );
  if( command == DistanceToTheTarget )
    myTarget->SetDistanceToTheTarget(DistanceToTheTarget->GetNewDoubleValue(newValue));
  if( command == AluminiumThickness )
    myTarget->SetAluminiumThickness(AluminiumThickness->GetNewDoubleValue(newValue));
}

#endif
