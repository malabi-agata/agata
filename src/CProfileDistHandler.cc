#include "CProfileDistHandler.hh"
#include "Randomize.hh"

#ifdef G4V10
using namespace CLHEP;
#endif

ProfileDistHandler::ProfileDistHandler( G4String inFileName )
{
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open Profile file " << inFileName << G4endl;
//            << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;
//   G4double th_min, th_max, dth, e_min, e_max, de;
//   inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
//   this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  G4cout << "Reading " << inFileName << G4endl;
  this->ReadDistribution(inputFile);
  inputFile.close();
  
//   phi_min =   0.*deg;
//   phi_dif = 360.*deg;
//   
//   fixedTheta = false;
    
}

void ProfileDistHandler::ReNew( G4String inFileName )
{
//   for( G4int ii=0; ii<(G4int)difProfileDist.size(); ii++ ) {
//     difProfileDist[ii].clear();
//     intProfileDist[ii].clear();  
//   }
  thickVal.clear();
  thickPb.clear();
  thickPbNorm.clear();
  intProfileDist.clear();
  
  goodDistribution = false;	   
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open Profile distribution file " << inFileName << G4endl;
//           << inFileName << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;
//   G4double th_min, th_max, dth, e_min, e_max, de;
//   inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
//  this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  G4cout << "Reading " << inFileName << G4endl;
  this->ReadDistribution(inputFile);
  inputFile.close();
}

// void ProfileDistHandler::InitData( G4double emin, G4double emax, G4double de, G4double thmin, G4double thmax, G4double dth)
// {
//   if( emax > emin ) {
//     E_min = emin;
//     E_max = emax;
//   }
//   else {
//     E_min = emax;
//     E_max = emin;
//   }
//   
//   E_dif = de;
//   if( E_dif == 0. )
//     E_dif = 1.0*MeV;
//   
//   if( thmax > thmin ) {
//     th_min = thmin;
//     th_max = thmax;
//   }
//   else {
//     th_min = thmax;
//     th_max = thmin;
//   }
//   th_dif = dth;
//   if( th_dif == 0. )
//     th_dif = 1.0*deg;
//   
//   G4int n_en = (G4int)((E_max-E_min)/E_dif)+1;
//   G4int n_th = (G4int)((th_max-th_min)/th_dif)+1;
//   
//   difProfileDist.resize(n_en);
//   intProfileDist.resize(n_en);
//   eneDifDist.resize(n_en);
//   eneIntDist.resize(n_en);
//   
//   for( G4int ii=0; ii<n_en; ii++ ) {
//     difProfileDist[ii].resize(n_th);
//     intProfileDist[ii].resize(n_th);
//     for( G4int jj=0; jj<n_th; jj++ ) {
//       difProfileDist[ii][jj] = 0.;
//       intProfileDist[ii][jj] = 0.;    
//     }
//     eneDifDist[ii] = 0.;  
//     eneIntDist[ii] = 0.;  
//   }
// }

ProfileDistHandler::~ProfileDistHandler()
{
  thickVal.clear();
  thickPb.clear();
  thickPbNorm.clear();
  intProfileDist.clear();
}

void ProfileDistHandler::ReadDistribution(std::ifstream& inFile)
{
  G4int ii=0;
  G4double dumm1, dumm2;
  
  G4cout << "inside ReadDistribution" << G4endl;
  
  // Read distribution from file, it ends with 0 0...
  inFile >> dumm1 >> dumm2;

  //thickVal.push_back(dumm1*0.001*mm);
  thickVal.push_back(dumm1*0.001);  //mm
  thickPb.push_back(dumm2);
  
//  G4cout << "reading line " << ii << G4endl;
  
  while( 1 ) {   
   ii++;
   inFile >> dumm1 >> dumm2;
   if( dumm2 == 0. ) 
       break;
   //   thickVal.push_back(dumm1*0.001*mm);
   thickVal.push_back(dumm1*0.001); // mm
   thickPb.push_back(dumm2);
//   G4cout << "reading line " << ii << G4endl;
   
  }

  G4cout << ii << " lines read from input file" << G4endl;

  intProfileDist.resize(thickPb.size());
  intProfileDist[0]=thickPb[0];
  for( ii=1; ii<(G4int)thickVal.size(); ii++ ) {
      intProfileDist[ii] = thickPb[ii]+intProfileDist[ii-1];
   }
   
  thickPbNorm.resize(thickPb.size());
   

  //nomalizes max value to 1
  for( int jj=0; jj<(G4int)thickVal.size(); jj++ ) {
      thickPbNorm[jj] =intProfileDist[jj]/intProfileDist[(G4int)thickVal.size()-1];
  }

}

G4double ProfileDistHandler::GetDepth()
{
  G4double z = G4UniformRand() * 1.0;
  G4double howmuch;
  G4int    jj = 0;
  
  while( thickPbNorm[jj] < z )     
    jj++;
  jj--;
//  ener =  thickVal[jj];
//  howmuch = //(z*(thickVal[jj+1]-thickVal[jj])-thickVal[jj+1]*thickPbNorm[jj]+thickVal[jj]*thickPbNorm[jj+1])/(thickPbNorm[jj+1]-thickPbNorm[jj]);

  howmuch = thickVal[jj] + (z-thickPbNorm[jj])*(thickVal[jj+1]-thickVal[jj])/(thickPbNorm[jj+1]-thickPbNorm[jj]);

  return howmuch;
}


