#ifdef ANCIL
#include "AgataAncillaryGanilChamb.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"




AgataAncillaryGanilChamb::AgataAncillaryGanilChamb(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  dirName     = name;

  
  matShell    = NULL;
  matName     = "Aluminum";


  ancSD       = NULL;
  ancName     = G4String("GanilChamb");
  ancOffset   = 27000;
  
  numAncSd = 0;
}



AgataAncillaryGanilChamb::~AgataAncillaryGanilChamb()
{}
#include "G4NistManager.hh"


G4int AgataAncillaryGanilChamb::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial =
    G4NistManager::Instance()->FindOrBuildMaterial("G4_Al");//G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryGanilChamb::InitSensitiveDetector()
{}


void AgataAncillaryGanilChamb::GetDetectorConstruction()
{  
	
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
  
}


void AgataAncillaryGanilChamb::Placement()
{	
  //    m_gdmlparser.Read("gdml/HoneyComb.gdml");
  
//  m_gdmlparser.Read("/home/joa/AGATA/agataganil/gdml-files/AGATA/GanilChamb/GanilVamosChamb2b.gdml");
//  m_gdmlparser.Read("/mnt/hgfs/Echanges/MyGitHubRep/gdml-files/AGATA/GanilChamb/GanilVamosChamb2b.gdml");
  m_gdmlparser.Read("/mnt/hgfs/Documents/geant4/gdml/AGATA//GanilChamb/GanilVamosChamb2b.gdml");
  m_LogicalVol2= m_gdmlparser.GetVolume("myworld2");

//  m_gdmlparser.Read("/home/joa/AGATA/agataganil/gdml-files/AGATA/GanilChamb/GanilVamosChamb3.gdml");
//  m_gdmlparser.Read("/mnt/hgfs/Echanges/MyGitHubRep/gdml-files/AGATA/GanilChamb/GanilVamosChamb3.gdml");
  m_gdmlparser.Read("/mnt/hgfs/Documents/geant4/gdml/AGATA//GanilChamb/GanilVamosChamb3.gdml");
  m_LogicalVol3= m_gdmlparser.GetVolume("myworld3");

    G4RotationMatrix* rm= new G4RotationMatrix();
    G4RotationMatrix rmY, rmZ;
	rmZ.rotateZ(0.*deg);
	rmY.rotateY(0.*deg);
   
	*rm=rmY*rmZ;

	//G4Transform3D TF(rm, rm*G4ThreeVector(0., 0., 0.));

    // gdml World box
    m_LogicalVol2->SetVisAttributes(G4VisAttributes::Invisible); 
    G4VisAttributes *AttVol2 = new G4VisAttributes(G4Colour(1.0, 1.0, 0.0)); 
    AttVol2->SetForceSolid(true);
    //m_LogicalVol2->SetVisAttributes(AttVol2);
    
    m_LogicalVol3->SetVisAttributes(G4VisAttributes::Invisible); 
    //G4VisAttributes *AttVol3 = new G4VisAttributes(G4Colour(1.0, .5, .5)); 
    //AttVol3->SetForceSolid(true);
    //m_LogicalVol3->SetVisAttributes(AttVol3); 
      
    new G4PVPlacement(rm, G4ThreeVector(0., 0., 0.), "GanilChamber2", m_LogicalVol2, theDetector->HallPhys(), false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(0., (353.5+105.+5.)*mm, 0.), "GanilChamber3", m_LogicalVol3, theDetector->HallPhys(), false, 0 );

	return ;
  
}


void AgataAncillaryGanilChamb::ShowStatus()
{
  G4cout << " GANIL Chamber support has been constructed." << G4endl;
}

void AgataAncillaryGanilChamb::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillaryGanilChamb::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}

#endif
