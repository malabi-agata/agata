#include "AgataExternalEmissionR.hh"
#include "AgataEventAction.hh"
#include "AgataEmitted.hh"

#include "Randomize.hh"
#include "G4RunManager.hh"
#include "globals.hh"
#include "G4ios.hh"
#include <stdio.h>
#include <stdlib.h>
#include <vector>

AgataExternalEmissionR::AgataExternalEmissionR( G4String path, G4bool hadr, G4bool value, G4String name )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  usePolar         = value;
  hadrons          = hadr;
  
  emitNuclei       = true;
  showStatus       = false;

  theExternalEmitter = new AgataExternalEmitter(name);
  theEmitter         = (AgataEmitter*)theExternalEmitter;
  
  theEmitted       = NULL;
  emittedLUT       = NULL;

  evFileName       = iniPath + "aevent";
  
  ///////////////////////////////////////////////////////////
  /// The following flags are always set to true, so that
  /// all of the information is always written out to the
  /// list-mode file
  ///////////////////////////////////////////////////////////
  theEmitter->SetRecoilDiffuse (true);
  theEmitter->SetSourceDiffuse (true);
  theEmitter->SetSourceLived   (true);
  theEmitter->SetMovingSource  (true);
  theEmitter->SetBetaDiffuse   (true);
  
  isBetaDecay      = false;
  
  verbose          = true;
  
  skipEvents       = -1;
  targetEvents     = 1000000000;
  
  directoryName    = name;
  
  myMessenger      = new AgataExternalEmissionRMessenger(this, hadrons, name);
}

AgataExternalEmissionR::~AgataExternalEmissionR()
{
  delete myMessenger;
}

//////////////////////////////////////////////////////////////////////////////
/// Begin of run: header of event file is decoded, the AgataEmitted objects
/// allocated, the emitted cascades are reset
//////////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::BeginOfRun()
{
  openedFile = false;
  abortRun   = false;

  G4cout << " >>> Opening event file " << evFileName << G4endl;
  evFile = new TFile(evFileName);
  if( evFile ==NULL ) {
    abortRun = true;
    abortMessage = " Could not open input file, aborting run ...";
    return;
  }  
  else
    openedFile = true;   
  
  ReadFileHeader();
  InitEventTree();
  
  if( abortRun )
    return;

  theExternalEmitter->BeginOfRun();
  
  cascadeDeltaTime = 0.;
  cascadeTime      = 0.;
  cascadeMult      = 0;
  cascadeOrder     = 0;
  startOfEvent     = true;
  endOfEvent       = false;
  endOfRun         = false;
  showStatus       = true;
  Emitterdecoded   = false;
  nextentry        = true;
  
  readEvents       = -1;
  processedEvents  = -1;
  if( skipEvents > 0 )
    targetEvents++;
  
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theEvent = (AgataEventAction *) runManager->GetUserEventAction();
}

void AgataExternalEmissionR::BeginOfEvent()
{}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// End of event: emitted cascade is reset, momentum conservation is applied, next line concerning
/// the source is decoded
/////////////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::EndOfEvent()
{
  if( startOfEvent ) {
    startOfEvent     = false;
  }

  if( endOfEvent ) {
    endOfEvent       = false;
    startOfEvent     = true;
    cascadeDeltaTime = 0.;
    cascadeTime      = 0.;
    cascadeMult      = 0;
    cascadeOrder     = 0;
    return;
  }
  
  // conservation of momentum
  if( !nucleusIsEmitted ) {
    if( (emittedAtomNum >  emitterAtomNum) || (emittedMassNum > emitterMassNum) ) {
      abortRun = true;
      abortMessage = " Emitted particle is not compatible with residual nucleus, aborting run ...";
      return;
    }
    else {
      if( isBetaDecay )
        theExternalEmitter->UpdateEmitter( emittedAtomNum, emittedMassNum ); 
      else
        theExternalEmitter->UpdateEmitter( emittedAtomNum, emittedMassNum, emittedMomentum ); 
      	 
      emitterAtomNum  = theExternalEmitter->GetEmitterAtomNum();
      emitterMassNum  = theExternalEmitter->GetEmitterMassNum();
    }
  }
}

/////////////////////////////////////////
/// End of run: input file is closed
////////////////////////////////////////
void AgataExternalEmissionR::EndOfRun()
{
  theExternalEmitter->EndOfRun();
  if( openedFile ) {
    G4cout << " Closing event file " << evFileName << G4endl;
    evFile->Close();
  }
}

void AgataExternalEmissionR::ReadFileHeader()
{
  createdEmitted = false;

  htree = (TTree *)evFile->Get("htree");

  // Emitter, emitted type
  htree->SetBranchAddress("emitterType", &emitterType);
  htree->SetBranchAddress("emittedType", &emittedType);
  // Reaction
  htree->SetBranchAddress("zBeam", &zBeam);
  htree->SetBranchAddress("aBeam", &aBeam);
  htree->SetBranchAddress("zTarg", &zTarg);
  htree->SetBranchAddress("aTarg", &aTarg);
  htree->SetBranchAddress("eBeam", &eBeam);
  // Emitted
  G4int nTypes;
  G4int *dummyTypes = new G4int[100];
  G4int *Types = new G4int[100];
  htree->SetBranchAddress("nTypes", &nTypes);
  htree->SetBranchAddress("Types", dummyTypes);

  // load header
  htree->GetEntry(0);

  // output
  G4cout << " Emitter type is " << emitterType << G4endl;
  G4cout << " Emitted type is " << emittedType << G4endl;

  // Reaction
  eBeam *= MeV;

  G4cout << " >>> Values read from file: " << G4endl;
  G4cout << "   Beam has Z = " << zBeam << ", A = " << aBeam << " at an energy of " << eBeam/MeV << " MeV" << G4endl;
  G4cout << " Target has Z = " << zTarg << ", A = " << aTarg << G4endl;
 
  theExternalEmitter->SetBeamNucleus   ( zBeam, aBeam );
  theExternalEmitter->SetBeamEnergy    ( eBeam/keV );
  theExternalEmitter->SetTargetNucleus ( zTarg, aTarg );

  // Emitted
  maxIndex = -1;
  nEmitted = 0;
  G4bool isGood = false;

  for(G4int ii=0; ii<nTypes; ii++){
    isGood = true;
    // check for repeated particles!
    for(G4int ij=0; ij<ii; ij++){
      if(dummyTypes[ii] == dummyTypes[ij]){
	G4cout << " Warning! Skipping repeated emitted type #" << dummyTypes[ii] << G4endl;
	isGood = false;
	break;
      }
    }
    if( !hadrons && IsHadron(dummyTypes[ii]) ) {
      G4cout << " Warning! Hadron emission not allowed, skipping emitted type #" << dummyTypes[ii] << "..." << G4endl;
      isGood = false;
    }
    if( isGood) {
      Types[nEmitted++] = dummyTypes[ii];
      if( dummyTypes[ii] > maxIndex )
	maxIndex = dummyTypes[ii];
    }
  }

  if( !nEmitted ) {
    abortRun = true;
    abortMessage = " No emitted particles defined, aborting run ...\n";
    return;
  }

  theEmitted = new AgataEmitted*[nEmitted];
  emittedLUT = new G4int[++maxIndex];
  for(G4int ii=0; ii<maxIndex; ii++ )
    emittedLUT[ii] = -1;

  for(G4int ii=0; ii<nEmitted; ii++ ) {
    G4String name = FetchEmittedName( Types[ii] );
    G4bool value;
    if( abortRun )
      return;
    if( name == "gamma" )
      value = usePolar;
    else
      value = false;  
    theEmitted[ii] = new AgataEmitted( directoryName, name, iniPath, value );
    emittedLUT[Types[ii]] = ii;
  }
  
  createdEmitted = true;
  
  for(G4int ij=0; ij<nEmitted; ij++ ) {
    G4cout << " Emitted # " << ij << " is a " << theEmitted[ij]->GetEmittedName() << G4endl;
  }

}


void AgataExternalEmissionR::InitEventTree()
{
  ievent = 0;   newemitter = true;
  Z_emitter = 0;  A_emitter = 0;  E_emitter = 0;
  px_emitter = py_emitter = pz_emitter = 0;
  vx_emitter = vy_emitter = vz_emitter = 0;
  type_emitted = 0;
  px_emitted = py_emitted = pz_emitted = 0;
  E_emitted = 0;
  vx_emitted = vy_emitted = vz_emitted = 0;
  E_emitted_CM = 0;
  vx_emitted_CM = vy_emitted_CM = vz_emitted_CM;
  T_emitted = 0;
  P1_emitted = P2_emitted = P3_emitted = 0;

  gtree = (TTree *)evFile->Get("gtree");
  gtree->SetBranchAddress("ievent",     &ievent);
  gtree->SetBranchAddress("newemitter", &newemitter);
  if(emitterType<4){
    gtree->SetBranchAddress("Z_emitter",  &Z_emitter);
    gtree->SetBranchAddress("A_emitter",  &A_emitter);
    if(emitterType<3){
      gtree->SetBranchAddress("E_emitter",  &E_emitter);
      if(emitterType<2){
	gtree->SetBranchAddress("vx_emitter", &vx_emitter);
	gtree->SetBranchAddress("vy_emitter", &vy_emitter);
	gtree->SetBranchAddress("vz_emitter", &vz_emitter);
	if(emitterType<1){
	  gtree->SetBranchAddress("px_emitter", &px_emitter);
	  gtree->SetBranchAddress("py_emitter", &py_emitter);
	  gtree->SetBranchAddress("pz_emitter", &pz_emitter);
	}
      }
    }
  }
  
  gtree->SetBranchAddress("type",       &type_emitted);
  if(emittedType==0 || emittedType==2)
    gtree->SetBranchAddress("E_emitted",  &E_emitted);
  if(emittedType==1 || emittedType==3 || emittedType==4)
    gtree->SetBranchAddress("E_emitted_CM",  &E_emitted_CM);

  if(emittedType==0 || emittedType==2){
    gtree->SetBranchAddress("vx_emitted", &vx_emitted);
    gtree->SetBranchAddress("vy_emitted", &vy_emitted);
    gtree->SetBranchAddress("vz_emitted", &vz_emitted);
  }
  if(emittedType==1 || emittedType==3){
    gtree->SetBranchAddress("vx_emitted_CM", &vx_emitted_CM);
    gtree->SetBranchAddress("vy_emitted_CM", &vy_emitted_CM);
    gtree->SetBranchAddress("vz_emitted_CM", &vz_emitted_CM);
  }
  
  if(emittedType==0 || emittedType==1){
    gtree->SetBranchAddress("px_emitted", &px_emitted);
    gtree->SetBranchAddress("py_emitted", &py_emitted);
    gtree->SetBranchAddress("pz_emitted", &pz_emitted);
  }
  
  if(gtree->GetBranch("T_emitted"))
    gtree->SetBranchAddress("T_emitted",  &T_emitted);
  if(gtree->GetBranch("P1_emitted"))
    gtree->SetBranchAddress("P1_emitted", &P1_emitted);
  if(gtree->GetBranch("P2_emitted"))
    gtree->SetBranchAddress("P2_emitted", &P2_emitted);
  if(gtree->GetBranch("P3_emitted"))
    gtree->SetBranchAddress("P3_emitted", &P3_emitted);

  nentries = gtree->GetEntriesFast();
  //G4cout << "nentries = " << nentries << G4endl;
  ientry = 0;
  ievent = -1;
}

G4bool AgataExternalEmissionR::IsHadron( G4int type )
{
  if( (type>1) && (type<97) )
    return true;
  else
    return false;  
}


//////////////////////////////////////
/// FetchEmittedName: a lookup table
//////////////////////////////////////
G4String AgataExternalEmissionR::FetchEmittedName( G4int type )
{
  G4String name;
  
  switch(type)
  {
    case 1:
      name = "gamma";
      break;
    case 2:
      name = "neutron";
      break;
    case 3:
      name = "proton";
      break;
    case 4:
      name = "deuteron";
      break;
    case 5:
      name = "triton";
      break;
    case 6:
      name = "He3";
      break;
    case 7:
      name = "alpha";
      break;
    case 8:
      name = "GenericIon";
      break;
    case 97:
      name = "e-";
      break;  
    case 98:
      name = "e+";
      break;  
    case 99:
      name = "geantino";
      break;  
    default:
      abortRun = true;
      name = "unknown particle";
      G4cout << " Undefined particle #" << type << ", run will be aborted ..." << G4endl;
      break;   
  }
  return name;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// Chooses next emitted particle and stores its name as emittedName, momentum as emittedMomentum
/////////////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::NextParticle()
{
  G4int type;
  G4int ii;
  
  char messaggio[256];

  if( endOfRun ) {
    abortRun = true;
    sprintf( messaggio, " >>>Regular end of run after %d processed events.", processedEvents );
    abortMessage = G4String(messaggio);
    return;
  }

  cascadeOrder = cascadeMult;

  while (ientry<nentries) {
    
    currentevent = ievent;
    if(nextentry){
      gtree->GetEntry(ientry++);
      nextentry = false;
    }

    // begin-of-event tag: in case emit current nucleus
    if( ievent!=currentevent ) {
      readEvents++;
      if( readEvents < skipEvents ){
	nextentry = true;
	continue;
      }
      

      processedEvents++;
      if( processedEvents > targetEvents ) {
        if( hadrons && emitNuclei && Emitterdecoded) {
          endOfRun          = true;
          endOfEvent        = true;
          nucleusIsEmitted  = true;
	  Emitterdecoded    = false;
          FetchEmitter();
          if( emittedAtomNum != 0 ) // if "residual nucleus" does not exist, should not emit it!
            return;
        }

	abortRun = true;
	if( skipEvents > 0 )
	  sprintf( messaggio, " >>>Number of requested events (%d) reached. Regular end of run.", processedEvents-1 );
	else
	  sprintf( messaggio, " >>>Number of requested events (%d) reached. Regular end of run.", processedEvents );
	abortMessage = G4String(messaggio);
	return;
      }

      if( hadrons && emitNuclei && Emitterdecoded) {
        nucleusIsEmitted  = true;
	Emitterdecoded    = false;
	endOfEvent        = true;
        FetchEmitter();
        if( emittedAtomNum != 0 ) {// if "residual nucleus" does not exist, should not emit it!
          cascadeMult++;
          return;
        }
      }
      else {
	Emitterdecoded    = false;
	endOfEvent        = true;
        theEvent->FlushAnalysis();
        EndOfEvent();
        continue;  
      }
    }

    if( readEvents < skipEvents ){
      nextentry = true;
      continue;
    }

    // recoil or something else?
    if(newemitter){ // recoil
      if( hadrons && emitNuclei && Emitterdecoded) {
	Emitterdecoded    = false;
	FetchEmitter();
	if( emittedAtomNum != 0 ) {// if "residual nucleus" does not exist, should not emit it!
	  nucleusIsEmitted  = true;
          cascadeMult++;
          return;
	}
      }

      Emitterdecoded    = true; 
      DecodeEmitter();      
    }

    if( !Emitterdecoded ) {
      abortRun = true;
      if( skipEvents > 0 )
	sprintf( messaggio, " Missing emitter line, aborting run after %d processed events ... ", processedEvents-1 );
      else
	sprintf( messaggio, " Missing emitter line, aborting run after %d processed events ... ", processedEvents );
      abortMessage = G4String(messaggio);
      return;
    }
    nucleusIsEmitted  = false;
    DecodeEmitted();
    cascadeMult++;
    nextentry = true;
    return;

  }

  // if file ends, eventually emits recoil
  endOfRun   = true;
  endOfEvent = true;
  if( hadrons && emitNuclei ) {
    nucleusIsEmitted  = true;
    Emitterdecoded    = false;
    FetchEmitter();
    if( emittedAtomNum != 0 ) // if "residual nucleus" does not exist, should not emit it!
      return;
  }
  if( endOfRun ) {
    abortRun = true;
    if( skipEvents > 0 )
      sprintf( messaggio, " >>>End of file found. Regular end of run after %d processed events.", processedEvents );
    else
      sprintf( messaggio, " >>>End of file found. Regular end of run after %d processed events.", processedEvents+1 );
    abortMessage = G4String(messaggio);
    return;
  }
  
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeEmitted: retrieves from input file the information on the next emitted particle
/////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::DecodeEmitted()
{
  G4int    dumType = type_emitted;
  G4int    index;
  G4double dumE;
  G4double dumT  = 0; // ns
  G4double dumP1 = 0., dumP2 = 0., dumP3 = 0.;
  
  if( dumType >= maxIndex ) {
    abortRun = true;
    abortMessage = " Emitted type out of range, aborting run ..." ;
    return;
  }   
  
  index = emittedLUT[dumType];
  if( index < 0 ) {
    abortRun = true;
    abortMessage = " Undefined emitted type, aborting run ..." ;
    return;
  }
  
  ////////////////////////////////////////////////////////
  /// 03/11/2006 Making dumT, dumP optional parameters
  ////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////
  /// 30/11/2009 Making dumP Stokes vector
  ////////////////////////////////////////////////////////
  switch( emittedType )
  {
    case 0:
      {
	G4double dumSx = px_emitted/10, dumSy = py_emitted/10, dumSz = pz_emitted/10;
	dumE  = E_emitted;
	G4double dumDx = vx_emitted,    dumDy = vy_emitted,    dumDz = vz_emitted;
	if(T_emitted>0) dumT = T_emitted;
	dumP1 = P1_emitted;  dumP2 = P2_emitted;  dumP3 = P3_emitted;
	theExternalEmitter->SetEmitterPosition( G4ThreeVector( dumSx, dumSy, dumSz )*cm );
	theEmitted[index]->setVelocityLab( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ), dumP1, dumP2, dumP3 );
/**/
      }
      break;
    case 1:
      {
	G4double dumSx = px_emitted/10, dumSy = py_emitted/10, dumSz = pz_emitted/10;
	dumE  = E_emitted_CM;
	G4double dumDx = vx_emitted_CM, dumDy = vy_emitted_CM, dumDz = vz_emitted_CM;
	if(T_emitted>0) dumT = T_emitted;
	dumP1 = P1_emitted;  dumP2 = P2_emitted;  dumP3 = P3_emitted;
	theExternalEmitter->SetEmitterPosition( G4ThreeVector( dumSx, dumSy, dumSz )*cm );
	theEmitted[index]->setVelocityCM( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ),
					  theEmitter->GetEmitterVelocity(), dumP1, dumP2, dumP3 );
/**/  
     }
      break;
    case 2:
      {
	dumE  = E_emitted;
	G4double dumDx = vx_emitted,    dumDy = vy_emitted,    dumDz = vz_emitted;
	if(T_emitted>0) dumT = T_emitted;
	dumP1 = P1_emitted;  dumP2 = P2_emitted;  dumP3 = P3_emitted;
	theEmitted[index]->setVelocityLab( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ), dumP1, dumP2, dumP3 );
/**/
      }
      break;
    case 3:
      {
	dumE  = E_emitted_CM;
	G4double dumDx = vx_emitted_CM, dumDy = vy_emitted_CM, dumDz = vz_emitted_CM;
	if(T_emitted>0) dumT = T_emitted;
	dumP1 = P1_emitted;  dumP2 = P2_emitted;  dumP3 = P3_emitted;
	theEmitted[index]->setVelocityCM( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ),
					  theEmitter->GetEmitterVelocity(), dumP1, dumP2, dumP3 );
/**/
      }
      break;
    case 4:
      {
	dumE  = E_emitted_CM;
	if(T_emitted>0) dumT = T_emitted;
	dumP1 = P1_emitted;  dumP2 = P2_emitted;  dumP3 = P3_emitted;
	theEmitted[index]->setVelocityCM( dumE*keV, 
					  theEmitter->GetEmitterVelocity(), dumP1, dumP2, dumP3 );
/**/
      }
      break;
  }

  if( dumE <= 0. ) {
    abortRun = true;
    abortMessage = " Invalid energy value, aborting run ..." ;
    return;
  }   
  
  // update emission time and emitter position
  cascadeDeltaTime  = cascadeTime;
  cascadeTime       = dumT * ns;
  cascadeDeltaTime  = cascadeTime - cascadeDeltaTime;
  if( cascadeDeltaTime && (emittedType > 1) ) {
    theEmitter->TraslateEmitter( cascadeDeltaTime );
  }  
  
  // update emitted particle
  emittedName       = theEmitted[index]->GetEmittedName();
  emittedAtomNum    = theEmitted[index]->GetChargeNumber();
  emittedMassNum    = theEmitted[index]->GetMassNumber();
  emittedEnergy     = theEmitted[index]->GetEnergyLab();
  emittedDirection  = theEmitted[index]->GetDirLab();
  emittedMomentum   = theEmitted[index]->GetMomentumLab();
  
  // only polarized gammas
  if( usePolar && (emittedName == "gamma") ) {
    emittedPolarization = theEmitted[index]->GetPolarizationLab();
    polarizedEmitted = true;
  }
  else
    polarizedEmitted = false;  
}


/////////////////////////////////////////////////////////////////////////////
/// DecodeEmitter: retrieves from input file the information on the source
/////////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::DecodeEmitter()
{
  G4int         dumZ = Z_emitter;
  G4int         dumA = A_emitter;
  
  G4double      dumE = E_emitter*A_emitter;
  
  G4ThreeVector dumD;
  G4ThreeVector dumS;
  
  switch( emitterType )
  {
    case 0:
      {
	G4double dumDx = vx_emitter,    dumDy = vy_emitter,    dumDz = vz_emitter;
	G4double dumSx = px_emitter/10, dumSy = py_emitter/10, dumSz = pz_emitter/10;
	dumD = G4ThreeVector( dumDx, dumDy, dumDz );
	if( dumD.mag2() ) {
	  dumD = dumD.unit();
	} 
	else
	  dumD = G4ThreeVector( 0., 0., 1. );
	dumS = G4ThreeVector( dumSx, dumSy, dumSz ) * cm;
	theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV, dumD, dumS );
      }
      break;
    case 1:
      {
	G4double dumDx = vx_emitter,    dumDy = vy_emitter,    dumDz = vz_emitter;
	dumD = G4ThreeVector( dumDx, dumDy, dumDz );
	if( dumD.mag2() )
	  dumD = dumD.unit();
	else
	  dumD = G4ThreeVector( 0., 0., 1. );
	theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV, dumD );
      }
      break;
    case 2:
      {
	theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV );
      }
      break;
    case 3:
      {
	theExternalEmitter->RestartEmitter( dumZ, dumA );
      }
      break;
    case 4:
      {
	theExternalEmitter->RestartEmitter();
      }
      break;
    default:
      {
	abortRun = true;
	abortMessage = " Unsupported emitter format, aborting run ...";
      }
      return;
  }

  emitterAtomNum  = theExternalEmitter->GetEmitterAtomNum();
  emitterMassNum  = theExternalEmitter->GetEmitterMassNum();
}

/////////////////////////////////////////////////////////////////////////
/// FetchEmitter: saves residual nucleus as the next emitted particle
/////////////////////////////////////////////////////////////////////////
void AgataExternalEmissionR::FetchEmitter( )
{
  emittedName = "GenericIon";
  emittedAtomNum    = theExternalEmitter->GetEmitterAtomNum();
  emittedMassNum    = theExternalEmitter->GetEmitterMassNum();
  emittedEnergy     = theExternalEmitter->GetEmitterEnergy();
  emittedDirection  = theEmitter->GetEmitterDirection();
  cascadeDeltaTime  = 0.; // same emission time as the last emitted particle!!!
  cascadeTime       = 0.; // resets cascade time (useful for binary reactions)!!!

}

void AgataExternalEmissionR::SetEventFile( G4String name )
{
  if( name(0) == '/' )
    evFileName = name;
  else {
    if( name.find( "./", 0 ) != string::npos ) {
      G4int position = name.find( "./", 0 );
      if( position == 0 )
        name.erase( position, 2 );
    }  
    evFileName = iniPath + name;
  }  
      
  G4cout << " ----> Events will be read from " << evFileName << G4endl;
}

void AgataExternalEmissionR::SetEmitNuclei( G4bool value )
{
  emitNuclei = value;
  if( emitNuclei && hadrons )
    G4cout << " ----> Emission of nuclei is now enabled. " << G4endl;
  else
    G4cout << " ----> Emission of nuclei is now disabled. " << G4endl;  
}

void AgataExternalEmissionR::GetStatus()
{
  if( showStatus ) {
    G4cout << " Emitted type: " << emittedType << G4endl;
    G4cout << " Emitted particles: ";
    for( G4int ii=0; ii<maxIndex; ii++ ) {
      if( emittedLUT[ii] > 0 )
        G4cout << FetchEmittedName( ii ) << " ";
    }
    G4cout << G4endl;

    G4cout << " Emitter type: " << emitterType << G4endl;
    theExternalEmitter->GetStatus( emitterType );
    
    if( verbose ) 
      G4cout << " Full information about the source to the output file." << G4endl;
    else  
      G4cout << " Reduced information about the source to the output file." << G4endl;
  }
}

void AgataExternalEmissionR::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  outFileLMD << "GENERATOR 1" << G4endl;
  outFileLMD << "EVENTFILE " << evFileName << G4endl;
  if( emitNuclei && hadrons )
    outFileLMD << "EMITTED " << emittedType << " " << nEmitted+1;
  else  
    outFileLMD << "EMITTED " << emittedType << " " << nEmitted;
  for( G4int ii=0; ii<maxIndex; ii++ ) {
    if( emittedLUT[ii] >= 0 )
      outFileLMD << " " << ii;
  }
  if( emitNuclei && hadrons )
    outFileLMD << " 8" << G4endl;
  else  
    outFileLMD << G4endl;
  
  outFileLMD << "EMITTER " << emitterType << G4endl;
  theExternalEmitter->PrintToFile( emitterType, outFileLMD, unitL, unitE );
  
  if( verbose )
    outFileLMD << "VERBOSE 1" << G4endl;
  else  
    outFileLMD << "VERBOSE 0" << G4endl;
  
  outFileLMD << "ENDGENERATOR" << G4endl;

}

void AgataExternalEmissionR::PrintToFile( G4String *sheader, G4double unitL, G4double unitE )
{

  char line[128];
  sheader[0] += G4String("GENERATOR 1\n");
  sheader[0] += G4String("EVENTFILE ") + evFileName + G4String("\n");
  if( emitNuclei && hadrons ){
    sprintf(line, "EMITTED %d %d", emittedType, nEmitted+1);
    sheader[0] += G4String(line);  
  }else{  
    sprintf(line, "EMITTED %d %d", emittedType, nEmitted);
    sheader[0] += G4String(line);  
  }
  for( G4int ii=0; ii<maxIndex; ii++ ) {
    if( emittedLUT[ii] >= 0 ){
      sprintf(line, " %d", ii);
      sheader[0] += G4String(line);  
    }
  }
  if( emitNuclei && hadrons )
    sheader[0] += G4String(" 8\n");
  else  
    sheader[0] += G4String("\n");
  
  sprintf(line, "EMITTER %d\n", emitterType);
  sheader[0] += G4String(line);  
  theExternalEmitter->PrintToFile( emitterType, sheader, unitL, unitE );
  
  if( verbose )
    sheader[0] += G4String("VERBOSE 1\n");
  else  
    sheader[0] += G4String("VERBOSE 0\n");
  
  sheader[0] += G4String("ENDGENERATOR\n");
/**/
}

G4String AgataExternalEmissionR::GetEventHeader( G4double unitLength )
{
  char aLine[128];
  G4String eventHeader = G4String("");
  
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// 07/06/2006 chenge suggested by O.Stezowski: disable the writing out of "-101" and "-102" ///
  /// lines (optional, should be used with caution!)                                           ///
  ////////////////////////////////////////////////////////////////////////////////////////////////
  if( !verbose ) return eventHeader;
  
  if( this->IsStartOfEvent() ) {
  // Internal generation:
  // First writes out what is common to the cascade (recoil velocity and source position)
  // External generation: same thing
    // Internal generation:
    // writes out beta and direction only when they may change event-by-event
    // External generation: always write (flags set to true)

    if( this->GetEmitterVelocity().mag2() > 0. ) {
      G4ThreeVector recoildir  = this->GetEmitterVelocity().unit();
      sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, this->GetEmitterBeta(), recoildir.x(), recoildir.y(), recoildir.z() );
    }
    else
      sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, 0., 0., 0., 1. );
    eventHeader += G4String(aLine);

    G4ThreeVector position = this->GetEmitterPosition();
    sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position.x()/unitLength, position.y()/unitLength, position.z()/unitLength );
    eventHeader += G4String(aLine);

    // writes out the time difference between the emission of two particles
    // in the cascade (or the time for the first particle) only when it is
    // non-zero
    if( (this->IsSourceLongLived()) && (this->GetCascadeDeltaTime() > 0.) ) {
      cascadeTime = this->GetCascadeTime();
      sprintf(aLine, "%5d  %9.3f\n", -103, cascadeTime/ns );
      eventHeader += G4String(aLine);
    }
  }
  else {  // following events
    // external event generation: always write out the recoil velocity line
    if( (this->GetCascadeOrder() > 0) ) {
      if( this->GetEmitterVelocity().mag2() > 0. ) {
        G4ThreeVector recoildir1  = this->GetEmitterVelocity().unit();
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, this->GetEmitterBeta(), recoildir1.x(), recoildir1.y(), recoildir1.z() );
      }
      else
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, 0., 0., 0., 1. );
      eventHeader += G4String(aLine);
      // always write out source position 
      G4ThreeVector position2 = this->GetEmitterPosition();
      sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position2.x()/unitLength, position2.y()/unitLength, position2.z()/unitLength );
      eventHeader += G4String(aLine);
    }
    // writes out the time difference between the emission of two particles
    // in the cascade (or the time for the first particle) only when it is
    // non-zero
    if( (this->IsSourceLongLived()) && (this->GetCascadeDeltaTime() > 0.) ) {
      cascadeTime = this->GetCascadeTime();
      sprintf(aLine, "%5d  %9.3f\n", -103, cascadeTime/ns );
      eventHeader += G4String(aLine);
    }
  }
  return eventHeader;  
}

G4String AgataExternalEmissionR::GetParticleHeader( const G4Event* evt, G4double /*unitLength*/, G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", -partType, epart/unitEnergy, direction.x(), direction.y(), direction.z(), evt->GetEventID());
  return G4String(aLine);

}

void AgataExternalEmissionR::SetBetaDecay( G4bool value )
{
  isBetaDecay = value;
  if( isBetaDecay )
    G4cout << " ----> Beta-decay behaviour has been activated." << G4endl;
  else
    G4cout << " ----> Beta-decay behaviour has been deactivated." << G4endl;  
}

void AgataExternalEmissionR::SetSkipEvents( G4int value )
{
  if( value > 0 ) {
    skipEvents = value;
    G4cout << " ----> The first " << skipEvents << " events will be skipped." << G4endl;
  }
  else 
    G4cout << " ----> No events will be skipped." << G4endl;
}

void AgataExternalEmissionR::SetTargetEvents( G4int value )
{
  if( value > 0 ) {
    targetEvents = value;
    G4cout << " ----> Processing " << targetEvents << " events ..." << G4endl;
    targetEvents--;
    
    G4int numberOfEvents = 1000000000;
    
    G4RunManager * runManager = G4RunManager::GetRunManager();
    runManager->BeamOn(numberOfEvents);
    
  }
  targetEvents = 1000000000;
}

void AgataExternalEmissionR::SetVerbose( G4bool value )
{
  verbose = value;
    
  if( verbose ) 
    G4cout << " --->Full information about the source to the output file." << G4endl;
  else  
    G4cout << " --->Reduced information about the source to the output file." << G4endl;
}

G4String AgataExternalEmissionR::GetBeginOfEventTag()
{
  char dummy[64];
#ifdef WRITE_EVNUM
  sprintf( dummy, "-100\t%10d\n", processedEvents+1 );
#else  
  sprintf( dummy, "-100\n" );
#endif
  return G4String(dummy);
}

///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataExternalEmissionRMessenger::AgataExternalEmissionRMessenger(AgataExternalEmissionR* pTarget, G4bool hadr, G4String name)
:myTarget(pTarget)
{ 

  hadrons = hadr;

  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of the event generation.");

  directoryName += "emitter/";
  
  commandName = directoryName + "eventFile";
  aLine = commandName.c_str();
  SetEventFileCmd = new G4UIcmdWithAString(aLine, this);  
  SetEventFileCmd->SetGuidance("Define input file with the events to be generated");
  SetEventFileCmd->SetGuidance("Required parameters: 1 string.");
  SetEventFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableBetaDecay";
  aLine = commandName.c_str();
  EnableBetaCmd = new G4UIcmdWithABool(aLine, this);  
  EnableBetaCmd->SetGuidance("Enables beta-decay behaviour");
  EnableBetaCmd->SetGuidance("Required parameters: none.");
  EnableBetaCmd->SetParameterName("isBetaDecay",true);
  EnableBetaCmd->SetDefaultValue(true);
  EnableBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "disableBetaDecay";
  aLine = commandName.c_str();
  DisableBetaCmd = new G4UIcmdWithABool(aLine, this);  
  DisableBetaCmd->SetGuidance("Disables beta-decay behaviour");
  DisableBetaCmd->SetGuidance("Required parameters: none.");
  DisableBetaCmd->SetParameterName("isBetaDecay",true);
  DisableBetaCmd->SetDefaultValue(false);
  DisableBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  if( hadrons ) {
    commandName = directoryName + "enableNuclei";
    aLine = commandName.c_str();
    EnableNucleiCmd = new G4UIcmdWithABool(aLine, this);
    EnableNucleiCmd->SetGuidance("Activate the emission of nuclei");
    EnableNucleiCmd->SetGuidance("Required parameters: none.");
    EnableNucleiCmd->SetParameterName("emitNuclei",true);
    EnableNucleiCmd->SetDefaultValue(true);
    EnableNucleiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName + "disableNuclei";
    aLine = commandName.c_str();
    DisableNucleiCmd = new G4UIcmdWithABool(aLine, this);
    DisableNucleiCmd->SetGuidance("Deactivate the emission of nuclei");
    DisableNucleiCmd->SetGuidance("Required parameters: none.");
    DisableNucleiCmd->SetParameterName("emitNuclei",true);
    DisableNucleiCmd->SetDefaultValue(false);
    DisableNucleiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
  

  directoryName = name + "/run/";
  
  commandName = directoryName + "skipEvents";
  aLine = commandName.c_str();
  SetSkipEventsCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetSkipEventsCmd->SetGuidance("Skip the first events in the input file.");
  SetSkipEventsCmd->SetGuidance("Required parameters: 1 integer.");
  SetSkipEventsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "beamOn";
  aLine = commandName.c_str();
  SetTargetEventsCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetTargetEventsCmd->SetGuidance("Process multiple events from the input file.");
  SetTargetEventsCmd->SetGuidance("Required parameters: 1 integer.");
  SetTargetEventsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  

  directoryName = name + "/file/";
  
  commandName = directoryName + "fullOutput";
  aLine = commandName.c_str();
  VerboseOutputCmd = new G4UIcmdWithABool(aLine, this);
  VerboseOutputCmd->SetGuidance("Select full information on the source for output to file.");
  VerboseOutputCmd->SetGuidance("Required parameters: none.");
  VerboseOutputCmd->SetParameterName("verbose",true);
  VerboseOutputCmd->SetDefaultValue(true);
  VerboseOutputCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "reduceOutput";
  aLine = commandName.c_str();
  ReducedOutputCmd = new G4UIcmdWithABool(aLine, this);
  ReducedOutputCmd->SetGuidance("Select reduced information on the source for output to file.");
  ReducedOutputCmd->SetGuidance("Required parameters: none.");
  ReducedOutputCmd->SetParameterName("verbose",true);
  ReducedOutputCmd->SetDefaultValue(false);
  ReducedOutputCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataExternalEmissionRMessenger::~AgataExternalEmissionRMessenger()
{
  delete   myDirectory;
  delete   mySubDirectory;
  delete   SetEventFileCmd;
  delete   SetSkipEventsCmd;
  delete   SetTargetEventsCmd;
  delete   EnableBetaCmd;
  delete   DisableBetaCmd;
  delete   EnableNucleiCmd;
  delete   DisableNucleiCmd;
  delete   VerboseOutputCmd;
  delete   ReducedOutputCmd;
}

void AgataExternalEmissionRMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == SetEventFileCmd ) {
    myTarget->SetEventFile(newValue);
  }
  if( command == SetSkipEventsCmd ) {
    myTarget->SetSkipEvents(SetSkipEventsCmd->GetNewIntValue(newValue));
  }
  if( command == SetTargetEventsCmd ) {
    myTarget->SetTargetEvents(SetTargetEventsCmd->GetNewIntValue(newValue));
  }
  if( command == EnableBetaCmd ) {
    myTarget->SetBetaDecay( EnableBetaCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableBetaCmd ) {
    myTarget->SetBetaDecay( DisableBetaCmd->GetNewBoolValue(newValue) );
  }
  if( hadrons ) {
    if( command == EnableNucleiCmd ) {
      myTarget->SetEmitNuclei( EnableNucleiCmd->GetNewBoolValue(newValue) );
    }
    if( command == DisableNucleiCmd ) {
      myTarget->SetEmitNuclei( DisableNucleiCmd->GetNewBoolValue(newValue) );
    }
  }
  if( command == VerboseOutputCmd ) {
    myTarget->SetVerbose( VerboseOutputCmd->GetNewBoolValue(newValue) );
  }
  if( command == ReducedOutputCmd ) {
    myTarget->SetVerbose( ReducedOutputCmd->GetNewBoolValue(newValue) );
  }
}

