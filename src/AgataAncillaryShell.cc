#include "AgataAncillaryShell.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryShell::AgataAncillaryShell(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  dirName = name;
  
  shellRmin   = 1.0*m;
  shellRmax   = 1.*m+1.*um;
  
  matShell    = NULL;
  matName     = "Silicon";
  
  ancSD       = NULL;
  ancName     = G4String("SHELL");
  ancOffset   = 2000;
  
  numAncSd = 0;

  myMessenger = new AgataAncillaryShellMessenger(this,name);
}

AgataAncillaryShell::~AgataAncillaryShell()
{
  delete myMessenger;
}

G4int AgataAncillaryShell::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary shell! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryShell::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/all", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
}


void AgataAncillaryShell::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryShell::Placement()
{
  ///////////////////////
  /// construct a shell
  ///////////////////////
  G4Sphere        *solidShell = new G4Sphere( "shell", shellRmin, shellRmax, 0.*deg, 360.*deg, 0.5*deg, 60.*deg);//0.5*deg, 179.*deg);
  G4LogicalVolume *logicShell = new G4LogicalVolume( solidShell, matShell, "Shell", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(), "Shell", logicShell, theDetector->HallPhys(), false, 0 );

  // Sensitive Detector
  logicShell->SetSensitiveDetector( ancSD );

  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  logicShell->SetVisAttributes( pVA );

  G4cout << "Shell radius " << shellRmin/cm << " -- " << shellRmax/cm << " cm" << G4endl;
  G4cout << "Shell material is " << matShell->GetName() << G4endl;

  return;
}

////////////////////////////////
/// methods for the messenger
////////////////////////////////

void AgataAncillaryShell::SetShellRmin( G4double radius )
{
  if( radius < 0. ) {
    shellRmin = 0.;
    G4cout << "Warning! Inner shell radius set to zero " << G4endl;
  }
  else {
    shellRmin = radius;
    G4cout << " ----> Inner shell radius is " << shellRmin/cm << " cm" << G4endl;
  }
}

void AgataAncillaryShell::SetShellRmax( G4double radius )
{
  if( radius < 0. ) {
    shellRmax = 0.;
    G4cout << "Warning! Outer shell radius set to zero " << G4endl;
  }
  else {
    shellRmax = radius;
    G4cout << " ----> Outer shell radius is " << shellRmax/cm << " cm" << G4endl;
  }
}

void AgataAncillaryShell::SetShellMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matName = materialName;
    G4cout << " ----> Shell material has been set to " << ptMaterial->GetName() << G4endl;
  }
  else {
    G4cout << " Material " << materialName << " not found! " << G4endl;
    if( matShell )
      G4cout << " ----> Keeping previous material (" << matShell->GetName() << ")" << G4endl;
    else
      G4cout << " ----> Keeping previous material (" << matName << ")" << G4endl; 
  }
}

void AgataAncillaryShell::ShowStatus()
{
  G4cout << " ANCILLARY SHELL has been constructed." << G4endl;
  G4cout << " Ancillary shell material is " << matShell->GetName() << G4endl;
  G4cout << " Ancillary shell size is " << shellRmin/cm << "--" << shellRmax/cm << " cm" << G4endl;
}

void AgataAncillaryShell::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillaryShell::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}


///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

AgataAncillaryShellMessenger::AgataAncillaryShellMessenger(AgataAncillaryShell* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 
  
  myDirectory = new G4UIdirectory("/Agata/detector/ancillary/Shell/");
  myDirectory->SetGuidance("Control of Shell detector construction.");

  SetShellSizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Shell/ancillarySize", this);  
  SetShellSizeCmd->SetGuidance("Define size of the ancillary shell.");
  SetShellSizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetShellSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ShellMatCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Shell/ancillaryMaterial", this);
  ShellMatCmd->SetGuidance("Select material of the ancillary.");
  ShellMatCmd->SetGuidance("Required parameters: 1 string.");
  ShellMatCmd->SetParameterName("choice",false);
  ShellMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAncillaryShellMessenger::~AgataAncillaryShellMessenger()
{
  delete myDirectory;
  delete ShellMatCmd;
  delete SetShellSizeCmd;
}

void AgataAncillaryShellMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetShellSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetShellRmin( e1*mm );
    myTarget->SetShellRmax( e2*mm );
  }
  if( command == ShellMatCmd ) {
    myTarget->SetShellMate(newValue);
  }
}    
