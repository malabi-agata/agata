////////////////////////////////////////////////////////
//  This class handles geometry to test attenuation, scattering and energy losses:
//  Added 09/2012 by Joa Ljungvall to make testing of the physics easier.
//  A target is positioned at 0,0,0 and encircled by a sphere. Energy loss in the
//  target is stored and the position and full energy of a particle when it hits the
//  sphere
////////////////////////////////////////////////////////

#include "AgataDetectorTest.hh"
#include "AgataSensitiveDetector.hh"
#include "AgataDetectorAncillary.hh"
#include "G4Material.hh"
#include "G4Polycone.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4UserLimits.hh"
#include "G4Ions.hh"

#include "G4NistManager.hh"


G4double MyUserLimits::GetMaxAllowedStep(const G4Track& track)

{
  if(track.GetParticleDefinition()->GetParticleType()=="nucleus" &&
     dynamic_cast<const G4Ions*>(track.GetParticleDefinition())->GetExcitationEnergy()>0){
    //We return flight path for 1/10 ps or 1/10 um, the largest one
    G4double maxpath = 1e-13*s*track.GetVelocity();
    return 0.0001*mm>maxpath ? 0.0001*mm : maxpath;
  }
  return DBL_MAX;
}

AgataDetectorTest::AgataDetectorTest(G4String ancType, G4String path,
				     G4String name)
{

  matTarget = NULL;
  matTargetName = "G4_Ge";
  matVac         = NULL;
  matVacName     = "G4_Galactic";
  
  readOut   = false;

  targetThickness = 0.1*mm;
  targetSide = 10*mm;
  



  targetposition = G4ThreeVector(0,0,0);
  
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;
  fScoreTestSphere = true;
  fScoreTarget = true;
  //  fUserLimits  = new MyUserLimits;
  if(ancType!="1 0"){
#ifndef DEIMOS
#ifndef NARRAY
    theAncillary       = new AgataDetectorAncillary(ancType,path,name);
#else
    theAncillary       = NULL;
#endif
#endif
  } else { theAncillary = 0;}
  myMessenger = new AgataDetectorTestMessenger(this,name);
}

AgataDetectorTest::~AgataDetectorTest()
{
  delete myMessenger;
}

G4int AgataDetectorTest::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4NistManager::Instance()->FindOrBuildMaterial(matTargetName);
  //G4Material::GetMaterial(matTargetName);
  if (ptMaterial) {
    matTarget = ptMaterial;
    G4String nome = matTarget->GetName();
    G4cout << "\n ----> The target material is "
	   << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matTargetName << G4endl;
    G4cout << " Could not build the target! " << G4endl;
    exit(1);
  }
  ptMaterial =  G4NistManager::Instance()->FindOrBuildMaterial(matVacName);//G4Material::GetMaterial(matVacName);
  if (ptMaterial) {
    matVac = ptMaterial;
    G4String nume = matVac->GetName();
    G4cout << "\n ----> The vacuum material is "
	   << nume << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matVacName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    exit(1);
    return 1;
  }
  return 0;  
}


void AgataDetectorTest::Placement()
{
  if( FindMaterials() ) return;
  
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  =
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  //"Counting sphere"
  G4Sphere *pSphere = new G4Sphere("TestSphere",1*m,1.2*m,0,2*pi,0,2*pi);
  G4LogicalVolume *pSphereL = new G4LogicalVolume(pSphere,matTarget,
						  "logTestSphere",0,0,0);
  G4VisAttributes *pSphereVA = new G4VisAttributes( G4Colour(0.0, 0.0, 1.0) );
  pSphereL->SetVisAttributes( pSphereVA );
  if (fScoreTestSphere) pSphereL->SetSensitiveDetector( theDetector->GeSD() );


  G4Box *pTarget = new G4Box("target",targetSide/2,targetSide/2,
			     targetThickness/2);
  G4LogicalVolume *pTargetL =
    new G4LogicalVolume(pTarget,matTarget,"logTarget",0,0,0);
  G4VisAttributes *pDetVA = new G4VisAttributes( G4Colour(1.0, 0.0, 0.0) );
  pTargetL->SetVisAttributes( pDetVA );
  if (fScoreTarget) pTargetL->SetSensitiveDetector( theDetector->GeSD() );
  G4RotationMatrix rm;
  new G4PVPlacement(G4Transform3D(rm, targetposition), "TargetPhys",
		    pTargetL, theDetector->HallPhys(), false, 0);
  new G4PVPlacement(G4Transform3D(rm, G4ThreeVector(0,0,0)), "TestSpherePhys",
		    pSphereL, theDetector->HallPhys(), false, 1);
  //  theDetector->HallLog()->SetUserLimits(fUserLimits);
  G4cout << " ----> Placed a single target" << G4endl;
  if(theAncillary) theAncillary->Placement();
}

void AgataDetectorTest::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  char line[256];
  outFileLMD << "TestGeometry" << G4endl << "SUMMARY 1" << G4endl;
  sprintf(line, "%3d %6.2f %6.2f %6.2f %6.2f %6.2f 1 1 1",
	  0, targetSide/unit, targetThickness/unit,
	  targetposition.x()/unit, targetposition.y()/unit, targetposition.z()/unit); 
  
  outFileLMD << G4String(line) << G4endl;
}

void AgataDetectorTest::WriteHeader(G4String *sheader, G4double unit)
{
  char line[256];
  sheader[0] += G4String("TestGeometry\nSUMMARY 1\n");
  sprintf(line, "%3d %6.2f %6.2f %6.2f %6.2f %6.2f 1 1 1",
               0, targetSide/unit, targetThickness/unit, targetposition.x()/unit, targetposition.y()/unit, targetposition.z()/unit ); 
  
  sheader[0] += G4String(line) + G4String("\n");
}


void AgataDetectorTest::ShowStatus()
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(6);
  G4cout.setf(ios::fixed);
  G4cout << " Placed a single target" << G4endl;
  G4cout << " Target size " << targetThickness/mm << " mm x " << targetSide/cm << " cm" << G4endl;
  G4cout << " Position " << targetposition/cm << " cm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////// methods for the messenger
/////////////////////////////////////////////////////////////////////////
void AgataDetectorTest::SetTargetThickness( G4double thickness )
{
  if( rad < 0. ) {
    G4cout << " Warning! Keeping previous value (" << targetThickness/mm << " mm)" << G4endl;
  }
  else {
    targetThickness =  thickness * mm;
    G4cout << " ----> Target thickness is " << targetThickness/mm << " mm" << G4endl;
  }
}

void AgataDetectorTest::SetTargetSide( G4double side )
{
  if( rad < 0. ) {
    G4cout << " Warning! Keeping previous value (" << targetSide/mm << " mm)" << G4endl;
  }
  else {
    targetSide = side * mm;
    G4cout << " ----> Target size is " << targetSide/mm << " mm" << G4endl;
  }
}


void AgataDetectorTest::SetTargetPosition( G4ThreeVector pos )
{
  targetposition = pos*mm;
  G4cout << " ----> Target will be placed at " << targetposition/mm << " mm" << G4endl;
}  


void AgataDetectorTest::SetTargetMaterial(G4String mat)
{
  matTargetName = mat;
  G4cout << " ----> Target will be made out of " << matTargetName << G4endl;
}

////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"

AgataDetectorTestMessenger::AgataDetectorTestMessenger(AgataDetectorTest* pTarget, G4String /*name*/)
  :myTarget(pTarget)
{ 

  SetThicknessCmd = new G4UIcmdWithADouble("/Agata/testing/targetThickness",this);  
  SetThicknessCmd->SetGuidance("Set thickness of target.");
  SetThicknessCmd->SetGuidance("Required parameters: 1 double (in mm)");
  SetThicknessCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetSideCmd = new G4UIcmdWithADouble("/Agata/testing/targetSide",this);  
  SetSideCmd->SetGuidance("Set side of target.");
  SetSideCmd->SetGuidance("Required parameters: 1 double (n mm)");
  SetSideCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetPositionCmd = new G4UIcmdWith3Vector("/Agata/testing/positionTarget",this);  
  SetPositionCmd->SetGuidance("Set position of target.");
  SetPositionCmd->SetGuidance("Required parameters: 3 double (x, y, z in mm).");
  SetPositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  SetTargetMaterial = new G4UIcmdWithAString("/Agata/testing/targetMaterial",this);
  SetTargetMaterial->SetGuidance("Set target material");
  SetTargetMaterial->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetScoreTestSphere = new G4UIcmdWithABool("/Agata/testing/SetScoreTestSphere",this);
  SetScoreTestSphere->SetGuidance("Score hits in testsphere, or not. A bool");
  SetScoreTestSphere->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetScoreTarget = new G4UIcmdWithABool("/Agata/testing/SetScoreTarget",this);
  SetScoreTarget->SetGuidance("Score hits in target, or not. A bool");
  SetScoreTarget->AvailableForStates(G4State_PreInit,G4State_Idle);

  
}

AgataDetectorTestMessenger::~AgataDetectorTestMessenger()
{

  delete SetThicknessCmd;
  delete SetSideCmd;
  delete SetPositionCmd;
  delete SetTargetMaterial;
}

void AgataDetectorTestMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetPositionCmd ) {
    myTarget->SetTargetPosition(SetPositionCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetThicknessCmd ) {
    myTarget->SetTargetThickness(SetThicknessCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetSideCmd ) {
    myTarget->SetTargetSide(SetSideCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetTargetMaterial ) {
    myTarget->SetTargetMaterial(newValue);
  }
  if(command==SetScoreTestSphere){
    myTarget->SetScoreTestSphere(SetScoreTestSphere->GetNewBoolValue(newValue));
  }
  if(command==SetScoreTarget){
    myTarget->SetScoreTarget(SetScoreTestSphere->GetNewBoolValue(newValue));
  }
  
}
  

