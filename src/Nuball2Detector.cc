#include "Nuball2Detector.hh"
#include "AgataSensitiveDetector.hh"
#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4ExtrudedSolid.hh"
#include "G4TwoVector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include <iomanip>
#include <sstream>
#include "Nuball2Angles.hh"

#include "NuballClover.hh"
#include "NuballCloverBGO.hh"
//#include "NuballPhaseI.hh"
//#include "NuballPhaseIBGO.hh"
#include "NuballLaBr3.hh"
#include "NuballLaBr3Valencia.hh"


namespace PhaseIGeomNuball2  {
  // -- -TRONCONIQUE PHASEI, NEW ---------------------
  // Detector definition for the tronconique (PhaseI)
  const G4double TronCrystalLength	= 70.0*mm; // cystal length
	
  const G4double TronCrystalEdgeDepth	= 30.0*mm; // depth at which starts TronCrystalRadiusMax	
  const G4double TronCrystalRadiusMin	=  32.0*mm; // radius of the crystal at the entrance face	
  const G4double TronCrystalRadiusMax	=  35.0*mm; // radius of the crystal at the back face	
	
  const G4double TronCrystalHoleDepth	= 15.0*mm; // depth at which starts the hole
  const G4double TronCrystalHoleRadius	=  5.0*mm; // radius of the hole
	
  const G4double TronBGOLength		= 190.0*mm; // length of the BGO sheild
  const G4double TronBGOFace2		=  73.0*mm;
  const G4double TronBGOFace1		=  54.0*mm;
  const G4double TronBGOFace2InnRad	=  52.0*mm;
  const G4double TronBGOFace1InnRad	=  33.0*mm;
  const G4double TronBGOCut1X		=  16.01*mm;
  const G4double TronBGOCut1Angle		=  15.04*deg;	//18.64*deg;
  const G4double TronBGOCut1AngleZ	=  30.86*deg;
  const G4double TronBGOCut1Y		=  73.0*mm;
  const G4double TronBGOCut1Z		=  190.0*mm;
  const G4double TronBGOCut1AnglePosition	=  30.86*deg;
	
  const G4double TronBGOCut2X		=  16.67*mm;
  const G4double TronBGOCut2Angle		=  15.4*deg;	//18.0*deg;
  const G4double TronBGOCut2AngleZ	=  90.0*deg;
  const G4double TronBGOCut2Y		=  73.0*mm;
  const G4double TronBGOCut2Z		=  190.0*mm;
  const G4double TronBGOCut2AnglePosition	=  90.0*deg;
	
  const G4double TronBGOCut3X		=  16.01*mm;
  const G4double TronBGOCut3Angle		=  23.05*deg;//25.65*deg;
  const G4double TronBGOCut3AngleZ	=  56.11*deg;
  const G4double TronBGOCut3Y		=  73.0*mm;
  const G4double TronBGOCut3Z		=  190.0*mm;
  const G4double TronBGOCut3AnglePosition	=  123.89*deg;
	
  const G4double TronBGOCut4X		=  15.89*mm;
  const G4double TronBGOCut4Angle		=  13.88*deg;//16.48*deg;
  const G4double TronBGOCut4AngleZ	=  0.0*deg;
  const G4double TronBGOCut4Y		=  73.0*mm;
  const G4double TronBGOCut4Z		=  190.0*mm;
  const G4double TronBGOCut4AnglePosition	=  180.0*deg;
			
  //	const 	G4double	TronColiPosition		=299.6*mm;
  //	const 	G4double	TronColiFace2Position		=334.8*mm;
  const 	G4double TronColiFace1		= 70.0*mm;
  const 	G4double TronColiFace1InnRad	= 27.0*mm;
  const 	G4double TronColiFace2InnRad	= 34.0*mm;
  const 	G4double TronColiLength		= 35.0*mm;
	
  // to shape correctly the collimator
  const G4double TronColiCut1X			=44.22*mm;
  const G4double TronColiCut1Angle		=16.04*deg;//18.64*deg;
  const G4double TronColiCut1AngleZ		=30.86*deg;
  const G4double TronColiCut1Y			=84.0*mm;
  const G4double TronColiCut1Z			=252.0*mm;
  const G4double TronColiCut1AnglePosition	=30.86*deg;
	
  const G4double TronColiCut2X			=44.3*mm;
  const G4double TronColiCut2Angle		=15.4*deg;//18.5*deg;
  const G4double TronColiCut2AngleZ		=90.0*deg;
  const G4double TronColiCut2Y			=84.0*mm;
  const G4double TronColiCut2Z			=252.0*mm;
  const G4double TronColiCut2AnglePosition	=90.0*deg;
	
  const G4double TronColiCut3X			=44.2*mm;
  const G4double TronColiCut3Angle		=23.05*deg;//25.65*deg;
  const G4double TronColiCut3AngleZ		=56.11*deg;
  const G4double TronColiCut3Y			=84.0*mm;
  const G4double TronColiCut3Z			=252.0*mm;
  const G4double TronColiCut3AnglePosition	=123.89*deg;
	
  const G4double TronColiCut4X			=43.87*mm;
  const G4double TronColiCut4Angle		=13.88*deg;//16.48*deg;
  const G4double TronColiCut4AngleZ		=0.0*deg;
  const G4double TronColiCut4Y			=84.0*mm;
  const G4double TronColiCut4Z			=252.0*mm;
  const G4double TronColiCut4AnglePosition	=180.0*deg;
			
  // ---------------------OLD--------------------------
	

	
  // 	---------TronCrys, CapsuleOut et Capsule
  const 	G4double TronCapsuleOutFace1		=33.5*mm;
  const   G4double TronCapsuleOutFace2		=46.0*mm;
  const 	G4double TronCapsuleOutWidth		=1.0*mm;
  const   G4double TronCapsuleOutPosition		=337.0*mm;
  const 	G4double TronCapsuleOutFace2Position	=457.0*mm;
  const 	G4double TronCapsuleOutFace3Position	=587.0*mm;
	
  const 	G4double TronCapsuleFace1		=34.0*mm;
  const   G4double TronCapsuleFace2		=44.0*mm;
  const 	G4double TronCapsuleWidth		=1.0*mm;
  const   G4double TronCapsulePosition		=358.0*mm;
  const 	G4double TronCapsuleFace2Position	=457.0*mm;
  const 	G4double TronCapsuleFace3Position	=647.0*mm;
	
  const 	G4double TronCrystalFace1		=32.0*mm;
  const   G4double TronCrystalFace2		=35.0*mm;
  const   G4double TronCrystalPosition		=374.0*mm;
  const 	G4double TronCrystalFace2Position	=414.0*mm;
  const 	G4double TronCrystalFace3Position	=444.0*mm;
	
  // 	-----------------OuterCapsule
  const G4double TronOuterCapsuleCut1X			=44.22*mm;
  const G4double TronOuterCapsuleCut1Angle		=16.04*deg;//18.64*deg;
  const G4double TronOuterCapsuleCut1AngleZ		=30.86*deg;
  const G4double TronOuterCapsuleCut1Y			=84.0*mm;
  const G4double TronOuterCapsuleCut1Z			=252.0*mm;
  const G4double TronOuterCapsuleCut1AnglePosition	=30.86*deg;
	
  const G4double TronOuterCapsuleCut2X			=44.3*mm;
  const G4double TronOuterCapsuleCut2Angle		=15.4*deg;//18.5*deg;
  const G4double TronOuterCapsuleCut2AngleZ		=90.0*deg;
  const G4double TronOuterCapsuleCut2Y			=84.0*mm;
  const G4double TronOuterCapsuleCut2Z			=252.0*mm;
  const G4double TronOuterCapsuleCut2AnglePosition	=90.0*deg;
	
  const G4double TronOuterCapsuleCut3X			=44.2*mm;
  const G4double TronOuterCapsuleCut3Angle		=23.05*deg;//25.65*deg;
  const G4double TronOuterCapsuleCut3AngleZ		=56.11*deg;
  const G4double TronOuterCapsuleCut3Y			=84.0*mm;
  const G4double TronOuterCapsuleCut3Z			=252.0*mm;
  const G4double TronOuterCapsuleCut3AnglePosition	=123.89*deg;
	
  const G4double TronOuterCapsuleCut4X			=43.87*mm;
  const G4double TronOuterCapsuleCut4Angle		=13.88*deg;//16.48*deg;
  const G4double TronOuterCapsuleCut4AngleZ		=0.0*deg;
  const G4double TronOuterCapsuleCut4Y			=84.0*mm;
  const G4double TronOuterCapsuleCut4Z			=252.0*mm;
  const G4double TronOuterCapsuleCut4AnglePosition	=180.0*deg;


  const 	G4double TronOuterCapsuleLength			=250.0*mm;
  const 	G4double TronOuterCapsuleWidth			=1.5*mm;
  const 	G4double TronOuterCapsulePosition		=335.0*mm;//335.0*mm;
  const 	G4double TronOuterCapsuleOutRad			=83.75*mm;
  const   G4double AdjactTronBGO2Capsule			=0.2*mm;
  // 	---------------------------------Arriere
  const 	G4double	TronArreFace1			=82.05*mm;
  const	G4double	TronArreFace1InnRad		=46.2*mm;
  const	G4double	TronArreFace2			=82.05*mm;
  const	G4double	TronArreFace2InnRad		=46.2*mm;
  const	G4double	TronArreFace3			=86.05*mm;
  const	G4double	TronArreFace3InnRad		=46.2*mm;
  const	G4double 	TronArreFace4			=86.05*mm;
  const	G4double	TronArreFace4InnRad		=44.2*mm;
  const	G4double	TronArreFace5			=82.05*mm;
  const	G4double	TronArreFace5InnRad		=44.2*mm;
  const	G4double	TronArreFace6			=82.05*mm;
  const	G4double	TronArreFace6InnRad		=44.2*mm; 	
  const	G4double	TronArreFace1Position		=574.0*mm;
  const	G4double	TronArreFace2Position		=586.9*mm;
  const	G4double	TronArreFace3Position		=587.0*mm;
  const	G4double	TronArreFace4Position		=592.0*mm;
  const	G4double	TronArreFace5Position		=592.1*mm;
  const	G4double	TronArreFace6Position		=607.1*mm;
  // 	-------------------------------------------TRONCOLIMATOR

  G4double ROrgam = 205*mm;

}

using namespace PhaseIGeomNuball2;



G4LogicalVolume *Nuball2Detector::GetTronBGO()
{	
  char sName[40]; 
  G4int nbSlice=2;
  G4double  zSlice[2]={0.0, TronBGOLength};
  G4double  InnRad[2]={TronBGOFace1InnRad, TronBGOFace2InnRad};
  G4double  OutRad[2]={TronBGOFace1, TronBGOFace2};
		
  G4int nbSliceTube=3;
  G4double  zSliceTube[3]={-0.1*mm, TronBGOLength-68.0*mm,TronBGOLength};
  G4double  InnRadTube[3]={0.0,0.0,0.0};
  G4double  OutRadTube[3]={34.8*mm, 47.0*mm,47.0*mm};
  if ( pTronBGO == NULL ) {     		
    sprintf(sName, "TronBGObox1");	
    G4Box *box1=new G4Box(G4String(sName),TronBGOCut1X,TronBGOCut1Y,TronBGOCut1Z);
    sprintf(sName, "TronBGObox2");	
    G4Box *box2=new G4Box(G4String(sName),TronBGOCut2X,TronBGOCut2Y,TronBGOCut2Z);
    sprintf(sName, "TronBGObox3");	
    G4Box *box3=new G4Box(G4String(sName),TronBGOCut3X,TronBGOCut3Y,TronBGOCut3Z);
    sprintf(sName, "TronBGObox4");	
    G4Box *box4=new G4Box(G4String(sName),TronBGOCut4X,TronBGOCut4Y,TronBGOCut4Z);
    sprintf(sName, "TronBGOTube");	
    G4Polycone *Tube=new G4Polycone(G4String(sName),0*deg, 360.0*deg, nbSliceTube,
				    zSliceTube, InnRadTube, OutRadTube);
		
    sprintf(sName, "TronBGO");
    G4Polyhedra *Tron =new G4Polyhedra(G4String(sName), 0.*deg, 360.0*deg, 10,
				       nbSlice, zSlice, InnRad, OutRad);
		
    sprintf(sName, "TronBGO1");
    G4RotationMatrix rm;
    rm.rotateY(-TronBGOCut1Angle);
    rm.rotateZ(-TronBGOCut1AngleZ);
    G4SubtractionSolid *Tron1=new G4SubtractionSolid(G4String(sName),Tron,box1,&rm,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut1AnglePosition),TronBGOFace1*sin(TronBGOCut1AnglePosition),0.0));
    sprintf(sName, "TronBGO2");	
    G4RotationMatrix rm1;
    rm1.rotateY(-TronBGOCut1Angle);
    rm1.rotateZ(TronBGOCut1AngleZ);
    G4SubtractionSolid *Tron2=new G4SubtractionSolid(G4String(sName),Tron1,box1,&rm1,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut1AnglePosition),-TronBGOFace1*sin(TronBGOCut1AnglePosition),0.0));
    sprintf(sName, "TronBGO3");
    G4RotationMatrix rm2;
    rm2.rotateZ(TronBGOCut2AngleZ);
    rm2.rotateY(TronBGOCut2Angle);
    G4SubtractionSolid *Tron3=new G4SubtractionSolid(G4String(sName),Tron2,box2,&rm2,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut2AnglePosition),TronBGOFace1*sin(TronBGOCut2AnglePosition),0.0));
    sprintf(sName, "TronBGO4");
    G4RotationMatrix rm4;
    rm4.rotateZ(TronBGOCut2AngleZ);
    rm4.rotateY(-TronBGOCut2Angle);
    G4SubtractionSolid *Tron4=new G4SubtractionSolid(G4String(sName),Tron3,box2,&rm4,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut2AnglePosition),-TronBGOFace1*sin(TronBGOCut2AnglePosition),0.0));
    sprintf(sName, "TronBGO5");
    G4RotationMatrix rm5;
    rm5.rotateY(TronBGOCut3Angle);
    rm5.rotateZ(-TronBGOCut3AngleZ);
    G4SubtractionSolid *Tron5=new G4SubtractionSolid(G4String(sName),Tron4,box3,&rm5,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut3AnglePosition),-TronBGOFace1*sin(TronBGOCut3AnglePosition),0.0));
    sprintf(sName, "TronBGO6");
    G4RotationMatrix rm6;
    rm6.rotateY(TronBGOCut3Angle);
    rm6.rotateZ(TronBGOCut3AngleZ);
    G4SubtractionSolid *Tron6=new G4SubtractionSolid(G4String(sName),Tron5,box3,&rm6,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut3AnglePosition),TronBGOFace1*sin(TronBGOCut3AnglePosition),0.0));
    sprintf(sName, "TronBGO7");
    G4RotationMatrix rm7;
    rm7.rotateY(TronBGOCut4Angle);
    rm7.rotateZ(TronBGOCut4AngleZ);
    G4SubtractionSolid *Tron7=new G4SubtractionSolid(G4String(sName),Tron6,box4,&rm7,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut4AnglePosition),TronBGOFace1*sin(TronBGOCut4AnglePosition),0.0));
    sprintf(sName, "TronBGO8");
    G4RotationMatrix rm8;
    G4SubtractionSolid *Tron8=new G4SubtractionSolid(G4String(sName),Tron7,Tube,&rm8,G4ThreeVector(0.0,0.0,0.0));
		
    sprintf(sName, "aTronBGO");
    pTronBGO  = new G4LogicalVolume( Tron8, matAntiCompton1, G4String(sName), 0, 0, 0 ); // PB with this one during conversion with VGM
    G4VisAttributes *pBGOVA = new G4VisAttributes(G4Colour(0.7, 0.7, 0));
    pTronBGO->SetVisAttributes(pBGOVA);
  }
  return 	pTronBGO;
}

G4LogicalVolume *Nuball2Detector::GetTronCrystal()
{
  char sName[40]; 
	
  G4int nbSlice = 5;
  G4double zSlice[5] = 
    { 0.0*mm, 
      TronCrystalHoleDepth, TronCrystalHoleDepth + 1.0*mm, 
      TronCrystalEdgeDepth, TronCrystalLength };  
	 
  G4double InnRad[5] =
    { 0.0*mm, 
      0.0*mm, TronCrystalHoleRadius, 
      TronCrystalHoleRadius, TronCrystalHoleRadius };  
	      
  G4double OutRad[5] = 
    { TronCrystalRadiusMin, 
      TronCrystalRadiusMin+
      (TronCrystalRadiusMax-TronCrystalRadiusMin)*TronCrystalHoleDepth/TronCrystalEdgeDepth, 
      TronCrystalRadiusMin+
      (TronCrystalRadiusMax-TronCrystalRadiusMin)*(TronCrystalHoleDepth+1.0*mm)/TronCrystalEdgeDepth,
      TronCrystalRadiusMax, TronCrystalRadiusMax }; 
				
  if ( pTronCrystal == NULL ){
    sprintf(sName,"Tron");
    G4Polycone *Tron = 
      new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
    sprintf(sName, "aTronCrystal");
    pTronCrystal  = new G4LogicalVolume( Tron, matCrystal, G4String(sName), 0, 0, 0 );
    G4VisAttributes *pDetVACaps = new G4VisAttributes( G4Colour(0.0, 1.0, 0.0) );
    pTronCrystal->SetVisAttributes(pDetVACaps);
  }
  return pTronCrystal;
}

G4LogicalVolume *Nuball2Detector::GetTronCapsule()

{
  char sName[40];
  G4int nbSlice=7;
  G4double zSlice[7]={	TronCapsulePosition,
			TronCapsulePosition+TronCapsuleOutWidth,
			TronCapsulePosition+TronCapsuleOutWidth,
			TronCapsuleFace2Position,
			TronCapsuleFace3Position-TronCapsuleWidth,
			TronCapsuleFace3Position-TronCapsuleWidth,
			TronCapsuleFace3Position};
  G4double InnRad[7]={	0.0,
			0.0,
			TronCapsuleFace1-TronCapsuleOutWidth,
			TronCapsuleFace2-TronCapsuleOutWidth,
			TronCapsuleFace2-TronCapsuleOutWidth,
			0.0,
			0.0};
				
  G4double OutRad[7]={	TronCapsuleFace1,
			TronCapsuleFace1,
			TronCapsuleFace1,
			TronCapsuleFace2,
			TronCapsuleFace2,
			TronCapsuleFace2,
			TronCapsuleFace2};
  if(pTronCapsule==NULL){
    sprintf(sName,"Tron");
    G4Polycone *Tron=new G4Polycone(G4String(sName),0.0*deg,360.0*deg, nbSlice,zSlice,InnRad,OutRad);
    sprintf(sName, "aTronCapsule");
    pTronCapsule  = new G4LogicalVolume( Tron, matCapsule, G4String(sName), 0, 0, 0 );
    G4VisAttributes *pCapsuleVA = new G4VisAttributes(G4Colour(0.8, 0.8, 0.5));
    //    pCapsuleVA->SetForceWireframe(true);
    pTronCapsule->SetVisAttributes(pCapsuleVA);
  }
  return pTronCapsule;

  
}

G4LogicalVolume *Nuball2Detector::GetTronColimator()
{	
  G4int nbSliceTube2=2;
  G4double  zSliceTube2[2]={-TronColiLength/2.-1e3*mm, TronColiLength/2.+1e-3*mm};
  G4double  InnRadTube2[2]={0,0};
  G4double  OutRadTube2[2]={TronColiFace2InnRad,TronColiFace1InnRad};
  G4Polycone *colhole=
    new G4Polycone("colhole",0*deg, 360.0*deg, nbSliceTube2, zSliceTube2, InnRadTube2, OutRadTube2);
  G4double zR1=.966028*mm;
  G4double sz1 = (ROrgam-73.9*mm)/zR1;
  G4double sz2 = (ROrgam-73.9*mm+TronColiLength)/zR1;
  //Get extra side position y
  G4double extray = 33*mm/sz1;//TronOuterCapsuleOutRad/sz1;  //!!! guessed !!!!
  //Get extrax1, extrax2
  G4double extrax1 = .0703483*mm+(.258437*mm-0.0703483*mm)*(.314713*mm-extray)/(.314713*mm-0);
  G4double extrax2 = .0703483*mm-(0.0703483*mm+.398168*mm)*(.314713*mm-extray)/(.314713*mm-0);
  //Get extra side x pos, same as y but neg
  G4double extrax = -extray;
  G4double extray1 = .314713*mm-(.314713*mm-0)*(0.0703483*mm-extrax)/(0.0703483*mm+.398168*mm);
  G4double coordR1[7][2]={{.258437*mm,0},
			  {extrax1,extray},
			  {extrax2,extray},
			  {extrax,extray1},
			  {extrax,-extray1},
			  {extrax2,-extray},
			  {extrax1,-extray}};
  std::vector<G4TwoVector> pentagonshape;
  for(int i=0; i<7; i++) pentagonshape.push_back(G4TwoVector(coordR1[i][0],coordR1[i][1]));
  G4ExtrudedSolid *shunkofTungsten = new G4ExtrudedSolid("shunkofTungsten",pentagonshape,
							 TronColiLength/2,G4TwoVector(0,0),sz1,
							 G4TwoVector(0,0),sz2);
  G4SubtractionSolid *coli = new G4SubtractionSolid("collimator",shunkofTungsten,colhole);
  pTronColimator  = new G4LogicalVolume( coli,  matCollimator, "logCol", 0, 0, 0 );
  G4VisAttributes *pColVA = new G4VisAttributes(G4Colour(0.3, 0.3, .3));
  pTronColimator->SetVisAttributes(pColVA);
  
   
  return 	pTronColimator;
}




Nuball2Detector::Nuball2Detector(G4String anctype, G4String path,
				 G4String name)
{
  G4cout<<"JL: Nuball2Detector, beginning"<<G4endl;
  // base to build one clover (shaped crystal)
  iniPath = path;
  directoryName      = name;
  
  // material definition
  matCrystal  = matCapsule = matAntiCompton1 = matAntiCompton2 = NULL;
  matCrystalName   = "Germanium"; 
  matCapsuleName   = "Aluminium";
  matAntiCompton1Name = "BGO"; matAntiCompton2Name = "CsI";

  //Commented by AO
  // G4SDManager* SDman = G4SDManager::GetSDMpointer();
  // SDman->
  //   AddNewDetector(bgoSD=new AgataSensitiveDetector("Nuball2", 
  // 						    "/BGO/all", 
  // 						    "BGOCollection",false ) );

	
  ancType = anctype;
  theAncillary = 0;
  mode=3;
  UseCloverBGO=true;
  UseCloverColl=true;
  UsePhaseIBGO=true;
  UsePhaseIColl=true;

  pCrystal=0;
  pCapsule=0;
  pBGOCrystal=0;
  pBGOCapsule=0;
   
  pTronCrystal=0;
  pTronCapsule=0;
  pTronCapsuleOut=0;
  pTronBGO=0;
  pTronOuterCapsule=0;
  
  pTronArre=0;
  pTronColimator=0;

  
  std::cout << "ancType :" << ancType << " ";
  if(ancType!="1 0") {
    theAncillary = new AgataDetectorAncillary(ancType,"./",
					      "OrsayPlasticCollection");
      std::cout << ancType << "\n";
  }
  myMessenger = new Nuball2DetectorMessenger(this);
}

Nuball2Detector::~Nuball2Detector()
{
  G4cout << "### ~Nuball2Detector "<< G4endl;
  delete myMessenger;
}

G4int Nuball2Detector::FindMaterials()
{
  G4NistManager* nistManager = G4NistManager::Instance();
  // search the material by their names
  matCrystal      = G4Material::GetMaterial(matCrystalName);
  // should be ok .. standard in Nuball2
  if(matCrystal==0) {
    std::cout << "Oups " << matCrystalName << " not found.\n";
    exit(-1);
  }
  matCapsule      = G4Material::GetMaterial(matCapsuleName);
  // should be ok .. standard in Nuball2
  if(matCapsule==0) {
    std::cout << "Oups " << matCapsuleName << " not found.\n";
    exit(-1);
  }
  matAntiCompton1 = G4Material::GetMaterial(matAntiCompton1Name);
  // in case BGO is not known
  if ( matAntiCompton1 == NULL ) {
    // Germanium isotopes
    G4Isotope* Ge70 = new G4Isotope("Ge70", 32, 70, 69.9242*g/mole);
    G4Isotope* Ge72 = new G4Isotope("Ge72", 32, 72, 71.9221*g/mole);
    G4Isotope* Ge73 = new G4Isotope("Ge73", 32, 73, 72.9235*g/mole);
    G4Isotope* Ge74 = new G4Isotope("Ge74", 32, 74, 73.9212*g/mole);
    G4Isotope* Ge76 = new G4Isotope("Ge76", 32, 76, 75.9214*g/mole);
    // germanium defined via its isotopes
    G4Element* elGe = new G4Element("Germanium","Ge", 5);
    elGe->AddIsotope(Ge70, 0.2123);
    elGe->AddIsotope(Ge72, 0.2766);
    elGe->AddIsotope(Ge73, 0.0773);
    elGe->AddIsotope(Ge74, 0.3594);
    elGe->AddIsotope(Ge76, 0.0744);

    matAntiCompton1 = new G4Material("BGO", 7.13*g/cm3, 3);
    matAntiCompton1->AddElement( new G4Element("Bismuth",  "Bi",
					       83., 208.98038*g/mole), 4);
    matAntiCompton1->AddElement(elGe, 3);
    matAntiCompton1->AddElement( new G4Element("Oxigen",   "O",
					       8.,  15.9994 *g/mole) , 12);
  }
  matAntiCompton2 = G4Material::GetMaterial(matAntiCompton2Name);
  // in case CsI is not known
  if ( matAntiCompton2 == NULL ) {
    matAntiCompton2 = new G4Material("CsI", 4.51*g/cm3, 2);
    matAntiCompton2->AddElement( new G4Element("Cesium", "Cs" ,
					       55., 132.90545*g/mole), 1);
    matAntiCompton2->AddElement( new G4Element("Iodine",  "I" ,
					       53., 126.90477*g/mole), 1);
  }

  matCollimator = G4Material::GetMaterial("Tungsten"); 
  if ( matCollimator == NULL ) {	
    G4cout << " ***** NOT KNOWN ****  TUNGSTEN *****" << G4endl;
  }	
  if (matCrystal && matAntiCompton1 && matAntiCompton2) {
    G4cout << "\n ---->  The detector material are " 
	   << matCrystalName << " " << matAntiCompton1Name << " "
	   << matAntiCompton2Name << G4endl;
  }
  else {
    G4cout << " Could not find the correct materials " <<  G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  //Constructing Hevimet
  std::vector<G4String> elHevimet = {"W", "Ni", "Fe"};
  std::vector<G4double> weightHevimet =
    {96.0*perCent, 3.5*perCent, 0.5*perCent};
  nistManager->ConstructNewMaterial("Hevimet", elHevimet,
				    weightHevimet, 17.0*g/cm3);
  //Constructing DurAl
  std::vector<G4String> elDurAl = {"Cu", "Mg", "Mn", "Al"};
  std::vector<G4double> weightDurAl = {3.5*perCent, 0.5*perCent,
				       0.6*perCent, 95.4*perCent};
  nistManager->ConstructNewMaterial("DurAl", elDurAl, weightDurAl, 2.8*g/cm3);
  //Constructing LaBr3
  std::vector<G4String> elLaBr3 = {"La", "Br"};
  std::vector<G4int> weightLaBr3 = {1, 3};
  nistManager->ConstructNewMaterial("LaBr3", elLaBr3, weightLaBr3, 17.0*g/cm3);
  //Constructing Glass
  std::vector<G4String> elGlass = {"H", "C", "O"};
  std::vector<G4int> weightGlass = {8, 5, 2};
  nistManager->ConstructNewMaterial("Glass", elGlass, weightGlass, 1.18*g/cm3);
  //Construction muMet
  std::vector<G4String> elMuMet = {"C", "Ni", "Mn", "Mo", "Si", "Fe"};
  std::vector<G4double> weightMuMet = {0.0002, 0.80, 0.005, 0.042,
				       0.0035, 0.1493};
  nistManager->ConstructNewMaterial("MuMet", elMuMet, weightMuMet,
				    8.747*g/cm3);
  //Constructing MgO
  std::vector<G4String> elMgO = {"Mg", "O"};
  std::vector<G4int> weightMgO = {1, 1};
  nistManager->ConstructNewMaterial("MgO", elMgO, weightMgO, 3.58*g/cm3);

  return 0;  
}





//#define _DEBUGGEOM_

void Nuball2Detector::Placement()
{
  fDetAngles.clear();
  std::cout << "Nuball2Detector::Placement()" << "\n";
  //  G4VPhysicalVolume *phyvol;
  if( FindMaterials() ) {
    std::cout << "FindMaterials()==1!\n";
    return;	// check if materials are known
  }
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theTarget  = 
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  G4bool Overlap = false;
  if(((mode>>0)&0x01)==1){
    std::cout << "Building clovers!\n";
    G4RotationMatrix rotMat;
    Clover* clovers = new Clover();
    CloverBGO* cloverbgos = new CloverBGO();
    std::fstream fileClover("CloverPosition.dat", std::ios_base::in);
    std::fstream fileCloverBGO("CloverBGOPosition.dat", std::ios_base::in);
    if(!fileClover.is_open() || !fileCloverBGO.is_open()){
      G4cerr << "ERROR: Input files for clover geometry not found !"
	     << G4endl;
      exit(-1);
    }
    for(int i = 0; i < 24; i++){
      G4double rC, thetaC, phiC;
      G4double rBGO, thetaBGO, phiBGO;
      G4String cloverName, cloverAlveolus;
      G4String cloverBGOName, cloverBGOAlveolus;
      G4int cloverLabel, cloverBGOLabel;
      
      rotMat.set(0,0,0);
      fileClover >> cloverName >> cloverAlveolus >> rC >>
	thetaC >> phiC >> cloverLabel;
      fileCloverBGO >> cloverBGOName >> cloverBGOAlveolus >> rBGO
		    >> thetaBGO >> phiBGO >> cloverBGOLabel;
      thetaC*=deg;
      phiC*=deg;
      thetaBGO*=deg;
      phiBGO*=deg;
      //--Clover--//
      rotMat.rotateY(thetaC);
      rotMat.rotateZ(phiC);
      G4double radius = (rC + 34.5)*mm;
      G4ThreeVector translation(radius*sin(thetaC)*cos(phiC),
				radius*sin(thetaC)*sin(phiC),
				radius*cos(thetaC));
      clovers->Placement(cloverLabel, (theTarget)->HallPhys(), Overlap,
			 translation,rotMat, i);
      //Here we get the angels of all four crystals
      double dPos = 20.125;
      G4double leafX,leafY;
      G4double leafZ=14.75;
      G4ThreeVector ClustCrystalPos;
      for(int l=0; l<4; l++){
	if(l < 2) {
	  leafX = dPos;
	} else {
	  leafX = -dPos;
	}
	//the y-translation
	if(l == 0 || l == 3 ) {
	  leafY = dPos;
	} else {
	  leafY = -dPos;
	}
	ClustCrystalPos=G4ThreeVector(leafX,leafY,leafZ+radius);
	fDetAngles[cloverLabel+l]=rotMat*ClustCrystalPos;
      }
      if(UseCloverBGO){
	//--BGO--//
	rotMat.set(0,0,0);
	rotMat.rotateZ(-45*deg+180*deg*(i%2-1));
	rotMat.rotateY(thetaBGO);
	rotMat.rotateZ(phiBGO);
	//G4double sradius = (rBGO - 35.)*mm;
	G4double sradius = (rBGO)*mm;
	G4ThreeVector stranslation(sradius*sin(thetaBGO)*cos(phiBGO),
				   sradius*sin(thetaBGO)*sin(phiBGO),
				   sradius*cos(thetaBGO));
	cloverbgos->SetHeavyMet(UseCloverColl);
	cloverbgos->Placement(300+cloverBGOLabel, (theTarget)->HallPhys(),
			      Overlap,
			      stranslation, rotMat, i);
      }
    }
    fileClover.close();
    fileCloverBGO.close();
    for(auto &it:clovers->GetScoringVolumes())
      it->SetSensitiveDetector(theTarget->GeSD());
    if(UseCloverBGO)
      for(auto &it:cloverbgos->GetScoringVolumes())
	it->SetSensitiveDetector(theTarget->GeSD());
  }
  if(((mode>>1)&0x01)==1){
    std::cout << "Building Phase1's!\n";
    GetTronBGO();
    GetTronCrystal();
    GetTronCapsule();
    GetTronColimator();
    G4double extraR = 0*mm;
    G4double RTron = //375.0*mm
      ROrgam+extraR, RTronBGO = RTron-37.5*mm, RTronColli = RTron-73.9*mm+TronColiLength/2.; 
    pTronCrystal->SetSensitiveDetector(theTarget->GeSD());
    pTronBGO->SetSensitiveDetector(theTarget->GeSD());
    std::ostringstream os;
    for(int detnb=0; detnb<20; detnb++){
      if(((mode>>2)&0x01)==1 && detnb<10) continue;
      os.str("");
      os << "PhaseI_crystal_" << detnb;
      std::cout << os.str() << std::endl;
      G4RotationMatrix rm;
      if(nuball2angles[detnb][0]>90) rm.rotateZ(180.*deg);
      rm.rotateY(nuball2angles[detnb][0]*deg);
      rm.rotateZ(nuball2angles[detnb][1]*deg);
      rm.rotateY(nuball2angles[detnb][2]*deg);
      rm.rotateZ(nuball2angles[detnb][3]*deg);
      fDetAngles[500+detnb] = rm(G4ThreeVector(0,0,RTron));
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTron))),
			G4String(os.str()),pTronCrystal,(theTarget)->HallPhys(),
			false, 500+detnb );
      os.str("");
      os << "PhaseI_capsule_" << detnb;
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm,
				      rm(G4ThreeVector(0,0,RTronColli-
						       TronCapsuleOutPosition))),
			G4String(os.str()),pTronCapsule,(theTarget)->HallPhys(),
			false, 500+detnb );
      os.str("");
      if(UsePhaseIBGO){
	os << "PhaseI_BGO_" << detnb;
	/*phyvol = */
	new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTronBGO))),
			  G4String(os.str()),pTronBGO,(theTarget)->HallPhys(),
			  false, detnb+800);
	os.str("");
	os << "PhaseI_Collimator_" << detnb;
	/*phyvol =*/
	new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTronColli))),
			  G4String(os.str()),pTronColimator,
			  (theTarget)->HallPhys(), false, detnb+800);
      }
    }
  }
  if(((mode>>2)&0x01)==1){
    G4RotationMatrix rotMat;
    LaBr3_valencia* LaBrValencia = new LaBr3_valencia();
    LaBr3* LaBr = new LaBr3();
    std::fstream file("LaBr3Position.dat", std::ios_base::in);
    if(!file.is_open()){
      G4cerr << "ERROR: Input files for LaBr3 geometry not found !" << G4endl;
      exit(-1);
    }
    G4double r1(0.),theta1(0.),phi1(0.);
    G4double r2(0.),theta2(0.),phi2(0.);
    for(int i = 0; i < 10; i++){
      G4String valenciaName, valenciaAlveolus;
      G4String UKName, UKAlveolus;
      G4int valenciaLabel, UKLabel;
      rotMat.set(0,0,0);
      file >> valenciaName >> valenciaAlveolus >> r1 >>
	theta1 >> phi1 >> valenciaLabel;
      file >> UKName >> UKAlveolus >> r2 >> theta2 >> phi2 >> UKLabel;
      theta1*=deg;
      phi1*=deg;
      theta2*=deg;
      phi2*=deg;
      G4double radius1 = (r1+128.75)*mm;
      G4double radius2 = r2*mm;
      rotMat.rotateY(theta1);
      rotMat.rotateZ(phi1);
      G4ThreeVector translation1(radius1*sin(theta1)*cos(phi1),
				 radius1*sin(theta1)*sin(phi1),
				 radius1*cos(theta1));
      LaBrValencia->Placement(valenciaLabel, (theTarget)->HallPhys(),
			      Overlap, translation1, rotMat, i);
      fDetAngles[valenciaLabel]=translation1;
      rotMat.set(0,0,0);
      rotMat.rotateX(180*deg);
      rotMat.rotateY(theta1); //same orientation than 1
      rotMat.rotateZ(phi1);
      G4ThreeVector translation2(radius2*sin(theta2)*cos(phi2),
				 radius2*sin(theta2)*sin(phi2),
				 radius2*cos(theta2));
      LaBr->Placement(UKLabel, (theTarget)->HallPhys(), Overlap,
		      translation2, rotMat,i);
      fDetAngles[UKLabel]=translation2;
    }
    file.close();
    for(auto &it:LaBrValencia->GetLogicalVolumes())
      it->SetSensitiveDetector(theTarget->GeSD());
    for(auto &it:LaBr->GetLogicalVolumes())
      it->SetSensitiveDetector(theTarget->GeSD());
  }
  if(theAncillary) {
    theAncillary->Placement();
  }
}


G4int Nuball2Detector::GetSegmentNumber( G4int i1, G4int i2, G4ThreeVector v1 )
{
  //std::cout << i1 << " " << i2 << "\n";
  //G4cout<<i2<<"  "<<v1<<G4endl;
  G4int pouet = -1;
  if(i1>0 && theAncillary) return theAncillary->GetSegmentNumber(i1,i2,v1 );
  pouet=0;
  return pouet;
}


void Nuball2Detector::WriteHeader( std::ofstream &outFileLMD, G4double unit )

{
  outFileLMD << "NUBALL WITH " << fDetAngles.size() 
	     << " DETECTORS nb theta[deg] phi[deg] \n"; 
  std::map<int,G4ThreeVector>::iterator itorgamdet = fDetAngles.begin();
  for(;itorgamdet!=fDetAngles.end(); ++itorgamdet){
    outFileLMD << std::setiosflags(ios::fixed) << std::setprecision(2)
	       << std::setw(10) << itorgamdet->first 
	       << std::setw(10) << itorgamdet->second.theta()/deg << " " 
	       << std::setw(10) 
	       << (itorgamdet->second.phi()/deg>0 ? 
		   itorgamdet->second.phi()/deg : 
		   itorgamdet->second.phi()/deg+360.) 
	       <<  "\n";  
  }
  outFileLMD << " NUBALL det coded as det number\n";
  outFileLMD << " BGO shield coded as 300+det number\n";
  outFileLMD << "END NUBALL\n";
  if(theAncillary) theAncillary->WriteHeader(outFileLMD,unit);
}

void Nuball2Detector::WriteHeader(G4String *sheader, G4double unit)
{
// Todo
}

////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"

#include "G4UImanager.hh"

Nuball2DetectorMessenger::Nuball2DetectorMessenger(Nuball2Detector* pTarget)
  :myTarget(pTarget)
{ 

  setMode = new G4UIcmdWithAnInteger("/Nuball2/detector/setMode",this);
  setMode->SetGuidance("Chose nuball2 mode=1*Clover+2*Phase1+4*LaBr3");
  setMode->AvailableForStates(G4State_PreInit,G4State_Idle);

  setCloverBGO = new G4UIcmdWithABool("/Nuball2/detector/CloverBGO",this);
  setCloverBGO->SetGuidance("Use CloverBGO or not true/false");
  setCloverBGO->AvailableForStates(G4State_PreInit,G4State_Idle);
  setCloverColl = new G4UIcmdWithABool("/Nuball2/detector/CloverColl",this);
  setCloverColl->SetGuidance("Use CloverColl or not true/false");
  setCloverColl->AvailableForStates(G4State_PreInit,G4State_Idle);
  setPhaseIBGO = new G4UIcmdWithABool("/Nuball2/detector/PhaseIBGO",this);
  setPhaseIBGO->SetGuidance("Use PhaseIBGO or not true/false");
  setPhaseIBGO->AvailableForStates(G4State_PreInit,G4State_Idle);
  setPhaseIColl = new G4UIcmdWithABool("/Nuball2/detector/PhaseIColl",this);
  setPhaseIColl->SetGuidance("Use PhaseIColl or not true/false");
  setPhaseIColl->AvailableForStates(G4State_PreInit,G4State_Idle);
}

Nuball2DetectorMessenger::~Nuball2DetectorMessenger()
{
  G4cout << "### ~Nuball2DetectorMessenger "<< G4endl;
  delete setMode;
}

void Nuball2DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == setMode ) {
    myTarget->SetMode(setMode->GetNewIntValue(newValue));
  }
  if(command==setCloverBGO){
    myTarget->SetCloverBGO(setCloverBGO->GetNewBoolValue(newValue));
  }
  if(command==setCloverColl){
    myTarget->SetCloverColl(setCloverColl->GetNewBoolValue(newValue));
  }
  if(command==setPhaseIBGO){
    myTarget->SetPhaseIBGO(setPhaseIBGO->GetNewBoolValue(newValue));
  }
  if(command==setPhaseIColl){
    myTarget->SetPhaseIColl(setPhaseIColl->GetNewBoolValue(newValue));
  }
}

