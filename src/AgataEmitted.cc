#include "AgataEmitted.hh"

#include "G4IonTable.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

#include <cstdio>
#include <string>
#include <cmath>

#include "G4LorentzRotation.hh"
#include "G4LorentzVector.hh"
//#include "G4Boost.hh"

AgataEmitted::AgataEmitted( G4String directory, G4String name, G4String path, G4bool value, G4bool createMenu )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  usePola = value;
  
  /// Mass and charge
  emittedName = name;

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* emitted = particleTable->FindParticle(emittedName);

  emittedMass       = emitted->GetPDGMass();
  emittedMassNumber = (G4int)nearbyint( emittedMass / amu_c2 );

  emittedCharge       = emitted->GetPDGCharge();
  emittedChargeNumber = (G4int)nearbyint( emittedCharge / eplus );
    
  /// Initialize the other relevant members
  InitData();
  if( createMenu )
    myMessenger = new AgataEmittedMessenger(this, emittedName, usePola, directory);
}

//////////////////////////////////
/// Special constructor for ions
/////////////////////////////////
AgataEmitted::AgataEmitted( G4String directory, G4int atNum, G4int massNum, G4String name, G4bool value, G4bool createMenu )
{
  usePola = value;
  
  // Mass and charge
  emittedName         = name;
  emittedChargeNumber = atNum; 
  emittedMassNumber   = massNum;  

#ifdef G4V10
  G4IonTable* particleTable = G4IonTable::GetIonTable();
#else
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#endif
  G4ParticleDefinition* emitted = particleTable->GetIon( emittedChargeNumber, emittedMassNumber, 0. );

  if( !emitted ) {
    G4cout << " Could not find ion with Z = " << emittedChargeNumber << ", A = " << emittedMassNumber << G4endl;
    /// Choose proton instead
    emittedChargeNumber = 1;
    emittedMassNumber   = 1;
    emitted = particleTable->GetIon( emittedChargeNumber, emittedMassNumber, 0. );
  }
  
  emittedCharge       = eplus * emittedChargeNumber;
  emittedMass         = emitted->GetPDGMass();

  // /Initialize the other relevant members
  InitData();
  if( createMenu )
    myMessenger = new AgataEmittedMessenger(this, emittedName, usePola, directory);
}

AgataEmitted::~AgataEmitted()
{
  if( myMessenger )
    delete myMessenger;
}

/////////////////////////////////////////////
/// Service method: member initialization
////////////////////////////////////////////
void AgataEmitted::InitData()
{
  energyCM  = 1000.*keV;
  energyLab = 1000.*keV;
  
  dirCM  = G4ThreeVector(0., 0., 1.);
  dirLab = G4ThreeVector(0., 0., 1.);
    
  emittedThMin    = 0.;
  emittedThMax    = 180*deg;

  emittedThMinCos = cos(emittedThMin);
  emittedThMaxCos = cos(emittedThMax);

  emittedPhMin    = 0.;
  emittedPhMax    = 360*deg;
  
  /// members needed to generate various spectra
  gunType         = 0;
    
  monochrEnergyCM   = 1000.5*keV;     

  
  if( emittedMassNumber < 1 ) {
    if( emittedName == "mu+" || emittedName == "mu-" )
      emittedMult     = 1;
    else
      emittedMult     = 0;  
  }

  if( emittedMassNumber < 1 ) {
    if( emittedName == "gamma" )
      emittedMult     = 1;
    else
      emittedMult     = 0;  
  }
  else {
    emittedMult     = 0;
    monochrEnergyCM = emittedMassNumber * 5000.5*keV;
  }       

  bandMin           =   80.5*keV;     
  bandDelta         =  100.0*keV;     
  bandMult          =   30;           

  fileEnerName      = iniPath + "Spectra/"; 
  fileEnerName     += emittedName; 
  fileEnerName     += ".dat"; 
  fileMult          =  0;

  flatMin           =   100.0*keV;    
  flatMax           =  1000.0*keV;    
  
  fileSpecName      = iniPath + "Spectra/";
  fileSpecName     += emittedName;
  fileSpecName     += ".spc";

  fileEnerIName     = iniPath + "Spectra/"; 
  fileEnerIName    += emittedName; 
  fileEnerIName    += "I.dat"; 
  
  //////////////////////////////////////
  /// Polarization (actually used only
  /// with gammas
  /////////////////////////////////////
  if( usePola ) {  
    polarValue.clear();
    cosPsi.clear();
    sinPsi.clear();
    polarValue.push_back( 0. );
    cosPsi.push_back(1.);
    sinPsi.push_back(0.);
    stokesPar.push_back( G4ThreeVector(1., 0., 0.) ); 
  }      
  polarizationAxis = G4ThreeVector( 0., 0., 1. ); 
  /////////////////////////////////////////////////////////////////////////////
  /// This flag is set to true if usePola is true and polarValue is non-zero.
  /// In any case, if the cross section for polarized Compton scattering are
  /// loaded, a polarization vector will be chosen (unpolarized --> mixture
  /// of two polarization states)
  ////////////////////////////////////////////////////////////////////////////
  considerPolarization.clear();
  considerPolarization.push_back( false );
  
  emittedTau.clear();
  emittedTau.push_back( 0. );
  
  myMessenger = NULL;
  
}

void AgataEmitted::ResetEmitted( G4int atNum, G4int massNum )
{
  if( emittedName != "GenericIon" ) return;

#ifdef G4V10
  G4IonTable* particleTable = G4IonTable::GetIonTable();
#else
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#endif
   G4ParticleDefinition* emitted = particleTable->GetIon( atNum, massNum, 0. );
  if( !emitted ) {
    G4cout << " ----> Could not reset emitted, keeping previous ion with Z = " << emittedChargeNumber
           << ", A = " << emittedMassNumber << G4endl;
  }
  else {
    emittedChargeNumber = atNum; 
    emittedCharge       = eplus * emittedChargeNumber;
    emittedMassNumber   = massNum;  
    emittedMass         = emitted->GetPDGMass();
  }
}

/////////////////////////
/// Emission methods
////////////////////////
void AgataEmitted::Emit( G4int order, G4ThreeVector emitterVelocity )
{
  EmitEnergyCM( order );


  if(gunType == 6)
    EmitE2DirCM();
  else if(gunType == 7)
    EmitM1DirCM();
  else
    EmitDirCM();
  
  TransformCMToLab( emitterVelocity );
  
  /////////////////////////////////////////////
  /// Should always ask for the polarization
  /// vector when the polarized Compton 
  /// scattering cross sections are loaded
  ////////////////////////////////////////////
  if( usePola ) {
    emittedPolarization = CalculatePolarizationLab( order, dirLab, emitterVelocity );
    emittedStokesParam  = stokesPar[order];
  }
}

////////////////////////////////////////////////////////
/// CM Energy (various possibilities for the spectra)
///////////////////////////////////////////////////////
void AgataEmitted::EmitEnergyCM( G4int order )
{
  //  G4bool   isPolarized = considerPolarization[0];
  //  Commented to get rid of warnings. What was the idea with it?
  // Not used
  G4int    index = 0;
  G4double z;
  
  ////////////////////////////
  // chose the CM spectrum
  ////////////////////////////
  switch( gunType )
  {
    case 0:                   // monochromatic
      energyCM = monochrEnergyCM;
      break;
    case 1:                   // a rotational band
      energyCM = bandMin + (order%bandMult) * bandDelta;
      break;
    case 2:                   // discrete energies read from file
      index    = order%fileMult;
      energyCM = fileEner[index];
      //      isPolarized = considerPolarization[index];
      break;
    case 3:                   // flat distribution
      energyCM = flatMin + (flatMax - flatMin) * G4UniformRand();
      break;
    case 4:                   // sampled from a spectrum
      energyCM = SampleSpectrum();
      break;
    case 5:                   // discrete energies read from file
      z = G4UniformRand() * 1.0;
      while( z > fileInte[index+1] ) index++;
      energyCM = fileEner[index];
      //      isPolarized = considerPolarization[index];
      break;
    case 6:                   // E2-Angular Distribution
      energyCM = monochrEnergyCM;
      break;
    case 7:                   // M1-Angular Distribution
      energyCM = monochrEnergyCM;
      break;

  }
}

/////////////////////////////////////////
/// CM direction (isotropical emission)
/////////////////////////////////////////
void AgataEmitted::EmitDirCM()
{
  G4double costheta = emittedThMinCos + G4UniformRand() * (emittedThMaxCos - emittedThMinCos);
  G4double phi      = emittedPhMin    + G4UniformRand() * (emittedPhMax    - emittedPhMin);
  G4double theta    = acos(costheta);
  G4double sintheta = sin( theta );
  
  dirCM.set( sintheta*cos(phi), sintheta*sin(phi), cos(theta) );
}

void AgataEmitted::EmitE2DirCM()
{
// root [0] .L plot_angular_distribution.hh 
// Inverse Cumulative Probability Polynome VALID FOR PURE E2 DISTRIBUTIONS
// Paramters from http://www-linux.gsi.de/~wolle/EB_at_GSI/FRS-WORKING/index.html
// For 2->0 transition 1 - 0.357*P2(costheta) - 0.64 P4(costheta)
// root [1] plotW()
// root [2] Fitting results:
// Parameters:
// NO.             VALUE           ERROR
// 0       1.490659e+01    4.154862e+00// parameterization of F^-1 distribution for E2
// 1       3.450897e+02    2.338696e+02// dW/dtheta in the CM System
// 2       -1.545199e+03   3.934532e+03// A2 = -0.357   
// 3       4.467706e+03    2.898033e+04// A4 = -0.64
// 4       -3.793734e+03   1.108714e+05
// 5       -7.769382e+03   2.367521e+05
// 6       2.187683e+04    2.842548e+05
// 7       -2.007946e+04   1.792204e+05
// 8       6.659156e+03    4.613114e+04

  const G4int n_deg = 9;
  const G4float pol[n_deg] = {1.490659e+01, // parameterization of F^-1 distribution for E2
			      3.450897e+02, // dW/dtheta in the CM System
			      -1.545199e+03,// A2 = -0.357
			      4.467706e+03, // A4 = -0.64
			      -3.793734e+03, 
			      -7.769382e+03, 
			      2.187683e+04, 
			      -2.007946e+04,
			      6.659156e+03};

  G4double x = G4UniformRand() ;

  G4double theta = 0;

  for(int i=0;i<n_deg;i++)
    theta += pol[i]*pow(x,i); // in degrees


  theta *= 2*3.14159/360.0; // in rad
  
  /*  G4double costheta = cos(theta); // IN CM SYSTEM*/

  //  cout << " DEBUG x = " << x << "    theta = " << theta << " (deg)    cos(theta) = " << costheta << endl;
  //  G4double costheta = emittedThMinCos + G4UniformRand() * (emittedThMaxCos - emittedThMinCos);
  G4double phi      = emittedPhMin    + G4UniformRand() * (emittedPhMax    - emittedPhMin);
  G4double sintheta = sin( theta );
  
  dirCM.set( sintheta*cos(phi), sintheta*sin(phi), cos(theta) );


//   const G4double beta = 0.43;
//   const G4int n_deg = 9;
//   const G4float pol[n_deg] = {8.439576e+00, // parameterization of F^-1 distribution for E2
// 			      1.867855e+02, // beta = 0.43
// 			      -6.146722e+02,// dW/dtheta in the Laboratory system
// 			      -3.767879e+02,// A2 = -0.357
// 			      1.347144e+04,// A4 = -0.64
// 			      -4.921566e+04,
// 			      8.178268e+04,
// 			      -6.542754e+04,
// 			      2.035759e+04};


//   G4double x = G4UniformRand() ;

//   G4double theta_lab = 0;

//   for(int i=0;i<n_deg;i++)
//     theta_lab += pol[i]*pow(x,i); // in degrees

//   theta_lab *= 2*3.14159/360.0; // in rad
  
//   G4double costheta_lab = cos(theta_lab); // IN LABORATORY SYSTEM!

//   G4double costheta = (costheta_lab - beta)/(1 - beta*costheta_lab); // IN CM SYSTEM

//   //  cout << " DEBUG x = " << x << "    theta = " << theta << " (deg)    cos(theta) = " << costheta << endl;
//   //  G4double costheta = emittedThMinCos + G4UniformRand() * (emittedThMaxCos - emittedThMinCos);
//   G4double phi      = emittedPhMin    + G4UniformRand() * (emittedPhMax    - emittedPhMin);
//   G4double theta    = acos(costheta);
//   G4double sintheta = sin( theta );
  
//   dirCM.set( sintheta*cos(phi), sintheta*sin(phi), cos(theta) );
}


void AgataEmitted::EmitM1DirCM()
{
// Marc: taken from EmitE2DirCM() and adapted for typical M1 distribution
// This remained to be modelled more accurately
//
  G4double x = G4UniformRand() ; // between 0 and 1

  G4double theta = 0;


    //theta= 180*x + 25*G4RandGauss::shoot( 0.3, 0.2) - 25*G4RandGauss::shoot( 0.7, 0.2);  // WRONG !!
    // With the expression below, the flat distribution of x is converted into a wide distribution of theta, peaked around 90 degree 
    theta= 180*x + 35*exp(-pow((x-0.1),2)/(2*pow(0.3,2))) - 35*exp(-pow((x-0.9),2)/(2*pow(0.3,2))); // + (40*x-20);

    //theta=90; //fixed theta CM to see any effect
    //theta=5;
    cout << "Theta: " << theta << " deg." << endl;

    theta *= 2*3.14159/360.0; // in rad
  
  /*  G4double costheta = cos(theta); // IN CM SYSTEM*/

  //  cout << " DEBUG x = " << x << "    theta = " << theta << " (deg)    cos(theta) = " << costheta << endl;
  //  G4double costheta = emittedThMinCos + G4UniformRand() * (emittedThMaxCos - emittedThMinCos);
  G4double phi      = emittedPhMin    + G4UniformRand() * (emittedPhMax    - emittedPhMin);
  G4double sintheta = sin( theta );
  
  dirCM.set( sintheta*cos(phi), sintheta*sin(phi), cos(theta) );

}



//////////////////////////////////////////////
/// CM to Lab transformation (relativistic)
/////////////////////////////////////////////
void AgataEmitted::TransformCMToLab( G4ThreeVector emitterVelocity )
{
  fourMomentumCM  = FourMomentum( energyCM, dirCM );
  
  // The next section can probably be removed with no harm
  // It is left for completeness
  momentumCM = G4ThreeVector( fourMomentumCM.x(), fourMomentumCM.y(), fourMomentumCM.z() );
  if( momentumCM.mag2() > 0. )
    dirCM = momentumCM.unit();
  else
    dirCM = G4ThreeVector( 0., 0., 1. );  
  energyCM = fourMomentumCM.t();
  if( emittedMass > 0. )
    energyCM = fourMomentumCM.t() - emittedMass; // kinetic energy only

  // From now on it is needed!
  if( emitterVelocity.mag2() > 0. ) {
    G4LorentzRotation  emitterLorentzRotation( emitterVelocity/c_light );

    fourMomentumLab = emitterLorentzRotation( fourMomentumCM );
  }
  else
    fourMomentumLab = fourMomentumCM;  
 
  momentumLab = G4ThreeVector( fourMomentumLab.x(), fourMomentumLab.y(), fourMomentumLab.z() );
  if( momentumLab.mag2() > 0. )
    dirLab = momentumLab.unit();
  else
    dirLab = G4ThreeVector( 0., 0., 1. );  
  energyLab = fourMomentumLab.t();
  if( emittedMass > 0. )
    energyLab = fourMomentumLab.t() - emittedMass; // kinetic energy only
}

void AgataEmitted::TransformCMToLab( G4ThreeVector emittedDirection, G4ThreeVector emitterVelocity  )
{
  dirCM = emittedDirection;
  TransformCMToLab( emitterVelocity );
   
}

/////////////////////////////////////////////////////////////
/// service method: 4-momentum from energy and direction
////////////////////////////////////////////////////////////
G4LorentzVector AgataEmitted::FourMomentum( G4double energy, G4ThreeVector direction )
{
  G4LorentzVector fourMomentum;

  if( emittedMass > 0. ) {
  
    G4double totalEnergy    = energy + emittedMass;
    G4double momentumModule = sqrt( totalEnergy*totalEnergy - emittedMass*emittedMass );

    G4ThreeVector threeMomentum = direction * momentumModule;

    fourMomentum = G4LorentzVector( threeMomentum, totalEnergy );
  
  }
  else {
     fourMomentum = energy * G4LorentzVector( direction, 1.0 );
  }
  
  return fourMomentum;
}

///////////////////////////////
/// Service methods for emit
///////////////////////////////

////////////////////////////////////////
/// Reads discrete spectrum from file
/// assuming that each line contains:
/// line# energy [pol tau]
////////////////////////////////////////
G4int AgataEmitted::ReadFileEner(G4String filename)
{
  FILE     *in;
  char      line[150];

  G4int       pippo;
  G4double    pluto, paperino = 0., topolino = 0., gambadilegno = 0.;
  
  if((in=fopen(filename, "r"))==NULL)
    return(1);
  
  fileEner.clear();
  emittedTau.clear();
  if( usePola ) {  
    polarValue.clear();
    cosPsi.clear();
    sinPsi.clear();
    stokesPar.clear();
    considerPolarization.clear();  
  }      
  G4cout << " Reading " << filename << " file" << G4endl;

  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);

  fileMult = 0;
  while(fgets(line, 150, in)) {
    if( line[0] == '#' ) continue;
    sscanf(line, "%d %lf", &pippo, &pluto);
        
    G4String timDummy(line);
    if( timDummy.find_first_of( 't', 0 ) != string::npos ) {
      G4int posT = timDummy.find_first_of( 't', 0 );
      timDummy.erase( 0, ++posT );
      sscanf( timDummy.c_str(), "%lf", &topolino );
    }
    else
      topolino = 0.;

    fileEner.push_back(  pluto * keV  );
    emittedTau.push_back( topolino * ns );
    
    if( usePola ) {
      G4String polDummy(line);
      if( polDummy.find_first_of( 'p', 0 ) != string::npos ) {
	G4int posP = polDummy.find_first_of( 'p', 0 );
	polDummy.erase( 0, ++posP );
	sscanf( polDummy.c_str(), "%lf %lf", &paperino, &gambadilegno );
      }
      else {
	paperino     = 0.;
	gambadilegno = 0.;
      }

      if( fabs(paperino) > 1. ) {
        G4cout << " Value out of range for line #" << pippo << G4endl;
        paperino = 0.;
      }
      polarValue.push_back( fabs(paperino) );
      cosPsi.push_back( cos(gambadilegno*deg) );
      sinPsi.push_back( sin(gambadilegno*deg) );
      stokesPar.push_back( G4ThreeVector( 1., fabs(paperino)*cos(2.*gambadilegno*deg), fabs(paperino)*sin(2.*gambadilegno*deg)));
      if( paperino )
        considerPolarization.push_back( true );
      else  
        considerPolarization.push_back( false );
      G4cout << "   Line " << std::setw(2)  << fileMult 
                           << std::setw(10) << fileEner[fileMult]/keV << " " << std::setw(5)
                           << polarValue[fileMult] << " " << std::setw(8) << acos(cosPsi[fileMult])/deg << " " << emittedTau[fileMult]/ns << G4endl;
    }
    else {
      considerPolarization.push_back( false );
      G4cout << "   Line " << std::setw(2)  << fileMult 
                           << std::setw(10) << fileEner[fileMult]/keV << " " 
                                                          << emittedTau[fileMult]/ns << G4endl;
    }
    
    fileMult++;

  }

  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);

  emittedMult = fileMult;
  fclose(in);
  return(0);
}

///////////////////////////////////////
/// Reads discrete spectrum from file
/// assuming that each line contains:
/// line# energy intensity [pol tau]
///////////////////////////////////////
G4int AgataEmitted::ReadFileEnerI(G4String filename)
{
  FILE     *in;
  char      line[150];

  G4int       i;
  G4int       pippo;
  G4double    pluto, paperone, paperino = 0., topolino = 0., gambadilegno = 0.;
  
  if((in=fopen(filename, "r"))==NULL)
    return(1);
  
  fileEner.clear();
  fileInte.clear();
  fileInte.push_back(0);
  emittedTau.clear();
  if( usePola ) {  
    polarValue.clear();
    cosPsi.clear();
    sinPsi.clear();
    stokesPar.clear();
    considerPolarization.clear();  
  }      
  G4cout << " Reading " << filename << " file" << G4endl;

  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);

  fileMult = 0;
  while(fgets(line, 150, in)) {
    if( line[0] == '#' ) continue;
    sscanf(line, "%d %lf %lf", &pippo, &pluto, &paperone);
    
    G4String timDummy(line);
    if( timDummy.find_first_of( 't', 0 ) != string::npos ) {
      G4int posT = timDummy.find_first_of( 't', 0 );
      timDummy.erase( 0, ++posT );
      sscanf( timDummy.c_str(), "%lf", &topolino );
    }
    else
      topolino = 0.;

    fileEner.push_back(  pluto * keV  );
    fileInte.push_back( paperone );
    emittedTau.push_back( topolino * ns );
    
    if( usePola ) {
      G4String polDummy(line);
      if( polDummy.find_first_of( 'p', 0 ) != string::npos ) {
	G4int posP = polDummy.find_first_of( 'p', 0 );
	polDummy.erase( 0, ++posP );
	sscanf( polDummy.c_str(), "%lf %lf", &paperino, &gambadilegno );
      }
      else {
	paperino     = 0.;
	gambadilegno = 0.;
      }
    
      if( fabs(paperino) > 1. ) {
        G4cout << " Value out of range for line #" << pippo << G4endl;
        paperino = 0.;
      }
      polarValue.push_back( fabs(paperino) );
      cosPsi.push_back( cos(gambadilegno*deg) );
      sinPsi.push_back( sin(gambadilegno*deg) );
      stokesPar.push_back( G4ThreeVector( 1., fabs(paperino)*cos(2.*gambadilegno*deg), fabs(paperino)*sin(2.*gambadilegno*deg)));
      if( paperino )
        considerPolarization.push_back( true );
      else  
        considerPolarization.push_back( false );
      G4cout << "   Line " << std::setw(2)  << fileMult 
                           << std::setw(10) << fileEner[fileMult]/keV << " " 
                           << std::setw(10) << fileInte[fileMult+1]   << " " 
                           << std::setw(5)  << polarValue[fileMult]   << " " 
			   << std::setw(8)  << acos(cosPsi[fileMult])/deg << " " 
			   << emittedTau[fileMult]/ns << G4endl;
    }
    else {
      considerPolarization.push_back( false );
      G4cout << "   Line " << std::setw(2)  << fileMult 
                           << std::setw(10) << fileEner[fileMult]/keV << " " 
                           << std::setw(10) << fileInte[fileMult+1]   << " " 
                                                          << emittedTau[fileMult]/ns << G4endl;
    }
    
    fileMult++;

  }

  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
  
  // normalize intensities to 1 (100%)
  for( i=1; i<fileMult+1; i++ )
    fileInte[i] += fileInte[i-1];
    
  for( i=1; i<fileMult+1; i++ )
    fileInte[i] /= fileInte[fileMult];
    
  emittedMult = fileMult;
  fclose(in);
  return(0);
}


/////////////////////////////////////////
// Reads continuous spectrum from file
/////////////////////////////////////////
G4int AgataEmitted::InitSpectrumCM( G4String filename )
{
  G4int i, j;
  G4double *spectrum=NULL, sum;
  std::vector<G4double> spec1MeVch;
  G4int npoints = 0;
   
  FILE     *in;
  char      line[150];
  G4double    pippo, pluto;
  
  
  G4cout << " Reading " << filename << " file" << G4endl;
  if((in=fopen(filename, "r"))==NULL) {
    G4cout << " Could not open " << filename << " file" << G4endl;
    return(1);
  }
  
  spec1MeVch.clear();
    
  while(fgets(line, 150, in))
  {
    if( line[0] == '#' ) continue;
    sscanf(line, "%lf %lf", &pippo, &pluto);
    spec1MeVch.push_back(pluto);
    npoints++;
  }
  fclose(in);
  G4cout << " File " << filename << " read without errors" << G4endl;
     
  // Transforms spectrum to 1 keV/channel through a linear interpolation
  G4int size = 1000*npoints;
  spectrum = new G4double[size];
  for(i=0; i<size; i++)
    spectrum[i]=0.;
  for(i=0; i<(npoints-1); i++)  // spe --> 1MeV/ch;   spectrum ---> 1 keV/ch
    for(j=0; j<1000; j++)
      spectrum[j+i*1000] = spec1MeVch[i]+(spec1MeVch[i+1]-spec1MeVch[i])*j/1000.;

  /* Spectrum integral */
  sum=0;
  fileSpecDistrib.clear();
  for(i=0; i<size; i++)
  {
    sum += spectrum[i]*1.0;   /* Each bin: 1.0 keV */
    fileSpecDistrib.push_back(sum); 
  }      

  /* Normalization */
  for(i=0; i<size; i++)
    fileSpecDistrib[i] /= sum;
  delete[] spectrum;  
  return(0);     

}

///////////////////////////////////////////////////
/// Samples energy according to CM distribution
//////////////////////////////////////////////////
G4double AgataEmitted::SampleSpectrum( )
{
  G4int    i = 0;
  G4double z = G4UniformRand() * 1.0;
  while( fileSpecDistrib[i] < z )     
    i++;
  i--;
  return  ( i + (z-fileSpecDistrib[i])/(fileSpecDistrib[i+1]-fileSpecDistrib[i]) ) * keV;
}

//////////////////////////////////////////////////////////////////////////////////////////
/// This method calculates the polarization vector in the laboratory reference frame.
/// If the gamma direction is close to the polarization axis (or recoil direction)
/// the polarization vector is evaluated assuming z as the photon direction,
/// x as the orthogonal state (+1), y as the parallel state (-1) and rotating to the 
/// actual gamma direction. In the other cases the polarization states are computed
/// via cross products.
/// The unpolarized fraction is emulated as an incoherent sum with equal amount of 
/// photons polarized in the same state or in an orthogonal state as the polarized 
/// fraction.
/////////////////////////////////////////////////////////////////////////////////////////
G4ThreeVector AgataEmitted::CalculatePolarizationLab( G4int num, G4ThreeVector direction, G4ThreeVector velocity )
{
  if( !usePola )
    return G4ThreeVector();
    
  G4ThreeVector axis = velocity;
  if( axis.mag2() == 0. )
    axis = polarizationAxis;
    
  const G4double tolerance = 0.999;
  
  G4ThreeVector finalPolarization;
  G4ThreeVector orthState;
  G4ThreeVector paraState;
  
  if( fabs(direction.unit().dot(axis.unit())) > tolerance ) {
    orthState = G4ThreeVector(1., 0., 0. );
    paraState = G4ThreeVector(0., 1., 0. );
    
    G4double theta = direction.theta();
    G4double phi   = direction.phi();

    G4RotationMatrix rm;
    rm.set(0,0,0);
    if( theta > 0. ) {  // this rotation makes sense only if theta > 0.
      rm.rotateY( theta );
      rm.rotateZ( phi   );
    }
    orthState = rm(orthState);
    paraState = rm(paraState);
  }
  else {
    orthState = direction.cross(axis).unit();
    paraState = direction.cross(orthState).unit();
  }
  finalPolarization = cosPsi[num] * orthState + sinPsi[num] * paraState;
  if( G4UniformRand()*1.0 > 0.5*(1.+polarValue[num]) )
    finalPolarization = direction.cross(finalPolarization).unit();
    
  return finalPolarization;
}

G4bool AgataEmitted::IsPolarized( G4int num )
{
  if( (gunType == 2) || (gunType == 5) ) {
    if( num < emittedMult )
      return considerPolarization[num];
    else
      return false;  
  }
  else {
    return considerPolarization[0];
  }
}


////////////////////////////////
// Methods for the Messenger
////////////////////////////////
void AgataEmitted::SetGun( G4int flag )
{
  
  G4double  dummy_time = emittedTau[0];
  // cancels previous values and keeps only the first one
  // case 2 takes already care of re-canceling and re-reading
  // the good values
  emittedTau.clear();
  emittedTau.push_back( dummy_time );  
  if( usePola ) {  
    G4bool    dummy_bool = considerPolarization[0];
    G4double  dummy_pol  = polarValue[0];
    G4double  dummy_cos  = cosPsi[0];
    G4double  dummy_sin  = sinPsi[0];
    G4ThreeVector  dummy_stokes  = stokesPar[0];
    polarValue.clear();
    polarValue.push_back( dummy_pol );  
    cosPsi.clear();
    cosPsi.push_back(dummy_cos);  
    sinPsi.clear();
    sinPsi.push_back(dummy_sin);  
    considerPolarization.clear();  
    considerPolarization.push_back( dummy_bool );
    stokesPar.clear();
    stokesPar.push_back(dummy_stokes);
  }      
  
  switch( flag )
  {
    case 0:                       // monochromatic
      gunType = flag; 
      emittedMult = 1;
      break;
    case 1:                       // bandMin + bandDelta * n
      gunType = flag;
      emittedMult = bandMult;
      break;
    case 2:                       // list of energies (from file)  
      gunType = flag;
      ReadFileEner( fileEnerName );
      emittedMult = fileMult;
      break;
    case 3:                       // flat distribution  [flatMin -- flatMax]
      gunType = flag;
      emittedMult  = 1;
      break;
    case 4:                       // energy sampled from spectrum
      gunType = flag;
      InitSpectrumCM( fileSpecName );
      emittedMult = 1;
      break;
    case 5:                       // energy sampled from spectrum
      gunType = flag;
      ReadFileEnerI( fileEnerIName );
      emittedMult = fileMult;
      break;
    case 6:                       // monochromatic E2
      gunType = flag; 
      emittedMult = 1;
      break;
    case 7:                       // monochromatic M1
      gunType = flag; 
      emittedMult = 1;
      break;

    default:
      gunType = 0;
      emittedMult = 1;
      break;  
  }
  G4cout << " ----> Gun type " << gunType << " chosen for " << emittedName << "s." << G4endl;
  G4cout << " ----> Multiplicity for " << emittedName << "s is " << emittedMult << G4endl;
}

void  AgataEmitted::SetMultiplicity( G4int mult )
{
  if( (gunType == 1) || (gunType == 2) || (gunType == 5) )
    G4cout << " Multiplicity is fixed at " << emittedMult <<
              ", could not change multiplicity!" << G4endl;
  else {
    if( mult > 0 )
      emittedMult = mult;
    else {
      SetGun(0);
      emittedMult = 0;
    }
    G4cout << " ----> The " << emittedName << " multiplicity is now " << emittedMult << G4endl;
  }
}            

void AgataEmitted::SetEmittedTau( G4double tau )
{
  if((gunType == 2) || (gunType == 5))
    G4cout << " Lifetimes already fixed! Could not change tau. " << G4endl;
  else {
    emittedTau.clear();
    if( tau > 0. )
      emittedTau.push_back( tau * ns );
    else
      emittedTau.push_back( 0. );
    G4cout << " ----> Lifetime for emitted " << emittedName << " is " << emittedTau[0]/ns << " ns" << G4endl;    
  }  
}

void AgataEmitted::SetMonochromaticEnergy( G4double ener )
{
  if( ener > 0. )
    monochrEnergyCM = ener*keV;
  else
    monochrEnergyCM = 1000.5*keV;
  G4cout << " ----> Energy = " << monochrEnergyCM/keV << " keV" << G4endl;      
  SetGun(0);
}

void AgataEmitted::SetBand( G4double ener, G4double delta, G4int mult )
{
  bandMin   = ener*keV;
  bandDelta = delta*keV;
  bandMult  = mult;
  G4cout << " ----> bandMin   = " << bandMin/keV << " keV" << G4endl;      
  G4cout << " ----> bandDelta = " << bandDelta/keV << " keV" << G4endl;      
  G4cout << " ----> bandMult  = " << bandMult << G4endl;      
  SetGun(1);
}

void AgataEmitted::SetFlat( G4double Emin, G4double Emax )
{
  flatMin   = Emin*keV;
  flatMax   = Emax*keV;
  G4cout << " ----> flatMin   = " << flatMin/keV << " keV" << G4endl;      
  G4cout << " ----> flatMax   = " << flatMax/keV << " keV" << G4endl;      
}

void AgataEmitted::SetFileEnerName( G4String name )
{
  if( name(0) == '/' )
    fileEnerName = name;
  else {
    if( name.find( "./", 0 ) != string::npos ) {
      G4int position = name.find( "./", 0 );
      if( position == 0 )
        name.erase( position, 2 );
    }  
    fileEnerName = iniPath + name;
  }  
      
  G4cout << " ----> Initializing energies from " << fileEnerName << " file" << G4endl;
  // initialize CM distributions
  G4int ret_ener=0;
  ret_ener = ReadFileEner( fileEnerName );
  if( ret_ener != 0 ) {
    G4cout << " Warning! Error initializing energies." << G4endl;
    SetGun( 0 );
  }  
}

void AgataEmitted::SetFileEnerIName( G4String name )
{
  if( name(0) == '/' )
    fileEnerIName = name;
  else {
    if( name.find( "./", 0 ) != string::npos ) {
      G4int position = name.find( "./", 0 );
      if( position == 0 )
        name.erase( position, 2 );
    }  
    fileEnerIName = iniPath + name;
  }  

  G4cout << " ----> Initializing energies from " << fileEnerIName << " file" << G4endl;
  // initialize CM distributions
  G4int ret_ener=0;
  ret_ener = ReadFileEnerI( fileEnerName );
  if( ret_ener != 0 ) {
    G4cout << " Warning! Error initializing energies." << G4endl;
    SetGun( 0 );
  }  
}

void AgataEmitted::SetFileSpecName( G4String name )
{
  if( name(0) == '/' )
    fileSpecName = name;
  else {
    if( name.find( "./", 0 ) != string::npos ) {
      G4int position = name.find( "./", 0 );
      if( position == 0 )
        name.erase( position, 2 );
    }  
    fileSpecName = iniPath + name;
  }  

  G4cout << " ----> Initializing distributions from " << fileSpecName << " file" << G4endl;
// initialize CM distributions
  G4int ret_distrib=0;
  ret_distrib = InitSpectrumCM( fileSpecName );
  if( ret_distrib != 0 ) {
    G4cout << " Warning! Error initializing CM distributions." << G4endl;
    SetGun( 0 );
  }  
}

//////////////////////////////////////////////////////////////////////////////
/// This method makes sense only when usePola is true
/// when discrete energies are read from file, this fixes the polarization
/// value for each line, otherwise a single polarization is considered
/// (same as lifetime)
//////////////////////////////////////////////////////////////////////////////
void AgataEmitted::SetPolarizationValue( G4double value )
{
  G4double dummy = polarValue[0];
  if( (gunType == 2) || (gunType==5) )
    G4cout << " Polarizations already fixed! Cannot change. " << G4endl;
  else {
    polarValue.clear();
    considerPolarization.clear();
    stokesPar.clear();  
    if( fabs(value) > 1. ){
      G4cout << " ----> Values out of range, keeping previous value " << dummy << G4endl;
      polarValue.push_back( dummy );
    }
    else {
      polarValue.push_back( fabs(value) );
      G4cout << " ---> Degree of polarization is now " << polarValue[0] << G4endl;
      if( polarValue[0] != 0. )
        considerPolarization.push_back( true );
      else  
        considerPolarization.push_back( false );
    }    
    stokesPar.push_back(G4ThreeVector(1., 0., 0.));
    stokesPar[0].setY(polarValue[0]*cos(2.*acos(cosPsi[0])));
    stokesPar[0].setZ(polarValue[0]*sin(2.*acos(cosPsi[0])));
    G4cout << " ---> The polarization will now be described by the following Stokes parameters: " << stokesPar[0] << G4endl;      
  }
}

void AgataEmitted::SetPolarizationState( G4double value )
{
  if( (gunType == 2) || (gunType==5) )
    G4cout << " Polarizations already fixed! Cannot change. " << G4endl;
  else {
    if( value < 0. ) {
      G4cout << " Invalid value, keeping previous one (" << acos(cosPsi[0])/deg << " deg.)" << G4endl;
      return;
    }
    cosPsi.clear();
    sinPsi.clear();
    stokesPar.clear();
    cosPsi.push_back(cos(value*deg));
    sinPsi.push_back(sin(value*deg));

    stokesPar.push_back(G4ThreeVector(1., 0., 0.));
    stokesPar[0].setY(polarValue[0]*cos(2.*value*deg));
    stokesPar[0].setZ(polarValue[0]*sin(2.*value*deg));

    G4cout << " ---> Angle between the polarization vector and the reference axis is now " << value << " degrees." << G4endl;
    G4cout << " ---> The polarization will now be described by the following Stokes parameters: " << stokesPar[0] << G4endl;      
  }
}


void AgataEmitted::SetStokes( G4ThreeVector values )
{
  if( (gunType == 2) || (gunType==5) )
    G4cout << " Polarizations already fixed! Cannot change. " << G4endl;
  else {
    /// clear old structures
    polarValue.clear();
    cosPsi.clear();
    sinPsi.clear();
    considerPolarization.clear();

    stokesPar.clear();
    stokesPar.push_back( values );

    //////////////////////////////////////////////////////
    /// By definition, we should have some intensity ...
    //////////////////////////////////////////////////////
    if( stokesPar[0].x() == 0. ) stokesPar[0].setX(1.);
    stokesPar[0] /= stokesPar[0].x();

    polarValue.push_back( sqrt(stokesPar[0].y()*stokesPar[0].y()+stokesPar[0].z()*stokesPar[0].z())/stokesPar[0].x() );
    
    //////////////////////////////////////////////////////
    /// Should check that the result is meaningful ...
    /////////////////////////////////////////////////////
    if( polarValue[0] > 1.0 ) {
      G4cout << " Warning!!! Input Stokes vector produces unphysical degree of polarization, renormalizing ..." << G4endl;
      stokesPar[0].setY(stokesPar[0].y()/polarValue[0]);
      stokesPar[0].setZ(stokesPar[0].z()/polarValue[0]);
      polarValue[0] = sqrt(stokesPar[0].y()*stokesPar[0].y()+stokesPar[0].z()*stokesPar[0].z())/stokesPar[0].x();
    }
    G4cout << " ---> Degree of polarization is now " << polarValue[0] << G4endl;

    if( polarValue[0] )
      considerPolarization.push_back(true);
    else
      considerPolarization.push_back(false);

    G4double psi;
    psi = 0.5*atan2( stokesPar[0].z(),stokesPar[0].y() );
    if( psi < 0. ) psi += 360.*deg;
    //if( stokesPar[0].y() )
    //  psi = 0.5*atan2( stokesPar[0].z()/stokesPar[0].y() );
    //else
    //  psi = 45.*deg;
    cosPsi.push_back(cos(psi));      
    sinPsi.push_back(sin(psi));
    G4cout << " ----> Angle between the polarization vector and the reference axis is now " << psi/deg << " degrees." << G4endl;

    G4cout << " ---> The polarization will now be described by the following Stokes parameters: " << stokesPar[0] << G4endl;      
  }
}

void AgataEmitted::SetEmittedTh( G4double thMin, G4double thMax )
{
  G4double thmin = thMin;
  G4double thmax = thMax;

  if(thmin <   0.) thmin =   0.;
  if(thmin > 180.) thmin = 180.;

  if(thmax <   0.) thmax =   0.;
  if(thmax > 180.) thmax = 180.;

  if(thmax > thmin) {
    emittedThMin = thmin*deg;
    emittedThMax = thmax*deg;
  }
  else {
    emittedThMax = thmin*deg;
    emittedThMin = thmax*deg;
  }
  emittedThMinCos = cos(emittedThMin);
  emittedThMaxCos = cos(emittedThMax);

  G4cout << " ----> Range of polar angle of " << emittedName << " emission [ " 
         << emittedThMin/deg << " -- " << emittedThMax/deg << " ] deg " << G4endl;   
}

void AgataEmitted::SetEmittedPh( G4double phMin, G4double phMax )
{
  G4double phmin = phMin;
  G4double phmax = phMax;
  
// Mar 2009: changes to cope with intervals across phi = 0.
  // should use numbers in the [-360, 360] range
  while( phmin < -360. )
    phmin += 360.;
  while( phmin > 360. )
    phmin -= 360.;
  while( phmax < -360. )
    phmax += 360.;
  while( phmax > 360. )
    phmax -= 360.;
    
  // just in case, phmax should be larger than phmin
  if(phmax > phmin) {
    emittedPhMin = phmin*deg;
    emittedPhMax = phmax*deg;
  }
  else {
    emittedPhMax = phmin*deg;
    emittedPhMin = phmax*deg;
  }

  G4cout << " ----> Range of azimuthal angle of " << emittedName << " emission [ " 
         << emittedPhMin/deg << " -- " << emittedPhMax/deg << " ] deg " << G4endl;   
}

void AgataEmitted::SetEnergyCM( G4double energy )
{
  energyCM = energy;
}

void AgataEmitted::SetDirCM( G4ThreeVector direction )
{
  dirCM = direction;
}

G4double AgataEmitted::GetEmittedTau( G4int num )
{
  if((gunType == 2) || (gunType == 5)) {
    if( num < emittedMult )
      return emittedTau[num];
    else
      return 0.;  
  }
  else {
    return emittedTau[0];
  }
}

///////////////
// Printouts
//////////////
void AgataEmitted::PrintToFile( std::ofstream &outFileLMD, G4double unit )
{
  G4int ii   = 0;
  G4int prec = outFileLMD.precision(4);
  outFileLMD.setf(ios::fixed);
  
  G4String capitalName = emittedName;
  capitalName.toUpper();
  
  switch (gunType)
  {
  case 0:
    outFileLMD << capitalName << " 1" << G4endl;
    outFileLMD << std::setw(8) << monochrEnergyCM/unit << G4endl;
    break;
  case 1:
    outFileLMD << capitalName << " " << bandMult << G4endl;
    for(ii = 0; ii < bandMult; ii++) {
      outFileLMD << std::setw(8) << ( bandMin + ii * bandDelta )/unit << G4endl;
    }
    break;
  case 2:
    outFileLMD << capitalName << " " << fileMult << G4endl;
    for(ii = 0; ii < fileMult; ii++) {
      outFileLMD << std::setw(8) << fileEner[ii]/unit << G4endl;
    }
    break;
  case 3:
    outFileLMD << capitalName << " -1" << G4endl;
    outFileLMD << std::setw(8) << flatMin/unit << " " << flatMax/unit << G4endl;
    break;
  case 4:
    outFileLMD << capitalName << " -2" << G4endl;
    break;
  case 5:
    outFileLMD << capitalName << " " << fileMult << G4endl;
    for(ii = 0; ii < fileMult; ii++) {
      outFileLMD << std::setw(8) << fileEner[ii]/unit << G4endl;
    }
    break;
  case 6:
    outFileLMD << capitalName << " 1" << G4endl;
    outFileLMD << std::setw(8) << monochrEnergyCM/unit << G4endl;
    break;
  default:
    break;
  }
  outFileLMD.unsetf(ios::fixed);
  outFileLMD.precision(prec);
}


void AgataEmitted::GetStatus()
{
  G4int ii;
  G4cout << G4endl;
  G4cout << emittedName << " multiplicity is   " << emittedMult << G4endl;
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  switch (gunType)
  {
    case 0:
      G4cout << " Monochromatic " << emittedName << " with energy " << monochrEnergyCM/keV << " keV" << G4endl;
      break;
    case 1:
      G4cout << " Rotational band composed of " << bandMult << emittedName << "s" << G4endl;
      G4cout << " Lowest energy: " << bandMin/keV << " keV" << G4endl;
      G4cout << " Spacing:       " << bandDelta/keV << " keV" << G4endl;
      break;
    case 2:
      G4cout << fileMult << " energies read from file " << fileEnerName << G4endl;
      for(ii = 0; ii < fileMult; ii++) 
        G4cout << emittedName << " # " << ii << " Energy: " << fileEner[ii]/keV << " keV" << G4endl;
      break;
    case 3:
      G4cout << " Flat distribution from " << std::setw(8) << flatMin/keV << " to " 
             << flatMax/keV << " keV" << G4endl;
     break;
    case 4:
      G4cout << " Continuous spectrum read from file " << fileSpecName << G4endl; 
      break;
    case 5:
      G4cout << fileMult << " energies read from file " << fileEnerIName << G4endl;
      for(ii = 0; ii < fileMult; ii++) 
        G4cout << emittedName << " # " << ii << " Energy: " << fileEner[ii]/keV << " keV" << G4endl;
      break;
    case 6:
      G4cout << " E2 Monochromatic " << emittedName << " with energy " << monochrEnergyCM/keV << " keV" << G4endl;
      break;
    default:
      break;
  }

  if( (gunType == 2) || (gunType==5) ) {
    for(ii = 0; ii < fileMult; ii++)
      if( emittedTau[ii] > 0. )
        G4cout << emittedName << " # " << ii << " Lifetime: " << emittedTau[ii]/ns << " ns" << G4endl;
    for(ii = 0; ii < fileMult; ii++)
      if( usePola && polarValue[ii] )
        G4cout << emittedName << " # " << ii << " Polarization: " << polarValue[ii] 
	       << " at an angle " << acos(cosPsi[ii])/deg << " deg." << G4endl;
  }
  else {
    if( emittedTau[0] > 0. )
      G4cout << " Lifetime for the transitions: " << emittedTau[0]/ns << " ns" << G4endl;
    if( usePola ) {
      G4cout << " Polarized " << emittedName << "s with polarization " 
             << polarValue[0] << " at an angle " << acos(cosPsi[0])/deg << " deg." << G4endl;  
    }    
  }    
 
  if( emittedThMin != 0. || emittedThMax != 180.*deg ) {
    G4cout << emittedName << " emission limited in the range theta " << emittedThMin/deg
           << " - " << emittedThMax/deg << " degrees" << G4endl;
  }
  if( emittedPhMin != 0. || emittedPhMax != 360.*deg ) {
    G4cout << emittedName << " emission limited in the range phi " << emittedPhMin/deg
           << " - " << emittedPhMax/deg << " degrees" << G4endl;
  }
  
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

//////////////////////
/// Service methods
/////////////////////
G4double AgataEmitted::MomentumFromEnergy( G4double mass, G4double energy )
{
  return sqrt( energy * ( energy + 2.*mass ) );
}

G4double AgataEmitted::EnergyFromMomentum( G4double mass, G4ThreeVector momentum )
{
  return sqrt( momentum.mag2() + mass * mass ) - mass;
}

G4double AgataEmitted::EnergyFromMomentum( G4double mass, G4double momentum )
{
  return sqrt( momentum*momentum + mass * mass ) - mass;
}

G4double AgataEmitted::BetaFromMomentum( G4double mass, G4double momentum )
{
  G4double energy = EnergyFromMomentum( mass, momentum );
  
  return momentum / ( energy + mass );
}

G4ThreeVector AgataEmitted::VelocityFromMomentum( G4double mass, G4ThreeVector momentum )
{
  G4double energy = EnergyFromMomentum( mass, momentum );
  
  return momentum * c_light / ( energy + mass );
}

G4ThreeVector AgataEmitted::MomentumFromVelocity( G4double mass, G4ThreeVector velocity )
{
  G4double beta = velocity.mag() / c_light;
  G4double gamma = sqrt( 1. - beta * beta ); // inverse of gamma as we know it
  
  return mass * velocity / ( c_light * gamma );
}

///////////////////////////////////////////////////////
/// set methods needed by the ExternalEmission class
//////////////////////////////////////////////////////
void AgataEmitted::setVelocityLab( G4double energy, G4ThreeVector direction )
{
  energyLab   = energy;
  if( direction.mag2() )
    dirLab      = direction.unit();
  
  momentumLab = MomentumFromEnergy( emittedMass, energyLab ) * dirLab;
}

void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector direction, G4ThreeVector emitterVelocity )
{
  energyCM    = energy;
  if( direction.mag2() )
    dirCM       = direction.unit();
  
  TransformCMToLab( emitterVelocity );
}

void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector emitterVelocity )
{
  energyCM   = energy;
  EmitDirCM();
  
  TransformCMToLab( emitterVelocity );
}

void AgataEmitted::setVelocityCM( G4int order, G4ThreeVector emitterVelocity )
{
  EmitEnergyCM( order );
  EmitDirCM();
  
  TransformCMToLab( emitterVelocity );
}

void AgataEmitted::setVelocityLab( G4double energy, G4ThreeVector direction, G4double pol )
{
  setVelocityLab( energy, direction );
  if( usePola )
    emittedPolarization = getPolarizationLab( pol, dirLab, G4ThreeVector() );
}

void AgataEmitted::setVelocityLab( G4double energy, G4ThreeVector direction, G4double pol1, G4double pol2, G4double pol3 )
{
  setVelocityLab( energy, direction );
  if( usePola )
    emittedPolarization = getPolarizationLab( G4ThreeVector( pol1, pol2, pol3 ), dirLab, G4ThreeVector() );
}


void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector direction, G4ThreeVector emitterVelocity, G4double pol )
{
  setVelocityCM( energy, direction, emitterVelocity );
  if( usePola )
    emittedPolarization = getPolarizationLab( pol, dirLab, emitterVelocity );
}

void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector direction, G4ThreeVector emitterVelocity, 
                                                                            G4double pol1, G4double pol2, G4double pol3 )
{
  setVelocityCM( energy, direction, emitterVelocity );
  if( usePola )
    emittedPolarization = getPolarizationLab( G4ThreeVector( pol1, pol2, pol3 ), dirLab, emitterVelocity );
}


void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector emitterVelocity, G4double pol )
{
  setVelocityCM( energy, emitterVelocity );
  if( usePola )
    emittedPolarization = getPolarizationLab( pol, dirLab, emitterVelocity );
}

void AgataEmitted::setVelocityCM( G4double energy, G4ThreeVector emitterVelocity, G4double pol1, G4double pol2, G4double pol3 )
{
  setVelocityCM( energy, emitterVelocity );
  if( usePola )
    emittedPolarization = getPolarizationLab( G4ThreeVector( pol1, pol2, pol3 ), dirLab, emitterVelocity );
}


void AgataEmitted::setVelocityCM( G4int order, G4ThreeVector emitterVelocity, G4double pol )
{
  setVelocityCM( order, emitterVelocity );
  if( usePola )
    emittedPolarization = getPolarizationLab( pol, dirLab, emitterVelocity );
}

/////////////////////////////////////////////////////////////////////////////////////////
/// This method calculates the polarization vector in the laboratory reference frame.
/// The angle between the polarization vector and the reference orthogonal state is
/// given as an input. In any case a non-zero vector is returned. User should take
/// care of the unpolarized fraction at the level of the input file.
/////////////////////////////////////////////////////////////////////////////////////////
G4ThreeVector AgataEmitted::getPolarizationLab( G4double pol, G4ThreeVector direction, G4ThreeVector emitterVelocity )
{
  G4ThreeVector axis = emitterVelocity;
  if( axis.mag2() == 0. )
    axis = polarizationAxis;

  const G4double tolerance = 0.999;
  
  G4ThreeVector finalPolarization;
  G4ThreeVector orthState;
  G4ThreeVector paraState;
  
  if( fabs(direction.unit().dot(axis.unit())) > tolerance ) {
    orthState = G4ThreeVector(1., 0., 0. );
    paraState = G4ThreeVector(0., 1., 0. );
    
    G4double theta = direction.theta();
    G4double phi   = direction.phi();

    G4RotationMatrix rm;
    rm.set(0,0,0);
    if( theta > 0. ) {  // this rotation makes sense only if theta > 0.
      rm.rotateY( theta );
      rm.rotateZ( phi   );
    }
    orthState = rm(orthState);
    paraState = rm(paraState);
  }
  else {
    orthState = direction.cross(axis).unit();
    paraState = direction.cross(orthState).unit();
  }
  
  finalPolarization = cos(pol) * orthState + sin(pol) * paraState;
  
  return finalPolarization;
}

// Chen:
G4ThreeVector AgataEmitted::getPolarizationLab( G4ThreeVector pol1, G4ThreeVector direction, G4ThreeVector emitterVelocity )
{
  G4ThreeVector pol = pol1;
  
  G4ThreeVector axis = emitterVelocity;
  if( axis.mag2() == 0. )
    axis = polarizationAxis;
    
  if( pol.x() == 0. ) pol.setX(1.); // should have some intensity  
  
  G4double polarValue = sqrt(pol.y()*pol.y()+pol.z()*pol.z())/pol.x();
  if( polarValue > 1.0 ) {
    pol.setY(pol.y()/polarValue);
    pol.setZ(pol.z()/polarValue);
    polarValue = sqrt(pol.y()*pol.y()+pol.z()*pol.z())/pol.x();
  }

  if( polarValue )
    considerPolarization.push_back(true);
  else
    considerPolarization.push_back(false);

  G4double psi;
  psi = 0.5*atan2( pol.z(),pol.y() );
  if( psi < 0. ) psi += 360.*deg;
  G4double cospsi = cos(psi);      
  G4double sinpsi = sin(psi);

  const G4double tolerance = 0.999;
  
  G4ThreeVector finalPolarization;
  G4ThreeVector orthState;
  G4ThreeVector paraState;
  
  if( fabs(direction.unit().dot(axis.unit())) > tolerance ) {
    orthState = G4ThreeVector(1., 0., 0. );
    paraState = G4ThreeVector(0., 1., 0. );
    
    G4double theta = direction.theta();
    G4double phi   = direction.phi();

    G4RotationMatrix rm;
    rm.set(0,0,0);
    if( theta > 0. ) {  // this rotation makes sense only if theta > 0.
      rm.rotateY( theta );
      rm.rotateZ( phi   );
    }
    orthState = rm(orthState);
    paraState = rm(paraState);
  }
  else {
    orthState = direction.cross(axis).unit();
    paraState = direction.cross(orthState).unit();
  }
  
  finalPolarization = cospsi * orthState + sinpsi * paraState;
  
  return finalPolarization;
}
// end Chen

void AgataEmitted::SetPolarizationAxis( G4ThreeVector axis )
{
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  
  if( axis.mag2() > 0. ) {
    polarizationAxis = axis.unit();
    G4cout  << " ----> Polarization axis set to ("
            << std::setw(8) << polarizationAxis.x()
            << std::setw(8) << polarizationAxis.y()
            << std::setw(8) << polarizationAxis.z() << " ) " << G4endl;
  }
  else {
    G4cout << " ----> Invalid value, keeping previous one (" 
           << std::setw(8) << polarizationAxis.x()
           << std::setw(8) << polarizationAxis.y()
           << std::setw(8) << polarizationAxis.z() << " ) " << G4endl;
  }
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"

AgataEmittedMessenger::AgataEmittedMessenger(AgataEmitted* pTarget, G4String name, G4bool value, G4String directory)
:myTarget(pTarget)
{ 
  usePola = value;

  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = directory + "/generator/" + name;
  
  commandName = directoryName + "/";
  myDirectory = new G4UIdirectory(commandName);
  commandName = "Control of " + name + " emission.";
  aLine = commandName.c_str();
  myDirectory->SetGuidance(aLine);
  
  commandName = directoryName + "/gunType";
  aLine = commandName.c_str();
  SetGunCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetGunCmd->SetGuidance("Define behaviour of the particle gun.");
  commandName = " 0 --> Monochromatic "+name+"s";
  aLine = commandName.c_str();
  SetGunCmd->SetGuidance(aLine);
  commandName = " 1 --> Equally spaced "+name+"s Eg = Offset + Delta * n";
  aLine = commandName.c_str();
  SetGunCmd->SetGuidance(aLine);
  SetGunCmd->SetGuidance(" 2 --> Discrete energies from file");
  SetGunCmd->SetGuidance(" 3 --> Flat energy distribution");
  SetGunCmd->SetGuidance(" 4 --> Energies sampled from spectrum");
  SetGunCmd->SetGuidance(" 5 --> Discrete energies from file (weighted with intensities)");
  SetGunCmd->SetGuidance(" 6 --> E2 transition");
  SetGunCmd->SetGuidance("Required parameters: 1 integer (0--6).");
  SetGunCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
  commandName = directoryName+"/energy";
  aLine = commandName.c_str();
  SetMonochromaticCmd = new G4UIcmdWithADouble(aLine, this);
  commandName = "Define energy of the monochromatic " + name + "s.";
  aLine = commandName.c_str();
  SetMonochromaticCmd->SetGuidance(aLine);
  SetMonochromaticCmd->SetGuidance("Required parameters: 1 double (energy in keV).");
  SetMonochromaticCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName+"/band";
  aLine = commandName.c_str();
  SetBandCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define "+name+" cascade as a rotational band.";  
  aLine = commandName.c_str();
  SetBandCmd->SetGuidance(aLine);
  SetBandCmd->SetGuidance("Eg = Emin + Delta * n  (Emin and Delta in keV)");
  SetBandCmd->SetGuidance("Required parameters: 2 double (Emin and Delta in keV), 1 integer (number of transitions).");
  SetBandCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName+"/flat";
  aLine = commandName.c_str();
  SetFlatCmd = new G4UIcmdWithAString(aLine,this);
  commandName = "Define energy range of flat "+name+" spectrum.";
  aLine = commandName.c_str();
  SetFlatCmd->SetGuidance(aLine);
  SetFlatCmd->SetGuidance("Required parameters: 2 double (Emin, Emax in keV).");
  SetFlatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName+"/fileEnergy";
  aLine = commandName.c_str();
  SetFileEnerNameCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define file with the energy of the "+name+" transitions.";
  aLine = commandName.c_str();
  SetFileEnerNameCmd->SetGuidance(aLine);
  SetFileEnerNameCmd->SetGuidance("Required parameters: 1 string.");
  SetFileEnerNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName+"/fileEnergyI";
  aLine = commandName.c_str();
  SetFileEnerINameCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define file with the energy of the "+name+" transitions.";
  aLine = commandName.c_str();
  SetFileEnerINameCmd->SetGuidance(aLine);
  SetFileEnerINameCmd->SetGuidance("Required parameters: 1 string.");
  SetFileEnerINameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "/fileSpectrum";
  aLine = commandName.c_str();
  SetFileSpecNameCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define file with the CM spectra for " + name + "s.";
  aLine = commandName.c_str();
  SetFileSpecNameCmd->SetGuidance(aLine);
  SetFileSpecNameCmd->SetGuidance("Required parameters: 1 string.");
  SetFileSpecNameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName+"/multiplicity";
  aLine = commandName.c_str();
  SetMultiplicityCmd = new G4UIcmdWithAnInteger(aLine, this);
  commandName = "Define " + name + " multiplicity.";
  aLine = commandName.c_str();
  SetMultiplicityCmd->SetGuidance(aLine);
  SetMultiplicityCmd->SetGuidance("Required parameters: 1 integer.");
  SetMultiplicityCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName+"/tau";
  aLine = commandName.c_str();
  SetTauCmd = new G4UIcmdWithADouble(aLine,this);
  commandName = "Define lifetime of the "+name+".";  
  aLine = commandName.c_str();
  SetTauCmd->SetGuidance(aLine);
  SetTauCmd->SetGuidance("Required parameters: 1 double (lifetime in ns).");
  SetTauCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  if( usePola ) {
    commandName = directoryName+"/polarization";
    aLine = commandName.c_str();
    SetPolarCmd = new G4UIcmdWithADouble(aLine,this);
    commandName = "Define degree of linear polarization of the "+name+".";  
    aLine = commandName.c_str();
    SetPolarCmd->SetGuidance(aLine);
    SetPolarCmd->SetGuidance("Required parameters: 1 double (0.0--1.0).");
    SetPolarCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName+"/state";
    aLine = commandName.c_str();
    SetPolarStateCmd = new G4UIcmdWithADouble(aLine,this);
    commandName = "Define angle between the polarization vector and the reference axis of the "+name+".";  
    aLine = commandName.c_str();
    SetPolarStateCmd->SetGuidance(aLine);
    SetPolarStateCmd->SetGuidance("Required parameters: 1 double (angle in degrees).");
    SetPolarStateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName+"/axis";
    aLine = commandName.c_str();
    SetPolAxisCmd = new G4UIcmdWith3Vector(aLine,this);
    commandName = "Define axis of linear polarization of the "+name+".";  
    aLine = commandName.c_str();
    SetPolAxisCmd->SetGuidance(aLine);
    SetPolAxisCmd->SetGuidance("Required parameters: 3 double (cartesian components of the axis).");
    SetPolAxisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName+"/direction";
    aLine = commandName.c_str();
    SetAxisCmd = new G4UIcmdWithAString(aLine,this);
    commandName = "Define axis of linear polarization of the "+name+" through it polar coordinates.";  
    aLine = commandName.c_str();
    SetAxisCmd->SetGuidance(aLine);
    SetAxisCmd->SetGuidance("Required parameters: 2 double  (polar and azimutal angles of the polarization axis in degrees).");
    SetAxisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName+"/stokes";
    aLine = commandName.c_str();
    SetStokesCmd = new G4UIcmdWith3Vector(aLine,this);
    commandName = "Define polarization of the "+name+" through the Stokes parameters.";  
    aLine = commandName.c_str();
    SetStokesCmd->SetGuidance(aLine);
    SetStokesCmd->SetGuidance("Required parameters: 3 double (I, P1, P2).");
    SetStokesCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
  commandName = directoryName + "/thetaRange";
  aLine = commandName.c_str();
  SetEmittedThCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define polar angle range for " + name + " emission.";  
  aLine = commandName.c_str();
  SetEmittedThCmd->SetGuidance(aLine);
  SetEmittedThCmd->SetGuidance("Required parameters: 2 double (thMin, thMax in deg).");
  SetEmittedThCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "/phiRange";
  aLine = commandName.c_str();
  SetEmittedPhCmd = new G4UIcmdWithAString(aLine, this);
  commandName = "Define azimuthal angle range for " + name + " emission.";  
  aLine = commandName.c_str();
  SetEmittedPhCmd->SetGuidance(aLine);
  SetEmittedPhCmd->SetGuidance("Required parameters: 2 double (phMin, phMax in deg).");
  SetEmittedPhCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataEmittedMessenger::~AgataEmittedMessenger()
{
  delete   myDirectory;
  delete   SetGunCmd;
  delete   SetMonochromaticCmd;
  delete   SetBandCmd;
  delete   SetFlatCmd;
  delete   SetFileEnerNameCmd;
  delete   SetFileEnerINameCmd;
  delete   SetFileSpecNameCmd;
  delete   SetMultiplicityCmd;
  delete   SetTauCmd;
  delete   SetEmittedThCmd;
  delete   SetEmittedPhCmd;
  if( usePola ) {
    delete   SetPolarCmd;
    delete   SetPolarStateCmd;
    delete   SetAxisCmd;
    delete   SetPolAxisCmd;
    delete   SetStokesCmd;
  }
}

void AgataEmittedMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetGunCmd ) {
    myTarget->SetGun(SetGunCmd->GetNewIntValue(newValue));
  }
  if( command == SetMultiplicityCmd ) {
    myTarget->SetMultiplicity(SetMultiplicityCmd->GetNewIntValue(newValue));
  }
  if( command == SetMonochromaticCmd ) {
    myTarget->SetMonochromaticEnergy(SetMonochromaticCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetBandCmd ) {
    G4double e1, e2;
    G4int    nn;
    sscanf( newValue, "%lf %lf %d", &e1, &e2, &nn);
    myTarget->SetBand(e1, e2, nn);
  }
  if( command == SetFileEnerNameCmd ) {
    myTarget->SetFileEnerName(newValue);
  }
  if( command == SetFileEnerINameCmd ) {
    myTarget->SetFileEnerIName(newValue);
  }
  if( command == SetFlatCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetFlat(e1, e2);
  }
  if( command == SetFileSpecNameCmd ) {
    myTarget->SetFileSpecName(newValue);
  }
  if( command == SetEmittedThCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetEmittedTh(e1, e2);
  }
  if( command == SetEmittedPhCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetEmittedPh(e1, e2);
  }
  if( command == SetTauCmd ) {
    myTarget->SetEmittedTau(SetTauCmd->GetNewDoubleValue(newValue));
  }  
  if( usePola ) {
    if( command == SetPolarCmd ) {
      myTarget->SetPolarizationValue(SetPolarCmd->GetNewDoubleValue(newValue));
    }  
    if( command == SetPolarStateCmd ) {
      myTarget->SetPolarizationState(SetPolarStateCmd->GetNewDoubleValue(newValue));
    }  
    if( command == SetPolAxisCmd ) {
      myTarget->SetPolarizationAxis(SetPolAxisCmd->GetNew3VectorValue(newValue));
    }  
    if( command == SetAxisCmd ) {
      G4double e1, e2;
      sscanf( newValue, "%lf %lf", &e1, &e2);
      e1 *= deg;
      e2 *= deg;
      G4ThreeVector axis( sin(e1)*cos(e2), sin(e1)*sin(e2), cos(e1) );
      myTarget->SetPolarizationAxis(axis);
    }  
    if( command == SetStokesCmd ) {
      myTarget->SetStokes(SetStokesCmd->GetNew3VectorValue(newValue));
    }  
  }
}
