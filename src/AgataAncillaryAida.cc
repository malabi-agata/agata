#ifdef ANCIL
#include "AgataAncillaryAida.hh"
//#include "AgataAncillaryCluster.hh" // doesn't work !!!???
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryAida::AgataAncillaryAida(G4String path, G4String name )
{
  G4String iniPath = path;
  
  dirName     = name;

  // files needed by AgataAncillaryCluster
  //G4String solidName = G4String("aisolid"); // doesn't work !!!???
  //G4String clustName = G4String("aiclust"); // doesn't work !!!???
  //G4String angleName = G4String("aieuler"); // doesn't work !!!???
  //G4String wallsName = G4String("aiwalls"); // doesn't work !!!???
  //G4String matName   = G4String("Silicon"); // doesn't work !!!???
  
   nam_vacuum          = "Vacuum";
   mat_vacuum          = NULL;
    
   nam_aluminium       = "Aluminum";
   mat_aluminium       = NULL;
    
   nam_silicon            = "Silicon";
   mat_silicon            = NULL;

  // members inherited from AgataAncillaryScheme
  ancSD       = NULL;
  ancName     = G4String("AIDA");
  ancOffset   = 17000;
  
  numAncSd = 0;
  
   
  // finally, instance a "Cluster" object
  //theCluster = new AgataAncillaryCluster( iniPath, name, solidName, clustName, angleName, wallsName, ancName, matName ); // doesn't work !!!???
  
}

AgataAncillaryAida::~AgataAncillaryAida()
{}

/*G4int AgataAncillaryAida::FindMaterials()
{
  return theCluster->FindMaterials(ancName);
  return 0;
}
*/

G4Material* FindMaterialAida (G4String Name){
    // search the material by its name
    G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> AIDA uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary AIDA! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int AgataAncillaryAida::FindMaterials()
{
    mat_vacuum      = FindMaterialAida(nam_vacuum);     if (!mat_vacuum)    return 1;
    mat_aluminium   = FindMaterialAida(nam_aluminium);  if (!mat_aluminium) return 1;
    mat_silicon     = FindMaterialAida(nam_silicon);       if (!mat_silicon)      return 1;
    return 0;
}


void AgataAncillaryAida::InitSensitiveDetector()
{
 cout << "Setting AIDA as sensitive !" << endl;
   G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif 


  G4String name   = G4String("/anc/Aida");
  G4String HCname = G4String("AidaCollection");

  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, name, HCname, offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }
  //theCluster->SetSensitiveDetector(ancSD);  // doesn't work !!!???

}

void AgataAncillaryAida::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  //theCluster->GetDetectorConstruction(); // doesn't work !!!???

}


void AgataAncillaryAida::Placement()
{  
  //theCluster->Placement();  // doesn't work !!!???
 
  // Aida Si: 80x80x1 mm3
   const double    Si_Height = 80*mm;         
   const double    Si_Width    = Si_Height;        
   const double    Si_thickness = 1.*mm;        

 
    //Building the Silicon crystal:
    G4Box *solid_AidaSi = new G4Box("AidaSi",Si_Height/2 , Si_Width/2, Si_thickness/2);
    G4LogicalVolume *logicAidaSi = new G4LogicalVolume( solid_AidaSi, mat_silicon, "log_AidaSi", 0, 0, 0 );
  

   //Assembly of AIDA CLUSTER:
    G4RotationMatrix Rid = G4RotationMatrix::IDENTITY;

    G4AssemblyVolume* Aida_Cluster = new G4AssemblyVolume();

// For 8x8 cm^2 Si stack
    G4Transform3D T0(Rid, G4ThreeVector( 0.*mm, 0.*mm,-6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T0);
    G4Transform3D T1(Rid, G4ThreeVector( 0.*mm, 0*mm,  0.*mm)); Aida_Cluster->AddPlacedVolume(logicAidaSi, T1);
    G4Transform3D T2(Rid, G4ThreeVector( 0.*mm, 0.*mm, 6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T2);

// additional detector for 24x8 cm^2 Si stack
    G4Transform3D T3(Rid, G4ThreeVector( Si_Width, 0.*mm,-6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T3);
    G4Transform3D T4(Rid, G4ThreeVector( Si_Width, 0*mm,  0.*mm)); Aida_Cluster->AddPlacedVolume(logicAidaSi, T4);
    G4Transform3D T5(Rid, G4ThreeVector( Si_Width, 0.*mm, 6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T5);

    G4Transform3D T6(Rid, G4ThreeVector( -Si_Width, 0.*mm,-6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T6);
    G4Transform3D T7(Rid, G4ThreeVector( -Si_Width, 0*mm,  0.*mm)); Aida_Cluster->AddPlacedVolume(logicAidaSi, T7);
    G4Transform3D T8(Rid, G4ThreeVector( -Si_Width, 0.*mm, 6*mm));  Aida_Cluster->AddPlacedVolume(logicAidaSi, T8);

    //Placing the clusters:
        G4Transform3D TF(Rid, Rid*G4ThreeVector(0., 0., 0.));
        Aida_Cluster->MakeImprint(theDetector->HallLog(),TF, 1000);

    //Scintillator is sensitive detector:
    logicAidaSi -> SetSensitiveDetector(ancSD);
    
    // Vis Attributes
    G4VisAttributes *pVA1 = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) ); logicAidaSi->SetVisAttributes( pVA1 );


  return;
 }


void AgataAncillaryAida::ShowStatus()
{
  //theCluster->ShowStatus(ancName); // doesn't work !!!???
}

void AgataAncillaryAida::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  cout << "Writing AIDA header done !" << unit << endl;
  outFileLMD.good();
  //theCluster->WriteHeader(outFileLMD, unit); // doesn't work !!!???
  cout << "Write AIDA header done !" << endl;
}

void AgataAncillaryAida::WriteHeader(G4String *sheader, G4double unit)
{
//  theCluster->WriteHeader(sheader, unit);
}

#endif
