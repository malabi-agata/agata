/////////////////////////////////////////////
// 
//  This class stops particles and deposits their kinetic energy
//  when they intersect with the "test sphere", see AgataDetectorTest class
//
//  Joa Ljungvall 09/2012
//
///////////////////////////////////


#include "AgataSpecialCuts.hh"
#include "G4VParticleChange.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Ions.hh"


G4bool AgataSpecialCuts::fDontKillGammas=false;
G4bool AgataSpecialCuts::fDontKillIons=false;
AgataSpecialCutsMessenger*    AgataSpecialCuts::myMessenger=0;

AgataSpecialCuts::AgataSpecialCuts(const G4String& aName)
  : G4VProcess(aName)
{
   if (verboseLevel>1) {
     G4cout << GetProcessName() << " is created "<< G4endl;
   }
   if(myMessenger==0) myMessenger = new AgataSpecialCutsMessenger(this,"/Agata/testing");
}

AgataSpecialCuts::~AgataSpecialCuts() 
{                                     
}                                     

G4VParticleChange* AgataSpecialCuts::PostStepDoIt(
			     const G4Track& aTrack,
			     const G4Step& 
			    )
//
// Stop the current particle, if requested by G4UserLimits 
// 			    			    			    
{
   aParticleChange.Initialize(aTrack);
   aParticleChange.ProposeEnergy(0.) ;
   aParticleChange.ProposeLocalEnergyDeposit (aTrack.GetKineticEnergy()) ;
   aParticleChange.ProposeTrackStatus(fStopButAlive);
   return &aParticleChange;
}

G4double AgataSpecialCuts::
PostStepGetPhysicalInteractionLength(const G4Track& track,
				     G4double   previousStepSize,
				     G4ForceCondition* condition)
{
  previousStepSize = previousStepSize;
  *condition = NotForced;
  if(track.GetVolume()){
    if(track.GetVolume()->GetName()){
      if(track.GetVolume()->GetName()=="TargetPhys"){//Stop some secondaries in target
	if(track.GetParticleDefinition()->GetParticleName()=="gamma" &&
	   fDontKillGammas) {
	  return DBL_MAX;
	}
	if(track.GetParticleDefinition()->GetParticleType()=="nucleus" && fDontKillIons ){
	  return DBL_MAX;
	}
	if(track.GetTrackID()!=1) return 0.;
      }
      if(track.GetVolume()->GetName()=="TestSpherePhys"){//Stop all particles in this sphere
	return 0.;
      }
    }
  }
  return DBL_MAX;
}



#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"


AgataSpecialCutsMessenger::AgataSpecialCutsMessenger(AgataSpecialCuts* mycut, G4String name)
  :myCuts(mycut)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/agataspecialcuts/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of special cuts used with AgataDetectorTest.");
  
  commandName = directoryName + "SetDontKillGammas";
  aLine = commandName.c_str();
  SetDontKillGammas = new G4UIcmdWithABool(aLine, this);
  SetDontKillGammas->SetGuidance("Turn on/off killing secondary gammas in target.");
  SetDontKillGammas->SetGuidance("Required parameters: 1 bool.");
  SetDontKillGammas->AvailableForStates(G4State_PreInit,G4State_Idle);
  commandName = directoryName + "SetDontKillIons";
  aLine = commandName.c_str();
  SetDontKillIons = new G4UIcmdWithABool(aLine, this);
  SetDontKillIons->SetGuidance("Turn on/off killing secondary ions in target.");
  SetDontKillIons->SetGuidance("Required parameters: 1 bool.");
  SetDontKillIons->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataSpecialCutsMessenger::~AgataSpecialCutsMessenger()
{
  delete   myCuts;
  delete   SetDontKillIons;
  delete SetDontKillGammas;
}

void AgataSpecialCutsMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command ==  SetDontKillGammas) {
    myCuts->SetDontKillGammas(SetDontKillGammas->GetNewBoolValue(newValue));
  }
  if( command ==  SetDontKillIons) {
    myCuts->SetDontKillIons(SetDontKillIons->GetNewBoolValue(newValue));
  }
}
