#include "OrgamDetector.hh"
#include "AgataSensitiveDetector.hh"
#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4ExtrudedSolid.hh"
#include "G4TwoVector.hh"

#include "G4Box.hh"
#include "G4Tubs.hh"
#include <iomanip>
#include <sstream>

#include "OrgamAngles.hh"

namespace PhaseIGeom  {
  // -- -TRONCONIQUE PHASEI, NEW ---------------------
  // Detector definition for the tronconique (PhaseI)
  const G4double TronCrystalLength	= 70.0*mm; // cystal length
	
  const G4double TronCrystalEdgeDepth	= 30.0*mm; // depth at which starts TronCrystalRadiusMax	
  const G4double TronCrystalRadiusMin	=  32.0*mm; // radius of the crystal at the entrance face	
  const G4double TronCrystalRadiusMax	=  35.0*mm; // radius of the crystal at the back face	
	
  const G4double TronCrystalHoleDepth	= 15.0*mm; // depth at which starts the hole
  const G4double TronCrystalHoleRadius	=  5.0*mm; // radius of the hole
	
  const G4double TronBGOLength		= 190.0*mm; // length of the BGO sheild
  const G4double TronBGOFace2		=  73.0*mm;
  const G4double TronBGOFace1		=  54.0*mm;
  const G4double TronBGOFace2InnRad	=  52.0*mm;
  const G4double TronBGOFace1InnRad	=  33.0*mm;
  const G4double TronBGOCut1X		=  16.01*mm;
  const G4double TronBGOCut1Angle		=  15.04*deg;	//18.64*deg;
  const G4double TronBGOCut1AngleZ	=  30.86*deg;
  const G4double TronBGOCut1Y		=  73.0*mm;
  const G4double TronBGOCut1Z		=  190.0*mm;
  const G4double TronBGOCut1AnglePosition	=  30.86*deg;
	
  const G4double TronBGOCut2X		=  16.67*mm;
  const G4double TronBGOCut2Angle		=  15.4*deg;	//18.0*deg;
  const G4double TronBGOCut2AngleZ	=  90.0*deg;
  const G4double TronBGOCut2Y		=  73.0*mm;
  const G4double TronBGOCut2Z		=  190.0*mm;
  const G4double TronBGOCut2AnglePosition	=  90.0*deg;
	
  const G4double TronBGOCut3X		=  16.01*mm;
  const G4double TronBGOCut3Angle		=  23.05*deg;//25.65*deg;
  const G4double TronBGOCut3AngleZ	=  56.11*deg;
  const G4double TronBGOCut3Y		=  73.0*mm;
  const G4double TronBGOCut3Z		=  190.0*mm;
  const G4double TronBGOCut3AnglePosition	=  123.89*deg;
	
  const G4double TronBGOCut4X		=  15.89*mm;
  const G4double TronBGOCut4Angle		=  13.88*deg;//16.48*deg;
  const G4double TronBGOCut4AngleZ	=  0.0*deg;
  const G4double TronBGOCut4Y		=  73.0*mm;
  const G4double TronBGOCut4Z		=  190.0*mm;
  const G4double TronBGOCut4AnglePosition	=  180.0*deg;
			
  //	const 	G4double	TronColiPosition		=299.6*mm;
  //	const 	G4double	TronColiFace2Position		=334.8*mm;
  const 	G4double TronColiFace1		= 70.0*mm;
  const 	G4double TronColiFace1InnRad	= 27.0*mm;
  const 	G4double TronColiFace2InnRad	= 34.0*mm;
  const 	G4double TronColiLength		= 35.0*mm;
	
  // to shape correctly the collimator
  const G4double TronColiCut1X			=44.22*mm;
  const G4double TronColiCut1Angle		=16.04*deg;//18.64*deg;
  const G4double TronColiCut1AngleZ		=30.86*deg;
  const G4double TronColiCut1Y			=84.0*mm;
  const G4double TronColiCut1Z			=252.0*mm;
  const G4double TronColiCut1AnglePosition	=30.86*deg;
	
  const G4double TronColiCut2X			=44.3*mm;
  const G4double TronColiCut2Angle		=15.4*deg;//18.5*deg;
  const G4double TronColiCut2AngleZ		=90.0*deg;
  const G4double TronColiCut2Y			=84.0*mm;
  const G4double TronColiCut2Z			=252.0*mm;
  const G4double TronColiCut2AnglePosition	=90.0*deg;
	
  const G4double TronColiCut3X			=44.2*mm;
  const G4double TronColiCut3Angle		=23.05*deg;//25.65*deg;
  const G4double TronColiCut3AngleZ		=56.11*deg;
  const G4double TronColiCut3Y			=84.0*mm;
  const G4double TronColiCut3Z			=252.0*mm;
  const G4double TronColiCut3AnglePosition	=123.89*deg;
	
  const G4double TronColiCut4X			=43.87*mm;
  const G4double TronColiCut4Angle		=13.88*deg;//16.48*deg;
  const G4double TronColiCut4AngleZ		=0.0*deg;
  const G4double TronColiCut4Y			=84.0*mm;
  const G4double TronColiCut4Z			=252.0*mm;
  const G4double TronColiCut4AnglePosition	=180.0*deg;
			
  // ---------------------OLD--------------------------
	

	
  // 	---------TronCrys, CapsuleOut et Capsule
  const 	G4double TronCapsuleOutFace1		=33.5*mm;
  const   G4double TronCapsuleOutFace2		=46.0*mm;
  const 	G4double TronCapsuleOutWidth		=1.0*mm;
  const   G4double TronCapsuleOutPosition		=337.0*mm;
  const 	G4double TronCapsuleOutFace2Position	=457.0*mm;
  const 	G4double TronCapsuleOutFace3Position	=587.0*mm;
	
  const 	G4double TronCapsuleFace1		=34.0*mm;
  const   G4double TronCapsuleFace2		=44.0*mm;
  const 	G4double TronCapsuleWidth		=1.0*mm;
  const   G4double TronCapsulePosition		=358.0*mm;
  const 	G4double TronCapsuleFace2Position	=457.0*mm;
  const 	G4double TronCapsuleFace3Position	=647.0*mm;
	
  const 	G4double TronCrystalFace1		=32.0*mm;
  const   G4double TronCrystalFace2		=35.0*mm;
  const   G4double TronCrystalPosition		=374.0*mm;
  const 	G4double TronCrystalFace2Position	=414.0*mm;
  const 	G4double TronCrystalFace3Position	=444.0*mm;
	
  // 	-----------------OuterCapsule
  const G4double TronOuterCapsuleCut1X			=44.22*mm;
  const G4double TronOuterCapsuleCut1Angle		=16.04*deg;//18.64*deg;
  const G4double TronOuterCapsuleCut1AngleZ		=30.86*deg;
  const G4double TronOuterCapsuleCut1Y			=84.0*mm;
  const G4double TronOuterCapsuleCut1Z			=252.0*mm;
  const G4double TronOuterCapsuleCut1AnglePosition	=30.86*deg;
	
  const G4double TronOuterCapsuleCut2X			=44.3*mm;
  const G4double TronOuterCapsuleCut2Angle		=15.4*deg;//18.5*deg;
  const G4double TronOuterCapsuleCut2AngleZ		=90.0*deg;
  const G4double TronOuterCapsuleCut2Y			=84.0*mm;
  const G4double TronOuterCapsuleCut2Z			=252.0*mm;
  const G4double TronOuterCapsuleCut2AnglePosition	=90.0*deg;
	
  const G4double TronOuterCapsuleCut3X			=44.2*mm;
  const G4double TronOuterCapsuleCut3Angle		=23.05*deg;//25.65*deg;
  const G4double TronOuterCapsuleCut3AngleZ		=56.11*deg;
  const G4double TronOuterCapsuleCut3Y			=84.0*mm;
  const G4double TronOuterCapsuleCut3Z			=252.0*mm;
  const G4double TronOuterCapsuleCut3AnglePosition	=123.89*deg;
	
  const G4double TronOuterCapsuleCut4X			=43.87*mm;
  const G4double TronOuterCapsuleCut4Angle		=13.88*deg;//16.48*deg;
  const G4double TronOuterCapsuleCut4AngleZ		=0.0*deg;
  const G4double TronOuterCapsuleCut4Y			=84.0*mm;
  const G4double TronOuterCapsuleCut4Z			=252.0*mm;
  const G4double TronOuterCapsuleCut4AnglePosition	=180.0*deg;


  const 	G4double TronOuterCapsuleLength			=250.0*mm;
  const 	G4double TronOuterCapsuleWidth			=1.5*mm;
  const 	G4double TronOuterCapsulePosition		=335.0*mm;//335.0*mm;
  const 	G4double TronOuterCapsuleOutRad			=83.75*mm;
  const   G4double AdjactTronBGO2Capsule			=0.2*mm;
  // 	---------------------------------Arriere
  const 	G4double	TronArreFace1			=82.05*mm;
  const	G4double	TronArreFace1InnRad		=46.2*mm;
  const	G4double	TronArreFace2			=82.05*mm;
  const	G4double	TronArreFace2InnRad		=46.2*mm;
  const	G4double	TronArreFace3			=86.05*mm;
  const	G4double	TronArreFace3InnRad		=46.2*mm;
  const	G4double 	TronArreFace4			=86.05*mm;
  const	G4double	TronArreFace4InnRad		=44.2*mm;
  const	G4double	TronArreFace5			=82.05*mm;
  const	G4double	TronArreFace5InnRad		=44.2*mm;
  const	G4double	TronArreFace6			=82.05*mm;
  const	G4double	TronArreFace6InnRad		=44.2*mm; 	
  const	G4double	TronArreFace1Position		=574.0*mm;
  const	G4double	TronArreFace2Position		=586.9*mm;
  const	G4double	TronArreFace3Position		=587.0*mm;
  const	G4double	TronArreFace4Position		=592.0*mm;
  const	G4double	TronArreFace5Position		=592.1*mm;
  const	G4double	TronArreFace6Position		=607.1*mm;
  // 	-------------------------------------------TRONCOLIMATOR

  G4double ROrgam = 205*mm;

}

using namespace PhaseIGeom;

OrgamDetector::OrgamDetector(G4String anctype, G4String path, G4String name)
{
  G4cout<<"JL: OrgamDetector, beginning"<<G4endl;
  // base to build one clover (shaped crystal)
  pCrystal = pCapsule = pBGOCrystal = pBGOCapsule = NULL;
  iniPath = path;
  pTronBGO = pTronCrystal = pTronCapsule = pTronColimator = 0;
  directoryName      = name;
  
  // material definition
  matCrystal  = matCapsule = matAntiCompton1 = matAntiCompton2 = NULL;
  matCrystalName   = "Germanium"; 
  matCapsuleName   = "Aluminium";
  matAntiCompton1Name = "BGO"; matAntiCompton2Name = "CsI";

  readOut   = false;
  fMakeFrame = false;
  fMakeChamber = true;
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;
	
  //Commented by AO
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  SDman->
    AddNewDetector(bgoSD=new AgataSensitiveDetector("Orgam", 
						    "/BGO/all", 
						    "BGOCollection",false ) );

	
  ancType = anctype;
  theAncillary = 0;
  std::cout << "ancType :" << ancType << " ";
  if(ancType!="1 0") {
    theAncillary = new AgataDetectorAncillary(ancType,"/ancorgam/",
					      "OrsayPlasticCollection");
      std::cout << ancType << "\n";
  }
  myMessenger = new OrgamDetectorMessenger(this);
  fExtraPhaseIs.clear();
  //We also make a table over Orgam angles
  std::ofstream oa("OrgamAngles.txt");
  for(int detnb=0; detnb<60; detnb++){
    G4RotationMatrix rm;
    if(orgamangles[detnb][0]>90) rm.rotateZ(180.*deg);
    rm.rotateY(orgamangles[detnb][0]*deg);
    rm.rotateZ(orgamangles[detnb][1]*deg);
    rm.rotateY(orgamangles[detnb][2]*deg);
    rm.rotateZ(orgamangles[detnb][3]*deg);
    fAllDetAngles[detnb] = rm(G4ThreeVector(0,0,1));
    G4double theta  = fAllDetAngles[detnb].theta()/deg;
    G4double phi = (fAllDetAngles[detnb].phi()/deg>0 ? 
		    fAllDetAngles[detnb].phi()/deg : 
		    fAllDetAngles[detnb].phi()/deg+360.);
    oa << detnb << " " << theta << " " << phi << std::endl;
  }
//  fDontMakeThisFrame.insert(0);
//  fDontMakeThisFrame.insert(1);
//  fDontMakeThisFrame.insert(2);
//  fDontMakeThisFrame.insert(3);
//  fDontMakeThisFrame.insert(4);
  fDontMakeThisFrame.insert(17);
  fDontMakeThisFrame.insert(18);
  fDontMakeThisFrame.insert(29);
  fDontMakeThisFrame.insert(34);
  fDontMakeThisFrame.insert(40);
  fDontMakeThisFrame.insert(43);
}

OrgamDetector::~OrgamDetector()
{
  G4cout << "### ~OrgamDetector "<< G4endl;
  delete myMessenger;
}

G4int OrgamDetector::FindMaterials()
{
  // search the material by their names
  matCrystal      = G4Material::GetMaterial(matCrystalName); // should be ok .. standard in Orgam
  if(matCrystal==0) {
    std::cout << "Oups " << matCrystalName << " not found.\n";
    exit(-1);
  }
  matCapsule      = G4Material::GetMaterial(matCapsuleName); // should be ok .. standard in Orgam
  if(matCapsule==0) {
    std::cout << "Oups " << matCapsuleName << " not found.\n";
    exit(-1);
  }
  matAntiCompton1 = G4Material::GetMaterial(matAntiCompton1Name); // in case BGO is not known
  if ( matAntiCompton1 == NULL ) {
    // Germanium isotopes
    G4Isotope* Ge70 = new G4Isotope("Ge70", 32, 70, 69.9242*g/mole);
    G4Isotope* Ge72 = new G4Isotope("Ge72", 32, 72, 71.9221*g/mole);
    G4Isotope* Ge73 = new G4Isotope("Ge73", 32, 73, 72.9235*g/mole);
    G4Isotope* Ge74 = new G4Isotope("Ge74", 32, 74, 73.9212*g/mole);
    G4Isotope* Ge76 = new G4Isotope("Ge76", 32, 76, 75.9214*g/mole);
    // germanium defined via its isotopes
    G4Element* elGe = new G4Element("Germanium","Ge", 5);
    elGe->AddIsotope(Ge70, 0.2123);
    elGe->AddIsotope(Ge72, 0.2766);
    elGe->AddIsotope(Ge73, 0.0773);
    elGe->AddIsotope(Ge74, 0.3594);
    elGe->AddIsotope(Ge76, 0.0744);

    matAntiCompton1 = new G4Material("BGO", 7.13*g/cm3, 3);
    matAntiCompton1->AddElement( new G4Element("Bismuth",  "Bi", 83., 208.98038*g/mole), 4);
    matAntiCompton1->AddElement(elGe, 3);
    matAntiCompton1->AddElement( new G4Element("Oxigen",   "O",  8.,  15.9994 *g/mole) , 12);
  }
  matAntiCompton2 = G4Material::GetMaterial(matAntiCompton2Name); // in case CsI is not known
  if ( matAntiCompton2 == NULL ) {
    matAntiCompton2 = new G4Material("CsI", 4.51*g/cm3, 2);
    matAntiCompton2->AddElement( new G4Element("Cesium", "Cs" , 55., 132.90545*g/mole), 1);
    matAntiCompton2->AddElement( new G4Element("Iodine",  "I" , 53., 126.90477*g/mole), 1);
  }

  matCollimator = G4Material::GetMaterial("Tungsten"); 
  if ( matCollimator == NULL ) {	
    G4cout << " ***** NOT KNOWN ****  TUNGSTEN *****" << G4endl;
  }	
  if (matCrystal && matAntiCompton1 && matAntiCompton2) {
    G4cout << "\n ---->  The detector material are " 
	   << matCrystalName << " " << matAntiCompton1Name << " " << matAntiCompton2Name << G4endl;
  }
  else {
    G4cout << " Could not find the correct materials " <<  G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  return 0;  
}




G4LogicalVolume *OrgamDetector::GetTronBGO()
{	
  char sName[40]; 
  G4int nbSlice=2;
  G4double  zSlice[2]={0.0, TronBGOLength};
  G4double  InnRad[2]={TronBGOFace1InnRad, TronBGOFace2InnRad};
  G4double  OutRad[2]={TronBGOFace1, TronBGOFace2};
		
  G4int nbSliceTube=3;
  G4double  zSliceTube[3]={-0.1*mm, TronBGOLength-68.0*mm,TronBGOLength};
  G4double  InnRadTube[3]={0.0,0.0,0.0};
  G4double  OutRadTube[3]={34.8*mm, 47.0*mm,47.0*mm};
  if ( pTronBGO == NULL ) {     		
    sprintf(sName, "TronBGObox1");	
    G4Box *box1=new G4Box(G4String(sName),TronBGOCut1X,TronBGOCut1Y,TronBGOCut1Z);
    sprintf(sName, "TronBGObox2");	
    G4Box *box2=new G4Box(G4String(sName),TronBGOCut2X,TronBGOCut2Y,TronBGOCut2Z);
    sprintf(sName, "TronBGObox3");	
    G4Box *box3=new G4Box(G4String(sName),TronBGOCut3X,TronBGOCut3Y,TronBGOCut3Z);
    sprintf(sName, "TronBGObox4");	
    G4Box *box4=new G4Box(G4String(sName),TronBGOCut4X,TronBGOCut4Y,TronBGOCut4Z);
    sprintf(sName, "TronBGOTube");	
    G4Polycone *Tube=new G4Polycone(G4String(sName),0*deg, 360.0*deg, nbSliceTube, zSliceTube, InnRadTube, OutRadTube);
		
    sprintf(sName, "TronBGO");
    G4Polyhedra *Tron =new G4Polyhedra(G4String(sName), 0.*deg, 360.0*deg, 10, nbSlice, zSlice, InnRad, OutRad);
		
    sprintf(sName, "TronBGO1");
    G4RotationMatrix rm;
    rm.rotateY(-TronBGOCut1Angle);
    rm.rotateZ(-TronBGOCut1AngleZ);
    G4SubtractionSolid *Tron1=new G4SubtractionSolid(G4String(sName),Tron,box1,&rm,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut1AnglePosition),TronBGOFace1*sin(TronBGOCut1AnglePosition),0.0));
    sprintf(sName, "TronBGO2");	
    G4RotationMatrix rm1;
    rm1.rotateY(-TronBGOCut1Angle);
    rm1.rotateZ(TronBGOCut1AngleZ);
    G4SubtractionSolid *Tron2=new G4SubtractionSolid(G4String(sName),Tron1,box1,&rm1,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut1AnglePosition),-TronBGOFace1*sin(TronBGOCut1AnglePosition),0.0));
    sprintf(sName, "TronBGO3");
    G4RotationMatrix rm2;
    rm2.rotateZ(TronBGOCut2AngleZ);
    rm2.rotateY(TronBGOCut2Angle);
    G4SubtractionSolid *Tron3=new G4SubtractionSolid(G4String(sName),Tron2,box2,&rm2,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut2AnglePosition),TronBGOFace1*sin(TronBGOCut2AnglePosition),0.0));
    sprintf(sName, "TronBGO4");
    G4RotationMatrix rm4;
    rm4.rotateZ(TronBGOCut2AngleZ);
    rm4.rotateY(-TronBGOCut2Angle);
    G4SubtractionSolid *Tron4=new G4SubtractionSolid(G4String(sName),Tron3,box2,&rm4,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut2AnglePosition),-TronBGOFace1*sin(TronBGOCut2AnglePosition),0.0));
    sprintf(sName, "TronBGO5");
    G4RotationMatrix rm5;
    rm5.rotateY(TronBGOCut3Angle);
    rm5.rotateZ(-TronBGOCut3AngleZ);
    G4SubtractionSolid *Tron5=new G4SubtractionSolid(G4String(sName),Tron4,box3,&rm5,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut3AnglePosition),-TronBGOFace1*sin(TronBGOCut3AnglePosition),0.0));
    sprintf(sName, "TronBGO6");
    G4RotationMatrix rm6;
    rm6.rotateY(TronBGOCut3Angle);
    rm6.rotateZ(TronBGOCut3AngleZ);
    G4SubtractionSolid *Tron6=new G4SubtractionSolid(G4String(sName),Tron5,box3,&rm6,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut3AnglePosition),TronBGOFace1*sin(TronBGOCut3AnglePosition),0.0));
    sprintf(sName, "TronBGO7");
    G4RotationMatrix rm7;
    rm7.rotateY(TronBGOCut4Angle);
    rm7.rotateZ(TronBGOCut4AngleZ);
    G4SubtractionSolid *Tron7=new G4SubtractionSolid(G4String(sName),Tron6,box4,&rm7,
						     G4ThreeVector(TronBGOFace1*cos(TronBGOCut4AnglePosition),TronBGOFace1*sin(TronBGOCut4AnglePosition),0.0));
    sprintf(sName, "TronBGO8");
    G4RotationMatrix rm8;
    G4SubtractionSolid *Tron8=new G4SubtractionSolid(G4String(sName),Tron7,Tube,&rm8,G4ThreeVector(0.0,0.0,0.0));
		
    sprintf(sName, "aTronBGO");
    pTronBGO  = new G4LogicalVolume( Tron8, matAntiCompton1, G4String(sName), 0, 0, 0 ); // PB with this one during conversion with VGM
    G4VisAttributes *pBGOVA = new G4VisAttributes(G4Colour(0.7, 0.7, 0));
    pTronBGO->SetVisAttributes(pBGOVA);
  }
  return 	pTronBGO;
}

G4LogicalVolume *OrgamDetector::GetTronCrystal()
{
  char sName[40]; 
	
  G4int nbSlice = 5;
  G4double zSlice[5] = 
    { 0.0*mm, 
      TronCrystalHoleDepth, TronCrystalHoleDepth + 1.0*mm, 
      TronCrystalEdgeDepth, TronCrystalLength };  
	 
  G4double InnRad[5] =
    { 0.0*mm, 
      0.0*mm, TronCrystalHoleRadius, 
      TronCrystalHoleRadius, TronCrystalHoleRadius };  
	      
  G4double OutRad[5] = 
    { TronCrystalRadiusMin, 
      TronCrystalRadiusMin+
      (TronCrystalRadiusMax-TronCrystalRadiusMin)*TronCrystalHoleDepth/TronCrystalEdgeDepth, 
      TronCrystalRadiusMin+
      (TronCrystalRadiusMax-TronCrystalRadiusMin)*(TronCrystalHoleDepth+1.0*mm)/TronCrystalEdgeDepth,
      TronCrystalRadiusMax, TronCrystalRadiusMax }; 
				
  if ( pTronCrystal == NULL ){
    sprintf(sName,"Tron");
    G4Polycone *Tron = 
      new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
    sprintf(sName, "aTronCrystal");
    pTronCrystal  = new G4LogicalVolume( Tron, matCrystal, G4String(sName), 0, 0, 0 );
    G4VisAttributes *pDetVACaps = new G4VisAttributes( G4Colour(0.0, 1.0, 0.0) );
    pTronCrystal->SetVisAttributes(pDetVACaps);
  }
  return pTronCrystal;
}

G4LogicalVolume *OrgamDetector::GetTronCapsule()

{
  //G4int nbSlice = 5;
  //G4double zSlice[5] = 
  //  { -1*mm,-0.5*mm,-0.5*mm, TronCrystalEdgeDepth, TronCrystalLength+1*mm };  
  //
  //G4double InnRad[5] =
  //  { 0,0,TronCrystalRadiusMin+.5*mm,TronCrystalRadiusMax+0.5*mm,TronCrystalRadiusMax+0.5*mm};
  //
  //G4double OutRad[5] = 
  //  { TronCrystalRadiusMin+1*mm,TronCrystalRadiusMin+1*mm,TronCrystalRadiusMax+1*mm,
  //    TronCrystalRadiusMax+1*mm,TronCrystalRadiusMax+1*mm};
  //G4Polycone *Tron = 
  //  new G4Polycone("GeAlCapsule",0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
  //pTronCapsule = new G4LogicalVolume(Tron,matCapsule,"LogicGeAlCapsule",0,0,0);
  //G4VisAttributes *pCapsuleVA = new G4VisAttributes(G4Colour(0.8, 0.8, 0.5));
  //pTronCapsule->SetVisAttributes(pCapsuleVA);
  //
  //return pTronCapsule;
  char sName[40];
  G4int nbSlice=7;
  G4double zSlice[7]={	TronCapsulePosition,
			TronCapsulePosition+TronCapsuleOutWidth,
			TronCapsulePosition+TronCapsuleOutWidth,
			TronCapsuleFace2Position,
			TronCapsuleFace3Position-TronCapsuleWidth,
			TronCapsuleFace3Position-TronCapsuleWidth,
			TronCapsuleFace3Position};
  G4double InnRad[7]={	0.0,
			0.0,
			TronCapsuleFace1-TronCapsuleOutWidth,
			TronCapsuleFace2-TronCapsuleOutWidth,
			TronCapsuleFace2-TronCapsuleOutWidth,
			0.0,
			0.0};
				
  G4double OutRad[7]={	TronCapsuleFace1,
			TronCapsuleFace1,
			TronCapsuleFace1,
			TronCapsuleFace2,
			TronCapsuleFace2,
			TronCapsuleFace2,
			TronCapsuleFace2};
  if(pTronCapsule==NULL){
    sprintf(sName,"Tron");
    G4Polycone *Tron=new G4Polycone(G4String(sName),0.0*deg,360.0*deg, nbSlice,zSlice,InnRad,OutRad);
    sprintf(sName, "aTronCapsule");
    pTronCapsule  = new G4LogicalVolume( Tron, matCapsule, G4String(sName), 0, 0, 0 );
    G4VisAttributes *pCapsuleVA = new G4VisAttributes(G4Colour(0.8, 0.8, 0.5));
    //    pCapsuleVA->SetForceWireframe(true);
    pTronCapsule->SetVisAttributes(pCapsuleVA);
  }
  return pTronCapsule;

  
}

G4LogicalVolume *OrgamDetector::GetTronColimator()
{	
  G4int nbSliceTube2=2;
  G4double  zSliceTube2[2]={-TronColiLength/2.-1e3*mm, TronColiLength/2.+1e-3*mm};
  G4double  InnRadTube2[2]={0,0};
  G4double  OutRadTube2[2]={TronColiFace2InnRad,TronColiFace1InnRad};
  G4Polycone *colhole=
    new G4Polycone("colhole",0*deg, 360.0*deg, nbSliceTube2, zSliceTube2, InnRadTube2, OutRadTube2);
  G4double zR1=.966028*mm;
  G4double sz1 = (ROrgam-73.9*mm)/zR1;
  G4double sz2 = (ROrgam-73.9*mm+TronColiLength)/zR1;
  //Get extra side position y
  G4double extray = 33*mm/sz1;//TronOuterCapsuleOutRad/sz1;  //!!! guessed !!!!
  //Get extrax1, extrax2
  G4double extrax1 = .0703483*mm+(.258437*mm-0.0703483*mm)*(.314713*mm-extray)/(.314713*mm-0);
  G4double extrax2 = .0703483*mm-(0.0703483*mm+.398168*mm)*(.314713*mm-extray)/(.314713*mm-0);
  //Get extra side x pos, same as y but neg
  G4double extrax = -extray;
  G4double extray1 = .314713*mm-(.314713*mm-0)*(0.0703483*mm-extrax)/(0.0703483*mm+.398168*mm);
  G4double coordR1[7][2]={{.258437*mm,0},
			  {extrax1,extray},
			  {extrax2,extray},
			  {extrax,extray1},
			  {extrax,-extray1},
			  {extrax2,-extray},
			  {extrax1,-extray}};
  std::vector<G4TwoVector> pentagonshape;
  for(int i=0; i<7; i++) pentagonshape.push_back(G4TwoVector(coordR1[i][0],coordR1[i][1]));
  G4ExtrudedSolid *shunkofTungsten = new G4ExtrudedSolid("shunkofTungsten",pentagonshape,
							 TronColiLength/2,G4TwoVector(0,0),sz1,
							 G4TwoVector(0,0),sz2);
  G4SubtractionSolid *coli = new G4SubtractionSolid("collimator",shunkofTungsten,colhole);
  pTronColimator  = new G4LogicalVolume( coli,  matCollimator, "logCol", 0, 0, 0 );
  G4VisAttributes *pColVA = new G4VisAttributes(G4Colour(0.3, 0.3, .3));
  pTronColimator->SetVisAttributes(pColVA);
  
   
  return 	pTronColimator;
}


//#define _DEBUGGEOM_

void OrgamDetector::Placement()
{  
  //  G4VPhysicalVolume *phyvol;
  if( FindMaterials() ) return;	// check if materials are known
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theTarget  = 
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  GetTronBGO();
  GetTronCrystal();
  GetTronCapsule();
  GetTronColimator();
  G4double extraR = 0*mm;
  G4double RTron = //375.0*mm
    ROrgam+extraR, RTronBGO = RTron-37.5*mm, RTronColli = RTron-73.9*mm+TronColiLength/2.; 
  pTronCrystal->SetSensitiveDetector(theTarget->GeSD());
  pTronBGO->SetSensitiveDetector(theTarget->GeSD());
  std::ostringstream os;
#ifndef _DEBUGGEOM_
  fDetAngles.clear();
  std::set<int>::iterator itorgamdet = fUsedOrgamDetectors.begin();
  for(;itorgamdet!=fUsedOrgamDetectors.end(); ++itorgamdet){
    int detnb = (*itorgamdet<300? *itorgamdet : *itorgamdet-300);
    os.str("");
    os << "PhaseI_crystal_" << detnb;
    std::cout << os.str() << std::endl;
    G4RotationMatrix rm;
    if(orgamangles[detnb][0]>90) rm.rotateZ(180.*deg);
    rm.rotateY(orgamangles[detnb][0]*deg);
    rm.rotateZ(orgamangles[detnb][1]*deg);
    rm.rotateY(orgamangles[detnb][2]*deg);
    rm.rotateZ(orgamangles[detnb][3]*deg);
    
    fDetAngles[detnb] = rm(G4ThreeVector(0,0,RTron));
    /*phyvol =*/
    new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTron))),
		      G4String(os.str()),pTronCrystal,(theTarget)->HallPhys(),
		      false, detnb );
    os.str("");
    os << "PhaseI_capsule_" << detnb;
    /*phyvol =*/
    new G4PVPlacement(G4Transform3D(rm,
				    rm(G4ThreeVector(0,0,RTronColli-
						     TronCapsuleOutPosition))),
		      G4String(os.str()),pTronCapsule,(theTarget)->HallPhys(),
		      false, detnb );
    os.str("");
    if(*itorgamdet>=300){
      os << "PhaseI_BGO_" << detnb;
      /*phyvol = */
      new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTronBGO))),
			G4String(os.str()),pTronBGO,(theTarget)->HallPhys(),
			false, detnb+300);
      os.str("");
      os << "PhaseI_Collimator_" << detnb;
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTronColli))),
			G4String(os.str()),pTronColimator,
			(theTarget)->HallPhys(), false, detnb);
    }
  }
  std::vector<PlacementOfAPhaseI>::iterator itextraphaseI =
    fExtraPhaseIs.begin();
  for(; itextraphaseI!=fExtraPhaseIs.end(); ++itextraphaseI){
    int detnb = (itextraphaseI->id<300 ?
		 itextraphaseI->id : itextraphaseI->id-300);
    G4ThreeVector Trans(itextraphaseI->x,itextraphaseI->y,itextraphaseI->z);
    fDetAngles[detnb] = Trans;
    G4RotationMatrix rm;
    rm.rotateZ(itextraphaseI->phi);
    rm.rotateY(itextraphaseI->theta);
    rm.rotateZ(itextraphaseI->psi);
    os.str("");
    os << "Extra_PhaseI_crystal_" << detnb;
    std::cout << os.str() << std::endl;
    /*phyvol = */
    new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RTron))-
				    rm(G4ThreeVector(0,0,RTron))+Trans),
		      G4String(os.str()),pTronCrystal,(theTarget)->HallPhys(),
		      false, detnb );
    os.str("");
    os << "PhaseI_capsule_" << detnb;
    /*phyvol =*/
    new G4PVPlacement(G4Transform3D(rm,
				    rm(G4ThreeVector(0,0,RTronColli-
						     TronCapsuleOutPosition))+
				    Trans),
		      G4String(os.str()),pTronCapsule,(theTarget)->HallPhys(),
		      false, detnb );
    os.str("");
    if(itextraphaseI->id>=300){
      os << "PhaseI_BGO_" << detnb;
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RTronBGO))-
				      rm(G4ThreeVector(0,0,RTron))+Trans),
			G4String(os.str()),pTronBGO,(theTarget)->HallPhys(),
			false, detnb+300);
      os.str("");
      os << "PhaseI_Collimator_" << detnb;
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RTronColli))-
				      rm(G4ThreeVector(0,0,RTron))+Trans),
			G4String(os.str()),pTronColimator,
			(theTarget)->HallPhys(), false, detnb);
    }
    
  }
#else
  os.str("");
  os << "PhaseI_crystal_0";
  std::cout << os.str() << std::endl;
  G4RotationMatrix rm;
  rm.rotateX(0*deg);
  rm.rotateZ(0*deg);
  /*phyvol =*/
  new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RTron))),
		    G4String(os.str()),pTronCrystal,
		    (theTarget)->HallPhys(), false, 0 );
  os.str("");
  os << "PhaseI_BGO_0";
  /*phyvol =*/
  new G4PVPlacement(G4Transform3D(rm, 
				  rm( G4ThreeVector(0,0,RTronBGO))),
		    G4String(os.str()),pTronBGO,
		    (theTarget)->HallPhys(), false, 300);
  os.str("");
  os << "PhaseI_Collimator_0";
  /*phyvol =*/
  new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RTronColli))),
		    G4String(os.str()),pTronColimator,
		    (theTarget)->HallPhys(), false, 0);
  
  
#endif
  //We also put in place our chambre
  //First the large alu piece
  if(fMakeChamber){
    std::cout << " -----> Building Orgam chamber\n";
    G4int cp1 = 10;
    G4double z[10] = {-490*mm,
		      -478*mm,
		      -478*mm,
		      -140.82*mm,//   -135.4493118526888*mm,
		      -130*mm, 
		      -121.54*mm,
		      -100*mm,
		      -80.51*mm,
		      -58.1464*mm,
		      52*mm};
    G4double router[10]={50*mm,
			 50*mm,
			 18.5*mm,
			 18.5*mm,
			 21.68*mm,
			 33.23*mm,
			 76.67*mm,
			 96.92*mm,
			 105*mm,
			 105*mm};
    G4double rinner[10] ={15.5*mm,
			  15.5*mm,
			  15.5*mm,
			  15.5*mm,
			  18.21*mm,//31.85486009350359*mm ,//<-
			  25.97*mm,
			  71.61*mm,
			  92.98*mm,
			  102*mm,
			  102*mm};
    G4Polycone *solchamber1 = new G4Polycone("chamber1sol",0*deg, 360.0*deg,
					     cp1,z,rinner,router);

    G4Tubs *subtub1 = new G4Tubs("subtub1",0*mm,42.5*mm,50*mm,0,360*deg);
    G4RotationMatrix rm1;
    rm1.rotateX(-90*deg);
    G4SubtractionSolid *sub1chamber = 
      new G4SubtractionSolid("sub1chamber",solchamber1,subtub1,
			     G4Transform3D(rm1,G4ThreeVector(0,-100*mm,0)));

    //Second part of alu
    G4int cp2 = 4;
    G4double z2[4] = {0,12*mm,12*mm,200*mm};
    G4double router2[4]={65*mm,65*mm,40.5*mm,40.5*mm};
    G4double rinner2[4]={37.5*mm,37.5*mm,37.5*mm,37.5*mm};
    G4Polycone *solchamber2 = new G4Polycone("chamber2sol",0*deg, 360.0*deg,
					     cp2,z2,rinner2,router2);
    G4Tubs *subtub2 = new G4Tubs("subtub2",0*mm,102*mm,52*mm,0,360*deg);
    G4RotationMatrix rm2;
    rm2.rotateX(90*deg);
    G4SubtractionSolid *sub2chamber = 
      new G4SubtractionSolid("sub2chamber",solchamber2,subtub2,
			     G4Transform3D(rm2,G4ThreeVector(0,0,137*mm)));
    G4RotationMatrix rm3;
    rm3.rotateX(-90*deg);
    rm3.rotateY(180*deg);
    G4UnionSolid *union1chamber = new G4UnionSolid("union1chamber",sub2chamber,sub1chamber,
						   G4Transform3D(rm3,G4ThreeVector(0,0,136.99*mm)));
    G4LogicalVolume *logchamber1 = new G4LogicalVolume(union1chamber,matCapsule,"chambre1log",0,0,0);
    G4VisAttributes *vischal = new G4VisAttributes(G4Colour(.6, .6, .6));
    logchamber1->SetVisAttributes(vischal);
    G4RotationMatrix rm4;
    rm4.rotateX(90*deg);
    rm4.rotateZ(180*deg);
    new G4PVPlacement(G4Transform3D(rm4,
				    G4ThreeVector(0,-137*mm,0)),
		      "chamber1phys",logchamber1,
		      (theTarget)->HallPhys(), false, 0);
    //Now the plastic part...
    G4int cp3 = 8;
    G4double z3[8]={52*mm,
		    60.6218*mm,
		    77.9423*mm,
		    105.258*mm,
		    126.143*mm,
		    185*mm,
		    185*mm,
		    200*mm};
    G4double router3[9] = {105*mm,
			   105*mm,
			   99*mm,
			   69.26*mm,
			   58*mm,
			   58*mm,
			   82.5*mm,
			   82.5*mm};
    G4double rinner3[9] = {97*mm,
			   97*mm,
			   91*mm,
			   61*mm,
			   50*mm,
			   50*mm,
			   50*mm,
			   50*mm};
    G4Polycone *solplastic = new G4Polycone("plasticsol",0*deg, 360.0*deg,cp3,
					    z3,rinner3,router3);
    G4NistManager* man = G4NistManager::Instance();
    G4Material *plastic =  man->FindOrBuildMaterial("G4_POLYETHYLENE");
    if(plastic==0){
      std::cout << "Did not find G4_POLYETHYLENE!!!\n";
      exit(-1);
    }
    G4LogicalVolume *logplastic = new G4LogicalVolume(solplastic,plastic,"plasticlog");
    G4VisAttributes *visplasitc = new G4VisAttributes(G4Colour(1., 1., 1.));
    logplastic->SetVisAttributes(visplasitc);
    new G4PVPlacement(G4Transform3D(),
		      "chamber2phys",logplastic,
		      (theTarget)->HallPhys(), false, 0);
  }
  //If asked for the frames
  if(fMakeFrame){
    G4double frameinnerrad = 508.4*mm;
    G4double frameouterrad = 612*mm;
    G4double cuty = -.24;
    G4double k = (0.0703483+.398168)/.314713;
    G4double mf = -.398168;  // changed m by mf (mframe) Marc
    G4double coordR1[5][2]={{0,.258437},
			    {.314713,.0703483},
			    {(cuty-mf)/k,cuty},
			    {-(cuty-mf)/k,cuty},
			    {-.314713,.0703483}};
    G4double zR1=.966028*mm;
    G4double sz1 = frameinnerrad/zR1;
    G4double sz2 = frameouterrad/zR1;
    std::vector<G4TwoVector> pentagonshape;
    for(int i=0; i<5; i++) 
      pentagonshape.push_back(G4TwoVector(coordR1[i][0],coordR1[i][1]));
    G4ExtrudedSolid *solframeex = 
      new G4ExtrudedSolid("framesolextrude",pentagonshape,
			  (frameouterrad-frameinnerrad)/2,G4TwoVector(0,0),sz1,
			  G4TwoVector(0,0),sz2);
    //The radius of this tube is "hearsay"
    G4Tubs *subtub = new G4Tubs("subtubframe",0*mm,100*mm,500*mm,0,360*deg);
    G4SubtractionSolid *solframesub1 = 
      new G4SubtractionSolid("framesolsub1",solframeex,subtub,
			     G4Transform3D(G4RotationMatrix(),
					   G4ThreeVector(0,0,0)));
    G4double holeinframez = 491.39*mm;
    G4double homeinframedZ = 55*mm;
    sz1 = holeinframez/zR1;
    G4ExtrudedSolid *solframeholex = 
      new G4ExtrudedSolid("penthileinframe",pentagonshape,
			  homeinframedZ/2,G4TwoVector(0,0),sz1,
			  G4TwoVector(0,0),sz1);
    G4SubtractionSolid *solframe = 
      new G4SubtractionSolid("framesol",solframesub1,solframeholex,
			     G4Transform3D(G4RotationMatrix(),
					   G4ThreeVector(0,0,(frameouterrad-frameinnerrad)/2-
							 homeinframedZ/2)));
    G4LogicalVolume *logframe = new G4LogicalVolume(solframe,matCapsule,"framelog",0,0,0);
    G4VisAttributes *visframe = new G4VisAttributes(G4Colour(.2, .2, 1.,.2));
    logframe->SetVisAttributes(visframe);
    //Then put the ones on Iolandas drawing  in there as default...
    for(int detnb=0; detnb<60; detnb++){
      if(std::find(fDontMakeThisFrame.begin(),fDontMakeThisFrame.end(),detnb)!=
	 fDontMakeThisFrame.end()) continue;
      os.str("");
      os << "Frame_" << detnb;
      std::cout << os.str() << std::endl;
      G4RotationMatrix rm;
      rm.rotateZ(-90*deg);
      if(orgamangles[detnb][0]>90) rm.rotateZ(180.*deg);
      rm.rotateY(orgamangles[detnb][0]*deg);
      rm.rotateZ(orgamangles[detnb][1]*deg);
      rm.rotateY(orgamangles[detnb][2]*deg);
      if(detnb<5 || detnb>54) rm.rotateZ(orgamangles[detnb][3]*deg);
      else rm.rotateZ(orgamangles[detnb][3]*deg); 
      //      rm.rotate(72*deg,G4ThreeVector(0,0,1));
      /*phyvol =*/
      new G4PVPlacement(G4Transform3D(rm,
				      rm(G4ThreeVector(0,0,
						       (frameinnerrad+
							frameouterrad)/2+
						       .5*mm))),
			G4String(os.str()),logframe,(theTarget)->HallPhys(),
			false, detnb );
    }    
  }
  if(theAncillary) theAncillary->Placement();
}


G4int OrgamDetector::GetSegmentNumber( G4int i1, G4int i2, G4ThreeVector v1 )
{
  //std::cout << i1 << " " << i2 << "\n";
  //G4cout<<i2<<"  "<<v1<<G4endl;
  G4int pouet = -1;
  if(i1>0 && theAncillary) return theAncillary->GetSegmentNumber(i1,i2,v1 );
  pouet=0;
  return pouet;
}


void OrgamDetector::WriteHeader( std::ofstream &outFileLMD, G4double unit )

{
  outFileLMD << "ORGAM WITH " << fDetAngles.size() 
	     << " DETECTORS nb theta[deg] phi[deg] \n"; 
  std::map<int,G4ThreeVector>::iterator itorgamdet = fDetAngles.begin();
  for(;itorgamdet!=fDetAngles.end(); ++itorgamdet){
    outFileLMD << std::setiosflags(ios::fixed) << std::setprecision(2)
	       << std::setw(10) << itorgamdet->first 
	       << std::setw(10) << itorgamdet->second.theta()/deg << " " 
	       << std::setw(10) 
	       << (itorgamdet->second.phi()/deg>0 ? 
		   itorgamdet->second.phi()/deg : 
		   itorgamdet->second.phi()/deg+360.) 
	       <<  "\n";  
  }
  outFileLMD << " Orgam det coded as det number\n";
  outFileLMD << " BGO shield coded as 300+det number\n";
  outFileLMD << "END ORGAM\n";
  if(theAncillary) theAncillary->WriteHeader(outFileLMD,unit);
}

void OrgamDetector::WriteHeader(G4String *sheader, G4double unit)
{
// Todo: This function needs to be checked by Orgam developpers:
 char line[128];

   sprintf(line, "ORGAM WITH %f DETECTORS nb theta[deg] phi[deg] \n", fDetAngles.size());
   sheader[0] += G4String(line);

  std::map<int,G4ThreeVector>::iterator itorgamdet = fDetAngles.begin();
  for(;itorgamdet!=fDetAngles.end(); ++itorgamdet){
    sprintf(line, "HpGe %3d %4.2f %4.2f \n",itorgamdet->first, itorgamdet->second.theta()/deg, 
	    (itorgamdet->second.phi()/deg>0 ? itorgamdet->second.phi()/deg : itorgamdet->second.phi()/deg+360.) );
    sheader[1] += G4String(line);
  }

  sheader[1] += " Orgam det coded as det number\n";
  sheader[1] += " BGO shield coded as 300+det number\n";
  sheader[1] += " END ORGAM\n";

  if(theAncillary) theAncillary->WriteHeader( sheader, unit );
}

void OrgamDetector::AddOrgamDetectors(G4String detectorstring)

{
  std::istringstream decode(detectorstring);
  int orgamnb;
  fUsedOrgamDetectors.clear();
  while(decode>>orgamnb){
    if(((orgamnb>=0 && orgamnb<60) || (orgamnb>=300 && orgamnb<360)) &&
       std::find(fDontMakeThisFrame.begin(),fDontMakeThisFrame.end(),
		 orgamnb>=300 ? orgamnb-300:orgamnb)==fDontMakeThisFrame.end()) 
      fUsedOrgamDetectors.insert(orgamnb);
  }
}

void OrgamDetector::AddPhaseIDetector(G4String detectorstring)

{
  std::istringstream decode(detectorstring);
  PlacementOfAPhaseI aphi;
  decode >> aphi.x >> aphi.y >> aphi.z >> aphi.phi 
	 >> aphi.theta >> aphi.phi >> aphi.id;
  aphi.x*=mm;aphi.y*=mm;aphi.z*=mm;
  aphi.phi*=deg;aphi.theta*=deg;aphi.psi*=deg;
  fExtraPhaseIs.push_back(aphi);
  std::cout << aphi.x << " " << aphi.y << " " << aphi.z << " " 
	    << aphi.phi << " " 
	    << aphi.theta << " " << aphi.phi << " " << aphi.id << std::endl;
}


void OrgamDetector::AddFrameNotToBuild(G4String frames)

{
  std::istringstream decode(frames);
  int framenb;
  while(decode>>framenb){
    if((framenb>=0 && framenb<60)) 
      fDontMakeThisFrame.insert(framenb);
  }
}

void OrgamDetector::AddFrameToBuild(G4String frames)

{
  std::istringstream decode(frames);
  int framenb;
  while(decode>>framenb){
    if((framenb>=0 && framenb<60)){
      std::set<int>::iterator toremove = std::find(fDontMakeThisFrame.begin(),fDontMakeThisFrame.end(),framenb);
      if(toremove!=fDontMakeThisFrame.end()) fDontMakeThisFrame.erase(toremove);
    } 
  }
}
////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"

#include "G4UImanager.hh"

OrgamDetectorMessenger::OrgamDetectorMessenger(OrgamDetector* pTarget)
  :myTarget(pTarget)
{ 

  SetUsedGePositionCmd = new G4UIcmdWithAString("/Orgam/detector/setUsedDetectors",this);  
  SetUsedGePositionCmd->SetGuidance("Give positions used in frame (add 300 to include BGO shield):");
  SetUsedGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetFreeGePositionCmd = new G4UIcmdWithAString("/Orgam/detector/setFreeDetector",this);  
  SetFreeGePositionCmd->SetGuidance("Give positions of \"free\" phase 1 (x y z phi[z] theta[x'] psi[z''] Id [mm] [deg])");
  SetFreeGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ClearFreeGePositionCmd = new G4UIcmdWithoutParameter("/Orgam/detector/ClearFreeDetectors",this);  
  ClearFreeGePositionCmd->SetGuidance("Clears list of extra PhaseIs");
  ClearFreeGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ClearOrgamGePositionCmd = new G4UIcmdWithoutParameter("/Orgam/detector/ClearOrgamDetectors",this);  
  ClearOrgamGePositionCmd->SetGuidance("Clears list of fixed pos. PhaseIs");
  ClearOrgamGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetMakeChamberCmd = new G4UIcmdWithABool("/Orgam/detector/SetMakeChamber",this);
  SetMakeChamberCmd->SetGuidance("Make Orgam Plunger chamber or not (true/false)");
  SetMakeChamberCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  
  SetMakeFrameCmd = new G4UIcmdWithABool("/Orgam/detector/SetMakeFrame",this);
  SetMakeFrameCmd->SetGuidance("Make Orgam frame or not (true/false)");
  SetMakeFrameCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  AddFrameNotToAdd=new G4UIcmdWithAString("/Orgam/detector/AddFrameNotToBuild",this);
  AddFrameNotToAdd->SetGuidance("Remove frame positions from Orgam (give list of positions)");
  AddFrameNotToAdd->AvailableForStates(G4State_PreInit,G4State_Idle);
  AddFrameToAdd=new G4UIcmdWithAString("/Orgam/detector/AddFrameToBuild",this);
  AddFrameToAdd->SetGuidance("Add frame positions to Orgam (give list of positions)");
  AddFrameToAdd->AvailableForStates(G4State_PreInit,G4State_Idle);

  G4String at = myTarget->GetAnc();
  if(at.find("19")!=std::string::npos){//Check if miniball is there
    AddMiniballCluster =new G4UIcmdWithAString("/Orgam/detector/AddMiniballCluster",this);
    AddMiniballCluster->SetGuidance("Add a miniball cluster: Orgampos R psi (rotation around cluster axis)");
    AddMiniballCluster->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
  
}

OrgamDetectorMessenger::~OrgamDetectorMessenger()
{
  G4cout << "### ~OrgamDetectorMessenger "<< G4endl;
  delete SetUsedGePositionCmd;
}

void OrgamDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetUsedGePositionCmd ) {
    myTarget->AddOrgamDetectors(newValue);
  }
  if( command == SetFreeGePositionCmd ) {
    myTarget->AddPhaseIDetector(newValue);
  }
  if(command==ClearFreeGePositionCmd){
    myTarget->ClearPhaseIDetectors();
  }
  if(command==ClearOrgamGePositionCmd){
    myTarget->ClearOrgamPhaseIDetectors();
  }
  if(command==SetMakeFrameCmd){
    myTarget->SetMakeFrame(SetMakeFrameCmd->GetNewBoolValue(newValue));
  }
  if(command==SetMakeChamberCmd){
    myTarget->SetMakeChamber(SetMakeChamberCmd->GetNewBoolValue(newValue));
  }
  if(command==AddFrameNotToAdd){
    myTarget->AddFrameNotToBuild(newValue);
  }
  if(command==AddFrameToAdd){
    myTarget->AddFrameToBuild(newValue);
  }
  if(command==AddMiniballCluster){
    //So here we will make a call to the miniball class with the correct string...
    //  MiniballAddDet = new G4UIcmdWithAString("/Orgam/detector/ancillary/Miniball/AddACluster",this);
    // MiniballAddDet->SetGuidance("Add a cluster at r theta phi psi eta gamma [mm deg]");
    //Decode
    std::map<int,G4ThreeVector> fAllDetAngles = myTarget->GetAllDetAngles();
    std::istringstream ins(newValue);
    int detnb; ins >> detnb;
    G4double r; ins >> r;
    G4double psi; ins >> psi;
    G4double theta,phi;
    theta  = fAllDetAngles[detnb].theta()/deg;
    phi = (fAllDetAngles[detnb].phi()/deg>0 ? 
	   fAllDetAngles[detnb].phi()/deg : 
	   fAllDetAngles[detnb].phi()/deg+360.);
    std::ostringstream Command;  
    Command << "/Orgam/detector/ancillary/Miniball/AddACluster " << r << " " << theta << " " << phi 
	    << " " << psi << " " << theta-180 << " " << phi; 
    std::cout << Command.str() << std::endl;
    G4UImanager * UI = G4UImanager::GetUIpointer();
    UI->ApplyCommand(Command.str());

  }
}

