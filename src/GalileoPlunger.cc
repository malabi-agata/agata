#ifdef ANCIL
#include "GalileoPlunger.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

GalileoPlunger::GalileoPlunger(G4String path, G4String name )
{
    // dummy assignment needed for compatibility with other implementations of this class
    G4String iniPath    = path;    
    dirName             = name;
    
    nam_vacuum          = "Vacuum";
    mat_vacuum          = NULL;
    
    nam_aluminium       = "Aluminum";
    mat_aluminium       = NULL;
    if(!getenv("G4GDMLPATH")){
        G4cerr << "Error! GDML Path is missing and I need it to construct GALILEO Plunger: please define G4GDMLPATH" << G4endl;
        std::exit(10);
    }
    gdml_path        = std::string(getenv("G4GDMLPATH"))+G4String("/GALILEO/Plunger/"); 
    ancSD               =NULL;
    ancName             =G4String("GalileoPlunger");
    ancOffset           =20000;
    plunger_distance    = 0.01*mm;
    numAncSd = 0;
    
    pTargetHolder.clear();
    pStopperHolder.clear();
    pPlungerSupport.clear();
    
    myMessenger = new GalileoPlungerMessenger(this,name);
    
}

GalileoPlunger::~GalileoPlunger()
{}

G4Material* FindMatP (G4String Name){
    // search the material by its name
    G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> Galileo uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary Fatima! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int GalileoPlunger::FindMaterials()
{
    mat_vacuum      = FindMatP(nam_vacuum);     if (!mat_vacuum)    return 1;
    mat_aluminium   = FindMatP(nam_aluminium);  if (!mat_aluminium) return 1;
    return 0;
}

void GalileoPlunger::InitSensitiveDetector()
{
    G4int offset = ancOffset;
#ifndef FIXED_OFFSET
    offset = theDetector->GetAncillaryOffset();
#endif
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
    if( !ancSD ) {
        ancSD = new AgataSensitiveDetector(dirName, "/anc/Plunger", "PlungerCollection", offset,/* depth */ 0,/* menu */ false );
        SDman->AddNewDetector( ancSD );
        numAncSd++;
    }
}


void GalileoPlunger::GetDetectorConstruction()
{
    G4RunManager* runManager = G4RunManager::GetRunManager();
    theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
    GetTargetHolder();
    GetStopperHolder();
    GetPlungerMechanics();
    GetPlungerMotor();
    
}


void GalileoPlunger::Placement()
{
    ///////////////////////
    /// construct a shell
    ///////////////////////
    G4RunManager* runManager                = G4RunManager::GetRunManager();
    AgataDetectorConstruction* theTarget  = 
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    std::map<int,G4LogicalVolume*>::iterator it;
    G4RotationMatrix rm ;
    rm.rotateX(-90*deg);
    rm.rotateY(180*deg);
    rm.rotateZ(-55*deg);
    
    
    for(it=pTargetHolder.begin();it!=pTargetHolder.end();it++){
        new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,-plunger_distance,0))), 
                          "GalSupportPlastic",it->second, 
                          theTarget->HallPhys(), false, 0);
    }
    for(it=pStopperHolder.begin();it!=pStopperHolder.end();it++){
        new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,0))), 
                          "GalSupportPlastic",it->second, 
                          theTarget->HallPhys(), false, 0);
    }
    for(it=pPlungerMotor.begin();it!=pPlungerMotor.end();it++){
        new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,0))), 
                          "GalSupportPlastic",it->second, 
                          theTarget->HallPhys(), false, 0);
    }
    rm = G4RotationMatrix::IDENTITY;
    rm.rotateX(-90*deg);
    rm.rotateZ(-90*deg);
    for(it=pPlungerSupport.begin();it!=pPlungerSupport.end();it++){
        new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,0))), 
                          "GalSupportPlastic",it->second, 
                          theTarget->HallPhys(), false, 0);
    }
    return;
}


G4bool GalileoPlunger::GetPlungerMechanics()
{
    G4bool status = false;
    string gdml_file = "plunger_support.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Plunger Support GDML file: please check file "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    check_file.close();
    status=true;
    gdmlparser.Read((gdml_path+gdml_file).c_str());
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    for(int j=0;j<10;++j){
        el_name.str("");name.str("");
        el_name << "plunger_support_" << j << "_vol";
        //    std::cout << el_name.str().c_str() << std::endl;
        name << "Plunger_support_" << j ;
        if(pPlungerSupport.find(j)==pPlungerSupport.end()){
            pPlungerSupport[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_aluminium,"Support",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(0.85,0.85,0.85));
            pDetVTC->SetForceWireframe (false);
            pPlungerSupport[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}


G4bool GalileoPlunger::GetStopperHolder()
{
    G4bool status = false;
    string gdml_file = "plunger_stopper.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open GALILEO Plunger stopper GDML file: Please check path "
        << (gdml_path+gdml_file).c_str()<<  G4endl;
        return status;
        std::exit(10);
    }
    check_file.close();
    status=true;
    gdmlparser.Read((gdml_path+gdml_file));
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    for(int j=0;j<15;++j){
        el_name.str("");name.str("");
        el_name << "plunger_stopper_" << j << "_vol";
        //    std::cout << el_name.str().c_str() << std::endl;
        name << "Plunger_stopper_holder_" << j ;
        if(pStopperHolder.find(j)==pStopperHolder.end()){
            pStopperHolder[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_aluminium,"StopperSupport",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(0.25-0.01*j, 0.5+0.01*j, 1-0.01*j) );
            pDetVTC->SetForceWireframe (false);
            pStopperHolder[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}


G4bool GalileoPlunger::GetPlungerMotor()
{
    G4bool status = false;
    string gdml_file = "plunger_motor.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open LNL Plunger Motor GDML file: please check file: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    check_file.close();
    status=true;
    gdmlparser.Read((gdml_path+gdml_file).c_str());
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    for(int j=0;j<21;++j){
        el_name.str("");name.str("");
        el_name << "plunger_motor_" << j << "_vol";
        //    std::cout << el_name.str().c_str() << std::endl;
        name << "Plunger_motor_" << j ;
        if(pPlungerMotor.find(j)==pPlungerMotor.end()){
            pPlungerMotor[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_aluminium,"PlungerMotor",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(0.25-0.01*j, 0.8+0.01*j, 1-0.01*j) );
            pDetVTC->SetForceWireframe (false);
            pPlungerMotor[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}


G4bool GalileoPlunger::GetTargetHolder()
{
    G4bool status = false;
    string gdml_file = "plunger_target.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Plunger target GDML file: please check path: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    status=true;
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file).c_str());
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    for(int j=0;j<22;++j){
        el_name.str("");name.str("");
        el_name << "plunger_target_" << j << "_vol";
        //    std::cout << el_name.str().c_str() << std::endl;
        name << "Plunger_target_holder_" << j ;
        if(pTargetHolder.find(j)==pTargetHolder.end()){
            pTargetHolder[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_aluminium,"TargetSupport",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(0.5-0.01*j, 0.5+0.01*j, 0.1+0.01*j) );
            pDetVTC->SetForceWireframe (false);
            pTargetHolder[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}

void GalileoPlunger::ShowStatus()
{
    G4cout << " Galileo Plunger has been constructed." << G4endl;
    G4cout << "     Plunger Material is " << mat_aluminium->GetName() << G4endl;
}

void GalileoPlunger::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void GalileoPlunger::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{
}

void GalileoPlunger::SetPlungerDistance(G4double value)
{
    if(value<0 || value > 1.5*cm){
        G4cout << "Warning!! Your value is not permitted. The distance is set to 10 micron by default!" << G4endl;
    }else{
        plunger_distance = value;
    } 
}
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

GalileoPlungerMessenger::GalileoPlungerMessenger(GalileoPlunger* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 
    G4String commandName;
    G4String directoryName = G4String("/Galileo/detector/ancillary/GalileoPlunger/");
    myDirectory = new G4UIdirectory(directoryName);
    myDirectory->SetGuidance("Control of Galileo Plunger construction.");
    commandName = directoryName + "SetPlungerDistance";
    SetGalileoPlungerDistanceCmd = new G4UIcmdWithADoubleAndUnit(commandName, this);  
    SetGalileoPlungerDistanceCmd->SetGuidance("Define position of the target with respect to the stopper");
    SetGalileoPlungerDistanceCmd->SetGuidance("Required parameters: double and unit");
    SetGalileoPlungerDistanceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

GalileoPlungerMessenger::~GalileoPlungerMessenger()
{
    delete myDirectory;
    delete SetGalileoPlungerDistanceCmd;
}

void GalileoPlungerMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
    if(command == SetGalileoPlungerDistanceCmd){
        myTarget->SetPlungerDistance(SetGalileoPlungerDistanceCmd->GetNewDoubleValue(newValue));
    }
}



#endif
