#ifdef ANCIL
#include "AgataAncillaryParis.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4Polyhedra.hh"

AgataAncillaryParis::AgataAncillaryParis(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath    = path;
    
  dirName             = name;
    
  nam_vacuum          = "Vacuum";
  mat_vacuum          = NULL;
    
  nam_aluminium       = "Aluminum";
  mat_aluminium       = NULL;
    
  nam_labr3           = "LaBr3";
  mat_labr3           = NULL;
    
  nam_nai		= "NaI";
  mat_nai		= NULL;

  nam_quartz          = "Quarz";
  mat_quartz          = NULL;

    
  ancSD               = NULL;
  ancName             = G4String("PARIS");
  ancOffset           = 19000;
    
  numAncSd            = 0;
}

AgataAncillaryParis::~AgataAncillaryParis()
{}

G4Material* FindMat (G4String Name){
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(Name);
  if (ptMaterial) {
    G4String nome = ptMaterial->GetName();
    G4cout << "----> PARIS uses material " << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << Name << G4endl;
    G4cout << " Could not build the ancillary PARIS! " << G4endl;
    return NULL;
  }
  return ptMaterial;
}

G4int AgataAncillaryParis::FindMaterials()
{
  mat_vacuum      = FindMat(nam_vacuum);     if (!mat_vacuum)    return 1;
  mat_aluminium   = FindMat(nam_aluminium);  if (!mat_aluminium) return 1;
  mat_labr3       = FindMat(nam_labr3);      if (!mat_labr3)     return 1;
  mat_nai         = FindMat(nam_nai);        if (!mat_nai)       return 1;
  mat_quartz      = FindMat(nam_quartz);     if (!mat_quartz)    return 1;
  return 0;
}

void AgataAncillaryParis::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
  if( !ancSD ) {
    ancSD = new AgataSensitiveDetector(dirName, "/anc/Paris", "ParisCollection", offset,/* depth */ 0,/* menu */ false );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }
}


void AgataAncillaryParis::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryParis::Placement()
{

    
  //visualization
  G4Colour red(1.,0.,0.);			 
  G4Colour blue(0.,0.,1.);
  G4Colour white(1.,1.,1.);
  G4VisAttributes attred(red);        	
  G4VisAttributes attblue(blue);
  G4VisAttributes attwhite(white);


  //Detector construction and placement

  //single  phoswich dimensions
  G4double cellX =  56.*mm; 
  G4double cellY =  56.*mm; 
  G4double cellZ = 226.*mm; 
  G4Box* parcell = new G4Box("parcell", cellX/2., cellY/2., cellZ/2. );	 
  G4LogicalVolume* parcell_log[4][9];

  //cluster dimensions
  G4double clustX =  56.*3.*mm; 
  G4double clustY =  56.*3.*mm; 
  G4double clustZ =  226.*mm; 
  G4Box* cluster_box = new G4Box("cluster_box", clustX/2., clustY/2., clustZ/2. ); 
  G4LogicalVolume* cluster_log[4];




  //single LaBr crystal dimensions
  G4double brX = 52.*mm; 
  G4double brY = 52.*mm; 
  G4double brZ = 50.8*mm; 
  G4Box* brbox = new G4Box("brbox", brX/2., brY/2., brZ/2. ); 

 
  //single NaI z dimension, x,y, the same as LaBr
  G4double naiZ = 152.4*mm;	 
  G4Box* naibox = new G4Box("naibox", brX/2., brY/2., naiZ/2. ); 

  //logical and physical volumes used
  G4LogicalVolume *br_log[4][9];
  G4LogicalVolume *nai_log[4][9];
  //  G4VPhysicalVolume *LaBr[4][9];
  // G4VPhysicalVolume *NaIcr[4][9];
  // G4VPhysicalVolume *detector[4][9];
  // G4VPhysicalVolume *cluster[9]; 

  char name_labr[200];
  char name_nai[200];

  // placing clusters
  // distance is 233 mm from the center, hardcoded 
  for(int i=0; i<4; ++i) 
    {
      G4ThreeVector pos = G4ThreeVector (0.,0.,0.);
      G4RotationMatrix rotateIT;
      if ( i == 0  ){
	pos = G4ThreeVector((233. + clustZ/2)*mm, 0.0*mm, 0.0*mm);
	rotateIT.rotateY(-90*deg);	// for 1st cluster (+X) 
      }
      else if ( i == 1 ){
	pos = G4ThreeVector(0.0*mm, (233. + clustZ/2)*mm, 0.0*mm);	
	rotateIT.rotateX(90*deg);	// for 2nd claster (+Y)
      }
      else if ( i == 2 ){
	pos = G4ThreeVector(-(233. + clustZ/2)*mm, 0.0*mm, 0.0*mm);
	rotateIT.rotateY(90*deg);	// for 3th clastr (-X) 
      }
      else if ( i ==3 ){
	pos = G4ThreeVector(0.0*mm, -(233. + clustZ/2)*mm, 0.0*mm);
	rotateIT.rotateX(-90*deg);	// for 4th claster (-Y)
      }


      cluster_log[i] = new G4LogicalVolume (cluster_box, mat_vacuum, "cluster_log"); 
      cluster_log[i]->SetVisAttributes(G4VisAttributes::Invisible); 
      /*cluster[i] =*/
      new G4PVPlacement(G4Transform3D(rotateIT, pos), "cluster",
			cluster_log[i], 
			theDetector->HallPhys(), true, i); 
      //detectors inside cluster
      for(int l=0; l<9; ++l) 
	{

	  parcell_log[i][l] = new G4LogicalVolume (parcell, mat_vacuum, "parcell_log"); 
	  parcell_log[i][l]->SetVisAttributes(G4VisAttributes::Invisible); 

	  G4ThreeVector detpos = AgataAncillaryParis::GetDetPos(l); 
	  G4RotationMatrix unrotated;
	  /*detector[i][l] =*/
	  new G4PVPlacement(G4Transform3D( unrotated, detpos),
			    parcell_log[i][l], "detector", cluster_log[i],
			    true, i*9+ l); 

	  //labr and nai crystals
	  sprintf(name_labr, "LaBr%d_%d", i, l);
	  sprintf(name_nai,  "NaI%d_%d", i, l);

	  br_log[i][l] = new G4LogicalVolume (brbox, mat_labr3, "br_log"); 
	  br_log[i][l]  -> SetVisAttributes(attred); 
	  nai_log[i][l] = new G4LogicalVolume (naibox, mat_nai, "nai_log"); 
	  nai_log[i][l]   -> SetVisAttributes(attblue); 

	  /*LaBr[i][l] =*/
	  new G4PVPlacement (0, G4ThreeVector(.0, .0, (cellZ/2.-2.-brZ/2.)*mm),
			     br_log[i][l], name_labr, parcell_log[i][l],
			     false, i*9+ l); 
	  /*NaIcr[i][l] =*/
	  new G4PVPlacement (0, G4ThreeVector(.0, .0,
					      (cellZ/2.-2.-brZ-naiZ/2.)*mm),
			     nai_log[i][l], name_nai, parcell_log[i][l],
			     false, i*9 + l+300);


	  br_log[i][l] -> SetSensitiveDetector(ancSD);
	  nai_log[i][l]-> SetSensitiveDetector(ancSD);

	  G4double i_wall[]  = {   0.0*mm,  27.5*mm,  27.5*mm,   27.5*mm,   28.0*mm };
	  G4double o_wall[]  = {  28.0*mm,  28.0*mm,  28.0*mm,   28.0*mm,   28.0*mm };
	  G4double z_wall[]  = { 113.0*mm, 112.5*mm, 112.5*mm, -113.0*mm, -113.0*mm };	
	  G4Polyhedra* p_wall = new G4Polyhedra ("p_wall", 45.0*degree,	360.0*degree, 4, 5,	
						 z_wall, i_wall, o_wall ); 
	  G4LogicalVolume* wall_log = new G4LogicalVolume (p_wall, mat_aluminium, "wall_log"); 
	  wall_log -> SetVisAttributes(attwhite); 
	  new G4PVPlacement (0, G4ThreeVector(.0, .0, .0*mm),
			     wall_log, "wall", parcell_log[i][l], false, 0);
	  
	}
    }
  return;
}

void AgataAncillaryParis::ShowStatus()
{
  G4cout << " ANCILLARY PARIS has been constructed." << G4endl;
  G4cout << "     Ancillary Vacuum      material is " << mat_vacuum->GetName() << G4endl;
  G4cout << "     Ancillary Scintilator material are " << mat_labr3->GetName() << " and " << mat_nai->GetName() << G4endl;
  G4cout << "     Ancillary Shielding   material is " << mat_aluminium->GetName() << G4endl;
}

void AgataAncillaryParis::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
void AgataAncillaryParis::WriteHeader(G4String */*sheader*/, G4double /*unit*/)
{}


G4ThreeVector AgataAncillaryParis::GetDetPos(int nb)
{
  const G4double detector_pos[9][3] = 
    {
      //		  X		  Y		  Z   

      { 	  0.,		  0.,		  0. }, 	// #1	center-center 
      { 	  0., 		 56., 		  0. }, 	// #2	up-center 
      { 	 56., 		 56., 		  0. }, 	// #3	up-right 
      {	 56., 		  0., 		  0. }, 	// #4	center-right 
      {	 56., 		-56., 		  0. }, 	// #5	down-right 
      {	  0., 		-56., 		  0. },		// #6	down-center 
      {	-56., 		-56., 		  0. },		// #7	down-left 
      {	-56., 		  0., 		  0. }, 	// #8	center-left 
      {	-56., 		 56., 		  0. },		// #9	up-left 
    }; 

  if( nb<0 || nb>=9 ) 
    G4cout << "!!! detector number (inside the cluster) out of range (should be in [0,9])" ; 
    
  const G4double* det = detector_pos[nb]; 
  return G4ThreeVector( det[0]*mm, det[1]*mm, det[2]*mm ); 
}

#endif
