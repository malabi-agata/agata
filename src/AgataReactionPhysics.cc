#include "AgataReactionPhysics.hh"


#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"

#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"
#include "G4GenericIon.hh"
#include "G4Proton.hh"
#include "G4Deuteron.hh"
#include "G4Triton.hh"
#include "G4He3.hh"
#include "G4Alpha.hh"


#include "G4PhysicsListHelper.hh"
#include "G4BuilderType.hh"

AgataReactionPhysics::AgataReactionPhysics(const G4String& name, G4int ver)
  :  G4VPhysicsConstructor(name), verbose(ver), wasActivated(false)
{}

AgataReactionPhysics::~AgataReactionPhysics()
{
  //if(wasActivated) delete fReactionProcess;
  delete fReactionProcess;
}


void AgataReactionPhysics::ConstructProcess()
//void AgataPhysicsList::ConstructHadronsEMStandard()
{

  //#ifndef G4MULTITHREADED
  if(wasActivated) return;
  wasActivated = true; 
  //#else
  //Do nothing for now but this has to be checked. One may have to find an alternative
  //#endif

  // Add Reaction Process
  fReactionProcess = new Reaction(BeamOut,BeamIn);
  fLimiterProcess = new G4StepLimiter();

#ifndef G4V10
  theParticleIterator->reset();
  //  G4ParticleDefinition* particle=0;
  //  G4ProcessManager* pmanager=0;
  G4GenericIon* ion = G4GenericIon::GenericIon();
  G4ProcessManager* pmanager = ion->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
  pmanager->AddProcess(fLimiterProcess, -1,-1, 4);

  G4Alpha *alpha = G4Alpha::Alpha();
  pmanager = alpha->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
  pmanager->AddProcess(fLimiterProcess, -1,-1, 4);

  G4He3 *he3 = G4He3::He3();
  pmanager = he3->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
  pmanager->AddProcess(fLimiterProcess, -1,-1, 4);

  G4Proton *proton = G4Proton::Proton();
  pmanager = proton->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
  pmanager->AddProcess(fLimiterProcess, -1,-1, 4);

  G4Deuteron *deutron = G4Deuteron::Deuteron();
  pmanager = deutron->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
  pmanager->AddProcess(fLimiterProcess, -1,-1, 4);
#else

 G4GenericIon* ion = G4GenericIon::GenericIon();
 
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  ph->RegisterProcess(fLimiterProcess, ion);
  //ph->RegisterProcess(fReactionProcess, ion); 
  // The line above makes the program crashes so instead we use:
  G4ProcessManager* pmanager = ion->GetProcessManager();
  pmanager->AddProcess(fReactionProcess,-1,-1, 3);
   
#endif



}

void AgataReactionPhysics::SetOutgoingBeam(Outgoing_Beam* bo)
{
  BeamOut = bo; 
}

void AgataReactionPhysics::SetIncomingBeam(Incoming_Beam* bi)
{
  BeamIn = bi; 
}
