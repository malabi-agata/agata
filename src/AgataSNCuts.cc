

#include "AgataSNCuts.hh"
#include "G4VParticleChange.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4Ions.hh"
#include "G4LogicalVolume.hh"

#ifdef G4V10
using namespace CLHEP;
#endif

G4double AgataSNCuts::fEnergyLimitCSN=1.*MeV;
G4double AgataSNCuts::fEnergyLimitHI=1.*MeV;
G4double AgataSNCuts::fEnergyLimitHIS=.1*MeV;
bool AgataSNCuts::KillBeforeDeg=false;
bool AgataSNCuts::KillAfterDeg=false;
AgataSNCutsMessenger*    AgataSNCuts::myMessenger=0;

AgataSNCuts::AgataSNCuts(const G4String& aName)
  : G4VProcess(aName)
{
  if (verboseLevel>1) {
     G4cout << GetProcessName() << " is created "<< G4endl;
   }
   if(myMessenger==0) 
     myMessenger = new AgataSNCutsMessenger(this,"/Agata/ScreenedElastic");
}

AgataSNCuts::~AgataSNCuts() 
{                                     
}                                     

G4VParticleChange* AgataSNCuts::PostStepDoIt(
			     const G4Track& aTrack,
			     const G4Step& 
			    )
//
// Stop the current particle, if requested by G4UserLimits 
// 			    			    			    
{
   aParticleChange.Initialize(aTrack);
   aParticleChange.ProposeEnergy(0.) ;
   aParticleChange.ProposeLocalEnergyDeposit(aTrack.GetKineticEnergy()) ;
   aParticleChange.ProposeTrackStatus(fStopButAlive);
   return &aParticleChange;
}

G4double 
AgataSNCuts::PostStepGetPhysicalInteractionLength(const G4Track& track,
						  G4double   previousStepSize,
						  G4ForceCondition* condition)
{
  previousStepSize = previousStepSize; //Get rid of warning
  *condition = NotForced;
  //We can kill gammas emitted before or after degarder start surface...
  //if created in this step
  if((KillBeforeDeg || KillAfterDeg)){
    if(track.GetParticleDefinition()->GetParticleName()=="gamma"){
      if(track.GetStep() && track.GetStep()->GetPreStepPoint()){
	if(!track.GetStep()->GetPreStepPoint()->GetProcessDefinedStep()){
	  //Means first step in track...
	  G4RunManager * runManager = G4RunManager::GetRunManager();
	  theDetector=(AgataDetectorConstruction*) 
	    runManager->GetUserDetectorConstruction();
	  G4ThreeVector degp = theDetector->GetDegraderPosition();
	  G4ThreeVector degthick = theDetector->GetDegraderSize();
	  if(KillBeforeDeg){
	    if(track.GetPosition().z()<(degp.z()-degthick.z()/2)) return 0;
	  } else {
	    if(track.GetPosition().z()>(degp.z()-degthick.z()/2)) return 0;
	  }
	}
      }
    }
  }
  //If gamma everything goes
  if(track.GetParticleDefinition()->GetParticleName()=="gamma") 
    return DBL_MAX;
  //Everything created by ScreenedElastic is stopped if slow
  if(track.GetCreatorProcess()){
    if(track.GetCreatorProcess()->GetProcessName()=="CoulombScat" &&
       track.GetKineticEnergy()/track.GetParticleDefinition()->GetAtomicMass()
       < fEnergyLimitCSN){
      return 0.;
    }
  }
  // In target and degrader everything goes or hall phys
  if(track.GetVolume()){
    if(track.GetVolume()->GetName()){
      if(track.GetVolume()->GetName()=="Target" ||
	 track.GetVolume()->GetName()=="Fronting" ||
	 track.GetVolume()->GetName()=="Backing" ||
	 track.GetVolume()->GetName()=="Degrader" ||
	 track.GetVolume()->GetName()=="hallPhys") 
	return DBL_MAX;
    }
  }
  if(track.GetParticleDefinition()->GetParticleType()=="nucleus"){
    //Here we stop slow heavy ions
    if((track.GetKineticEnergy()/
	track.GetParticleDefinition()->GetAtomicMass())<fEnergyLimitHIS){
      return 0.;
    }
    //If not in a senstive volume stop also "fast" ions 
    const G4Step *astep = track.GetStep();
    const G4StepPoint *steppoint = astep->GetPreStepPoint();
    const G4TouchableHandle touch = steppoint->GetTouchableHandle();
    const G4VPhysicalVolume *volume = touch->GetVolume();
    const G4LogicalVolume *lvolume = volume->GetLogicalVolume();
    if(!lvolume->GetSensitiveDetector()){
      if((track.GetKineticEnergy()/
	  track.GetParticleDefinition()->GetAtomicMass())<fEnergyLimitHI){
	return 0.;
      }
    }
  }
  //Kill electrons in the gas detector
  if(track.GetVolume()){
    if(track.GetVolume()->GetName()){
      if(track.GetVolume()->GetName()=="Gasphys"){
	if(track.GetParticleDefinition()->GetParticleName()=="e-")
	  return 0;
      }
    }
  }
  return DBL_MAX;
}



#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

AgataSNCutsMessenger::AgataSNCutsMessenger(AgataSNCuts* mycut, G4String name)
  :myCuts(mycut)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of special cuts used with AgataDetectorTest.");

  commandName = directoryName + "SetEnergyLimitCreatedByScreenedElastic";
  aLine = commandName.c_str();
  cmdSetCSN= new   G4UIcmdWithADoubleAndUnit(aLine,this);
  cmdSetCSN->AvailableForStates(G4State_PreInit,G4State_Idle);
  cmdSetCSN->SetGuidance("For energies lower than this all particles created by ScreenedElastic are stopped, give in MeV/u");
  commandName = directoryName + "SetEnergyLimitHeavyIons";
  aLine = commandName.c_str();
  cmdSetHI= new   G4UIcmdWithADoubleAndUnit(aLine,this);
  cmdSetHI->AvailableForStates(G4State_PreInit,G4State_Idle);
  cmdSetHI->SetGuidance("All heavy ions with MeV/u lower than this are stopped");
  cmdSetHI->SetGuidance("if not in a sensitive detector volume");
  commandName = directoryName + "SetEnergyLimitHeavyIonsInSensitiveVolume";
  aLine = commandName.c_str();
  cmdSetHIS= new   G4UIcmdWithADoubleAndUnit(aLine,this);
  cmdSetHIS->AvailableForStates(G4State_PreInit,G4State_Idle);
  cmdSetHIS->SetGuidance("Lower energy limit for heavy ions in sensitive");
  cmdSetHIS->SetGuidance("detector. MeV/u");

  commandName = directoryName + "SetKillGammaIfEmittedBeforeDegrader";
  aLine = commandName.c_str();
  cmdkillbeforedegrader= new G4UIcmdWithABool(aLine,this);
  cmdkillbeforedegrader->AvailableForStates(G4State_PreInit,G4State_Idle);
  cmdkillbeforedegrader->
    SetGuidance("Kill gammas if emitted before degrader, bool");
  commandName = directoryName + "SetKillGammaIfEmittedAfterDegrader";
  aLine = commandName.c_str();
  cmdkillafterdegrader = new G4UIcmdWithABool(aLine,this);
  cmdkillafterdegrader->AvailableForStates(G4State_PreInit,G4State_Idle);
  cmdkillafterdegrader->
    SetGuidance("Kill gammas if emitted at and after degrader, bool");
  
}

AgataSNCutsMessenger::~AgataSNCutsMessenger()
{
  delete   myCuts;
  delete   cmdSetHIS;
  delete   cmdSetHI;
  delete   cmdSetCSN;
}

void AgataSNCutsMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command ==  cmdSetCSN) {
    myCuts->
      SetEnergyCutCreatedScreenedElastic(cmdSetCSN->
					 GetNewDoubleValue(newValue));
  }
  if( command ==  cmdSetHI) {
    myCuts->SetEnergyCutHeavyIon(cmdSetHI->GetNewDoubleValue(newValue));
  }
  if( command ==  cmdSetHIS) {
    myCuts->SetEnergyCutHeavyIonSensitive(cmdSetHIS->
					  GetNewDoubleValue(newValue));
  }
  if( command ==  cmdkillbeforedegrader) {
    bool value = cmdkillbeforedegrader->GetNewBoolValue(newValue);
    myCuts->SetKillGammasBefore(value);
  }
  if( command ==  cmdkillafterdegrader) {
    bool value = cmdkillbeforedegrader->GetNewBoolValue(newValue);
    myCuts->SetKillGammasAfter(value);
  }

}
