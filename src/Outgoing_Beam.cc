#include "Outgoing_Beam.hh"
//#include "GammaDecayChannel.hh"
#include "G4DecayTable.hh"
#include "G4Decay.hh"
#include "G4ProcessManager.hh"
#include "G4RunManager.hh"
#include "AgataDetectorConstruction.hh"
#include <iostream>
#include <fstream>
#include <istream>

//G4Decay Outgoing_Beam::decay;      //there for the original version. we do not need any more

#ifdef G4V10
using namespace CLHEP;
#endif

Outgoing_Beam::Outgoing_Beam( G4String name )
{
  DZ=0;
  DA=0;
  Ex=1.*MeV;
  projEx=1.0*MeV;
  TarEx1=511.5*keV;
  TarEx2=500.0*keV;
  TFrac=0.;
  CFrac=1.;
  tau=0.020*ns;
  Ztar =  82;
  Atar = 208;
  reacted=false;
  NQ=1;SQ=0;
  SetUpChargeStates();
  angDistFileName = G4String("aadist");                        
  //angular distribution file taken as default as ./aadist
  theAngDist = new AngDistHandler( angDistFileName );
  evapDistFileName[0] = G4String("evapn.dat");          
  //evap en distribution file taken as default as ./evap[..].dat     
  evapDistFileName[1] = G4String("evapp.dat");                                 
  evapDistFileName[2] = G4String("evapa.dat");                                 
  theEvapDist[0] = new EvapDistHandler( evapDistFileName[0] );
  theEvapDist[1] = new EvapDistHandler( evapDistFileName[1] );
  theEvapDist[2] = new EvapDistHandler( evapDistFileName[2] );
  //gaussian energy cm distribution for each evap. particle
  n_part[0] = 1;                                               
  //if energy distrib. file not specified
  e_part[0] = 2.*MeV;                                          
  s_part[0] = 0.7*MeV;                                
  //0->neutrons   1->protons   2->alphas; 1 neutron as default    
  n_part[1] = 0;                                             
  e_part[1] = 4.*MeV;                                          
  s_part[1] = 0.9*MeV;                                         
  n_part[2] = 0;                                               
  e_part[2] = 6.*MeV;                                          
  s_part[2] = 1*MeV;                                         
  beamIn = NULL;
  ion    = NULL;
  iongs  = NULL;
  iontar = NULL;
  ionpro = NULL;
  iontl  = NULL;
  myMessenger = new Outgoing_Beam_Messenger(this,name);
  for( G4int i=0; i<3; i++ )
    //energy of the evaporated particle NOT taken from file as default
    evapFile[i] = false;                                     
  theSpectrum = new CSpec1D(8192,22);
  /////////////////////////////////////////////////////////////////////
  // energy_distr.dat: packed spectrum, |l:8 format
  // 
  // 
  // 00 --> Beam kinetic energy when reaction occurred (0.1 MeV/ch)
  // 01 --> beam theta (0.1 deg units)
  // 02 --> beam phi   (0.1 deg units)
  // 
  // 03 --> Initial kinetic energy of the reaction product 
  //        (following evaporation of 1n);
  // 04 --> reaction product theta
  // 05 --> reaction product phi
  // 
  // 06 --> neutron CM energy
  // 07 --> neutron CM theta
  // 08 --> neutron CM phi
  // 
  // 09 --> neutron LAB energy 
  // 10 --> neutron LAB theta
  // 11 --> neutron LAB phi
  //
  // 12 --> beam depth at the reaction moment	 			     
  // 13 -->  	 			     
  // 14 --> beta distribution of the emitting nucleus on the beam axis  
  //each reaction process is associated to a number between 0 and 1; 
  //the sum of all the listed p has to be 1
  //ATT: at least for the moment, each of them is supposed to 
  //bring to the same final product
  p_fe  = 1.;   //fusion-evaporation --the only process as default
  p_tr  = 0.;   //transfer
  p_clx = 0.;   //Coulex
  p_ff  = 0.;   //fusion-fission
  p_if = 0.;    //incomplete fusion
  theMatrix = new CSpec2D(8192,8192,1);
  nybar=2;
  sigmanybar=0.5;
  fractionKinE = .83; //Taken from thermal fission of 235U
  phiMin=0;
  phiMax=360.*deg;
  thetaMin=0*deg;
  thetaMax=180*deg;
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)runManager->
    GetUserDetectorConstruction();
  EvapAMaxAngleLab=acos(-1);
  EvapAMinAngleLab=0;
}

Outgoing_Beam::~Outgoing_Beam()
{
  if( theAngDist )
    delete theAngDist;

  for( G4int hh=0; hh<3; hh++ ) {
    if( theEvapDist[hh] )
      delete theEvapDist[hh];
  }

  delete myMessenger;  
 
  delete theSpectrum;
  // delete beamIn;
  if( ion )
    delete ion;
  if( iongs )
    delete iongs;
  if( iontar )
    delete iontar;
  if( ionpro )
    delete ionpro;
  if( iontl )
    delete iontl;
}

void Outgoing_Beam::defaultIncomingIon(Incoming_Beam *bi)
{
  if (bi == NULL) {
    return;
  }
  beamIn = bi;
}

void Outgoing_Beam::setDecayProperties()
{
  theAngDist->ReNew( angDistFileName );
  theEvapDist[0]->ReNew( evapDistFileName[0] );
  theEvapDist[1]->ReNew( evapDistFileName[1] );
  theEvapDist[2]->ReNew( evapDistFileName[2] );  
  //it has to "ReNew" at the beginning of each run...
  if (beamIn == NULL) {
    G4cerr << "Can not set decay properties as incoming beam has not been set" 
	   << G4endl;
    exit(EXIT_FAILURE);
  }
  Zin = beamIn->getZ();
  Ain = beamIn->getA();

#ifdef G4V10
  ionpro = G4IonTable::GetIonTable()->GetIon(Zin,Ain,0.);
#else
  ionpro = G4ParticleTable::GetParticleTable()->GetIon(Zin,Ain,0.);
#endif
  if (ionpro == NULL) {
    G4cerr << "Could not find projectile ion in particle table "
	   << Zin << " " << Ain << G4endl;
    exit(EXIT_FAILURE);
  }

#ifdef G4V10
  iontar = G4IonTable::GetIonTable()->GetIon(Ztar,Atar,0.);
#else
  iontar = G4ParticleTable::GetParticleTable()->GetIon(Ztar,Atar,0.);
#endif
  if (iontar == NULL) {
    G4cerr << "Could not find target ion in particle table "
	   << Ztar << " " << Atar << G4endl;
    exit(EXIT_FAILURE);
  }
  if( Ztar-DZ > 0 ) {
    //used in case of transfer
#ifdef G4V10
    iontl = G4IonTable::GetIonTable()->GetIon(Ztar-DZ,Atar-DA,0.); 
#else
    iontl = G4ParticleTable::GetParticleTable()->GetIon(Ztar-DZ,Atar-DA,0.); 
#endif
     if (iontl == NULL) {
      G4cerr << "Could not find target-like ion in particle table "
    	     << Ztar-DZ << " " << Atar-DA << G4endl;
      exit(EXIT_FAILURE);
    }
  } else {
    iontl = G4ParticleTable::GetParticleTable()->FindParticle("neutron");
  }
  // -- G4ParticleDefinition *ion(iongs) -- //
#ifdef G4V10
  ion = 
    G4IonTable::GetIonTable()->GetIon(Zin+DZ,Ain+DA,Ex);
#else
  ion = 
    G4ParticleTable::GetParticleTable()->GetIon(Zin+DZ,Ain+DA,Ex);
#endif
  G4cout << "---->PDGName for ion is " << ion->GetParticleName() << G4endl;
  G4cout << "---->PDGLifeTime for ion is " 
	 << (ion->GetPDGLifeTime())/(0.001*ns) << " ps" << G4endl;
#ifdef G4V10
  iongs = G4IonTable::GetIonTable()->GetIon(Zin+DZ,Ain+DA,0.);  
#else
  iongs = G4ParticleTable::GetParticleTable()->GetIon(Zin+DZ,Ain+DA,0.);  
#endif
  iongs->SetPDGStable(true);
  if (ion == NULL) {
    G4cerr << "Could not find outgoing ion (projectile-like) in particle table "
	   << Zin + DZ << " " << Ain+DA << G4endl;
    exit(EXIT_FAILURE);
  }
  if (iongs == NULL) {
    G4cerr << "Could not find outgoing ion (projectile-like) "
	   << "g.s. in particle table "
	   << Zin + DZ << " " << Ain+DA << G4endl;
    exit(EXIT_FAILURE);
  }

  ion->SetPDGStable(false);
}

//---------------------------------------------------------
void Outgoing_Beam::ScanInitialConditions(const G4Track & aTrack)
{
 
  dirIn = aTrack.GetMomentumDirection();
  posIn = aTrack.GetPosition();
  pIn   = aTrack.GetMomentum();
  KEIn  = aTrack.GetDynamicParticle()->GetKineticEnergy();
  Ain   = aTrack.GetDynamicParticle()->GetDefinition()->GetAtomicMass();
  Zin   = aTrack.GetDynamicParticle()->GetDefinition()->GetAtomicNumber();
  tauIn = aTrack.GetProperTime();
  beam4p = aTrack.GetDynamicParticle()->Get4Momentum();
  targetrecoil = NULL;
  theSpectrum->Incr((G4int)(KEIn/(0.1*MeV)),0);
  theSpectrum->Incr((G4int)( 4000+posIn.z()/(0.001*0.001*mm) ),12);       
  //4000=offset; otherwise it writes on negative index
  G4double theta = pIn.theta()/deg;
  G4double phi = pIn.phi()/deg;
  if( phi < 0. ) phi += 360.;
  theSpectrum->Incr((G4int)(10.*theta),1);
  theSpectrum->Incr((G4int)(10.*phi),2);
  ReactionFlag = -1;  //not used now ... ??
  if(aTrack.GetVolume()->GetLogicalVolume()->GetName()=="Target")
    ReactionFlag = 0;
  if(aTrack.GetVolume()->GetLogicalVolume()->GetName()=="Degrader")
    ReactionFlag = 1;
}

//---------------------------------------------------------
G4ThreeVector Outgoing_Beam::ReactionPosition()
{
  posOut=posIn;
  posOut.setZ(posIn.getZ()+OBeps);  // #define  eps (0.000001*mm)
  return posOut;
}
//---------------------------------------------------------

void Outgoing_Beam::SelectExcitionEnergy()

{
  if(listEx.size()>0){
    unsigned int which=0;
    G4double rand = totFeeding*G4UniformRand();
    while(listEx[which].second<rand && which<listEx.size()){
      which++;
    }
    if(which>=listEx.size()){
      std::cout << "Error at " << __LINE__ << " in " << __FILE__ << std::endl;
      exit(1);
    }
    Ex = listEx[which].first;
  }
  if(listEx_partner.size()>0){
    unsigned int which=0;
    G4double rand = totFeeding_partner*G4UniformRand();
    while(listEx_partner[which].second<rand && which<listEx_partner.size()){
      which++;
    }
    if(which>=listEx_partner.size()){
      std::cout << "Error at " << __LINE__ << " in " << __FILE__ << std::endl;
      exit(1);
    }
    Ex_partner = listEx_partner[which].first;
  }
}


std::vector<G4DynamicParticle*> Outgoing_Beam::ReactionProduct()
{
  std::vector<G4DynamicParticle*> aReactionProduct;
  SelectExcitionEnergy();
#ifdef G4V10
  ion=G4IonTable::GetIonTable()->GetIon(Zin+DZ,Ain+DA,Ex); 
#else
  ion=G4ParticleTable::GetParticleTable()->GetIon(Zin+DZ,Ain+DA,Ex); 
#endif
  targetrecoil=0;
  G4ThreeVector pp = GetOutgoingMomentum();
  ion->SetPDGStable(false);
  aReactionProduct.push_back(new G4DynamicParticle(ion,pp));
  if(targetrecoil){
    targetrecoil->SetPDGStable(false);
    aReactionProduct.push_back(new G4DynamicParticle(targetrecoil,
						     ptargetrecoil));
  }
  return aReactionProduct;
}
//---------------------------------------------------------
G4DynamicParticle* Outgoing_Beam::ProjectileGS()              //????
{
  G4ThreeVector momentum = GetOutgoingMomentum();
  G4DynamicParticle* aReactionProduct =new G4DynamicParticle(iongs,momentum);
  
  //  aReactionProduct->SetProperTime(tauIn);
  // G4cout<<" Proper time set to "<<aReactionProduct->GetProperTime()<<G4endl;

  return aReactionProduct;
}
//---------------------------------------------------------
G4DynamicParticle* Outgoing_Beam::TargetExcitation()            //????
{
  particleTable = G4ParticleTable::GetParticleTable();
  G4DynamicParticle* aReactionProduct = 
    new G4DynamicParticle(particleTable->FindParticle("gamma"),
			  TargetAngularDistribution(),GetTargetExcitation());
  return aReactionProduct;
}
//---------------------------------------------------------
G4double Outgoing_Beam::GetTargetExcitation()           //????
{
  if( G4UniformRand() < CFrac ) 
    return TarEx1;
  else
    return TarEx2;	
}
//---------------------------------------------------------
G4ThreeVector Outgoing_Beam::TargetAngularDistribution()        //????
{
  return G4RandomDirection();
}

//---------------------------------------------------------
G4ThreeVector Outgoing_Beam::GetOutgoingMomentum()  
{
  G4ThreeVector momentum = pIn;
  if(RecoilData){
    //Find a line to use
    int line = static_cast<int>(RecoilData->size()*G4UniformRand());
    G4double massI  = ion->GetPDGMass();
    double p = massI*(*RecoilData)[line].v/sqrt(1-(*RecoilData)[line].v*
						(*RecoilData)[line].v);
    momentum[0]=p*(*RecoilData)[line].vx;
    momentum[1]=p*(*RecoilData)[line].vy;
    momentum[2]=p*(*RecoilData)[line].vz;
    
  } else {
    G4double dummy_sum = p_fe + p_tr + p_clx + p_ff + p_if;
    if( dummy_sum !=1. )
      G4cout << "Warning: cumulative probability of " 
	     << "the reaction processes diff. from 1 !!!! " 
	     << dummy_sum << G4endl;
    G4double pp_tr  =  p_fe+p_tr;
    G4double pp_clx =  p_fe+p_tr+p_clx;
    G4double pp_ff  =  p_fe+p_tr+p_clx+p_ff;
    G4double pp_if  =  p_fe+p_tr+p_clx+p_ff+p_if;
    bool anglenotok = false;
    int maxtries=1000;
    G4double seed = 1.0*G4UniformRand();
    if( !(seed > p_fe) ) 
      momentum = GetOutgoingMomentumFE();
    else if( !(seed > pp_tr) ){
      do{
	momentum = GetOutgoingMomentumTR();
	G4ThreeVector tmp = momentum;
	if(fabs(atan(tmp.x()/tmp.z()))<theDetector->GetSpecOpeningTheta() && 
	   fabs(atan(tmp.y()/tmp.z()))<theDetector->GetSpecOpeningPhi()){
	  anglenotok=false;
	} else  anglenotok=true;
	maxtries--;
      } while(theDetector->GetBuildSpectrometer()&&anglenotok&&maxtries>0);
    } else if( !(seed > pp_clx) )
      momentum = GetOutgoingMomentumCLX();
    else if( !(seed > pp_ff) ){
      momentum = GetOutgoingMomentumFF(); 
    }
    else if( !(seed > pp_if) )
      momentum = GetOutgoingMomentumIF(); 
    else 
      G4cout << "Att: all possible reaction mechanism has 0 probability;" << 
	" momentum equal to the incoming one!!" << G4endl;
  }
  if(listEx_partner.size()>0 && !targetrecoil){
    //We have to create the partner as well. We do this from conservation of
    //four momentum. 
#ifdef G4V10
    targetrecoil=G4IonTable::GetIonTable()->GetIon(Ztar-DZ,Atar-DA,Ex_partner); 
#else
    targetrecoil=G4ParticleTable::GetParticleTable()->
      GetIon(Ztar-DZ,Atar-DA,Ex_partner); 
#endif
    G4double massTar = targetrecoil->GetPDGMass();
    G4LorentzVector prod4p(momentum,sqrt(momentum.mag2()+massTar*massTar));
    G4LorentzVector tar4p = (beam4p+
			     G4LorentzVector(G4ThreeVector(0,0,0),
					     iontar->GetPDGMass()))-
      prod4p;
    ptargetrecoil = tar4p.vect();    
  }
  return momentum;
}

G4ThreeVector Outgoing_Beam::GetOutgoingMomentumFE()
{
  //--- ligth particles masses ---//
  G4double massPart[3];
  massPart[0] =  939.565*MeV; //neutron
  massPart[1] =  938.272*MeV; //proton
  massPart[2] = 3727.379*MeV; //alpha
  //--- nuclear masses ---//
#ifdef  _INCSPEC_
  G4double massR = iongs ->GetPDGMass();  // recoil
#endif
  G4double massT = iontar->GetPDGMass();  // target
  G4double massP = ionpro->GetPDGMass();  // projectile
  // start with initial momentum
  G4ThreeVector ppOut = pIn;
  // centre-of-mass velocity
  G4double betaCM = sqrt(KEIn*(KEIn+2*massP))/(massT+massP+KEIn);
  G4LorentzRotation emitterLorentzRotation( betaCM * pIn.unit() );
  
  G4double totalEnergy;
  G4double momentumModule;
  G4double costheta;
  G4double sintheta;
#ifdef _INCSPEC_
  G4double theta;
#endif
  G4double phi;
  G4ThreeVector threeMomentum;
  G4LorentzVector fourMomentumCM;
  G4LorentzVector fourMomentumLab;
  G4double dummy[3];
  for( G4int dd=0; dd<3; dd++ )
    dummy[dd] = 0.;
  //from each particle's energy obtain the momentum of the recoil 
  //by means of conserv. laws...!
  for( G4int jj=0; jj<3; jj++ ) {
    //jj=0->neutron(s)  jj=1->proton(s)  jj=2->alpha(s)
    for( G4int ii=0; ii<n_part[jj]; ii++ ) {
      if( evapFile[jj] ) {
	dummy[jj] = theEvapDist[jj]->GetEnergy();
      } else {    
	while(1){
	  dummy[jj] =  e_part[jj]+G4RandGauss::shoot( 0., s_part[jj] );
	  if( dummy[jj] > 0 )
	    break;
	}
      }
      totalEnergy = massPart[jj] + dummy[jj];
      if( totalEnergy < 0. )   //old check ... 
	totalEnergy = massPart[jj];  
      // relativistic momentum
      momentumModule = sqrt(totalEnergy*totalEnergy-massPart[jj]*massPart[jj]);
      // isotropical distrib in CM
      int tries = 0;
      do{//If alpha check angle in LAB
	costheta = 2.*G4UniformRand() - 1.0;
	sintheta = sqrt( 1. - costheta * costheta );
#ifdef _INCSPEC_
	theta = acos(costheta);
#endif
	phi    = 360.*deg*G4UniformRand();       
	threeMomentum  = momentumModule * 
	  G4ThreeVector(sintheta*cos(phi),sintheta*sin(phi),costheta);
	fourMomentumCM  = G4LorentzVector( threeMomentum, totalEnergy );       
	// Lorentz transformation to Lab
	fourMomentumLab = emitterLorentzRotation( fourMomentumCM );
	tries++;
      }while(jj==2 && (fourMomentumLab.vect().theta()<EvapAMinAngleLab
		       || fourMomentumLab.vect().theta()>EvapAMaxAngleLab)
	     && tries<100);
      G4double eneu  = fourMomentumLab.x()*fourMomentumLab.x();
      eneu += fourMomentumLab.y()*fourMomentumLab.y()+ 
	fourMomentumLab.z()*fourMomentumLab.z();
      eneu = 0.5*sqrt(eneu)/massPart[jj];
#ifdef _INCSPEC_
      theta = G4ThreeVector( fourMomentumLab.x(), fourMomentumLab.y(), 
			     fourMomentumLab.z() ).theta()/deg;
      phi = G4ThreeVector( fourMomentumLab.x(), fourMomentumLab.y(), 
			   fourMomentumLab.z() ).phi()/deg;
      if( phi < 0. ) phi += 360.;
      theSpectrum->Incr((G4int)((fourMomentumLab.t()-massPart)/(0.1*MeV)),9);
      theSpectrum->Incr((G4int)(10.*theta),10); //tmp
      theSpectrum->Incr((G4int)(10.*phi),11);   //tmp
#endif
      // momentum conservation
      ppOut -= G4ThreeVector(fourMomentumLab.x(),fourMomentumLab.y(),
			     fourMomentumLab.z() );
    }
  }  
#ifdef _INCSPEC_
  theSpectrum->Incr((G4int)(dummy[0]/(0.1*MeV)),6);  
  theSpectrum->Incr((G4int)(10.*theta/deg),7);      //tmp
  theSpectrum->Incr((G4int)(10.*phi/deg),8);        //tmp
  theSpectrum->Incr((G4int)((0.5*ppOut.mag2()/massR)/(0.1*MeV)),3);
  G4double th = ppOut.theta()/deg;
  G4double ph = ppOut.phi()/deg;
  if( ph < 0. ) ph += 360.;
  theSpectrum->Incr((G4int)(10.*th),4); 
  theSpectrum->Incr((G4int)(10.*ph),5);
  G4double beta_z = (ppOut.z()/massR);
  theSpectrum->Incr((G4int)(1000*beta_z),14);
#endif
  return ppOut;
}

G4ThreeVector Outgoing_Beam::GetOutgoingMomentumIF()


//I assume transfer of 3H to what will be the product
//I then evaporate alphas, neutrons end protons according to how
//they are set for FE, so I assume 7Li target or beam...
{
  // //We sample the outgoing alpha angle (from given dist in future)
  // G4double theta = 100*G4UniformRand()*3.14/180.;
  
  // //1, the beam
  // G4double massP = ionpro->GetPDGMass();
  // G4double pbeam = sqrt(KEIn*(KEIn+massP));
  // G4double pbeamx = pbeam*dirIn.unit().x();
  // G4double pbeamy = pbeam*dirIn.unit().y();
  // G4double pbeamz = pbeam*dirIn.unit().z();
  //2, mass of other stuff

// #ifdef G4V10
//   G4ParticleDefinition* alpha = 
//     G4IonTable::GetIonTable()->GetIon(2,4,0);  
// #else
//   G4ParticleDefinition* alpha = 
//     G4ParticleTable::GetParticleTable()->GetIon(2,4,0);  
// #endif
//   G4double massAlpha = alpha->GetPDGMass();

// #ifdef G4V10
//   G4ParticleDefinition* beforeevap = 
//     G4IonTable::GetIonTable()->GetIon(Zin+1,Ain+3,0);  
// #else
//   G4ParticleDefinition* beforeevap = 
//     G4ParticleTable::GetParticleTable()->GetIon(Zin+1,Ain+3,0);  
// #endif
//   G4double massbeforeevap = beforeevap->GetPDGMass();
  
  return pIn;
}

G4ThreeVector Outgoing_Beam::GetOutgoingMomentumCLX()
{
  //Start with getting v/c of CoM to create the boost
  //v_cm = Ptot/Etot
  G4double massT = iontar->GetPDGMass();  // target
  G4double massP = ionpro->GetPDGMass();  // projectile
  G4double pbeam = sqrt(KEIn*(KEIn+2*massP));
  G4double betaCoM = pbeam/(massT+massP+KEIn);
  //What to give back if no angular dist.
  G4ThreeVector ppOut = pIn.unit();
  //Get the lorentz boost
  G4LorentzRotation emitterLorentzRotation(betaCoM*pIn.unit());
  G4LorentzRotation invemitterLorentzRotation(-betaCoM*pIn.unit());
  //If no angular distribution given we do nothing...
  if(theAngDist->IsGoodDistribution()) {
    theAngDist->SelectTheta();
    G4ThreeVector momCoM = theAngDist->GetDirection(KEIn);
    //Rotate so that we are working in the frame of LAB, not frame
    //of beam In
    momCoM.rotateY(pIn.theta());
    momCoM.rotateZ(pIn.phi());    
    G4LorentzVector vec4Proj(pIn,KEIn+massP);
    G4LorentzVector vec4Tar(G4ThreeVector(0,0,0),massT);
    vec4Proj=invemitterLorentzRotation(vec4Proj);
    vec4Tar=invemitterLorentzRotation(vec4Tar);
    G4double massPstar = massP+Ex;
    G4double EtotCM = vec4Proj.e()+vec4Tar.e();
    G4double pCoM = 
      sqrt(pow(EtotCM,4)-2*pow(EtotCM,2)*pow(massPstar, 2)- 
	   2*pow(EtotCM,2)*pow(massT,2)+pow(massPstar,4)- 
	   2*pow(massPstar,2)*pow(massT,2)+pow(massT, 4))/(2*EtotCM);
    G4double EprojCoM = sqrt(pCoM*pCoM+massPstar*massPstar);
    G4LorentzVector pCoM4vec(pCoM*momCoM,EprojCoM);
    pCoM4vec=emitterLorentzRotation(pCoM4vec);
    ppOut = pCoM4vec.vect();
    G4double EtarCoM = sqrt(pCoM*pCoM+massT*massT);
    G4LorentzVector pCoM4vecRecoil(-pCoM*momCoM,EtarCoM);
    pCoM4vecRecoil = emitterLorentzRotation(pCoM4vecRecoil);
    ptargetrecoil = pCoM4vecRecoil.vect();
    targetrecoil=iontar;
  } 
  return ppOut;
}

/******************************************************************************
 *                      Code generated with sympy 0.7.1                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 *****************************************************************************/

double f(const double &beta, const double &gamma, const double &thetaCM, 
	 const double &thetaLAB, const double &u) {
  return tan(thetaLAB) - sin(thetaCM)/(gamma*(beta/u + cos(thetaCM)));
}

/******************************************************************************
 *                      Code generated with sympy 0.7.1                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 *****************************************************************************/

double fp(const double &beta, const double &gamma, const double &thetaCM, 
	  const double &u) {
  return -cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) - 
    pow(sin(thetaCM), 2)/(gamma*pow(beta/u + cos(thetaCM), 2));
}


/******************************************************************************
 *                      Code generated with sympy 0.7.1                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 *****************************************************************************/

double thetaLAB(double beta, double gamma, double thetaCM, double u) {
  return atan(sin(thetaCM)/(gamma*(beta/u + cos(thetaCM))));
}

/******************************************************************************
 *                      Code generated with sympy 0.7.1                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 *****************************************************************************/

double dthetaLAB(double beta, double gamma, double thetaCM, double u) {
  return (cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) + 
	  pow(sin(thetaCM), 2)/(gamma*pow(beta/u + cos(thetaCM), 2)))/
    (1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2)));
}

/******************************************************************************
 *                      Code generated with sympy 0.7.1                       *
 *                                                                            *
 *              See http://www.sympy.org/ for more information.               *
 *                                                                            *
 *                       This file is part of 'project'                       *
 *****************************************************************************/

double ddthetaLAB(double beta, double gamma, double thetaCM, double u) {

  return -(sin(thetaCM)/(gamma*(beta/u + cos(thetaCM))) - 
	   3*sin(thetaCM)*cos(thetaCM)/(gamma*pow(beta/u + 
						  cos(thetaCM), 2)) - 
	   2*pow(sin(thetaCM), 3)/(gamma*pow(beta/u + cos(thetaCM), 3)))/
    (1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2))) 
    - 2*(cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) + pow(sin(thetaCM), 2)/
	 (gamma*pow(beta/u + cos(thetaCM), 2)))*
    (sin(thetaCM)*cos(thetaCM)/
     (pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2)) + 
     pow(sin(thetaCM), 3)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 3)))/
    pow(1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*
				  pow(beta/u + cos(thetaCM), 2)), 2);
}

G4ThreeVector Outgoing_Beam::GetOutgoingMomentumFF()
{
  //First we make a compound and evaporate neutrons
  G4double massT = iontar->GetPDGMass();  // target
  G4double massP = ionpro->GetPDGMass();  // projectile
  G4double massCompound =  //Total mass compound
    sqrt(massP*massP+2*massT*massP+massT*massT+2*KEIn*massT);
  G4double pbeam = sqrt(KEIn*(KEIn+2*massP));
  //Now we can get the Lorentz boost CM->Lab
  G4double betaCM = pbeam/massCompound*
    sqrt(1/(1+pbeam*pbeam/massCompound/massCompound)); 
  G4double gammaCM = 1./sqrt(1-betaCM*betaCM);
  G4LorentzRotation emitterLorentzRotation(betaCM*pIn.unit());
  //First we evaporate some neutrons in 4pi CM
  G4ThreeVector pevapn(0,0,0);
  G4double neutronE = 0;
  G4int nn = (G4int)ceil(G4RandGauss::shoot(nybar,sigmanybar));
  G4double  massn =  939.565*MeV; //neutron
  for(G4int i=0; i<nn; i++){//We repeat what has been done for FE
    G4double En = e_part[0]+G4RandGauss::shoot(0.,s_part[0]);
    while(En<0) En = e_part[0]+G4RandGauss::shoot(0.,s_part[0]);
    neutronE+=En;
    G4double momN = sqrt(En*(En+2*massn));
    G4double costheta = 2.*G4UniformRand() - 1.0;
    G4double sintheta = sqrt( 1. - costheta * costheta );
    G4double phi    = 360.*deg*G4UniformRand();   
    pevapn+=momN*G4ThreeVector(sintheta*cos(phi),sintheta*sin(phi),costheta); 
    //Get recoilenergy to reduce excitation energy with the correct
    //amount.
    massCompound = 
      ((massn-massCompound+En)*
       sqrt(massn*massn-2*massCompound*massn+
	    massCompound*massCompound-2*En*massCompound)+
       massn*massn-2*massCompound*massn+
       massCompound*massCompound-2*En*massCompound)/
      (sqrt(massn*massn-2*massCompound*massn+
	    massCompound*massCompound-2*En*massCompound)+
       massn-massCompound+En);
  }
  //Here we are going to calculate the "CM" after neutron evaporation
  //(in principle we should do this after each neutron if we want
  //isotropic neutrons in CM... but we don't care as they should not be
  //isotropic anyway (spin 1/2 particles ;)
  G4double magp = pevapn.mag();
  G4double betaCM2 = magp/massCompound*
    sqrt(1+magp*magp/massCompound/massCompound);
  G4LorentzRotation emitterLorentzRotation2(-betaCM2*pevapn.unit());
  //Now are going to fission the compound
  //First get the energy aviable 
#ifdef G4V10
  G4ParticleDefinition *partner = G4IonTable::GetIonTable()->
    GetIon(Zin+Ztar-iongs->GetAtomicNumber(),
	   Ain+Atar-iongs->GetAtomicMass()-nn,0);
#else
  G4ParticleDefinition *partner = G4ParticleTable::GetParticleTable()->
    GetIon(Zin+Ztar-iongs->GetAtomicNumber(),
	   Ain+Atar-iongs->GetAtomicMass()-nn,0);
#endif
  G4double Eavial = massCompound - iongs->GetPDGMass() - partner->GetPDGMass();
  //Calculate the part that goes into kinetical energy,
  //I assume that this is equal as for thermal fission of 235U,
  //i.e ~83%
  //An alternative would be a viable physical model...
  //I split the energy that rests equally between the fragments
  //This is up for discussion but since we ignore where the fragments
  //are populated in the next step this might be ok...
  G4double massf1 = iongs->GetPDGMass()+Eavial*(1.-fractionKinE)/2.;
  G4double massf2 = partner->GetPDGMass()+Eavial*(1.-fractionKinE)/2.;
  G4double EKf1 = (massCompound*massCompound-2*massf1*massCompound-
		   massf2*massf2+massf1*massf1)/2./massCompound;
  /* If I want to add the other fragment one of these days...
  G4double EKf2 = (massCompound*massCompound-2*massf2*massCompound+
		   massf2*massf2-massf1*massf1)/2./massCompound;
  */
  //Here we pick a random direction isotropic in 4pi CM, but limited by
  //theta angles in LAB relative to beam axis to save computer time
  G4double momfrag1 = sqrt(EKf1*(EKf1+2*massf1));
  G4double u = momfrag1/(massf1+EKf1);
  //Here we do a newton raphson to get the CM angles to within 1 deg...
  //We got that (v/c=speed of CM in LAB)
  //tan(theta_LAB)=sin(theta_CM)/gamma/(cos(theta_CM)+beta/u)
  //u=p/E, p and E of fission fragment in CM
  //we need to be sure that the angles are allowed...
  //So we have to calculate the max allowed LAB angle
  //This requires solving the equation
  //d(atan(sin(theta_CM)/gamma/(cos(theta_CM)+beta/u)))/d(theta_CM)=0
  G4double MAXTHETA=pi/2;
  G4double dtheta = 100*deg;
  G4int ctr=0;
  do{
    dtheta=dthetaLAB(betaCM,gammaCM,MAXTHETA,u)/
      ddthetaLAB(betaCM,gammaCM,MAXTHETA,u);
    MAXTHETA-=dtheta;
  }while(fabs(dtheta)>1*deg && ++ctr<10);
  G4double MAXTHETALAB = thetaLAB(betaCM,gammaCM,MAXTHETA,u);
  G4double LABAngle = thetaMin<MAXTHETALAB?thetaMin:MAXTHETALAB-.5*deg;
  G4double thetamin = LABAngle;
  ctr=0;
  dtheta=100.*deg;
  do{
    dtheta=f(betaCM,gammaCM,thetamin,LABAngle,u)/
      fp(betaCM,gammaCM,thetamin,u);
    thetamin-=dtheta;
  }while(fabs(dtheta)>1*deg && ++ctr<10);
  LABAngle = thetaMax<MAXTHETALAB?thetaMax:MAXTHETALAB-.5*deg;
  G4double thetamax = LABAngle;
  ctr=0;
  dtheta=100.*deg;
  do{
    dtheta=f(betaCM,gammaCM,thetamax,LABAngle,u)/
      fp(betaCM,gammaCM,thetamax,u);
    thetamax-=dtheta;
  }while(fabs(dtheta)>1*deg && ++ctr<10);
  while(thetamin<0) thetamin+=halfpi;
  while(thetamax<0) thetamax+=halfpi;
  if(thetamax<thetamin){
    G4double tmp=thetamin;
    thetamin=thetamax;
    thetamax=tmp;
  }
  G4double costheta = cos(thetamin)+
    G4UniformRand()*(cos(thetamax)-cos(thetamin));
  G4double sintheta = sqrt(1.-costheta*costheta);
  G4double phi = (phiMin+(phiMax-phiMin)*G4UniformRand())*deg;   
  G4ThreeVector pfissionfrag =
    momfrag1*G4ThreeVector(sintheta*cos(phi),sintheta*sin(phi),costheta); 
  //But now put into correct system of coordinates
  //The theta is given as if pIn was z so we need to rotate pfissionfrag
  //the inverse of what is needed to put pIn parallell to the z axis
  pfissionfrag.rotateY(pIn.theta());
  pfissionfrag.rotateZ(pIn.phi());
  G4LorentzVector lfission(pfissionfrag,EKf1+massf1);
  //Then we do Lorentz transformations...
  G4LorentzVector lfission2 = emitterLorentzRotation2(lfission);
  G4LorentzVector lfission3 = emitterLorentzRotation(lfission2);
  return lfission3.vect();
}

G4ThreeVector Outgoing_Beam::GetOutgoingMomentumTR()

{
  G4double massT  = iontar->GetPDGMass(); //Target
  G4double massTL = iontl-> GetPDGMass(); //Partner to product
  G4double massP  = ionpro->GetPDGMass(); //Projectile
  G4double massI  = ion->GetPDGMass();    //Product excited state
  // velocity of the CM in the Lab ref frame
  G4double pbeam= sqrt(KEIn*(KEIn+2*massP));
  G4double betaCM = pbeam/(massT+massP+KEIn);
  G4double gammaCM = 1./sqrt(1.-betaCM*betaCM);
  //square module initial tri-momentum c.of m.  
  G4double p2in = pow(gammaCM*betaCM*massT,2);
  //Total energy COM
  G4double etotCM = sqrt(massT*massT+p2in)+sqrt(massP*massP+p2in);
  //square module final tri-momentum c.of m.
  G4double p2fin = 
    (pow(etotCM,4)-2*pow(etotCM,2)*pow(massI, 2)+pow(massI, 4)+pow(massTL,4) -
     2*pow(massTL,2)*(pow(etotCM, 2)+pow(massI, 2)))/(4*pow(etotCM,2));
  //kinetic energy PL in the c.o.m. system
  G4double momProCM  = sqrt(p2fin); 
  G4double etotProCM = 0.;
  G4ThreeVector momCM; 
  G4LorentzVector fourMomentumCM;
  G4double momProLab  = 0.;  
  G4ThreeVector ppOut = pIn.unit();
  G4LorentzVector fourMomentumLab = G4LorentzVector();
  if(theAngDist->IsGoodDistribution()) {
    while(1) {
      theAngDist->SelectTheta();
      G4double ttt = theAngDist->GetTheta();
      theSpectrum->Incr((G4int)(10.*ttt/deg),19);
      //The energy here is just a dummy...
      momCM = theAngDist->GetDirection(KEIn); 
      //but not in the correct frame, the theta (and phi...) should be relative
      //the beam axis, not z-lab...
      momCM = beamIn->getBeamRotation()(momCM);
      //G4cout << " Direction " << momCM << G4endl;
      //we have now the direction in the CM ref. frame
      //total energy of the excited nucleus in the CM ref frame
      //mass + kin. energy (which takes Q-value into account)
      etotProCM = sqrt(massI*massI+p2fin);
      momCM = momProCM * momCM;
      fourMomentumCM = G4LorentzVector( momCM, etotProCM );
      // Lorentz transformation
      if( betaCM > 0. ) {
	G4LorentzRotation  lorentzRotation( betaCM*pIn.unit() );
	fourMomentumLab = lorentzRotation( fourMomentumCM );
      }
      else
	fourMomentumLab = fourMomentumCM;
      // now, transform the 4-vector into "regular" items
      // ---> done at the end of this method (as before)
      break;    
    }
    theSpectrum->Incr((G4int)((etotProCM-massI)/(10.*keV)),3);
    G4double thett = momCM.theta();
    theSpectrum->Incr((G4int)(10.*thett/deg),18);
    theMatrix->Incr((G4int)((etotProCM-massI)/(10.*keV)), 
		    (G4int)(100.*thett/deg), 0);
    G4double phii = momCM.phi();
    if( phii < 0. ) phii += 360.*deg;
    theSpectrum->Incr((G4int)(10.*phii/deg),5);
  } else {
    std::cout << "You have not given a angular distribution file for your ";
    std::cout << "transer reaction (DIS or MNT). Since we don't now what you ";
    std::cout << "want in terms of mechanism you should do that!!!!\n";
    exit(-1);
  }
  ppOut = 
    G4ThreeVector(fourMomentumLab.x(),fourMomentumLab.y(),fourMomentumLab.z());
  if( !(ppOut.mag2() > 0.) )
    return pIn.unit() * momProLab;
  G4double thetaLab = ppOut.theta();
  theSpectrum->Incr((G4int)(100.*thetaLab/deg),7); 
  G4double enLab = 0.5*ppOut.mag2() / massI;
  theSpectrum->Incr((G4int)(enLab/(10.*keV)),20); 
  theSpectrum->Incr((G4int)(10.*thetaLab/deg),21); 
  return ppOut;
}



void Outgoing_Beam::EndOfRun()
{
  //   G4int ii;
  //   G4int size = theAngDist->GetSize();
  // 
  //   for( ii=0; ii<size; ii++ ) {
  //     theSpectrum->set( (G4int)(theAngDist->GetSigma(ii)), ii, 6 );
  //   }


  theSpectrum->write("energy_distr.dat");
  //  theMatrix->write("theta_energy.mat");
}

G4double Outgoing_Beam::Q_value( G4double energy, G4double costheta )  //formula not so clear (comparison with reaction prog. output...)???
{
  G4double massPL = ion->GetPDGMass();
  //  G4double massion = ion->GetPDGMass();
  //  G4cout << "masses ..." << massPL << " " << massion << G4endl;
  G4double massTL = iontl->GetPDGMass();
  G4double massP  = ionpro->GetPDGMass();
  
  G4double qval = energy * ( 1.+ massPL/massTL );
  qval -= KEIn * ( 1. - massP/massTL );
  qval -= 2. * costheta * sqrt( energy*KEIn*(massP*massPL)/(massTL*massTL) );

  return qval;
}

//---------------------------------------------------------
void Outgoing_Beam::Report()
{
  setDecayProperties();

  G4cout<<"----> Delta Z for the outgoing beam set to  "<<DZ<< G4endl;
  G4cout<<"----> Delta A for the outgoing beam set to  "<<DA<< G4endl;
  G4cout<<"----> Excitation energy of the outgoing beam set to "<<
    G4BestUnit(Ex,"Energy")<<G4endl;
  G4cout<<"----> Lifetime of the excited state for the outgoing beam set to "<<
    G4BestUnit(tau,"Time")<<G4endl; 
  G4cout<<"----> Number of charge states "<<NQ<<G4endl;
  vector<Charge_State*>::iterator itPos = Q.begin();
  for(; itPos < Q.end(); itPos++) 
    {
      G4cout<<"----> Charge state "     <<(*itPos)->GetCharge()<<G4endl;
      G4cout<<"   => UnReactedFraction "<<(*itPos)->GetUnReactedFraction()
	    <<G4endl;
      G4cout<<"   =>   ReactedFraction "<<(*itPos)->GetReactedFraction()
	    <<G4endl;
      if((*itPos)->GetUseSetKEu())
	{
	  G4cout<<"   =>              KE/A "<<(*itPos)->GetSetKEu()/MeV
		<<" MeV"<<G4endl; 
	  G4cout<<"   =>                KE "<<(*itPos)->GetSetKE()/MeV
		<<" MeV"<<G4endl;
	}
      else
	G4cout<<"   =>                KE "<<(*itPos)->GetSetKE()/MeV
	      <<" MeV"<<G4endl;
    }
  CalcQUR();
  CalcQR();
}

//---------------------------------------------------------
void Outgoing_Beam::setA(G4int Ain1)
{

  Atar=Ain1;
  G4cout<<"----> A of the target set to  "<<Atar<< G4endl;
  
}
//---------------------------------------------------------
void Outgoing_Beam::setZ(G4int Zin1)
{

  Ztar=Zin1;
  G4cout<<"----> Z of the target set to  "<<Ztar<< G4endl;
  
}

//---------------------------------------------------------
void Outgoing_Beam::setDA(G4int da)
{

  DA=da;
  G4cout<<"----> Delta A for the outgoing beam set to  "<<DA<< G4endl; 
}
//---------------------------------------------------------
void Outgoing_Beam::setDZ(G4int dz)
{

  DZ=dz;
  G4cout<<"----> Delta Z for the outgoing beam set to  "<<DZ<< G4endl;
  
}
//---------------------------------------------------------
void Outgoing_Beam::setEx(G4String values)
{
  listEx.clear();
  totFeeding=0;
  std::istringstream input(values);
  G4double e,I;
  while(input >> e >> I){
    totFeeding+=I;
    listEx.push_back(std::pair<G4double,G4double>(e*keV,totFeeding));
    G4cout<<"----> Excitation energy of the outgoing beam set to "<<
      G4BestUnit(listEx.back().first,"Energy")<<G4endl;
  }

}

void Outgoing_Beam::setPartnerEx(G4String values)
{
  listEx_partner.clear();
  totFeeding_partner=0;
  std::istringstream input(values);
  G4double e,I;
  while(input >> e >> I){
    totFeeding_partner+=I;
    listEx_partner.push_back(std::pair<G4double,G4double>(e*keV,
							  totFeeding_partner));
    G4cout<<"----> Excitation energy of the partner set to "<<
      G4BestUnit(listEx_partner.back().first,"Energy")<<G4endl;
  }

}
//---------------------------------------------------------
void Outgoing_Beam::setProjEx(G4double ex)
{
  if( ex > 0. )
    projEx=ex;
  
  G4cout<<"----> Excited level of the outgoing beam set to "
	<< G4BestUnit(projEx,"Energy")<<G4endl;

}
//---------------------------------------------------------
void Outgoing_Beam::setTarEx1(G4double ex)
{

  TarEx1=ex;
  
  G4cout<<"----> Target excitation energy 1 set to "
	<< G4BestUnit(TarEx1,"Energy")<<G4endl;

}
//---------------------------------------------------------
void Outgoing_Beam::setTarEx2(G4double ex)
{

  TarEx2=ex;
  
  G4cout<<"----> Target excitation energy 2 set to "
	<< G4BestUnit(TarEx2,"Energy")<<G4endl;

}
//---------------------------------------------------------
void Outgoing_Beam::setTFrac(G4double ex)
{
  TFrac=ex;
  if(TFrac>1) TFrac=1.;
  if(TFrac<0) TFrac=0.;

  
  G4cout<<"----> Fraction of target excitations set to "<<TFrac<<G4endl;

}
//---------------------------------------------------------
void Outgoing_Beam::setCFrac(G4double ex)
{
  CFrac=ex;
  if(CFrac>1) CFrac=1.;
  if(CFrac<0) CFrac=0.;
  G4cout<<"----> Fraction of target excitation energy 1 to energy 2 set to "
	<<CFrac<<G4endl;
}
//-----------------------------------------------------------------
void Outgoing_Beam::settau(G4double t)
{

  tau=t;

  G4cout<<"----> Lifetime of the excited state for the outgoing beam set to "<<
    G4BestUnit(tau,"Time")<<G4endl; 
}
//---------------------------------------------------------
void Outgoing_Beam::SetUpChargeStates()
{

  vector<Charge_State*>::iterator itPos = Q.begin();
  // clear all elements from the array
  for(; itPos < Q.end(); itPos++)
    delete *itPos;    // free the element from memory
  // finally, clear all elements from the array
  Q.clear();
  for(G4int i=0;i<NQ;i++) 
    Q.push_back(new Charge_State);
}

//---------------------------------------------------------
void Outgoing_Beam::SetQCharge(G4int q)
{

  if(SQ>=0&&SQ<NQ){
      vector<Charge_State*>::iterator itPos = Q.begin();
      for(G4int i=0;i<SQ;i++) itPos++;
      (*itPos)->SetCharge(q);
    }
  else
    G4cout<<" Number of defined charge states ="<<NQ<<" is too small"<<G4endl;
}

//---------------------------------------------------------
void Outgoing_Beam::SetQUnReactedFraction(G4double f)
{
  if(SQ>=0&&SQ<NQ){
      vector<Charge_State*>::iterator itPos = Q.begin();
      for(G4int i=0;i<SQ;i++) itPos++;
      (*itPos)->SetUnReactedFraction(f);
      CalcQUR();
    }
  else
    G4cout<<" Number of defined charge states ="<<NQ<<" is too small"<<G4endl;
}
//---------------------------------------------------------
void Outgoing_Beam::SetQReactedFraction(G4double f)
{
  if(SQ>=0&&SQ<NQ)    {
      vector<Charge_State*>::iterator itPos = Q.begin();
      for(G4int i=0;i<SQ;i++) itPos++;
      (*itPos)->SetReactedFraction(f);
      CalcQR();
    }
  else
    G4cout<<" Number of defined charge states ="<<NQ<<" is too small"<<G4endl;
}
//---------------------------------------------------------
void Outgoing_Beam::SetQKEu(G4double e)
{
  G4int A;

  A=beamIn->getA()+DA;
  if(SQ>=0&&SQ<NQ)
    {
      vector<Charge_State*>::iterator itPos = Q.begin();

      for(G4int i=0;i<SQ;i++) itPos++;
      (*itPos)->SetKEu(e,A);
      CalcQR();
      CalcQUR();
    }
  else
    G4cout<<" Number of defined charge states ="<<NQ<<" is too small"<<G4endl;
}
//---------------------------------------------------------
void Outgoing_Beam::SetQKE(G4double e)
{

  if(SQ>=0&&SQ<NQ)
    {
      vector<Charge_State*>::iterator itPos = Q.begin();

      for(G4int i=0;i<SQ;i++) itPos++;
      (*itPos)->SetKE(e);
      CalcQR();
      CalcQUR();
    }
  else
    G4cout<<" Number of defined charge states ="<<NQ<<" is too small"<<G4endl;
}
//---------------------------------------------------------
void Outgoing_Beam::CalcQR()
{
  G4double sum;
  vector<Charge_State*>::iterator itPos = Q.begin();

  sum=0.;
  for(; itPos < Q.end(); itPos++)
    sum+=(*itPos)->GetReactedFraction();
  //  G4cout<<" Sum = "<<sum<<G4endl;
  itPos = Q.begin();
  G4int ind=0,i,max,n=0;
  for(; itPos < Q.end(); itPos++)
    { 
  
      max=(G4int)(1000.*(*itPos)->GetReactedFraction()/sum);
      //      G4cout<<" Sum = "<<sum<<" Max = "<<max<<" n "<<n<<G4endl;
      for(i=0;i<max;i++) 
	{
	  QR[ind]=(*itPos)->GetSetKE();
	  QRI[ind]=n;
	  ind++;
	}
      n++;
    }

}
//---------------------------------------------------------
void Outgoing_Beam::CalcQUR()
{
  G4double sum;
  vector<Charge_State*>::iterator itPos = Q.begin();

  sum=0.;
  for(; itPos < Q.end(); itPos++)
    sum+=(*itPos)->GetUnReactedFraction();

  itPos = Q.begin();
  G4int ind=0,i,max,n=0;
  for(; itPos < Q.end(); itPos++)
    { 
      max=(G4int)(1000.*(*itPos)->GetUnReactedFraction()/sum);
      for(i=0;i<max;i++) 
	{
	  QUR[ind]=(*itPos)->GetSetKE();
	  QURI[ind]=n;
	  ind++;
	}
      n++;
    }
}
//---------------------------------------------------------
G4double Outgoing_Beam::GetURsetKE()
{
  G4int i;

  i=(G4int)(1000.*G4UniformRand());
  Index=QURI[i];
  return QUR[i];
}
//---------------------------------------------------------
G4double Outgoing_Beam::GetRsetKE()
{
  G4int i;

  i=(G4int)(1000.*G4UniformRand());
  Index=QRI[i];
  return QR[i];
}
//---------------------------------------------------------
void Outgoing_Beam::SetAngDistFile(G4String name)
{

  angDistFileName = name;

  G4cout<<"----> Angular distribution for emitted ions will be read from  "
	<< angDistFileName <<G4endl; 
}

//---------------------------------------------------------
void Outgoing_Beam::setPhiRange(G4double phimin, G4double phimax)
{
  phiMin=phimin;
  phiMax=phimax;
  if( theAngDist )
    theAngDist->SetPhiRange(phimin,phimax); 
}

//---------------------------------------------------------
void Outgoing_Beam::setThRange(G4double phimin, G4double phimax)
{
  thetaMin=phimin*deg;
  thetaMax=phimax*deg;
  if( theAngDist )
    theAngDist->SetThRange(phimin,phimax); 
}

void Outgoing_Beam::SetNumEvapN( G4int nn)
{
  n_part[0] = nn;
}
void Outgoing_Beam::SetNumEvapP( G4int nn)
{
  n_part[1] = nn;
}
void Outgoing_Beam::SetNumEvapA( G4int nn)
{
  n_part[2] = nn;
}
void Outgoing_Beam::SetEnEvapN( G4double value )
{
  e_part[0] = value;
}
void Outgoing_Beam::SetEnEvapP( G4double value )
{
  e_part[1] = value;
}
void Outgoing_Beam::SetEnEvapA( G4double value )
{
  e_part[2] = value;
}
void Outgoing_Beam::SetSigEvapN( G4double value )
{
  s_part[0] = value;
}
void Outgoing_Beam::SetSigEvapP( G4double value )
{
  s_part[1] = value;
}
void Outgoing_Beam::SetSigEvapA( G4double value )
{
  s_part[2] = value;
}
void Outgoing_Beam::SetEvapFromFileN( G4bool choice )
{
  evapFile[0]=choice;
}
void Outgoing_Beam::SetEvapFromFileP( G4bool choice )
{
  evapFile[1]=choice;
}
void Outgoing_Beam::SetEvapFromFileA( G4bool choice )
{
  evapFile[2]=choice;
}

void Outgoing_Beam::SetPfe( G4double value )
{
  p_fe = value;
}
void Outgoing_Beam::SetPtr( G4double value )
{
  p_tr = value;
}
void Outgoing_Beam::SetPclx( G4double value )
{
  p_clx = value;
}
void Outgoing_Beam::SetPff( G4double value )
{
  p_ff = value;
}

void Outgoing_Beam::SetPif( G4double value )
{
  p_if = value;
}


void Outgoing_Beam::SetEvapAMaxAngleLab(G4double maxa)

{
  EvapAMaxAngleLab=maxa;
}
  
void Outgoing_Beam::SetEvapAMinAngleLab(G4double mina)

{
  EvapAMinAngleLab=mina;
}





////////////////////////////
// The Messenger
////////////////////////////
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"

Outgoing_Beam_Messenger::Outgoing_Beam_Messenger( Outgoing_Beam* pTarget, 
						  G4String name )
 :myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/emitter/BeamOut/";

  BeamOutDir = new G4UIdirectory(directoryName);
  BeamOutDir->SetGuidance("Outgoing beam control.");

  commandName = directoryName + "DA";
  aLine = commandName.c_str();
  DACmd = new G4UIcmdWithAnInteger(aLine,this);
  DACmd->SetGuidance("Select the mass number change for the outgoing beam.");
  DACmd->SetParameterName("choice",false);
  DACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "DZ";
  aLine = commandName.c_str();
  DZCmd = new G4UIcmdWithAnInteger(aLine,this);
  DZCmd->SetGuidance("Select the atomic number change for the outgoing beam.");
  DZCmd->SetParameterName("choice",false);
  DZCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "A";
  aLine = commandName.c_str();
  ACmd = new G4UIcmdWithAnInteger(aLine,this);
  ACmd->SetGuidance("Select the mass number for the target.");
  ACmd->SetParameterName("choice",false);
  ACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Z";
  aLine = commandName.c_str();
  ZCmd = new G4UIcmdWithAnInteger(aLine,this);
  ZCmd->SetGuidance("Select the atomic number for the target.");
  ZCmd->SetParameterName("choice",false);
  ZCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ProjectileExcitation";
  aLine = commandName.c_str();
  ExCmd = new G4UIcmdWithAString(aLine,this);
  ExCmd->SetGuidance("Set excitation energy for the outgoing beam, give in pairs of energy of level [keV] and feeding fraction. ");
  ExCmd->SetParameterName("choice",false);
  ExCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "PartnerExcitation";
  aLine = commandName.c_str();
  PartnerExCmd = new G4UIcmdWithAString(aLine,this);
  PartnerExCmd->SetGuidance("Set excitation energy for the partner, give in pairs of energy of level [keV] and feeding fraction. ");
  PartnerExCmd->SetParameterName("choice",false);
  PartnerExCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ProjectileLevel";
  aLine = commandName.c_str();
  projExCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  projExCmd->SetGuidance("Set excited level for the outgoing beam.");
  projExCmd->SetParameterName("choice",false);
  projExCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "TargetExcitation_1";
  aLine = commandName.c_str();
  TEx1Cmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  TEx1Cmd->SetGuidance("Set target excitation energy 1.");
  TEx1Cmd->SetParameterName("choice",false);
  TEx1Cmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "TargetExcitation_2";
  aLine = commandName.c_str();
  TEx2Cmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  TEx2Cmd->SetGuidance("Set target excitation energy 2.");
  TEx2Cmd->SetParameterName("choice",false);
  TEx2Cmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Target_E1_to_E2_Fraction";
  aLine = commandName.c_str();
  CExFCmd = new G4UIcmdWithADouble(aLine,this);
  CExFCmd->SetGuidance("Set fraction of target energy 1 excitations as compared to the target energy 2 excitations.");
  CExFCmd->SetParameterName("choice",false);
  CExFCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "TargetExcitationFraction";
  TExFCmd = new G4UIcmdWithADouble(aLine,this);
  TExFCmd->SetGuidance("Set fraction of target excitation as compared to the projectile excitation.");
  TExFCmd->SetParameterName("choice",false);
  TExFCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "tau";
  aLine = commandName.c_str();
  tauCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  tauCmd->SetGuidance("Set lifetime of the excited state of the outgoing beam.");
  tauCmd->SetParameterName("choice",false);
  tauCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "adistFile";
  aLine = commandName.c_str();
  aFileCmd = new G4UIcmdWithAString(aLine,this);
  aFileCmd->SetGuidance("Set angular distribution file for ejectiles");
  aFileCmd->SetParameterName("choice",false);
  aFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "phi";
  aLine = commandName.c_str();
  phiCmd = new G4UIcmdWithAString(aLine,this);
  phiCmd->SetGuidance("Set phi range for ejectiles");
  phiCmd->SetParameterName("choice",false);
  phiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "theta";
  aLine = commandName.c_str();
  thCmd = new G4UIcmdWithAString(aLine,this);
  thCmd->SetGuidance("Set theta range for ejectiles");
  thCmd->SetParameterName("choice",false);
  thCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Report";
  aLine = commandName.c_str();
  RepCmd = new G4UIcmdWithoutParameter(aLine,this);
  RepCmd->SetGuidance("Report parameters for the outgoing beam.");

  commandName = directoryName + "setEvapNumberN";
  aLine = commandName.c_str();
  npartNCmd = new G4UIcmdWithAnInteger(aLine,this);
  npartNCmd->SetGuidance("Number of neutrons evaporated.");
  npartNCmd->SetParameterName("choice",false);
  npartNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapNumberP";
  aLine = commandName.c_str();
  npartPCmd = new G4UIcmdWithAnInteger(aLine,this);
  npartPCmd->SetGuidance("Number of protons evaporated.");
  npartPCmd->SetParameterName("choice",false);
  npartPCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapNumberA";
  aLine = commandName.c_str();
  npartACmd = new G4UIcmdWithAnInteger(aLine,this);
  npartACmd->SetGuidance("Number of alphas evaporated.");
  npartACmd->SetParameterName("choice",false);
  npartACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapEnerN";
  aLine = commandName.c_str();
  epartNCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  epartNCmd->SetGuidance("Set mean value energy evaporated neutrons");
  epartNCmd->SetParameterName("choice",false);
  epartNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapEnerP";
  aLine = commandName.c_str();
  epartPCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  epartPCmd->SetGuidance("Set mean value energy evaporated protons");
  epartPCmd->SetParameterName("choice",false);
  epartPCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapEnerA";
  aLine = commandName.c_str();
  epartACmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  epartACmd->SetGuidance("Set mean value energy evaporated alphas");
  epartACmd->SetParameterName("choice",false);
  epartACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapSigmaN";
  aLine = commandName.c_str();
  spartNCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  spartNCmd->SetGuidance("Set sigma energy evaporated neutrons");
  spartNCmd->SetParameterName("choice",false);
  spartNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapSigmaP";
  aLine = commandName.c_str();
  spartPCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  spartPCmd->SetGuidance("Set sigma energy evaporated protons");
  spartPCmd->SetParameterName("choice",false);
  spartPCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setEvapSigmaA";
  aLine = commandName.c_str();
  spartACmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  spartACmd->SetGuidance("Set sigma energy evaporated alphas");
  spartACmd->SetParameterName("choice",false);
  spartACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableEvapFromFileN";
  aLine = commandName.c_str();
  enableEvapFromFileNCmd = new G4UIcmdWithABool(aLine, this);
  enableEvapFromFileNCmd->SetGuidance("Activate reading neutron evaporation spectrum from file");
  enableEvapFromFileNCmd->SetGuidance("Required parameters: none.");
  enableEvapFromFileNCmd->SetParameterName("evapFile[0]",true);
  enableEvapFromFileNCmd->SetDefaultValue(true);
  enableEvapFromFileNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableEvapFromFileN";
  aLine = commandName.c_str();
  disableEvapFromFileNCmd = new G4UIcmdWithABool(aLine, this);
  disableEvapFromFileNCmd->SetGuidance("De-Activate reading neutron evaporation spectrum from file");
  disableEvapFromFileNCmd->SetGuidance("Required parameters: none.");
  disableEvapFromFileNCmd->SetParameterName("evapFile[0]",true);
  disableEvapFromFileNCmd->SetDefaultValue(false);
  disableEvapFromFileNCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableEvapFromFileP";
  aLine = commandName.c_str();
  enableEvapFromFilePCmd = new G4UIcmdWithABool(aLine, this);
  enableEvapFromFilePCmd->SetGuidance("Activate reading proton evaporation spectrum from file");
  enableEvapFromFilePCmd->SetGuidance("Required parameters: none.");
  enableEvapFromFilePCmd->SetParameterName("evapFile[1]",true);
  enableEvapFromFilePCmd->SetDefaultValue(true);
  enableEvapFromFilePCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableEvapFromFileP";
  aLine = commandName.c_str();
  disableEvapFromFilePCmd = new G4UIcmdWithABool(aLine, this);
  disableEvapFromFilePCmd->SetGuidance("De-Activate reading proton evaporation spectrum from file");
  disableEvapFromFilePCmd->SetGuidance("Required parameters: none.");
  disableEvapFromFilePCmd->SetParameterName("evapFile[1]",true);
  disableEvapFromFilePCmd->SetDefaultValue(false);
  disableEvapFromFilePCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableEvapFromFileA";
  aLine = commandName.c_str();
  enableEvapFromFileACmd = new G4UIcmdWithABool(aLine, this);
  enableEvapFromFileACmd->SetGuidance("Activate reading alpha evaporation spectrum from file");
  enableEvapFromFileACmd->SetGuidance("Required parameters: none.");
  enableEvapFromFileACmd->SetParameterName("evapFile[2]",true);
  enableEvapFromFileACmd->SetDefaultValue(true);
  enableEvapFromFileACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableEvapFromFileA";
  aLine = commandName.c_str();
  disableEvapFromFileACmd = new G4UIcmdWithABool(aLine, this);
  disableEvapFromFileACmd->SetGuidance("De-Activate reading alpha evaporation spectrum from file");
  disableEvapFromFileACmd->SetGuidance("Required parameters: none.");
  disableEvapFromFileACmd->SetParameterName("evapFile[2]",true);
  disableEvapFromFileACmd->SetDefaultValue(false);
  disableEvapFromFileACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetEvapAMaxAngleLab";
  aLine = commandName.c_str();
  AlphaMaxThetaLab = new G4UIcmdWithADoubleAndUnit(aLine,this);
  AlphaMaxThetaLab->SetGuidance("Set Max Lab angle for evaporated alphas");
  AlphaMaxThetaLab->SetParameterName("choice",false);
  AlphaMaxThetaLab->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetEvapAMinAngleLab";
  aLine = commandName.c_str();
  AlphaMinThetaLab = new G4UIcmdWithADoubleAndUnit(aLine,this);
  AlphaMinThetaLab->SetGuidance("Set Min Lab angle for evaporated alphas");
  AlphaMinThetaLab->SetParameterName("choice",false);
  AlphaMinThetaLab->AvailableForStates(G4State_PreInit,G4State_Idle);

  
  commandName = directoryName + "setPfe";
  aLine = commandName.c_str();
  PfeCmd = new G4UIcmdWithADouble(aLine,this);
  PfeCmd->SetGuidance("Set fusion-evaporation probability.");
  PfeCmd->SetParameterName("choice",false);
  PfeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setPtr";
  aLine = commandName.c_str();
  PtrCmd = new G4UIcmdWithADouble(aLine,this);
  PtrCmd->SetGuidance("Set transfer probability.");
  PtrCmd->SetParameterName("choice",false);
  PtrCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setPclx";
  aLine = commandName.c_str();
  PclxCmd = new G4UIcmdWithADouble(aLine,this);
  PclxCmd->SetGuidance("Set coulex probability.");
  PclxCmd->SetParameterName("choice",false);
  PclxCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setPff";
  aLine = commandName.c_str();
  PffCmd = new G4UIcmdWithADouble(aLine,this);
  PffCmd->SetGuidance("Set fusion-fission probability.");
  PffCmd->SetParameterName("choice",false);
  PffCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setPif";
  aLine = commandName.c_str();
  PifCmd = new G4UIcmdWithADouble(aLine,this);
  PifCmd->SetGuidance("Set incomplete fission probability.");
  PifCmd->SetParameterName("choice",false);
  PifCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  
  directoryName = name + "/generator/emitter/BeamOut/Q/";
  aLine = commandName.c_str();
  QDir = new G4UIdirectory(directoryName);
  QDir->SetGuidance("Charge state control for the outgoing beam.");

  commandName = directoryName + "SetNumberOfChargeStates";
  aLine = commandName.c_str();
  NQCmd = new G4UIcmdWithAnInteger(aLine,this);
  NQCmd->SetGuidance("Set number of charge states for the outgoing beam.");
  NQCmd->SetParameterName("choice",false);
  NQCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "ChargeStateSelect";
  aLine = commandName.c_str();
  SQCmd = new G4UIcmdWithAnInteger(aLine,this);
  SQCmd->SetGuidance("Select a charge state to be setup.");
  SQCmd->SetParameterName("choice",false);
  SQCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Charge";
  aLine = commandName.c_str();
  SCCmd = new G4UIcmdWithAnInteger(aLine,this);
  SCCmd->SetGuidance("Set charge for the charge state to be setup.");
  SCCmd->SetParameterName("choice",false);
  SCCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "UnReactedFraction";
  aLine = commandName.c_str();
  QUFCmd = new G4UIcmdWithADouble(aLine,this);
  QUFCmd->SetGuidance("Set a fraction of the selected charge state in the unreacted beam");
  QUFCmd->SetParameterName("choice",false);
  QUFCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "ReactedFraction";
  aLine = commandName.c_str();
  QRFCmd = new G4UIcmdWithADouble(aLine,this);
  QRFCmd->SetGuidance("Set a fraction of the selected charge state in the reacted beam");
  QRFCmd->SetParameterName("choice",false);
  QRFCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "KE";
  aLine = commandName.c_str();
  QKECmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  QKECmd->SetGuidance("Set kinetic energy of the central S800 trajectory for the selected charge state");
  QKECmd->SetParameterName("choice",false);
  QKECmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "KEu";
  aLine = commandName.c_str();
  QKEuCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  QKEuCmd->SetGuidance("Set kinetic energy per nucleon of the central S800 trajectory for the selected charge state");
  QKEuCmd->SetParameterName("choice",false);
  QKEuCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


}

Outgoing_Beam_Messenger::~Outgoing_Beam_Messenger()
{
  delete BeamOutDir;
  delete tauCmd;
  delete ExCmd;
  delete PartnerExCmd;
  delete projExCmd;
  delete TEx1Cmd;
  delete TEx2Cmd;
  delete CExFCmd;
  delete TExFCmd;
  delete RepCmd;
  delete DZCmd;
  delete DACmd;
  delete QDir;
  delete ZCmd;
  delete ACmd;
  delete NQCmd;
  delete SQCmd;
  delete SCCmd;
  delete QUFCmd;
  delete QRFCmd;
  delete QKECmd;
  delete QKEuCmd;
  delete aFileCmd;
  delete phiCmd;
  delete thCmd;
  delete spartNCmd;
  delete spartPCmd;
  delete spartACmd;
  delete epartNCmd; 
  delete epartPCmd; 
  delete epartACmd; 
  delete npartNCmd; 
  delete npartPCmd; 
  delete npartACmd; 
  delete enableEvapFromFileNCmd;
  delete disableEvapFromFileNCmd;
  delete enableEvapFromFilePCmd;
  delete disableEvapFromFilePCmd;
  delete enableEvapFromFileACmd;
  delete disableEvapFromFileACmd;
  delete AlphaMinThetaLab;
  delete AlphaMaxThetaLab;
  delete PfeCmd;
  delete PtrCmd;
  delete PffCmd;
  delete PifCmd;
  delete PclxCmd;
}


void Outgoing_Beam_Messenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == ACmd ) {
    myTarget->setA(ACmd->GetNewIntValue(newValue));
  }
  if( command == ZCmd ) {
    myTarget->setZ(ZCmd->GetNewIntValue(newValue));
  }
  if( command == DACmd ) {
    myTarget->setDA(DACmd->GetNewIntValue(newValue));
  }
  if( command == DZCmd ) {
    myTarget->setDZ(DZCmd->GetNewIntValue(newValue));
  }
  if( command == ExCmd ) {
    myTarget->setEx(newValue);
  }
  if( command == PartnerExCmd ) {
    myTarget->setPartnerEx(newValue);
  }
  if( command == projExCmd ) {
    myTarget->setProjEx(projExCmd->GetNewDoubleValue(newValue));
  }
  if( command == TEx1Cmd ) {
    myTarget->setTarEx1(TEx1Cmd->GetNewDoubleValue(newValue));
  }
  if( command == TEx2Cmd ) {
    myTarget->setTarEx2(TEx2Cmd->GetNewDoubleValue(newValue));
  }
  if( command == CExFCmd ) {
    myTarget->setCFrac(CExFCmd->GetNewDoubleValue(newValue));
  }
  if( command == TExFCmd ) {
    myTarget->setTFrac(TExFCmd->GetNewDoubleValue(newValue));
  }
  if( command == tauCmd ) {
    myTarget->settau(tauCmd->GetNewDoubleValue(newValue));
  }
  if( command == aFileCmd ) {
    myTarget->SetAngDistFile(newValue);
  }
  if( command == phiCmd ) {
    G4double th1, th2;
    sscanf( newValue, "%lf %lf", &th1, &th2);
    myTarget->setPhiRange(th1,th2);
  }
  if( command == thCmd ) {
    G4double th1, th2;
    sscanf( newValue, "%lf %lf", &th1, &th2);
    myTarget->setThRange(th1,th2);
  }
  if( command == RepCmd ) {
    myTarget->Report();
  }
  
  if( command == npartNCmd ) {
    myTarget->SetNumEvapN(npartNCmd->GetNewIntValue(newValue)); 
  }
  if( command == npartPCmd ) {
    myTarget->SetNumEvapP(npartPCmd->GetNewIntValue(newValue)); 
  }
  if( command == npartACmd ) {
    myTarget->SetNumEvapA(npartACmd->GetNewIntValue(newValue)); 
  }

  if( command == epartNCmd ) {
    myTarget->SetEnEvapN(epartNCmd->GetNewDoubleValue(newValue));
  }
  if( command == spartNCmd ) {
    myTarget->SetSigEvapN(spartNCmd->GetNewDoubleValue(newValue));
  }
  if( command == epartPCmd ) {
    myTarget->SetEnEvapP(epartPCmd->GetNewDoubleValue(newValue));
  }
  if( command == spartPCmd ) {
    myTarget->SetSigEvapP(spartPCmd->GetNewDoubleValue(newValue));
  }
  if( command == epartACmd ) {
    myTarget->SetEnEvapA(epartACmd->GetNewDoubleValue(newValue));
  }
  if( command == spartACmd ) {
    myTarget->SetSigEvapA(spartACmd->GetNewDoubleValue(newValue));
  }
  
  if( command == NQCmd ) {
    myTarget->SetNQ(NQCmd->GetNewIntValue(newValue));
  }
  if( command == SQCmd ) {
    myTarget->SelectQ(SQCmd->GetNewIntValue(newValue));
  }
  if( command == SCCmd ) {
    myTarget->SetQCharge(SCCmd->GetNewIntValue(newValue));
  }
  if( command == QUFCmd ) {
    myTarget->SetQUnReactedFraction(QUFCmd->GetNewDoubleValue(newValue));
  }
  if( command == QRFCmd ) {
    myTarget->SetQReactedFraction(QRFCmd->GetNewDoubleValue(newValue));
  }
  if( command == QKECmd ) {
    myTarget->SetQKE(QKECmd->GetNewDoubleValue(newValue));
  }
  if( command == QKEuCmd ) {
    myTarget->SetQKEu(QKEuCmd->GetNewDoubleValue(newValue));
  }
  if( command == enableEvapFromFileNCmd ) {
    myTarget->SetEvapFromFileN(enableEvapFromFileNCmd->GetNewBoolValue(newValue));
  }
  if( command == enableEvapFromFilePCmd ) {
    myTarget->SetEvapFromFileP(enableEvapFromFilePCmd->GetNewBoolValue(newValue));
  }
  if( command == enableEvapFromFileACmd ) {
    myTarget->SetEvapFromFileA(enableEvapFromFileACmd->GetNewBoolValue(newValue));
  }
  if( command == disableEvapFromFileNCmd ) {
    myTarget->SetEvapFromFileN(disableEvapFromFileNCmd->GetNewBoolValue(newValue));
  }
  if( command == disableEvapFromFilePCmd ) {
    myTarget->SetEvapFromFileP(disableEvapFromFilePCmd->GetNewBoolValue(newValue));
  }
  if( command == disableEvapFromFileACmd ) {
    myTarget->SetEvapFromFileA(disableEvapFromFileACmd->GetNewBoolValue(newValue));
  }
  if( command == PfeCmd ) {
    myTarget->SetPfe(PfeCmd->GetNewDoubleValue(newValue));
  }
  if( command == PtrCmd ) {
    myTarget->SetPtr(PtrCmd->GetNewDoubleValue(newValue));
  }
  if( command == PclxCmd ) {
    myTarget->SetPclx(PclxCmd->GetNewDoubleValue(newValue));
  }
  if( command == PffCmd ) {
    myTarget->SetPff(PffCmd->GetNewDoubleValue(newValue));
  }
  if( command == PifCmd ) {
    myTarget->SetPif(PifCmd->GetNewDoubleValue(newValue));
  }

  if(command==AlphaMaxThetaLab){
    myTarget->SetEvapAMaxAngleLab(AlphaMaxThetaLab->
				  GetNewDoubleValue(newValue));
  }
  if(command==AlphaMinThetaLab){
    myTarget->SetEvapAMinAngleLab(AlphaMinThetaLab->
				  GetNewDoubleValue(newValue));
  }
}

