/////////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides an implementation of the abstract AgataEmission class. In this case
/// variable multiplicity events can be treated. Event structure and sequence are decoded
/// from a formatted text file. A nuclear reaction is considered to calculate
/// the position and velocity of the source, which is recomputed following each particle 
/// emission so that momentum and energy are conserved. This is done through an
/// AgataExternalEmitter object. Momentum conservation can be overridden to treat 
/// "beta-decay-like" experiments.
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataExternalEmission_h
#define AgataExternalEmission_h 1

#include "AgataDefaultGenerator.hh"
#include "AgataExternalEmitter.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"

using namespace std;

class AgataEventAction;
class AgataExternalEmitter;
class AgataExternalEmissionMessenger;
// For AGATA-PRISMA: 
class G4Transportation;

class AgataExternalEmission : protected AgataEmission
{
  public:
     AgataExternalEmission( G4String, G4bool, G4bool, G4String );
    ~AgataExternalEmission();
    
  private:
    AgataExternalEmissionMessenger* myMessenger;

  private:
    AgataEventAction*          theEvent;
    AgataExternalEmitter*      theExternalEmitter;

  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// The following members specify the format of the file containing the events to be processed
  ///////////////////////////////////////////////////////////////////////////////////////////////
  private:
    G4int        emitterType;         //> format of the lines containing information on the source
    G4int        emittedType;         //> format of the lines containing information on the emitted particles

  ///////////////////////////////////
  /// Information on the input file
  ///////////////////////////////////
  private:
    G4String      iniPath;            //> directory where it lies
    G4String      evFileName;         //> name
    FILE         *evFile;             //> pointer

  private:
    char          bufferLine[256];    //> string where each line of the input file is copied

  ///////////////////////////////////////////
  /// Flag to control some verbosity for ///
  /// the output to file                 ///
  //////////////////////////////////////////
  private:
    G4bool        verbose;           //> true: "standard" output; false: "compact" output
  
  ////////////////////////////////////////////
  /// Information on the event processing
  ///////////////////////////////////////////
  private:
    G4bool        endOfRun;          //> true: run will be aborted at the next emitted particle
    G4bool        nucleusIsEmitted;  //> true: next emitted particle will be the residual nucleus
    G4bool        firstLine;         //> true: the first line describing a new event is being decoded
    G4bool        openedFile;        //> true: event file has been opened
    G4bool        beginOfEvents;     //> true: the begin-of-event tag has been reached decoding the input file
    G4bool        emitNuclei;        //> true: emission of residual nuclei is enabled
    G4bool        showStatus;        //> true: status is printed on-screen (disabled at class construction)
    G4bool        decodeEmitter;     //> true: lines concerning the source can be decoded from the input file
    G4bool        isBetaDecay;       //> true: momentum conservation is disabled
    
  private:
    G4int         readEvents;        //> number of events already read
    G4int         skipEvents;        //> number of events to be skipped (at the beginning of file)
    G4int         processedEvents;   //> number of events already processed
    G4int         targetEvents;      //> number of events which should be processed
    

  ////////////////////////////////////////////
  /// Information on the emitted objects
  ///////////////////////////////////////////
  private:
    G4int         nEmitted;         //> number of emitted objects
    G4int        *emittedLUT;       //> lookup table for the emitted objects (particle type --> index)
    G4int         maxIndex;         //> maximum index for the emitted objects

  ////////////////////////////////
  /// Information on the beam
  ////////////////////////////////
  private:
    G4int         aBeam;            //> mass number
    G4int         zBeam;            //> charge number
    G4double      eBeam;            //> energy

  ////////////////////////////////
  /// Information on the target
  ////////////////////////////////
  private:
    G4int         aTarg;            //> mass number
    G4int         zTarg;            //> charge number
    
  //////////////////////////////////////////////////////////////////
  /// Information on the emitted particle (changes event-by-event)
  //////////////////////////////////////////////////////////////////
  private:
    G4int         emittedAtomNum;   //> mass number    
    G4int         emittedMassNum;   //> charge number  
    G4ThreeVector emittedMomentum;  //> momentum         
    
  //////////////////////////////////////////////////////////////////
  /// Information on the residual nucleus (changes event-by-event)
  //////////////////////////////////////////////////////////////////
  private:
    G4int         emitterAtomNum;   //> mass number  
    G4int         emitterMassNum;   //> charge number
    
  private:
    G4bool        createdEmitted;

  //////////////////////////
  /// Command directory  ///
  //////////////////////////
  private:
    G4String      directoryName;     //> directory name


  ////////////////////////
  /// Private methods ///
  ///////////////////////
  
  private:
    void      ReadFileHeader  ( FILE *ifp );  //> decodes the input file header
    void      InitReaction    ( FILE *ifp );  //> retrieves the masses of beam and target and calculates the velocity of the
                                	      //>   residual nucleus
    void      InitEmitted     ( FILE *ifp );  //> allocates an array of AgataEmitted objects, each of them corresponding
                                	      //>  to a particle which can be emitted in the current run
    void      InitEmittedRange( FILE *ifp );  //> changes the angular emission range for the Emitted objects
     
  private:
    G4String  FetchEmittedName ( G4int  );   //> lookup table particle code --> particle name
    void      DecodeEmitted    ( char * );   //> decodes a line containing information on an emitted particle
    void      DecodeEmitter    ( char * );   //> decodes a line containing information on the source
    void      FetchEmitter     ();           //> sets the residual nucleus as the next emitted particle
    G4bool    IsHadron         ( G4int  );   //> true: particle corresponding to the argument is a hadron

  ////////////////////////
  /// Public methods ///
  ///////////////////////

  ////////////////////////
  /// Begin/end of run
  ///////////////////////
  public:
    void      BeginOfRun();
    void      EndOfRun();

  ////////////////////////
  /// Begin/end of event
  ///////////////////////
  public:
    void      BeginOfEvent();
    void      EndOfEvent();
    
  ///////////////////////////////////
  /// Set beam characteristics
  /////////////////////////////////
  private:
    G4int     InitZBeam      ( FILE *ifp );
    G4int     InitABeam      ( FILE *ifp );
    G4double  InitEBeam      ( FILE *ifp );
    
  ///////////////////////////////////
  /// Set target characteristics
  /////////////////////////////////
  private:
    G4int     InitZTarg      ( FILE *ifp );
    G4int     InitATarg      ( FILE *ifp );
    
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// Stores name, energy, direction, polarizatin, time of emission of the next emitted particle
  ///////////////////////////////////////////////////////////////////////////////////////////////
  public:
    void      NextParticle();  
        
  public:
    void      GetStatus();
    void      PrintToFile( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV );
    void      PrintToFile( G4String *sheader, G4double=1.*mm, G4double=1.*keV );

  public:
    inline G4String GetBeginOfEventTag();

  public:
    G4String GetEventHeader    ( G4double=1.*mm );
    G4String GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
        
  ///////////////////////////////////
  /// Methods for the messenger
  ///////////////////////////////////
  public:
    void      SetEventFile   ( G4String );
    void      SetEmitNuclei  ( G4bool   );
    void      SetBetaDecay   ( G4bool   );
    void      SetSkipEvents  ( G4int    );
    void      SetTargetEvents( G4int    );
    void      SetVerbose     ( G4bool   );
   
  /////////////////////////////////////////////
  /// Inline "get" methods: recoil nucleus
  /////////////////////////////////////////////
  public:
    inline G4int         GetEmitterAtomNum()      { return emitterAtomNum;   };
    inline G4int         GetEmitterMassNum()      { return emitterMassNum;   };

  /////////////////////////////////////////////
  /// Inline "get" methods: emitted particle
  /////////////////////////////////////////////
  public:
    inline G4ThreeVector GetEmittedMomentum()     { return emittedMomentum;  };
    inline G4int         GetEmittedAtomNum()      { return emittedAtomNum;   };
    inline G4int         GetEmittedMassNum()      { return emittedMassNum;   };

  // AGATA-PRISMA
  public:
    inline G4int GetEmitterIonicCharge() {return emitterIonicCharge;}
  private:
    G4int emitterIonicCharge;
  
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;

class AgataExternalEmissionMessenger: public G4UImessenger
{
  public:
    AgataExternalEmissionMessenger(AgataExternalEmission*, G4bool, G4String);
   ~AgataExternalEmissionMessenger();

  private:
    G4bool                     hadrons;
    
  private:
    AgataExternalEmission*     myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             mySubDirectory;
    G4UIcmdWithAString*        SetEventFileCmd;
    G4UIcmdWithABool*          EnableBetaCmd;
    G4UIcmdWithABool*          DisableBetaCmd;
    G4UIcmdWithABool*          EnableNucleiCmd;
    G4UIcmdWithABool*          DisableNucleiCmd;
    G4UIcmdWithABool*          VerboseOutputCmd;
    G4UIcmdWithABool*          ReducedOutputCmd;
    G4UIcmdWithAnInteger*      SetSkipEventsCmd;
    G4UIcmdWithAnInteger*      SetTargetEventsCmd;

  public:
    void SetNewValue(G4UIcommand*, G4String);

};


#endif
