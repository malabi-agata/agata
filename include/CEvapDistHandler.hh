////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the generation of random 
/// energy distib of one particle evaprorated in fusion process (Cascade output)  100729 cate
////////////////////////////////////////////////////////////////////////////////////////

#ifndef EvapDistHandler_h
#define EvapDistHandler_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>
//#include <ios>
//#include <iostream>
//#include <istream>
#include <fstream>

using namespace std;
#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

class EvapDistHandler
{
  public:
    EvapDistHandler( G4String );
    ~EvapDistHandler();
    
  private:
    void ReadDistribution(std::ifstream& inFile);  
    
  public:
    void ReNew           (G4String);  
    
  public:
    G4double      GetEnergy   ( );
    G4ThreeVector GetDirection( G4double );
    
  public:
    void          SetPhiRange ( G4double, G4double );
    void          SetThRange  ( G4double, G4double );
    
//   private:
//     void InitData( G4double, G4double, G4double, G4double, G4double, G4double );    

  // range in energy
  private:
    double E_min;
    double E_max;
    double E_dif;
    
  // range in angle
  private:
    double th_min;
    double th_max;
    double th_dif;
    
  // phi range for emission
  private:
    double phi_min;
    double phi_dif;
    
  // theta range for emission
  private:
    double the_min;
    double the_dif;

  private:
    G4bool goodDistribution;

  private:
    G4bool fixedTheta;
    
  // angular distribution data in differential and integral form  
  private:
//     std::vector<std::vector<double> > difEvapDist;  
//     std::vector<std::vector<double> > intEvapDist; 
//     std::vector<double>               eneDifDist; 
//     std::vector<double>               eneIntDist; 
  std::vector<double>               kinEnVal;
  std::vector<double>               kinEnPb;
  std::vector<double>               kinEnPbNorm;
  std::vector<double>               intEvapDist;
    
  public:
    inline G4bool IsGoodDistribution() { return goodDistribution; };   


};

#endif
