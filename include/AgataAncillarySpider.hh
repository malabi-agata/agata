#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillarySpider_h
#define AgataAncillarySpider_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "G4GDMLParser.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillarySpiderMessenger;

class AgataAncillarySpider : public AgataAncillaryScheme
{
    
public:
    AgataAncillarySpider(G4String,G4String);
    ~AgataAncillarySpider();
    
private:
    AgataAncillarySpiderMessenger* myMessenger;

  ///////////////////////////////////////
  /// Methods required by the Messenger
  //////////////////////////////////////
public:
  void   SetRotation   (G4ThreeVector);
//  void   SetGeometry   (G4int);


private:
  G4ThreeVector spider_rotation    ;
//  G4int         spider_geometry    ;

    
    /////////////////////////////
    /// Material and its name
    ////////////////////////////
private:
    G4Material* FindMaterial (G4String Name);
    G4int  FindMaterials           ();
    bool   GetSupport              ();
    bool   GetSegments              ();
    G4String     nam_vacuum;
    G4Material  *mat_vacuum;
    
    G4String     nam_aluminium;
    G4String     gdml_path;
    G4Material  *mat_aluminium;
    
    G4String     nam_silicon;
    G4Material  *mat_silicon;
    G4GDMLParser gdmlparser;
    std::map<int,G4LogicalVolume *> pSpiderSupport;
    std::map<int,G4LogicalVolume *> pSpiderSegments;
    ///////////////////////////////////////////
    /// Methods required by AncillaryScheme
    ///////////////////////////////////////////
public:
    
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();
    
    
public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void  WriteHeader  ( G4String *sheader, G4double=1.*mm );
    void   ShowStatus              ();
    
public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector )  { return 0; };
    inline G4int GetCrystalType    ( G4int )                        { return -1; };
    
};


#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
//class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;

class AgataAncillarySpiderMessenger: public G4UImessenger
{
  public:
    AgataAncillarySpiderMessenger(AgataAncillarySpider*,G4String);
   ~AgataAncillarySpiderMessenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataAncillarySpider*        myTarget      ;
  G4UIdirectory*             SpiderDir       ;
  G4UIcmdWith3Vector*        setSpiderRotCmd ;
  //G4UIcmdWithAnInteger*      setSpiderGeoCmd ;
};



#endif

#endif
