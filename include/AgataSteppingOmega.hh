/////////////////////////////////////////////////////////////
/// This class is used only during solid angle calculation.
/// Since geantinos do not interact, they can only be
/// "detected" by examining the step!
////////////////////////////////////////////////////////////
#ifndef AgataSteppingOmega_h
#define AgataSteppingOmega_h 1

#include "G4UserSteppingAction.hh"
#include <vector>
#include <algorithm>
#include <sstream>

using namespace std;

class AgataDetectorConstruction;

class AgataSteppingOmega : public G4UserSteppingAction
{
  public:
  AgataSteppingOmega();
   ~AgataSteppingOmega() {};
    
  private:
    AgataDetectorConstruction*  theDetector;
  std::vector<int> DeadDetectors;
  /////////////////////////////////////////////////////////
  /// This method is needed by G4UserSteppingAction
  ////////////////////////////////////////////////////////
  public:
    void UserSteppingAction(const G4Step*);
};

#endif
