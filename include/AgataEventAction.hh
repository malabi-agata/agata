////////////////////////////////////////////////////////////////////////////////////////////////////////
/// This class, inheriting from G4UserEventAction, handles the printout of event number at beginning
/// of event. Its main task is to format the information which should be sent to the output file.
/// In case the on-line analysis is enabled, the same information is sent to the on-line analysis.
///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef AgataEventAction_h
#define AgataEventAction_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4UserEventAction.hh"
#include "AgataHitDetector.hh"

//Chen:
#include "TTree.h"
#include <vector> 

using namespace std;

class G4Event;
class AgataAnalysis;
class AgataRunAction;
class AgataGeneratorAction;
class AgataDetectorConstruction;
class AgataEventActionMessenger;
class AgataSteppingPrisma;

class AgataEventAction : public G4UserEventAction
{
public:
  AgataEventAction(AgataAnalysis*,G4String);
  ~AgataEventAction();

private:
  AgataAnalysis*               theAnalysis;
  AgataDetectorConstruction*   theDetector;
  AgataGeneratorAction*        theGenerator;
  AgataRunAction*              theRun;
  AgataEventActionMessenger*   myMessenger;
  AgataSteppingPrisma*         thestepping;
private:
  G4int    eventID;      //> event number         
  G4int    interval;     //> interval for event number printout        
  G4bool   WriteRandomInfoToFile; /* true; Store random generator status 
				     and event number every interval events*/
private:
  G4bool   writeAll;     //> false: header of events without interactions within the active parts
  //>        of the geometry will be skipped
  G4bool   drawRays;     //> true: particle tracks will be sketched (if visualization is enabled)
  G4bool   drawHits;     //> true: particle hits will be sketched (if visualization is enabled)

  ///////////////////////////////////////////
  /// Information about the output file
  //////////////////////////////////////////
private:
  G4String outMask;        //> bit mask telling which parameters are actually written out
  //    G4bool   writeData[9];   //> the same bit mask translated into boolean variables
  G4bool   writeData[10];   //> changed to 10 for number of Hits for Sigma (the same bit mask translated into boolean variables)
  G4bool   pulseShape;     //> true: data will be written out in PULSE SHAPE format
  //> false (default): data will be written out in TRACKING format
private:
  G4bool   mgsFormat;      //> true: if also pulseShape is true, data will be written out with MGS reference frame
  G4double packDist;       //> interaction points closer than packDist will be packed together
  G4double unitLength;     //> unit length for the output file
  G4double unitEnergy;     //> unit energy for the output file
private:
  G4double  lightThreshold;  //> threshold on the light emitted for the neutron detection in NW and NEDA
  std::map<int,G4double> neutron_above_thr;  
private:
  char     *aLine;         //> buffer line where the information is formatted before sending to the output file

  //////////////////////
  /// Private methods
  /////////////////////
  
  //////////////////////////////////////////////////////////////////////////////////////////
  /// Prepare the next line to be sent to the output file according to the output_mask
  //////////////////////////////////////////////////////////////////////////////////////////
private:
  //    G4String PrepareLine( G4int, G4double, G4ThreeVector, G4ThreeVector, G4int, G4double, G4int );
  G4String PrepareLine( G4int, G4double, G4ThreeVector, G4ThreeVector, G4int, G4double, G4int, G4int, G4double ); // added for NeutronDet det

// Chen:
  private:
    void     ClearHit();
    void     WriteHit( G4int, G4double, G4ThreeVector, G4ThreeVector, G4int, G4double, G4int );
    void     WriteParticleInfo( const G4Event*, G4double, G4double );
    void     WriteEventInfo(  G4double );

  /////////////////////////
  /// Public methods
  /////////////////////////

  //////////////////////////////////////////////////////
  /// Begin/end of event (needed by G4UserEventAction)
  //////////////////////////////////////////////////////
public:
  void BeginOfEventAction (const G4Event*);
  void EndOfEventAction   (const G4Event*);

  ////////////////////////////
  /// Begin of run
  ////////////////////////////
public:
  void BeginOfRun();

public:
  void ShowStatus  ();
  void WriteHeader ( std::ostream &outFileLMD );
// Chen:
     void WriteHeader ( G4String *sheader );
     public:
      void InitGTree   ();
     ////////////////////////////
     /// End of run
     ////////////////////////////
     public:
       void EndOfRun();


  /////////////////////////////////////////////////////////////////////////////
  /// This method is used by AgataExternalEmission, so that it does not need
  /// to look for the pointer to the AgataAnalysis class
  /////////////////////////////////////////////////////////////////////////////
public:
  void FlushAnalysis ();

 private:
    TTree*                  ogtree;
  
    G4int                   ievent;
    // event header
    G4double                Beta_recoil;
    G4double                vx_recoil;
    G4double                vy_recoil;
    G4double                vz_recoil;
    G4double                px_recoil;
    G4double                py_recoil;
    G4double                pz_recoil;
    G4double                Tdecay_recoil;
    // particle headr
    G4int                   partType;
    G4double                e_part;
    G4double                vx_part;
    G4double                vy_part;
    G4double                vz_part;
    G4int                   ipart;
    // hit informatin
    G4int                   nhits;
    vector<G4int>           vndet;
    vector<G4double>        vedep;
    vector<G4double>        x_lab;
    vector<G4double>        y_lab;
    vector<G4double>        z_lab;
    vector<G4double>        x_det;
    vector<G4double>        y_det;
    vector<G4double>        z_det;
    vector<G4double>        x_mgs;
    vector<G4double>        y_mgs;
    vector<G4double>        z_mgs;
    vector<G4int>           vnseg;
    vector<G4double>        vtime;
    vector<G4int>           vinter;
    vector<G4int>           vnhits;
    vector<G4int>           vlight;

  
  /////////////////////////////////
  /// Methods for the messenger
  ////////////////////////////////
public:
  void SetWriteAll        ( G4bool   );
  void SetDrawRays        ( G4bool   );
  void SetDrawHits        ( G4bool   );

public:
  void SetPackDist        ( G4double );
  void SetUnitLength      ( G4double );
  void SetUnitEnergy      ( G4double );

public:
  void SetInterval        ( G4int    );
  void SetWriteRandomInfoToFile(G4bool);
  ////////////////////////////////////////////////////////////////
  //// Methods selecting the information sent to the output file
  ////////////////////////////////////////////////////////////////
public:
  void SetOutMask         ( G4String );
  void SetEnableMgsFormat ( G4bool );
  void SetEnableNDet      ( G4bool );
  void SetEnableEner      ( G4bool );
  void SetEnablePosA      ( G4bool );
  void SetEnablePosI      ( G4bool );
  void SetEnablePosM      ( G4bool );
  void SetEnableNSeg      ( G4bool );
  void SetEnableTime      ( G4bool );
  void SetEnableInte      ( G4bool );
  void SetEnableNhits     ( G4bool ); // added for Sigma det
  void SetEnableLight     ( G4bool ); // added for NeutronWall and NEDA

  void SetNedaLightThr            ( G4double );
  void SetNedaThrTimeGate         ( G4double );
  void SetNedaIntegrationTimeGate ( G4double );
  
  ///////////////////////////////////////////////////////////////////////////
  //// Methods selecting the information sent to the output file (shortcuts)
  ///////////////////////////////////////////////////////////////////////////
public:
  void DataForTracking    ();
  void DataForPulseShape  ();

public:
  inline G4double GetUnitLength () { return unitLength; };
  inline G4double GetUnitEnergy () { return unitEnergy; };

public:
  inline G4int    GetEventID    () { return eventID;    };
  inline G4int    GetInterval   () { return interval;   };
  inline G4bool GetWriteRandomInfoToFile(){return WriteRandomInfoToFile;}

  G4double GetNedaLightThr()           {return neda_light_thr;};
  G4double GetNedaThrTimeGate()        {return neda_thr_time_gate;};
  G4double GetNedaIntegrationTimeGate(){return neda_integration_time_gate;};
  

private:
  G4double  neda_light_thr;
  G4double  neda_thr_time_gate;
  G4double  neda_integration_time_gate;
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;

class AgataEventActionMessenger: public G4UImessenger
{
public:
  AgataEventActionMessenger(AgataEventAction*,G4String);
  ~AgataEventActionMessenger();
    
private:
  AgataEventAction*          myTarget;
  G4UIdirectory*             myDirectory;
  G4UIdirectory*             myFileDirectory;
  G4UIcmdWithABool*          EnableAllCmd;
  G4UIcmdWithABool*          DisableAllCmd;
  G4UIcmdWithABool*          EnableRaysCmd;
  G4UIcmdWithABool*          DisableRaysCmd;
  G4UIcmdWithABool*          EnableHitsCmd;
  G4UIcmdWithABool*          DisableHitsCmd;
  G4UIcmdWithADouble*        SetPackDistCmd;
  G4UIcmdWithoutParameter*   StatusCmd;   
  G4UIcmdWithADouble*        SetUnitLengthCmd;
  G4UIcmdWithADouble*        SetUnitEnergyCmd;
  G4UIcmdWithAnInteger*      SetIntervalCmd;
  G4UIcmdWithABool*          SetWriteRandomInfoCmd;
  G4UIcmdWithAString*        SetOutMaskCmd;
  G4UIcmdWithABool*          EnableMgsFormatCmd;
  G4UIcmdWithABool*          EnableNDetCmd;
  G4UIcmdWithABool*          EnableEnerCmd;
  G4UIcmdWithABool*          EnablePosACmd;
  G4UIcmdWithABool*          EnableNSegCmd;
  G4UIcmdWithABool*          EnablePosICmd;
  G4UIcmdWithABool*          EnablePosMCmd;
  G4UIcmdWithABool*          EnableTimeCmd;
  G4UIcmdWithABool*          EnableInteCmd;
  G4UIcmdWithABool*          EnableNhitsCmd;  // added for Sigma det
  G4UIcmdWithABool*          EnableLightCmd; // added for NeutronWall and Neda det
  G4UIcmdWithABool*          DisableMgsFormatCmd;
  G4UIcmdWithABool*          DisableNDetCmd;
  G4UIcmdWithABool*          DisableEnerCmd;
  G4UIcmdWithABool*          DisablePosACmd;
  G4UIcmdWithABool*          DisableNSegCmd;
  G4UIcmdWithABool*          DisablePosICmd;
  G4UIcmdWithABool*          DisablePosMCmd;
  G4UIcmdWithABool*          DisableTimeCmd;
  G4UIcmdWithABool*          DisableInteCmd;
  G4UIcmdWithABool*          DisableNhitsCmd; // added for Sigma det
  G4UIcmdWithABool*          DisableLightCmd; // added for NeutronWall and Neda det
    
  G4UIcmdWithoutParameter*   TrackDataCmd;   
  G4UIcmdWithoutParameter*   PSADataCmd;   

  G4UIcmdWithADoubleAndUnit* setNedaLightThrCmd;
  G4UIcmdWithADoubleAndUnit* setNedaThrTimeGateCmd;
  G4UIcmdWithADoubleAndUnit* setNedaIntegrationTimeGateCmd;
  
  
public:
  void SetNewValue(G4UIcommand*, G4String);
};
#endif

    
