#ifndef AgataGeneratorAction_h
#define AgataGeneratorAction_h 1

#include "AgataEmitter.hh"
#include "AgataDistributedParameters.hh"
#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

using namespace std;

#ifdef G4V10
using namespace CLHEP;
#endif

class G4Event;
class AgataGeneratorAction;

/////////////////////////////////////////////////////////////////////////////////////
/// This abstract class provides only the interface to the specific implementations
/// of user-defined event generators. 
///////////////////////////////////////////////////////////////////////////////////// 
class AgataGeneration
{
  public:
     AgataGeneration(){};
  virtual ~AgataGeneration(){};

  /////////////////////////////////////////////////////////////////////
  /// Virtual methods which should be implemented
  ////////////////////////////////////////////////////////////////////
  public:
    virtual void BeginOfRun   ()			                                     = 0;
    virtual void EndOfRun     ()			                                     = 0;
    
  public:
    virtual void BeginOfEvent ()			                                     = 0;
    virtual void EndOfEvent   ()			                                     = 0;
  virtual void GetEventInfo(std::ostream &eventinfo) = 0;
  public:
    virtual void GetStatus    ( )			                                     = 0;
    virtual void PrintToFile  ( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV ) = 0;
    virtual void PrintToFile  ( G4String *sheader, G4double=1.*mm, G4double=1.*keV )         = 0;   
    
  public:
    virtual G4String GetBeginOfEventTag ()						     = 0;
    virtual G4String GetEventHeader     ( G4double = 1.*mm )				     = 0;
    virtual G4String GetParticleHeader  ( const G4Event*, G4double=1.*mm, G4double=1.*keV )  = 0;
    
  public:
    virtual G4bool   IsStartOfEvent ()                                                       = 0;
    virtual G4bool   IsEndOfEvent   ()                                                       = 0;
    virtual G4bool   IsAbortRun     ()                                                       = 0;

  public:
// added by Chen:
    virtual G4ThreeVector GetEmitterVelocity ()                                              = 0;
    virtual G4ThreeVector GetEmitterPosition ()                                              = 0;
    virtual G4double      GetEmitterBeta     ()                                              = 0;
    virtual G4int         GetCascadeOrder    ()                                              = 0;
    virtual G4bool        IsSourceLongLived  ()                                              = 0;
    virtual G4double      GetCascadeDeltaTime()                                              = 0;
    virtual G4double      GetCascadeTime     ()                                              = 0;
/**/
    
  public:
    virtual G4String GetAbortMessage()						             = 0;
    
  public:
    virtual G4int    GetCascadeMult ()						             = 0;
    
  public:
    virtual void     GeneratePrimaries ( G4Event* )				             = 0;
};

/////////////////////////////////////////////////////////////////////////////
/// This class provides the implementation of G4VUserPrimaryGeneratorAction.
/// The actual event generation is provided by an AgataGeneration object
/// where the complication is hidden.
////////////////////////////////////////////////////////////////////////////
class AgataGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    //AgataGeneratorAction(G4String, G4int, G4bool, G4bool, G4String, G4bool);    // if passes boolean Addon (Marc)
    //AgataGeneratorAction(G4String, G4int, G4bool, G4bool, G4String);    
    AgataGeneratorAction(G4String, G4int, G4bool,G4bool, G4bool, G4String);    
   ~AgataGeneratorAction();
    
  private:
    G4bool                                internal;       //> true: built-in generator; false: events decoded from file
    G4bool                                root;       //> true: produce root output file
    G4bool                                hadrons;        //> true: hadrons can be emitted
    G4bool                                usePola;        //> true: polarized gammas can be considered

  private:
    G4String                              iniPath;        //> directory where the data files are stored

  private:
    AgataGeneration*                      theGeneration;    
  /////////////////////////////////////////////////////////////
  /// This method is needed by G4VUserPrimaryGeneratorAction 
  ////////////////////////////////////////////////////////////  
  public:
    void GeneratePrimaries(G4Event*);

  ///////////////////////////////////////////////
  /// Begin/end of run methods
  /////////////////////////////////////////////
  public:
    void BeginOfRun();
    void EndOfRun();
    
  //////////////////////////////////////////////////
  /// Begin/end of event (single particle) methods
  /////////////////////////////////////////////////
  public:
    void BeginOfEvent();
    void EndOfEvent();

  public:
  void GetEventInfo(std::ostream &eventinfo) {
    theGeneration->GetEventInfo(eventinfo);
  }
    void GetStatus   ();
    void PrintToFile ( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV  );
    void PrintToFile ( G4String *sheader, G4double=1.*mm, G4double=1.*keV  );

  public:
    inline G4bool        IsAbortRun()         { return theGeneration->IsAbortRun();          };
    inline G4String      GetAbortMessage()    { return theGeneration->GetAbortMessage();     };

  public:
    inline G4bool        IsStartOfEvent()     { return theGeneration->IsStartOfEvent();      };
    inline G4bool        IsEndOfEvent()       { return theGeneration->IsEndOfEvent();        };      

  public:
    inline G4ThreeVector GetEmitterVelocity() { return theGeneration->GetEmitterVelocity();  };
    inline G4ThreeVector GetEmitterPosition() { return theGeneration->GetEmitterPosition();  };
    inline G4double      GetEmitterBeta()     { return theGeneration->GetEmitterBeta();      };
    inline G4int         GetCascadeOrder()    { return theGeneration->GetCascadeOrder();     };
    inline G4bool        IsSourceLongLived()  { return theGeneration->IsSourceLongLived();   };
    inline G4double      GetCascadeDeltaTime(){ return theGeneration->GetCascadeDeltaTime(); };
    inline G4double      GetCascadeTime()     { return theGeneration->GetCascadeTime();      };

  public:
    inline G4int         GetCascadeMult()     { return theGeneration->GetCascadeMult();      };    
    
  public:
    inline G4String      GetBeginOfEventTag() { return theGeneration->GetBeginOfEventTag();  };
    
  public:
    inline G4String      GetEventHeader    ( G4double=1.*mm );
    inline G4String      GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV );


};

inline G4String AgataGeneratorAction::GetEventHeader ( G4double unitLength )
{
  return theGeneration->GetEventHeader( unitLength );  
}

inline G4String AgataGeneratorAction::GetParticleHeader ( const G4Event* evt, G4double unitLength, G4double unitEnergy )
{
  return theGeneration->GetParticleHeader( evt, unitLength, unitEnergy );  
}

#endif
