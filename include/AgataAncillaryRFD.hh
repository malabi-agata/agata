#ifdef ANCIL
#ifndef RFDDetectorConstruction_h
#define RFDDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Tubs.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4Material.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;


class AgataAncillaryRFDMessenger;


class AgataAncillaryRFD : public AgataAncillaryScheme
{
public:
  AgataAncillaryRFD(G4String,G4String);
  ~AgataAncillaryRFD();

private:
  G4bool  useIonGuide;
  G4bool  useRFDCones;
  G4bool  useTargetChamber;
  G4bool  useTarget;

  G4double RFDTargetChamber_Phi;
  G4double RFDTargetChamber_Thickness;

  G4double RFDIonGuide_Length;
  G4double RFDIonGuide_Thickness;
  G4double RFDIonGuide_Phi;

  G4double RFDMylarFoilsDistanceToTarget;
  G4double SensitivePartsMylarThickness;

  G4double LengthOfBellow;
  G4double LengthOfBaniak;

  G4double RFDOneCone_InnerPhiSt;
  G4double RFDOneCone_InnerPhiEnd;
  G4double RFDOneCone_Thickness;

  G4double RFDTarget_Thick;
  G4double RFDTarget_Diameter;
  G4double RFDTarget_Offset;

public:
  G4int FindMaterials();
  void GetDetectorConstruction();
  void InitSensitiveDetector();
  void Placement();
  void ShowStatus();
  void WriteHeader (std::ofstream &outFileLMD, G4double=1.0*mm);
  void WriteHeader  ( G4String *sheader, G4double=1.*mm );


public:
  G4int GetSegmentNumber(G4int,G4int,G4ThreeVector);
  inline G4int GetCrystalType  (G4int)			   {return -1;};

//  public:
//    inline G4String GetName () { return G4String("RFD"); };

public:
  void SetUseIonGuide      ( G4bool );
  void SetUseRFDCones      ( G4bool );
  void SetUseTargetChamber ( G4bool );
  void SetUseTarget        ( G4bool );

  void SetTargetChamberDiameter  ( G4double );
  void SetTargetChamberThickness ( G4double );
  void SetIonGuideLength         ( G4double );
  void SetIonGuideThickness      ( G4double );
  void SetIonGuideInnerDiameter  ( G4double );
  void SetRFDMylarFoilThickness  ( G4double );
  void SetRFDMylarFoilsDistanceToTarget ( G4double );
  void SetLengthOfBellow         ( G4double );
  void SetRFDTargetDiameter      ( G4double );
  void SetRFDTargetThickness     ( G4double );
  void SetRFDTargetMaterial      ( G4String );
  void SetRFDTargetOffset        ( G4double );

private:
  void PlaceMylar();
  void PlaceTargetChamber();
  void PlaceIonGuide();
  void PlaceRFDCones();
  void PlaceTarget();

  AgataAncillaryRFDMessenger* myMessenger;

  
private:
  G4String    matMylarRFDName;
  G4Material* matMylarRFD;

  G4String    matRFDTargetChamberName;
  G4Material* matRFDTargetChamber;

  G4String    matRFDIonsTubeName;
  G4Material* matRFDIonsTube;

  G4String    matRFDOneConeName;
  G4Material* matRFDOneCone;

  G4String    matRFDTargetName;
  G4Material* matRFDTarget;

private:
  G4double ZeroPositionX;
  G4double ZeroPositionY;
  G4double ZeroPositionZ;

private:
  G4double SensitivePartsAlThickness;
  G4double SensitivePartsMylarHalfThickness;
  G4double SensitivePartsAlHalfThickness;
  G4double SensitivePartsRmin;
  G4double SensitivePartsPhi;
  G4double SensitivePartsRmax;
  G4double SensitivePartsStartAngle;
  G4double SensitivePartsSegmentAngle;
  G4double SensitivePartsInnerRingMylarDistanceToTarget;
  G4double SensitivePartsMiddleRingMylarDistanceToTarget;
  G4double SensitivePartsOuterRingMylarDistanceToTarget;
  G4double SensitivePartsInnerRingTheta;
  G4double SensitivePartsMiddleRingTheta;
  G4double SensitivePartsOuterRingTheta;
  G4double ThetaIR;
  G4double ThetaMR;
  G4double ThetaOR;
  G4double SensitivePartInnerRingPhi1;
  G4double SensitivePartInnerRingPhi2;
  G4double SensitivePartInnerRingPhi3;
  G4double SensitivePartInnerRingPhi4;
  G4double SensitivePartInnerRingPhi5;
  G4double SensitivePartInnerRingPhi6;
  G4double SensitivePartMiddleRingPhi1;
  G4double SensitivePartMiddleRingPhi2;
  G4double SensitivePartMiddleRingPhi3;
  G4double SensitivePartMiddleRingPhi4;
  G4double SensitivePartMiddleRingPhi5;
  G4double SensitivePartMiddleRingPhi6;
  G4double SensitivePartOuterRingPhi1;
  G4double SensitivePartOuterRingPhi2;
  G4double SensitivePartOuterRingPhi3;
  G4double SensitivePartOuterRingPhi4;
  G4double SensitivePartOuterRingPhi5;
  G4double SensitivePartOuterRingPhi6;
  G4double SensitivePartInnerRingDet1MylarPositionX;
  G4double SensitivePartInnerRingDet1MylarPositionY;
  G4double SensitivePartInnerRingDet1MylarPositionZ;
  G4double SensitivePartInnerRingDet2MylarPositionX;
  G4double SensitivePartInnerRingDet2MylarPositionY;
  G4double SensitivePartInnerRingDet2MylarPositionZ;
  G4double SensitivePartInnerRingDet3MylarPositionX;
  G4double SensitivePartInnerRingDet3MylarPositionY;
  G4double SensitivePartInnerRingDet3MylarPositionZ;
  G4double SensitivePartInnerRingDet4MylarPositionX;
  G4double SensitivePartInnerRingDet4MylarPositionY;
  G4double SensitivePartInnerRingDet4MylarPositionZ;
  G4double SensitivePartInnerRingDet5MylarPositionX;
  G4double SensitivePartInnerRingDet5MylarPositionY;
  G4double SensitivePartInnerRingDet5MylarPositionZ;
  G4double SensitivePartInnerRingDet6MylarPositionX;
  G4double SensitivePartInnerRingDet6MylarPositionY;
  G4double SensitivePartInnerRingDet6MylarPositionZ;
  G4double SensitivePartMiddleRingDet1MylarPositionX;
  G4double SensitivePartMiddleRingDet1MylarPositionY;
  G4double SensitivePartMiddleRingDet1MylarPositionZ;
  G4double SensitivePartMiddleRingDet2MylarPositionX;
  G4double SensitivePartMiddleRingDet2MylarPositionY;
  G4double SensitivePartMiddleRingDet2MylarPositionZ;
  G4double SensitivePartMiddleRingDet3MylarPositionX;
  G4double SensitivePartMiddleRingDet3MylarPositionY;
  G4double SensitivePartMiddleRingDet3MylarPositionZ;
  G4double SensitivePartMiddleRingDet4MylarPositionX;
  G4double SensitivePartMiddleRingDet4MylarPositionY;
  G4double SensitivePartMiddleRingDet4MylarPositionZ;
  G4double SensitivePartMiddleRingDet5MylarPositionX;
  G4double SensitivePartMiddleRingDet5MylarPositionY;
  G4double SensitivePartMiddleRingDet5MylarPositionZ;
  G4double SensitivePartMiddleRingDet6MylarPositionX;
  G4double SensitivePartMiddleRingDet6MylarPositionY;
  G4double SensitivePartMiddleRingDet6MylarPositionZ;
  G4double SensitivePartOuterRingDet1MylarPositionX;
  G4double SensitivePartOuterRingDet1MylarPositionY;
  G4double SensitivePartOuterRingDet1MylarPositionZ;
  G4double SensitivePartOuterRingDet2MylarPositionX;
  G4double SensitivePartOuterRingDet2MylarPositionY;
  G4double SensitivePartOuterRingDet2MylarPositionZ;
  G4double SensitivePartOuterRingDet3MylarPositionX;
  G4double SensitivePartOuterRingDet3MylarPositionY;
  G4double SensitivePartOuterRingDet3MylarPositionZ;
  G4double SensitivePartOuterRingDet4MylarPositionX;
  G4double SensitivePartOuterRingDet4MylarPositionY;
  G4double SensitivePartOuterRingDet4MylarPositionZ;
  G4double SensitivePartOuterRingDet5MylarPositionX;
  G4double SensitivePartOuterRingDet5MylarPositionY;
  G4double SensitivePartOuterRingDet5MylarPositionZ;
  G4double SensitivePartOuterRingDet6MylarPositionX;
  G4double SensitivePartOuterRingDet6MylarPositionY;
  G4double SensitivePartOuterRingDet6MylarPositionZ;

private:
  //solids
  G4Tubs *solidSensitivePartInnerRingDet1Mylar;
  G4Tubs *solidSensitivePartInnerRingDet2Mylar;
  G4Tubs *solidSensitivePartInnerRingDet3Mylar;
  G4Tubs *solidSensitivePartInnerRingDet4Mylar;
  G4Tubs *solidSensitivePartInnerRingDet5Mylar;
  G4Tubs *solidSensitivePartInnerRingDet6Mylar;

  G4Tubs *solidSensitivePartMiddleRingDet1Mylar;
  G4Tubs *solidSensitivePartMiddleRingDet2Mylar;
  G4Tubs *solidSensitivePartMiddleRingDet3Mylar;
  G4Tubs *solidSensitivePartMiddleRingDet4Mylar;
  G4Tubs *solidSensitivePartMiddleRingDet5Mylar;
  G4Tubs *solidSensitivePartMiddleRingDet6Mylar;

  G4Tubs *solidSensitivePartOuterRingDet1Mylar;
  G4Tubs *solidSensitivePartOuterRingDet2Mylar;
  G4Tubs *solidSensitivePartOuterRingDet3Mylar;
  G4Tubs *solidSensitivePartOuterRingDet4Mylar;
  G4Tubs *solidSensitivePartOuterRingDet5Mylar;
  G4Tubs *solidSensitivePartOuterRingDet6Mylar;

  //logical volumes
  G4LogicalVolume *logicSensitivePartInnerRingDet1Mylar;
  G4LogicalVolume *logicSensitivePartInnerRingDet2Mylar;
  G4LogicalVolume *logicSensitivePartInnerRingDet3Mylar;
  G4LogicalVolume *logicSensitivePartInnerRingDet4Mylar;
  G4LogicalVolume *logicSensitivePartInnerRingDet5Mylar;
  G4LogicalVolume *logicSensitivePartInnerRingDet6Mylar;

  G4LogicalVolume *logicSensitivePartMiddleRingDet1Mylar;
  G4LogicalVolume *logicSensitivePartMiddleRingDet2Mylar;
  G4LogicalVolume *logicSensitivePartMiddleRingDet3Mylar;
  G4LogicalVolume *logicSensitivePartMiddleRingDet4Mylar;
  G4LogicalVolume *logicSensitivePartMiddleRingDet5Mylar;
  G4LogicalVolume *logicSensitivePartMiddleRingDet6Mylar;

  G4LogicalVolume *logicSensitivePartOuterRingDet1Mylar;
  G4LogicalVolume *logicSensitivePartOuterRingDet2Mylar;
  G4LogicalVolume *logicSensitivePartOuterRingDet3Mylar;
  G4LogicalVolume *logicSensitivePartOuterRingDet4Mylar;
  G4LogicalVolume *logicSensitivePartOuterRingDet5Mylar;
  G4LogicalVolume *logicSensitivePartOuterRingDet6Mylar;

  //physical volumes
  G4VPhysicalVolume *physSensitivePartInnerRingDet1Mylar;
  G4VPhysicalVolume *physSensitivePartInnerRingDet2Mylar;
  G4VPhysicalVolume *physSensitivePartInnerRingDet3Mylar;
  G4VPhysicalVolume *physSensitivePartInnerRingDet4Mylar;
  G4VPhysicalVolume *physSensitivePartInnerRingDet5Mylar;
  G4VPhysicalVolume *physSensitivePartInnerRingDet6Mylar;

  G4VPhysicalVolume *physSensitivePartMiddleRingDet1Mylar;
  G4VPhysicalVolume *physSensitivePartMiddleRingDet2Mylar;
  G4VPhysicalVolume *physSensitivePartMiddleRingDet3Mylar;
  G4VPhysicalVolume *physSensitivePartMiddleRingDet4Mylar;
  G4VPhysicalVolume *physSensitivePartMiddleRingDet5Mylar;
  G4VPhysicalVolume *physSensitivePartMiddleRingDet6Mylar;

  G4VPhysicalVolume *physSensitivePartOuterRingDet1Mylar;
  G4VPhysicalVolume *physSensitivePartOuterRingDet2Mylar;
  G4VPhysicalVolume *physSensitivePartOuterRingDet3Mylar;
  G4VPhysicalVolume *physSensitivePartOuterRingDet4Mylar;
  G4VPhysicalVolume *physSensitivePartOuterRingDet5Mylar;
  G4VPhysicalVolume *physSensitivePartOuterRingDet6Mylar;

private:

  G4double RotationAngleSensitivePartInnerRing;
  G4double RotationAngleSensitivePartMiddleRing;
  G4double RotationAngleSensitivePartOuterRing;

  G4RotationMatrix RotationMatrixSensitivePartInnerRing;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRing;
  G4RotationMatrix RotationMatrixSensitivePartOuterRing;

private:

  G4double RotationAngleSensitivePartInnerRingDet1x;
  G4double RotationAngleSensitivePartInnerRingDet1y;
  G4double RotationAngleSensitivePartInnerRingDet2x;
  G4double RotationAngleSensitivePartInnerRingDet2y;
  G4double RotationAngleSensitivePartInnerRingDet3x;
  G4double RotationAngleSensitivePartInnerRingDet3y;
  G4double RotationAngleSensitivePartInnerRingDet4x;
  G4double RotationAngleSensitivePartInnerRingDet4y;
  G4double RotationAngleSensitivePartInnerRingDet5x;
  G4double RotationAngleSensitivePartInnerRingDet5y;
  G4double RotationAngleSensitivePartInnerRingDet6x;
  G4double RotationAngleSensitivePartInnerRingDet6y;

  G4double RotationAngleSensitivePartMiddleRingDet1x; 
  G4double RotationAngleSensitivePartMiddleRingDet1y;
  G4double RotationAngleSensitivePartMiddleRingDet2x;
  G4double RotationAngleSensitivePartMiddleRingDet2y;
  G4double RotationAngleSensitivePartMiddleRingDet3x;
  G4double RotationAngleSensitivePartMiddleRingDet3y;
  G4double RotationAngleSensitivePartMiddleRingDet4x;
  G4double RotationAngleSensitivePartMiddleRingDet4y;
  G4double RotationAngleSensitivePartMiddleRingDet5x;
  G4double RotationAngleSensitivePartMiddleRingDet5y;
  G4double RotationAngleSensitivePartMiddleRingDet6x;
  G4double RotationAngleSensitivePartMiddleRingDet6y;

  G4double RotationAngleSensitivePartOuterRingDet1x;
  G4double RotationAngleSensitivePartOuterRingDet1y;
  G4double RotationAngleSensitivePartOuterRingDet2x;
  G4double RotationAngleSensitivePartOuterRingDet2y;
  G4double RotationAngleSensitivePartOuterRingDet3x;
  G4double RotationAngleSensitivePartOuterRingDet3y;
  G4double RotationAngleSensitivePartOuterRingDet4x;
  G4double RotationAngleSensitivePartOuterRingDet4y;
  G4double RotationAngleSensitivePartOuterRingDet5x;
  G4double RotationAngleSensitivePartOuterRingDet5y;
  G4double RotationAngleSensitivePartOuterRingDet6x;
  G4double RotationAngleSensitivePartOuterRingDet6y;

private:
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet1;
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet2;
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet3;
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet4;
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet5;
  G4RotationMatrix RotationMatrixSensitivePartInnerRingDet6;

  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet1;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet2;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet3;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet4;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet5;
  G4RotationMatrix RotationMatrixSensitivePartMiddleRingDet6;

  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet1;
  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet2;
  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet3;
  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet4;
  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet5;
  G4RotationMatrix RotationMatrixSensitivePartOuterRingDet6;
};

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
 
class AgataAncillaryRFDMessenger: public G4UImessenger
{
public:
  AgataAncillaryRFDMessenger(AgataAncillaryRFD*,G4String);
  ~AgataAncillaryRFDMessenger();
  
private:
  AgataAncillaryRFD*        myTarget;
  G4UIdirectory*            myDirectory;
  G4UIcmdWithABool*         enableIonGuide;
  G4UIcmdWithABool*         disableIonGuide;
  G4UIcmdWithABool*         enableRFDCones;
  G4UIcmdWithABool*         disableRFDCones;
  G4UIcmdWithABool*         enableTargetChamber;
  G4UIcmdWithABool*         disableTargetChamber;
  G4UIcmdWithABool*           enableTarget;
  G4UIcmdWithABool*           disableTarget;
  
  G4UIcmdWithADoubleAndUnit* TargetChamberDiameter;
  G4UIcmdWithADoubleAndUnit* TargetChamberThickness;
  
  G4UIcmdWithADoubleAndUnit* IonGuideLength;
  G4UIcmdWithADoubleAndUnit* IonGuideThickness;
  G4UIcmdWithADoubleAndUnit* IonGuideInnerDiameter;
  G4UIcmdWithADoubleAndUnit* RFDMylarFoilThickness;
  G4UIcmdWithADoubleAndUnit* RFDMylarFoilsDistanceToTarget;
  G4UIcmdWithADoubleAndUnit* LengthOfBellow;
  G4UIcmdWithADoubleAndUnit*  RFDTargetDiameter;
  G4UIcmdWithADoubleAndUnit*  RFDTargetThickness;
  G4UIcmdWithADoubleAndUnit*  RFDTargetOffset;

  G4UIcmdWithAString*         RFDTargetMaterial;
  
public:
  void SetNewValue(G4UIcommand * command,G4String newValues);
};






#endif

#endif
