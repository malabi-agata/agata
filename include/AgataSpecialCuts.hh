/////////////////////////////////////////////
// 
//  This class stops particles and deposits their kinetic energy
//  when they intersect with the "test sphere", see AgataDetectorTest class
//
//  Joa Ljungvall 09/2012
//
///////////////////////////////////



#ifndef AgataSpecialCuts_h
#define AgataSpecialCuts_h 1

#include "G4ios.hh"
#include "globals.hh"
#include "G4VProcess.hh"

class AgataSpecialCutsMessenger;



class AgataSpecialCuts : public G4VProcess 
{
  public:     

     AgataSpecialCuts(const G4String& processName ="AgataSpecialCut" );

     virtual ~AgataSpecialCuts();

     virtual G4double PostStepGetPhysicalInteractionLength(
                             const G4Track& track,
			     G4double   previousStepSize,
			     G4ForceCondition* condition
			    );

     virtual G4VParticleChange* PostStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    );
			    
     //  no operation in  AtRestGPIL
     virtual G4double AtRestGetPhysicalInteractionLength(
                             const G4Track& ,
			     G4ForceCondition* 
			    ){ return -1.0; };
			    
     //  no operation in  AtRestDoIt      
     virtual G4VParticleChange* AtRestDoIt(
			     const G4Track& ,
			     const G4Step&
			    ){return NULL;};

     //  no operation in  AlongStepGPIL
     virtual G4double AlongStepGetPhysicalInteractionLength(
                             const G4Track&,
			     G4double  ,
			     G4double  ,
			     G4double& ,
                             G4GPILSelection*
			    ){ return -1.0; };

     //  no operation in  AlongStepDoIt
     virtual G4VParticleChange* AlongStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    ) {return NULL;};

  void SetDontKillGammas(G4bool val){fDontKillGammas=val; std::cout << "Setting DontKillGammas to " <<fDontKillGammas  << "\n";};
  void SetDontKillIons(G4bool val){fDontKillIons=val;std::cout << "Setting DontKillIons to " << 
						       fDontKillIons << "\n";}
  private:
  
  // hide assignment operator as private 
     AgataSpecialCuts& operator=(const AgataSpecialCuts&){return *this;};
  static G4bool fDontKillGammas,fDontKillIons;
  static AgataSpecialCutsMessenger*    myMessenger;
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class AgataSpecialCutsMessenger: public G4UImessenger
{
  public:
    AgataSpecialCutsMessenger(AgataSpecialCuts*, G4String);
   ~AgataSpecialCutsMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataSpecialCuts*     myCuts;
  G4UIdirectory*         myDirectory;
  G4UIcmdWithABool*      SetDontKillGammas;
  G4UIcmdWithABool*      SetDontKillIons;

};


#endif



