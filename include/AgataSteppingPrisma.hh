/////////////////////////////////////////////////////////////
/// This class is normally NOT used. It might come handy in
/// case of infinite loops. It is used also after a solid
/// angle calculation has been performed (but it just
/// performs no action!)
////////////////////////////////////////////////////////////
#ifndef AgataSteppingPrisma_h
#define AgataSteppingPrisma_h 1

#include "G4UserSteppingAction.hh"
#include "AgataDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;

class CSpec1D;

class AgataSteppingPrismaMessenger;

class AgataSteppingPrisma : public G4UserSteppingAction
{
  public:
    AgataSteppingPrisma(G4String);
   ~AgataSteppingPrisma();

  /////////////////////////////////////////////////////////
  /// This method is needed by G4UserSteppingAction
  ////////////////////////////////////////////////////////
  public:
    void UserSteppingAction(const G4Step*);

  public:
    void BeginOfRunAction ( G4int );
    void EndOfRunAction   ( );
    
  public:
    void BeginOfEventAction ( );
    void EndOfEventAction   ( );  
    
  private:
    G4int runNumber;
    G4int evtNumber;
    
  private:
    G4int OpenOutputFile  ( );
    void  CloseOutputFile ( );  
    void  WriteHeader     ( );  

  public:
    void EnableWrite      ( G4bool );      //> Enables/disables the output to the LM file
    void SetMaxFileSize   ( G4int );       //> Sets the maximum size of the LM file
    void SetWorkingPath   ( G4String );    //> Chooses the directory where the LM file is written

  public:
    void  SplitOutputFile ( );
    
  private:
    G4int  maxFileSize;
    G4bool writeFile;
    G4bool builtTarget;
    
  private:
    AgataSteppingPrismaMessenger* myMessenger;
    
  private:
    std::ofstream outFile;
    G4String outFileName;  

  private:
    G4String workPath;                         //> Directory where the list-mode file is written
    
  private:
    G4ThreeVector position;
    G4ThreeVector direction;
    G4ThreeVector momentum;
    G4int         atoNum;
    G4int         masNum;
    G4int         energy;
    
  private:
    G4ThreeVector position_i;
    G4ThreeVector direction_i;
    G4ThreeVector momentum_i;
    G4int         energy_i;
    
  private:
    G4double  timeDiff;
    
  private:
    G4bool eventOK;  // OK if reaction occurs and secondary nucleus reaches OutOfWorld
    
  private:
    G4int    fileNumber;                       //> Current file number (within a given run)
  AgataDetectorConstruction *theDetector;

    
  public:
    inline void SetRunNumber ( G4int num ) { runNumber = num; };
    inline void SetEvtNumber ( G4int num ) { evtNumber = num; };
      
  public:
    inline G4int  GetMaxFileSize()                  { return maxFileSize;     };  
    inline G4int  GetOutputFileSize()               { return outFile.tellp(); };
      
  public:
    inline G4int         GetAtoNum   () { return atoNum;   };
    inline G4int         GetMasNum   () { return masNum;   };
    inline G4double      GetEnergy   () { return energy;   };
    inline G4ThreeVector GetMomentum () { return momentum; };
    inline G4ThreeVector GetPosition () { return position; };
      
  public:
    inline G4double      GetEnergyI   () { return energy_i;   };
    inline G4ThreeVector GetMomentumI () { return momentum_i; };
    inline G4ThreeVector GetPositionI () { return position_i; };
  
  private:
    CSpec1D* theSpectrum;      
    
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;

class AgataSteppingPrismaMessenger: public G4UImessenger
{
  public:
    AgataSteppingPrismaMessenger(AgataSteppingPrisma*, G4String);
   ~AgataSteppingPrismaMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataSteppingPrisma*            myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithABool*          EnableWriteCmd;
    G4UIcmdWithABool*          DisableWriteCmd;
    G4UIcmdWithAString*        WorkPathCmd;
    G4UIcmdWithAnInteger*      SetFileSizeCmd;
};

#endif
