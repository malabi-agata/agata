#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryNeda_h
#define AgataAncillaryNeda_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "G4GDMLParser.hh"
#include "G4NistManager.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryNedaMessenger;

class AgataAncillaryNeda : public AgataAncillaryScheme
{
  
public:
  AgataAncillaryNeda(G4String,G4String);
  ~AgataAncillaryNeda();

private:
  AgataAncillaryNedaMessenger* myMessenger;

private:
  G4Material* FindMaterial (G4String Name);
  G4int  FindMaterials           ();
  bool   GetPmt                  ();
  bool   GetVessel               ();
  bool   GetTop                  ();
  bool   GetScintillator         ();

  ///////////////////////////////////////
  /// Methods required by the Messenger
  //////////////////////////////////////
public:
  void   SetRotation   (G4ThreeVector);
  void   SetTranslation(G4ThreeVector);
  void   SetGeometry   (G4int);
  //  void   SetLightThr   (G4double);

  //  G4double GetNedaLightThr(){return neda_light_thr;};

private:
  G4ThreeVector neda_translation ;
  G4ThreeVector neda_rotation    ;
  G4int         neda_geometry    ;
  //  G4double      neda_light_thr   ;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
private:
  G4NistManager *nistManager;
  G4String    nam_vacuum;
  G4Material *mat_vacuum;
    
  G4String    nam_alum;
  G4Material *mat_alum;
    
  G4String    nam_scint;
  G4Material *mat_scint;
  
  G4String    nam_glass;
  G4Material *mat_glass;

  G4String    gdml_path;
  G4GDMLParser gdmlparser;




  std::map<int,G4LogicalVolume *> pNedaPmt;
  std::map<int,G4LogicalVolume *> pNedaVessel;
  std::map<int,G4LogicalVolume *> pNedaTop;
  std::map<int,G4LogicalVolume *> pNedaScint;
  std::map<int,G4LogicalVolume *>::iterator it_log;


  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
public:

  void   GetDetectorConstruction ();
  void   InitSensitiveDetector   ();
  void   Placement               ();


public:
  void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
  void WriteHeader  ( G4String *sheader, G4double=1.*mm );
  void   ShowStatus              ();

public:
  inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector )  { return 0; };
  inline G4int GetCrystalType    ( G4int )                        { return -1; };

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
//class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;

class AgataAncillaryNedaMessenger: public G4UImessenger
{
  public:
    AgataAncillaryNedaMessenger(AgataAncillaryNeda*,G4String);
   ~AgataAncillaryNedaMessenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataAncillaryNeda*        myTarget      ;
  G4UIdirectory*             NedaDir       ;
  G4UIcmdWith3Vector*        setNedaTraCmd ;
  G4UIcmdWith3Vector*        setNedaRotCmd ;
  G4UIcmdWithAnInteger*      setNedaGeoCmd ;
  //  G4UIcmdWithADoubleAndUnit* setNedaLightThrCmd;
};

#endif

#endif
