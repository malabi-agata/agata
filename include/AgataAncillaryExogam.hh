#ifdef ANCIL
#ifndef AgataAncillaryExogam_h
#define AgataAncillaryExogam_h 1


#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Tubs.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4Material.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"


#include "G4Trap.hh"
using namespace std;

class G4Box;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;

class AgataSensitiveDetector;
class AgataAncillaryExogamMessenger;

//gj 08/02/2010
class AgataDetectorAncillary;


class AgataAncillaryExogam : public AgataAncillaryScheme
{
public:
  AgataAncillaryExogam(G4String,G4String);
  ~AgataAncillaryExogam();

private:
  //przestawialne double

public:
  G4int FindMaterials();
  void GetDetectorConstruction();
  void InitSensitiveDetector();
  void Placement();
  void ShowStatus();
  void WriteHeader (std::ofstream &outFileLMD, G4double=1.0*mm);
  void WriteHeader  ( G4String *sheader, G4double=1.*mm );
  void SetEXOGAMInput(G4String filename) {EXOGAMinput = filename;}

public:
  G4int GetSegmentNumber(G4int,G4int,G4ThreeVector);
  inline G4int GetCrystalType  (G4int)			   {return -1;};

public:
  //wszelkie metody od komend

private:
  void PlaceEverything();
  void PlaceTarget();
  void PlaceCatcherIonGuide();
  void PlaceElectronDetectors();
  //klasy placeSth
  AgataAncillaryExogamMessenger* myMessenger;

private:
  G4String    nameVacuum;
  G4Material* Vacuum;
  G4String    nameAluminum;
  G4Material* Aluminum;
  G4String    nameCopper;
  G4Material* Copper;
  G4String    nameGermanium;
  G4Material* Germanium;
  G4String    nameBGO;
  G4Material* BGO;
  G4String    nameCsI;
  G4Material* CsI;
  G4String    nameHeavymet;
  G4Material* Heavymet;
 
  G4String    nameMatTarget;
  G4Material* matTarget;
  G4String    nameMatLayer;
  G4Material* matLayer;
  G4String    nameMatElDet;
  G4Material* matElDet;

  //private:
  //solids which wasnt inicialized at cc
  //logical volumes
  G4LogicalVolume* logicVac;

  //physical volumes

    private:
// Some rotation matrices
    G4RotationMatrix rm90;
    G4RotationMatrix rm90m;
    G4RotationMatrix rmCut2;
    G4RotationMatrix rm180;
    G4RotationMatrix rm270;


  //gj for newest exo
  private:
     G4double SOffset;
  
  private:
    G4VPhysicalVolume* physiSupClover;
    G4LogicalVolume* logicSupClover;
    G4VPhysicalVolume* physiVac;

  //takoz
  private:
     void ConstructClover();
     void ConstructSmallClover();
     void ConstructSideCatcher();
     void ConstructBackCatcher();
     void ConstructSideShield();
     void ConstructCollimator();

  //takoz
  private:
    G4LogicalVolume* logicCloverCan;
    G4LogicalVolume* logicEnvColdFinger;
    G4LogicalVolume* logicDewar;
    G4LogicalVolume* logicAGe1;
    G4LogicalVolume* logicHole1;
    G4LogicalVolume* logicAbsorb;
    G4LogicalVolume* logicSAbsorb;
    G4LogicalVolume* logicGeA;
    G4LogicalVolume* logicGeB;
    G4LogicalVolume* logicGeC;
    G4LogicalVolume* logicGeD;
    G4LogicalVolume* logicSmallCloverCan;
    G4LogicalVolume* logicSmallVac;
    G4LogicalVolume* logicSmallDewar;
    G4LogicalVolume* logicSmallAGe1;
    G4LogicalVolume* logicSmallHole1;

    G4LogicalVolume* logicSmallGeA;
    G4LogicalVolume* logicSmallGeB;
    G4LogicalVolume* logicSmallGeC;
    G4LogicalVolume* logicSmallGeD;
// Side Catcher
    G4LogicalVolume* logicAlSCatcher;
    G4LogicalVolume* logicBGOSCatcherA;
    G4LogicalVolume* logicBGOSCatcherB;
// Back Catcher
    G4LogicalVolume* logicBack;
    G4LogicalVolume* logicBackCsI;
// Side Shield
    G4LogicalVolume* logicSShield;
    G4LogicalVolume* logicBGOSShieldA;
    G4LogicalVolume* logicBGOSShieldB;
    G4LogicalVolume* logicColl;
  
  private:
    G4double distCollimatorToBGOSShield; // distance from the front face of the 
					 // Collimator to the front face of the 
					 // BGO Side Shield=length of the collimator

  
  //kurde troche chaos
  //tu jest potrzebne przy konstrukcji cloveru
  private:
    G4double HalfLengthCan;
    G4double TaperLengthVac;
    G4double HalfLengthVac;

    G4double TaperLengthCan;	// used to build the shield
    G4double rOuterCan[3];	// used to build the shield

  //small clover
  private:
    G4double SHalfLengthCan;
    G4double STaperLengthVac;
    G4double SHalfLengthVac;
    G4VPhysicalVolume* physiSmallSupClover;
  //  gj 08/02/2010
    G4double PositionZ[22];
    G4int CloverInArray[22];


  private:
     G4int NumberOfClover;
     G4int EventMult;
     G4double dzEnv;
     G4double Offset;
     G4int IDCloverInArray[22];

  private:
     char InputFilename[256];
  G4String EXOGAMinput;
     char Config;
     bool Tracking;
     char GammaGenerator[10];

};

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class AgataAncillaryExogamMessenger: public G4UImessenger
{
public:
  AgataAncillaryExogamMessenger(AgataAncillaryExogam*,G4String);
  ~AgataAncillaryExogamMessenger();
  
private:
  AgataAncillaryExogam*          myTarget;
  //2010/02/05
  G4UIdirectory*            myDirectory;
  G4UIcmdWithAString*       cmdSetEXOGAMinput;
  //wszelakie komendy

public:
  void SetNewValue(G4UIcommand * command,G4String newValues);

};


#endif

#endif
