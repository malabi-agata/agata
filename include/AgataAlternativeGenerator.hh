//////////////////////////////////////////////////////////////
/// This class is used only as an example of "non-standard"
/// event generator (opposite to the "standard" one for
/// the Agata code). User should provide his own code!!!
//////////////////////////////////////////////////////////////

#ifndef AgataAlternativeGenerator_h
#define AgataAlternativeGenerator_h 1
#include "AgataGeneratorAction.hh"
#include "globals.hh"
#include "Incoming_Beam.hh"
#include "Outgoing_Beam.hh"
#include "Outgoing_Beam_TargetEx.hh"
#include "CProfileDistHandler.hh"



using namespace std;

class G4ParticleGun;
class G4Event;

class Incoming_Beam;
class Outgoing_Beam;
class Outgoing_Beam_TargetEx;
class AgataDetectorConstruction;
class AgataEventAction;
class AgataSteppingPrisma;
class AgataAlternativeGeneratorMessenger;

#ifndef _asource_
#define _asource_

enum SourceGeometry_t {Point=0, Line=1, Plane=2, Sphere=3};
enum EnergyDist_t {Discrete=0,Exp=1};

struct DecayParticle {
  G4String particle;//Gamma, beta etc
  EnergyDist_t EnergyDist;//Disc or exp more to be added
  std::vector<G4double> EnergyPar;//Parameters for the above
  G4double tau;//Lifetime of state
  std::vector<G4double> As; //Ang corr coeff.
  std::vector<G4double> Integral;
};

class DecayBranch{
public:
  DecayBranch(){;}
  ~DecayBranch(){;}
  G4double prob;
  std::vector<DecayParticle> particles;
};

class asource {
public:
  asource() {;}
  ~asource(){;}
  void CreateParticles(G4Event* anEvent,G4ParticleGun *particleGun);
  //    , const G4double &eventTime);
  G4String source;
  G4double activity;
  G4double decay_at_time;
  bool ReadDataFile();
  double Legendre(int l,double x);
private:
  SourceGeometry_t sourcegeometry;//What geometry of source
  std::vector<G4double> GeometryParameters; //Everything needed to describe it
  std::vector<DecayBranch> DecayBranches;
};
 
#endif

#ifndef _RecoilFile_
#define _RecoilFile_
struct onelinefromfile {
  G4double v,vx,vy,vz;
};
#endif

class AgataAlternativeGenerator : public AgataGeneration
{
public:
AgataAlternativeGenerator(G4String, G4int ob=0);    
  ~AgataAlternativeGenerator();

private:
  G4ParticleGun*              particleGun;
  Incoming_Beam*              BeamIn;
  Outgoing_Beam*              BeamOut;
  AgataDetectorConstruction*  theDetector;
  AgataEventAction*           theEvent;
  AgataSteppingPrisma*        theStepping;

private:
  AgataAlternativeGeneratorMessenger* myMessenger;

private:
  G4bool                      builtTarget;
  G4bool                      builtDegrader;
  G4bool                      fixDepth;
  G4bool                      builtProductionTarget;
  G4int                       whichoutgoingbeam;
private:
  G4double                    targetZThick;
  G4double                    productiontargetZThick;
  G4double                    degraderZThick;
  G4ThreeVector               normal2Target;

private:
  G4double                    tarFraction;

private:
  G4int                       eventNum;
  G4double                    eventTime; //seconds, zeros each min
  G4double                    oldeventTime;
  std::vector<asource>       thesources; 
  G4int                       min;
  G4int                       hours;
  G4int                       days;
  G4double                    HFtime;
  G4double                    Bunchtime;
  G4double                       pps; //Particles per second
  G4double                     HF; //Acc High freq
  G4double                     WidthBeamBunch;
  G4double                     particlesperbunch;
  //Width of beam particle bunch, square pulse
  /* If recoiling ion is to be taken from distribution read from file*/
  G4String                     RecoilFile;
  std::vector<onelinefromfile> RecoilData;
public:
  void                        SetTarFraction ( G4double );
    

public:
  void BeginOfRun   ();
  void EndOfRun     ();
    
public:
  void BeginOfEvent () {};
  void EndOfEvent   () {};
    
public:
  void GetEventInfo(std::ostream &eventinfo);
  void GetStatus    () {};
    
public:
  void PrintToFile  ( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV ) {};
  void PrintToFile  ( G4String */*sheader*/, G4double=1.*mm, G4double=1.*keV ) {};  //Chen  

    
public:
  G4String GetBeginOfEventTag ();
  G4String GetEventHeader     ( G4double = 1.*mm );
    
public:
  G4String GetParticleHeader  ( const G4Event*, G4double=1.*mm, 
				G4double=1.*keV );
    
public:
  inline G4bool   IsStartOfEvent () { return true;  };
  inline G4bool   IsEndOfEvent   () { return true;  };
  inline G4bool   IsAbortRun     () { return false; }; 

  public:
    inline G4ThreeVector GetEmitterVelocity () { return G4ThreeVector(0,0,1); };
    inline G4ThreeVector GetEmitterPosition () { return G4ThreeVector(0,0,0); };
    inline G4double      GetEmitterBeta     () { return 0; };
    inline G4int         GetCascadeOrder    () { return 0; };
    inline G4bool        IsSourceLongLived  () { return false; };
    inline G4double      GetCascadeDeltaTime() { return 0; };
    inline G4double      GetCascadeTime     () { return 0; };
    
public:
  inline G4String GetAbortMessage() { return G4String(""); };
    
public:
  inline G4int    GetCascadeMult () { return 1; };
    
public:
  void     GeneratePrimaries ( G4Event* );
    
private:  
  ProfileDistHandler* theProfileDist;
  G4String profileDistFileName;

public:
  void SetDepth( G4double );
  void SetFixDepth( G4bool );
  void SetUnifDistrib( G4bool );
  void SetSlopeExpBkg(double val) {SlopeExpBkg=val;}
  void SetMaxEBkg(double val){MaxEBkg=val;}
  void SetNbExtraBkgGammas(G4int val){nbextragammasbkg=val;}
  void SetPPS(G4double rate){
    pps=rate;
    std::cout << " ------------> PPS = " << pps << "/s.\n";
  }
  void SetHF(G4double val){
    HF= (val>=0 ? val:0);
    if(HF>0) particlesperbunch=pps/(HF*s);
    std::cout << " ------------> HF = " << HF*s << " Hz.\n";
    std::cout << " ------------> ParticlePerBunch = " << particlesperbunch 
	      << "\n";
  }
  void SetWidthBeamBunch(G4double val){
    if(HF>0){
      WidthBeamBunch=(val<1/HF ? val:1/HF);
    } else WidthBeamBunch=0;
    std::cout << " ------------> BunchWidth = " << WidthBeamBunch/s 
	      << " s.\n";
  }
  void AddExtraDiscreteGamma(G4double E,G4double p){
    fDiscreteExtraGammas.push_back(std::pair<G4double,G4double>(E,p));
  }
  void ClearExtraGammas(){fDiscreteExtraGammas.clear();}
  void SetSourceType(G4String src,G4double act);
  void SetTrackIonEnergy(G4bool val){TrackIonEnergy=val;}
  void SetIonRecoilFile(G4String file);
private:
  G4double depth;
  G4bool isUnifDistrib; 
  G4double SlopeExpBkg; //1/MeV
  G4double MaxEBkg;//MeV
  std::vector<std::pair<G4double,G4double> > fDiscreteExtraGammas;
  G4int nbextragammasbkg;
  G4bool TrackIonEnergy;
    
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAString;

class AgataAlternativeGeneratorMessenger: public G4UImessenger
{
public:
  AgataAlternativeGeneratorMessenger(AgataAlternativeGenerator*,G4String);
  ~AgataAlternativeGeneratorMessenger();
    
private:
  AgataAlternativeGenerator*      myTarget;
  G4UIdirectory*                  myDirectory;
  G4UIdirectory*                  topDirectory;

private:
  G4UIcmdWithADouble*             SetTarFractionCmd;
  G4UIcmdWithADouble*             SetDepthCmd;
  G4UIcmdWithABool*               EnableFixDepthCmd;
  G4UIcmdWithABool*               DisableFixDepthCmd;
  G4UIcmdWithABool*               EnableUnifDistribCmd;
  G4UIcmdWithABool*               DisableUnifDistribCmd;
  G4UIcmdWithABool*           TrackIonEnergyCmd;
  G4UIcmdWithADouble*         SetSlopeExpBkgCmd;
  G4UIcmdWithADouble*         SetMaxEBgkGamma;
  G4UIcmdWithAnInteger*       SetNbExtraGammasCmd;
  G4UIcmdWithAnInteger*       SetEventsPerReaction;
  G4UIcmdWithADouble*       SetPPS;
  G4UIcmdWithADoubleAndUnit*       SetHF;
  G4UIcmdWithADoubleAndUnit*       SetWidthBeamBunch;
  G4UIcmdWithoutParameter*    ClearDiscreteGammas;
  G4UIcmdWithAString*         AddDiscreteGamma;
  G4UIcmdWithAString*         SetSource;
  G4UIcmdWithAString*         SetRecoilFile;
public:
  void SetNewValue(G4UIcommand*, G4String);
    
};


#endif
