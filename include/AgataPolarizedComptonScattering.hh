//////////////////////////////////////////////////////////////////////////////////////
/// This class handles the polarized Compton scattering in a similar way as the
/// "standard" electromagnetic processes. It looks like the default treatment
/// provided by the Geant4 distribution is not correct!!!
/// Written and mantained by Dino Bazzacco.
//////////////////////////////////////////////////////////////////////////////////////


#ifndef G4PolarizedComptonScattering_h
#define G4PolarizedComptonScattering_h 1

#ifdef G4V47
#include "G4ComptonScattering52.hh"
class AgataPolarizedComptonScattering : public G4ComptonScattering52
#else
# include "G4ComptonScattering.hh"
class AgataPolarizedComptonScattering : public G4ComptonScattering
#endif

{
 public:  // with description

  AgataPolarizedComptonScattering(const G4String& processName = "AgataPCompt");

  G4VParticleChange* PostStepDoIt(const G4Track& aTrack, const G4Step& aStep);
  
 private:
  // hide assignment operator as private 
  AgataPolarizedComptonScattering& operator=(const AgataPolarizedComptonScattering &right);
  AgataPolarizedComptonScattering(const AgataPolarizedComptonScattering& );

public: 
  void FirstThetaSet(G4double egamma, G4double theta)
    { fixTh = true; valEg = egamma; valTh = theta; }
  void FirstThetaSet(G4double egamma, G4double thetaMin, G4double /*thetaMax*/)
    { fixTh = true; valEg = egamma; valTh = thetaMin; }	// to be completed
  void FirstThetaReset()
    { fixTh = false; }

private:
  bool      fixTh;
  G4double  valEg;
  G4double  valTh;

};

#endif
