#ifdef ANCIL
#ifndef AgataAncillaryEuclides_h
#define AgataAncillaryEuclides_h 1

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>

using namespace std;

class G4Material;
class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Polyhedra;
class G4Tubs;
class CConvexPolyhedron;
class G4SubtractionSolid;
class G4VisAttributes;
class AgataSensitiveDetector;
class AgataAncillaryEuclidesMessenger;

///////////////////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the transformations needed to place the individual telescopes within the array
///////////////////////////////////////////////////////////////////////////////////////////////////////
class angles
{
  // default empty creator/destructor
  public:
     angles() {};
    ~angles() {};

  // all members are public  
  public:  
    G4int     whichGe;     //> type of telescope to place
    G4int     numPhys;     //> number of this placed telescope

  public:  
    G4double  ps, th, ph;  //> Rotation

  public:  
    G4double  x, y, z;     //> traslation
};

class cylLayer
{
  public:
    cylLayer() {
      // default values
      thickAbs = 0.012*mm;
      thet1Abs =  10.*deg;
      thet2Abs = 170.*deg;
      positAbs = G4ThreeVector();
      matAbsName = G4String("Vacuum");

      matAbs   = NULL;
      solidAbs = NULL;
      logicAbs = NULL;
      physiAbs = NULL;
    };
    ~cylLayer() {};
    
  public:
    G4double 		 thickAbs;
    
  public:
    G4double 		 thet1Abs;
    G4double 		 thet2Abs;
    
  public:
    G4ThreeVector 	 positAbs;
    
  public:
    G4Tubs* 		 solidAbs;
    G4LogicalVolume* 	 logicAbs;
    G4VPhysicalVolume*   physiAbs;
    
  public:
    G4Material* 	 matAbs;
    G4String 		 matAbsName;
};

//////////////////////////////////////////////////////////////
/// This class handles the construction of ISIS or EUCLIDES
//////////////////////////////////////////////////////////////
class AgataAncillaryEuclides : public AgataAncillaryScheme
{
  public:
    AgataAncillaryEuclides  ( G4String, G4String );
    ~AgataAncillaryEuclides ();

  private:
    AgataDetectorConstruction*       theDetector;
    AgataAncillaryEuclidesMessenger* myMessenger;

  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  /// In this case, two instances of AgataSensitiveDetector are needed to cope with E, DelteE detectors
  /// One of them is inherited from AgataAncillaryScheme
  ///////////////////////////////////////////////////////////////////////////////////////////////////////
  private:
    AgataSensitiveDetector*          ancSE; 

  private:
    std::vector<angles>   eulero;       //> transformations to place the telescopes
    G4String              eulerFile;    //> file where such information is stored
    G4int                 nEuler;       //> number of telescopes
    G4int                 nSegmented;   //> number of segmented telescopes

  private:
    G4int                 geomType;     //> ISIS or EUCLIDES
    
  //////////////////////////
  /// Pentagonal absorbers
  /////////////////////////
  private:
     CConvexPolyhedron*	    pAbsorPS;    //> solid
     G4LogicalVolume*       pAbsorPL;    //> logical 
     G4VPhysicalVolume*     pAbsorPP;    //> physical
     
  ////////////////////////////////////
  /// DeltaE active part (pentagons)
  ////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDIPS;    //> solid
     G4LogicalVolume*       pDetDIPL;    //> logical 
     G4VPhysicalVolume*     pDetDIPP;    //> physical
     
  //////////////////////////////////////
  /// DeltaE passive part (pentagons)
  //////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDOPS;    //> solid
     G4LogicalVolume*       pDetDOPL;    //> logical 
     G4VPhysicalVolume*     pDetDOPP;    //> physical
     
  ////////////////////////////////
  /// E passive part (pentagons)
  ////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEIPS;    //> solid
     G4LogicalVolume*       pDetEIPL;    //> logical 
     G4VPhysicalVolume*     pDetEIPP;    //> physical
     
  ////////////////////////////////
  /// E passive part (pentagons)
  ////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEOPS;    //> solid
     G4LogicalVolume*       pDetEOPL;    //> logical 
     G4VPhysicalVolume*     pDetEOPP;    //> physical
     
  /////////////////////////
  /// Pentagonal frames
  //////////////////////////
  private:
     G4Polyhedra*	    pFramePS;    //> solid
     G4SubtractionSolid*    pframePS;    //> solid
     G4LogicalVolume*       pFramePL;    //> logical 
     G4VPhysicalVolume*     pFramePP;    //> physical

  /////////////////////////
  /// hexagonal absorbers
  ////////////////////////
  private:
     CConvexPolyhedron*	    pAbsorHS;    //> solid
     G4LogicalVolume*       pAbsorHL;    //> logical 
     G4VPhysicalVolume*     pAbsorHP;    //> physical
     
  //////////////////////////////////
  /// DeltaE active part (hexagons)
  ///////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDIHS;    //> solid
     G4LogicalVolume*       pDetDIHL;    //> logical 
     G4VPhysicalVolume*     pDetDIHP;    //> physical
     
  ////////////////////////////////////
  /// DeltaE passive part (hexagons)
  ///////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDOHS;    //> solid
     G4LogicalVolume*       pDetDOHL;    //> logical 
     G4VPhysicalVolume*     pDetDOHP;    //> physical
     
  ///////////////////////////////
  /// E passive part (hexagons)
  //////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEIHS;    //> solid
     G4LogicalVolume*       pDetEIHL;    //> logical 
     G4VPhysicalVolume*     pDetEIHP;    //> physical
     
  //////////////////////////////
  /// E passive part (hexagons)
  /////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEOHS;    //> solid
     G4LogicalVolume*       pDetEOHL;    //> logical 
     G4VPhysicalVolume*     pDetEOHP;    //> physical
     
  ///////////////////////
  /// hexagonal frames
  //////////////////////
  private:
     G4SubtractionSolid*    pFrameHS;    //> solid
     G4LogicalVolume*       pFrameHL;    //> logical 
     G4VPhysicalVolume*     pFrameHP;    //> physical

  /////////////////////////
  /// segmented hexagons
  /// hexagonal absorbers
  ////////////////////////
  private:
     CConvexPolyhedron*	    pAbsorSS;    //> solid
     G4LogicalVolume*       pAbsorSL;    //> logical 
     G4VPhysicalVolume*     pAbsorSP;    //> physical
     
  /////////////////////////////////////////////
  /// DeltaE active part (segmented hexagons)
  //////////////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDISS;    //> solid
     G4LogicalVolume*       pDetDISL;    //> logical 
     G4VPhysicalVolume*     pDetDISP;    //> physical
     
  /////////////////////////////////////////////
  /// DeltaE passive part (segmented hexagons)
  /////////////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetDOSS;    //> solid
     G4LogicalVolume*       pDetDOSL;    //> logical 
     G4VPhysicalVolume*     pDetDOSP;    //> physical
     
  /////////////////////////////////////////////
  /// E passive part (segmented hexagons)
  /////////////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEISS;    //> solid
     G4LogicalVolume*       pDetEISL;    //> logical 
     G4VPhysicalVolume*     pDetEISP;    //> physical
     
  /////////////////////////////////////////////
  /// E passive part (segmented hexagons)
  /////////////////////////////////////////////
  private:
     CConvexPolyhedron*	    pDetEOSS;    //> solid
     G4LogicalVolume*       pDetEOSL;    //> logical 
     G4VPhysicalVolume*     pDetEOSP;    //> physical

  /////////////////////////////////////////////
  /// hexagonal frames (segmented hexagons)
  /////////////////////////////////////////////
  private:
     G4SubtractionSolid*    pFrameSS;    //> solid
     G4LogicalVolume*       pFrameSL;    //> logical 
     G4VPhysicalVolume*     pFrameSP;    //> physical
     
  ///////////////////////////////
  /// cylindrical absorber(s)
  ////////////////////////////////
  private:
    std::vector<cylLayer>            cylAbs;
    G4double    	             innerRadius;   //> radius of the cylinder
    G4int                            nLayers;
    			 
  ///////////////////	 
  /// Fixed absorber	 
  ///////////////////	 
  private:		 
    G4double		    spesAbs;       //> thickness
    G4double		    distEDE;       //> E-DeltaE distance
    G4double		    distADE;       //> DeltaE-absorber distance
    			 
  /////////////////////////////////////////////////////////	
  /// Individual absorbers (other than the "fixed" ones)
  /////////////////////////////////////////////////////////	
  private:		 
    G4double		    spesAbI[5];       //> thickness
    G4double		    distAAI;       //> Individual-fixed absorber distance

  /////////////////////////////////////
  /// hexagonal individual absorbers
  /// (one per ring!)
  ////////////////////////////////////
  private:
     CConvexPolyhedron*	    pAbsoIHS[5];	//> solid
     G4LogicalVolume*       pAbsoIHL[5];	//> logical 
     G4VPhysicalVolume*     pAbsoIHP[5];	//> physical
    
  //////////////////////////
  /// Pentagonal absorbers
  /////////////////////////
  private:
     CConvexPolyhedron*	    pAbsoIPS[5];        //> solid
     G4LogicalVolume*       pAbsoIPL[5];        //> logical 
     G4VPhysicalVolume*     pAbsoIPP[5];        //> physical

  /////////////////////////////////////
  /// Materials with their names
  ////////////////////////////////////
  private:
    G4String                matCrystName;  //> crystals
    G4String                matAbsorName;  //> fixed absorber
    G4String                matAbsoIName;  //> individual absorber
    G4String                matFrameName;  //> detector frames

  private:
    G4Material*             matCryst;      //> crystals 	           
    G4Material*             matAbsor;      //> fixed absorber	         
    G4Material*             matAbsoI;      //> individual absorber	         
    G4Material*             matFrame; 	   //> detector frames           
    
  private:
    G4String                iniPath;       //> directory where the data files are stored
 
  private:
    std::vector<G4bool>     segmLUT;       //> lookup table: is the detector segmented or not
    G4int                   maxIndex;      //> maximum detector index
    std::vector<G4int>      ringLUT;       

  ///////////////////////////////////////////////
  /// Construction of the parts of the geometry
  ///////////////////////////////////////////////
  private:
    void ConstructAll          ();
    void ConstructThePentagons ();
    void ConstructTheHexagons  ();
    void ConstructTheSegmented ();
    void ConstructTheFrames    ();
    void ConstructTheCylinder  ();
    void ConstructTheIndAbs    ();
    
  ///////////////////////////////////////////////
  /// Placement of the parts of the geometry
  ///////////////////////////////////////////////
  private:
    void PlaceAPentagon        (G4int);
    void PlaceAHexagon         (G4int);
    void PlaceASegmented       (G4int);
    void PlaceTheCylinder      ();
    void PlaceAll              ();
    
  private:
    void ReadEulerFile         ();        //> reads the transformations to place the telescopes
#if 0
    void DefineSpecificMaterials();
#endif    
    
  private:
    void BuildRingLUT          ();
    void BuildSegmentLUT       ();       //> builds the lookup table telling whether a given detector 
                                         //> is segmented or not

  public:
    void   Placement               ();
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   WriteHeader  ( G4String *sheader, G4double=1.*mm );
    void   ShowStatus              ();

  public:
    inline G4int GetCrystalType    ( G4int ) { return -1; };

//  public:
//    inline G4String GetName () { return G4String("EUCLIDES"); };

  public:
    G4int GetSegmentNumber ( G4int, G4int, G4ThreeVector );

  /////////////////////////////////
  /// Methods for the messenger
  ////////////////////////////////
  public:
    void SetGeometry          ( G4int );
    void SetNumberOfLayers    ( G4int );
    void setAbsThick          ( G4int, G4double );
    void setAbsThet1          ( G4int, G4double );
    void setAbsThet2          ( G4int, G4double );
    void setAbsMate           ( G4int, G4String ); 
    void setFrameMate         ( G4String ); 
    void setAbIMate           ( G4String );
    void setAbIThick          ( G4int, G4double );

  public:
    inline G4int GetNumberOfSi()  { return ((AgataDetectorConstructed*)this)->GetNumberOfDetectors(); };
    inline G4int GetMaxSiIndex()  { return ((AgataDetectorConstructed*)this)->GetMaxDetectorIndex();  };

  public:
    inline void ResetNumberOfSi() { nDets = 0; };
    inline void ResetMaxSiIndex() { iGMin = 0; };

#ifdef EUCLIDES
  public:
    void GetEnergyLoss ( G4int, G4int, G4double, G4String );
#endif
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
//class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;

class AgataAncillaryEuclidesMessenger: public G4UImessenger
{
  public:
    AgataAncillaryEuclidesMessenger(AgataAncillaryEuclides*, G4String);
   ~AgataAncillaryEuclidesMessenger();
    
  private:
    AgataAncillaryEuclides*    myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAString*        absMatCmd;
    G4UIcmdWithAString*        frmMatCmd;
    G4UIcmdWithAString*        absThickCmd;
    G4UIcmdWithAString*        absAngCmd;
    G4UIcmdWithAnInteger*      setGeomCmd;
    G4UIcmdWithAnInteger*      absLayersCmd;
    G4UIcmdWithAString*        abIMatCmd;
    G4UIcmdWithAString*        abIThickCmd;
#ifdef EUCLIDES
    G4UIcmdWithAString*        ElossCmd;
#endif
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};


#endif

#endif
