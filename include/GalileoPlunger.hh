////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef GalileoPlunger_h
#define GalileoPlunger_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "G4GDMLParser.hh"
using namespace std;

class G4Material;
class AgataSensitiveDetector;
class GalileoPlungerMessenger;

class GalileoPlunger : public AgataAncillaryScheme
{
  
public:
  GalileoPlunger(G4String,G4String);
  ~GalileoPlunger();

private:
  GalileoPlungerMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
private:
  G4String     nam_vacuum;
  G4Material  *mat_vacuum;
    
  G4String     nam_aluminium;
    G4String     gdml_path;
  G4Material  *mat_aluminium;
  G4double plunger_distance;
  G4GDMLParser gdmlparser;
  std::map<int,G4LogicalVolume *> pPlungerSupport;
  std::map<int,G4LogicalVolume *> pStopperHolder;
  std::map<int,G4LogicalVolume *> pTargetHolder;
  std::map<int,G4LogicalVolume *> pPlungerMotor;
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
public:
  G4int  FindMaterials           ();
  void   GetDetectorConstruction ();
  void   InitSensitiveDetector   ();
  void   Placement               ();
  bool   GetPlungerMechanics     ();
  bool   GetTargetHolder         ();
  bool   GetStopperHolder        ();
  bool   GetPlungerMotor         ();
public:
  void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
  void   WriteHeader  ( G4String *sheader, G4double=1.*mm );

  void   ShowStatus              ();
  void    SetPlungerDistance(G4double);
public:
  inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector )  { return 0; };
  inline G4int GetCrystalType    ( G4int )                        { return -1; };
  
};
#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class GalileoPlungerMessenger: public G4UImessenger
{
public:
  GalileoPlungerMessenger(GalileoPlunger*, G4String);
  ~GalileoPlungerMessenger();
    
private:
  GalileoPlunger*         myTarget;
  G4UIdirectory*          myDirectory;
  G4UIcmdWithADoubleAndUnit*     SetGalileoPlungerDistanceCmd;
  
public:
  void SetNewValue(G4UIcommand*, G4String);
            
};

#endif
