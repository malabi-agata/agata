
#ifndef AgataGPSGenerator_h
#define AgataGPSGenerator_h 1

#include "AgataGeneratorAction.hh"
#include "globals.hh"

using namespace std;

class G4GeneralParticleSource;
class G4Event;

class AgataGPSGenerator : public AgataGeneration
{
  public:
    AgataGPSGenerator();
    ~AgataGPSGenerator();

  private:
    G4GeneralParticleSource* particleGun;

  public:
     void GeneratePrimaries(G4Event* anEvent);

// The following is added to conform to AgataGeneration
 
   public:
    void BeginOfRun   () {};
    void EndOfRun     () {};
    
   public:
    void BeginOfEvent () {};
    void EndOfEvent   () {};
    
   public:
	void GetEventInfo(std::ostream &eventinfo);
	void GetStatus    () {};
    
   public:
    void PrintToFile  ( std::ofstream &/*outFileLMD*/, G4double=1.*mm, G4double=1.*keV ) {};
    void PrintToFile  ( G4String */*sheader*/, G4double=1.*mm, G4double=1.*keV ) {};  //Chen


   public:
    inline G4String GetBeginOfEventTag ()                   { return G4String(""); };
    inline G4String GetEventHeader     ( G4double = 1.*mm ) { return G4String(""); };
    
   public:
           G4String GetParticleHeader  ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
    
   public:
    inline G4bool   IsStartOfEvent () { return true;  };
    inline G4bool   IsEndOfEvent   () { return true;  };
    inline G4bool   IsAbortRun     () { return false; }; 
 
   public:
    inline G4ThreeVector GetEmitterVelocity () { return G4ThreeVector(0,0,1); };
    inline G4ThreeVector GetEmitterPosition () { return G4ThreeVector(0,0,0); };
    inline G4double      GetEmitterBeta     () { return 0; };
    inline G4int         GetCascadeOrder    () { return 0; };
    inline G4bool        IsSourceLongLived  () { return false; };
    inline G4double      GetCascadeDeltaTime() { return 0; };
    inline G4double      GetCascadeTime     () { return 0; };
   
   public:
    inline G4String GetAbortMessage() { return G4String(""); };
    
   public:
    inline G4int    GetCascadeMult () { return 1; };
     
    


};

#endif
