#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary  defined in gdml for the 
///  AGATA simulation.
/// The geometry included is the Aluminium HoneyComb of Agata
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryDiamantFTgt_h
#define AgataAncillaryDiamantFTgt_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "G4GDMLParser.hh"


using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryDiamant_FTgtMessenger;

class AgataAncillaryDiamant_FTgt : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryDiamant_FTgt(G4String,G4String);
    ~AgataAncillaryDiamant_FTgt();

  private:
    AgataAncillaryDiamant_FTgtMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName;
    G4Material  *matShell;

    G4LogicalVolume* m_LogicalDiamantFTgt;
 
	G4GDMLParser m_gdmlparser;

  
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void WriteHeader  ( G4String *sheader, G4double=1.*mm );
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

};

#endif

#endif
