#ifndef Outgoing_Beam_h
#define Outgoing_Beam_h 1

#include <cmath>
#include <vector>

#include "globals.hh"
#include "G4UnitsTable.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4DynamicParticle.hh"
#include "G4ParticleTable.hh"
#include "G4RandomDirection.hh"
#include "G4Decay.hh"
#include "Randomize.hh"
#include "Incoming_Beam.hh"
#include "Charge_State.hh"
#include "G4LorentzRotation.hh"
#include "G4LorentzVector.hh"

#ifdef G4V10
#include "G4IonTable.hh"
#endif

#include "CAngDistHandler.hh"
#include "CEvapDistHandler.hh"

#include "CSpec1D.hh"
#include "CSpec2D.hh"

// this is? it may be to separate the old and new track...
//#define  eps 0.00001
#define  OBeps (0.000001*mm)

using namespace std;

#ifndef _RecoilFile_
#define _RecoilFile_
struct onelinefromfile {
  G4double v,vx,vy,vz;
};
#endif

class AgataDetectorConstruction;
class Outgoing_Beam_Messenger;
class Outgoing_Beam
{
  public:
     Outgoing_Beam(G4String);
    virtual ~Outgoing_Beam();

  public:
    virtual void Report();

  public:
    virtual void defaultIncomingIon(Incoming_Beam *);
    virtual void setDecayProperties(void);

  public:
    virtual void setA(G4int);
    virtual void setZ(G4int);

  public:
    virtual void setDA(G4int);
    virtual void setDZ(G4int);
    virtual void setEx(G4String);
    virtual void setPartnerEx(G4String);
    virtual void setProjEx(G4double);
    virtual void settau(G4double);

  public:
    virtual void setTarEx1(G4double);
    virtual void setTarEx2(G4double);
    virtual void setCFrac(G4double);
    virtual void setTFrac(G4double);

  public:
    virtual void setPhiRange(G4double, G4double);
    virtual void setThRange (G4double, G4double);

  public:
    virtual void ScanInitialConditions(const G4Track &);
    
  public:
    virtual void EndOfRun ();  

  public:
    virtual inline void     SetReactionOn () { reacted = true;  };
    virtual inline void     SetReactionOff() { reacted = false; };
    virtual inline G4double getTime()        { return tau;      };
    virtual inline G4bool   ReactionOn()     { return reacted;  };
    virtual inline G4double getTFrac()       { return TFrac;    };
  virtual inline void SetRecoilData(std::vector<onelinefromfile> *RD)
  {
    RecoilData=RD;
  }
  public:
    virtual inline G4int    GetReactionFlag()       { return ReactionFlag; };
    virtual inline void     SetReactionFlag(G4int f){ ReactionFlag=f;      };
    virtual inline G4int    GetIndex()              { return Index;        };
    
  private:
    virtual G4double Q_value (G4double,G4double);  

  public:
  std::vector<G4DynamicParticle*> ReactionProduct();
  G4DynamicParticle* ProjectileGS();
  G4DynamicParticle* TargetExcitation();

  public:
  virtual   G4ThreeVector ReactionPosition();
  virtual   G4double GetTargetExcitation();

  public:
  virtual   void     SetNQ(G4int n)  { NQ=n;SetUpChargeStates(); };
  virtual   void     SelectQ(G4int q){ SQ=q;G4cout<<" Charge state "<<SQ<<" selected for setup"<<G4endl; };

  public:
  virtual   void     SetUpChargeStates();
  virtual   void     SetQCharge(G4int);
  virtual   void     SetQUnReactedFraction(G4double);
  virtual   void     SetQReactedFraction(G4double);

  public:
  virtual   void     SetQKEu(G4double);
  virtual   void     SetQKE(G4double);

  public:
  virtual   void     CalcQR();
  virtual   void     CalcQUR();

  public:
  virtual   G4double GetURsetKE();
  virtual   G4double GetRsetKE();

  public:
  virtual   void     SetdpFWHM(G4double){;}
  virtual   void     Setdp(G4double){;}
  virtual   void     SetAngDistFile(G4String);
  virtual   void SelectProductExcitation(){;}
  
  private:
   virtual  G4ThreeVector GetOutgoingMomentum();  //tmp
   virtual  G4ThreeVector GetOutgoingMomentumFE();
   virtual  G4ThreeVector GetOutgoingMomentumTR();
   virtual  G4ThreeVector GetOutgoingMomentumCLX();
   virtual  G4ThreeVector GetOutgoingMomentumFF();
   virtual  G4ThreeVector GetOutgoingMomentumIF();
   virtual  G4ThreeVector TargetAngularDistribution();
   virtual  void SelectExcitionEnergy();

  private:
    Outgoing_Beam_Messenger* myMessenger;

  private:
    G4int Ain;
    G4int Zin;

  private:
    G4ThreeVector dirIn;
    G4ThreeVector posIn;
    G4ThreeVector posOut;
    G4ThreeVector pIn;
  AgataDetectorConstruction *theDetector;
  private:
    G4int  ReactionFlag;

  private:
    G4ParticleTable* particleTable; 

  private:
    G4double      tauIn;
    G4double      KEIn;

  private:
    G4int         n_part[3];
    G4double      e_part[3];
    G4double      s_part[3];

  private:
    G4int DZ;
    G4int DA;

  private:
    G4int Ztar;
    G4int Atar;

  private:
  G4double Ex;
  std::vector<std::pair<G4double,G4double> > listEx;
  G4double totFeeding;
  G4double Ex_partner;
  std::vector<std::pair<G4double,G4double> > listEx_partner;
  G4double totFeeding_partner;
  G4LorentzVector beam4p;
  //Fusion-fission
  G4double nybar; //mean number of neutrons
  G4double sigmanybar; //
  G4double fractionKinE;
  G4double phiMin,phiMax;//In CM (same as LAB)
  G4double thetaMin,thetaMax;//In LAB
  G4double projEx;
  G4double TarEx1;
  G4double TarEx2;
  G4double CFrac;
  G4double TFrac;
  G4double tau;
  std::vector<onelinefromfile> *RecoilData;
private:
  G4int    NQ;
  G4int    SQ;
  vector<Charge_State*> Q;
  G4double  QR[1000];
  G4double  QUR[1000];
  G4int     QRI[1000];
  G4int     QURI[1000];
  G4int     Index;


  private:
    G4ParticleDefinition* ion;
  G4ParticleDefinition* targetrecoil;
  G4ThreeVector         ptargetrecoil;
    G4ParticleDefinition* iongs;
    G4ParticleDefinition* iontar;
    G4ParticleDefinition* ionpro;
    G4ParticleDefinition* iontl;


  private:
    G4bool evapFile[3];
 
  private:
//    static G4Decay decay;
    G4bool  reacted;

  
  private:
   Incoming_Beam *beamIn;
   
  private:
    G4ThreeVector partMomentum;  

  
  private:
    AngDistHandler* theAngDist;
    G4String angDistFileName;
    EvapDistHandler* theEvapDist[3];
    G4String evapDistFileName[3];
  G4double EvapAMaxAngleLab,EvapAMinAngleLab;

  
  private:
    CSpec1D* theSpectrum;
    CSpec2D* theMatrix;
    
  private:
    G4double p_fe;
    G4double p_tr; 
    G4double p_clx;
    G4double p_ff; 
    G4double p_if;
 
 public:
    virtual void SetNumEvapN( G4int );
    virtual void SetNumEvapP( G4int );
    virtual void SetNumEvapA( G4int );
    virtual void SetEnEvapN( G4double );
    virtual void SetSigEvapN( G4double );
    virtual void SetEnEvapP( G4double );
    virtual void SetSigEvapP( G4double );
    virtual void SetEnEvapA( G4double );
    virtual void SetSigEvapA( G4double );
    virtual void SetEvapFromFileN( G4bool );
    virtual void SetEvapFromFileP( G4bool );
    virtual void SetEvapFromFileA( G4bool );
  virtual void SetEvapAMaxAngleLab(G4double);
  virtual void SetEvapAMinAngleLab(G4double);
    virtual void SetPfe( G4double );
    virtual void SetPtr( G4double );
    virtual void SetPclx( G4double );
    virtual void SetPff( G4double );
    virtual void SetPif( G4double );
 
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class Outgoing_Beam_Messenger: public G4UImessenger
{
  public:
    Outgoing_Beam_Messenger(Outgoing_Beam*,G4String);
   ~Outgoing_Beam_Messenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    Outgoing_Beam*             myTarget;    
    G4UIdirectory*             BeamOutDir;
    G4UIdirectory*             QDir;
    G4UIcmdWithAnInteger*      ACmd;
    G4UIcmdWithAnInteger*      ZCmd;
    G4UIcmdWithAnInteger*      DACmd;
    G4UIcmdWithAnInteger*      DZCmd;
    G4UIcmdWithAString* ExCmd;
    G4UIcmdWithAString* PartnerExCmd;
    G4UIcmdWithADoubleAndUnit* projExCmd;
    G4UIcmdWithADoubleAndUnit* TEx1Cmd;
    G4UIcmdWithADoubleAndUnit* TEx2Cmd;
    G4UIcmdWithADouble*        CExFCmd;
    G4UIcmdWithADouble*        TExFCmd;
    G4UIcmdWithADoubleAndUnit* tauCmd;
    G4UIcmdWithoutParameter*   RepCmd;
    G4UIcmdWithAnInteger*      NQCmd;    
    G4UIcmdWithAnInteger*      SQCmd;  
    G4UIcmdWithAnInteger*      SCCmd;  
    G4UIcmdWithADouble*        QUFCmd;  
    G4UIcmdWithADouble*        QRFCmd;  
    G4UIcmdWithADoubleAndUnit* QKECmd;
    G4UIcmdWithADoubleAndUnit* QKEuCmd;
    G4UIcmdWithAString*        aFileCmd;
    G4UIcmdWithAString*        phiCmd;
    G4UIcmdWithAString*        thCmd;
    G4UIcmdWithAnInteger*      npartNCmd;
    G4UIcmdWithAnInteger*      npartPCmd;
    G4UIcmdWithAnInteger*      npartACmd;
    G4UIcmdWithADoubleAndUnit* epartNCmd;
    G4UIcmdWithADoubleAndUnit* spartNCmd;
    G4UIcmdWithABool*          enableEvapFromFileNCmd;
    G4UIcmdWithABool*          disableEvapFromFileNCmd;
    G4UIcmdWithADoubleAndUnit* epartPCmd;
    G4UIcmdWithADoubleAndUnit* spartPCmd;
    G4UIcmdWithABool*          enableEvapFromFilePCmd;
    G4UIcmdWithABool*          disableEvapFromFilePCmd;
    G4UIcmdWithADoubleAndUnit* epartACmd;
    G4UIcmdWithADoubleAndUnit* spartACmd;
    G4UIcmdWithABool*          enableEvapFromFileACmd;
    G4UIcmdWithABool*          disableEvapFromFileACmd;
    G4UIcmdWithADouble*        PfeCmd;  
    G4UIcmdWithADouble*        PtrCmd;  
    G4UIcmdWithADouble*        PclxCmd;  
    G4UIcmdWithADouble*        PffCmd;  
    G4UIcmdWithADouble*        PifCmd;  
  G4UIcmdWithADoubleAndUnit* AlphaMaxThetaLab;
  G4UIcmdWithADoubleAndUnit* AlphaMinThetaLab;
};

#endif


           
