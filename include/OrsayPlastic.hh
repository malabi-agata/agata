////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef OrsayPlastic_h
#define OrsayPlastic_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class OrsayPlasticMessenger;

class OrsayPlastic : public AgataAncillaryScheme
{
  
  public:
  OrsayPlastic(G4String, G4String);
  ~OrsayPlastic();

  private:
    OrsayPlasticMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName;
    G4Material  *matOrsayPlastic;
  
  //////////////////////////
  /// Size of the shell
  /////////////////////////
  private:
    G4double     OrsayPlasticLength;
    G4double     OrsayPlasticInnerR;
    G4double     OrsayPlasticOuterR;
  G4double OrsayPlasticZ;

  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   WriteHeader  ( G4String *sheader, G4double=1.*mm );
    void   ShowStatus              ();

  public:
  inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector );// { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

//  public:
//    inline G4String GetName () { return G4String("SHELL"); };
    
  ////////////////////////////////
  //// Methods for the messenger
  ////////////////////////////////
  public:
    void SetOrsayPlasticRmin              ( G4double );
    void SetOrsayPlasticRmax              ( G4double );
  void SetOrsayPlasticLength              ( G4String ){;} 

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class OrsayPlasticMessenger: public G4UImessenger
{
  public:
    OrsayPlasticMessenger(OrsayPlastic*, G4String);
   ~OrsayPlasticMessenger();
    
  private:
    OrsayPlastic*    myTarget;
    G4UIdirectory*          myDirectory;
    G4UIcmdWithAString*     OrsayPlasticMatCmd;
    G4UIcmdWithAString*     SetOrsayPlasticSizeCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
            
};

#endif
