#ifndef Incoming_Beam_h
#define Incoming_Beam_h 1

#include "globals.hh"
#include "G4UnitsTable.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4ParticleDefinition.hh"
#include "G4DynamicParticle.hh"
#include "Randomize.hh"

using namespace std;

class Incoming_Beam_Messenger;
class Incoming_Beam
{
  public:
     Incoming_Beam(G4String);
    ~Incoming_Beam();

  public:
    void Report();

  public:
    void setA(G4int);
    void setZ(G4int);

  public:
    void setKE(G4double);
    void setKEu(G4double); 

  public:
    void setEx(G4double);

  public:
    void setfcZ         (G4double);
    void setDpp         (G4double);
    void setAvgDirection(G4ThreeVector);
    void setOpAngle     (G4double);
  void setSpotSize(G4double val) {spotSize=val;}
  public:
    G4double      getKE       (G4ParticleDefinition*);
    G4ThreeVector getDirection();

  public:
    inline G4int            getA             () { return A;                      };
    inline G4int            getZ             () { return Z;                      };

  public:
    inline G4RotationMatrix getBeamRotation  () { return beamRotation;           };
    inline G4RotationMatrix getIBeamRotation () { return beamRotation.inverse(); };
    inline G4ThreeVector    getBeamDirection () { return avgDir;                 };
  G4ThreeVector    getPosition();
    inline G4double         getEx            () { return Ex;                     };
    inline G4double         getKE            () { return KE;                     };

  private:
    Incoming_Beam_Messenger* myMessenger;

  private:
    G4int A;
    G4int Z;

  private:
    G4double Ex;

  private:
    G4double KE;
    G4double KEu;
    G4double Dpp;

  private:
    G4double            fcZ;
    G4ThreeVector       avgDir;
    G4double            opAng;
  G4double            spotSize; //Beam spot size
    G4RotationMatrix    beamRotation;          //> rotation from z axis (in case the AGATA
    G4ThreeVector       iniPosition;

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3VectorAndUnit;

class Incoming_Beam_Messenger: public G4UImessenger
{
  public:
    Incoming_Beam_Messenger(Incoming_Beam*, G4String);
   ~Incoming_Beam_Messenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    Incoming_Beam*             myTarget;    
    G4UIdirectory*             BeamInDir;
    G4UIcmdWithAnInteger*      ACmd;
    G4UIcmdWithAnInteger*      ZCmd;
    G4UIcmdWithADoubleAndUnit* KECmd;
    G4UIcmdWithADoubleAndUnit* KEuCmd;
    G4UIcmdWithADoubleAndUnit* fcZCmd;
    G4UIcmdWithADouble*        DppCmd;
    G4UIcmdWithADouble*        ExCmd;
    G4UIcmdWithoutParameter*   RepCmd;
    G4UIcmdWithADouble*        opACmd;
    G4UIcmdWithADoubleAndUnit* spotSCmd;
    G4UIcmdWithAString*        bDirCmd;
};

#endif


           
