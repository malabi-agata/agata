/////////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides an implementation of the abstract AgataEmission class. In this case
/// variable multiplicity events can be treated. Event structure and sequence are decoded
/// from a formatted text file. A nuclear reaction is considered to calculate
/// the position and velocity of the source, which is recomputed following each particle 
/// emission so that momentum and energy are conserved. This is done through an
/// AgataExternalEmitter object. Momentum conservation can be overridden to treat 
/// "beta-decay-like" experiments.
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataExternalEmissionR_h
#define AgataExternalEmissionR_h 1

#include "AgataDefaultGenerator.hh"
#include "AgataExternalEmitter.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"

#include "TFile.h"
#include "TTree.h"

using namespace std;

class AgataEventAction;
class AgataExternalEmitter;
class AgataExternalEmissionRMessenger;
// For AGATA-PRISMA: 
class G4Transportation;

class AgataExternalEmissionR : protected AgataEmission
{
  public:
     AgataExternalEmissionR( G4String, G4bool, G4bool, G4String );
    ~AgataExternalEmissionR();
    
  private:
    AgataExternalEmissionRMessenger* myMessenger;

  private:
    AgataEventAction*          theEvent;
    AgataExternalEmitter*      theExternalEmitter;

  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// The following members specify the format of the file containing the events to be processed
  ///////////////////////////////////////////////////////////////////////////////////////////////
  private:
    int        emitterType;         //> format of the lines containing information on the source
    int        emittedType;         //> format of the lines containing information on the emitted particles

  ///////////////////////////////////
  /// Information on the input file
  ///////////////////////////////////
  private:
    G4String      iniPath;            //> directory where it lies
    G4String      evFileName;         //> name

    TFile        *evFile;             //> rootfile
    TTree        *htree;              //> header info
    TTree        *gtree;              //> event tree

    G4bool        newemitter;
    G4int         Z_emitter;
    G4int         A_emitter;
    G4double      E_emitter;
    G4double      px_emitter;
    G4double      py_emitter;
    G4double      pz_emitter;
    G4double      vx_emitter;
    G4double      vy_emitter;
    G4double      vz_emitter;

    G4int         type_emitted;
    G4double      px_emitted;
    G4double      py_emitted;
    G4double      pz_emitted;
    G4double      E_emitted;
    G4double      vx_emitted;
    G4double      vy_emitted;
    G4double      vz_emitted;
    G4double      E_emitted_CM;
    G4double      vx_emitted_CM;
    G4double      vy_emitted_CM;
    G4double      vz_emitted_CM;
    G4double      T_emitted;  // emitted time
    G4double      P1_emitted; // polarization
    G4double      P2_emitted; // polarization
    G4double      P3_emitted; // polarization
  
    G4int         nentries;
    G4int         ientry;
    G4int         ievent;
    G4int         currentevent;

  private:
    char          bufferLine[256];    //> string where each line of the input file is copied

  ///////////////////////////////////////////
  /// Flag to control some verbosity for ///
  /// the output to file                 ///
  //////////////////////////////////////////
  private:
    G4bool        verbose;           //> true: "standard" output; false: "compact" output
  
  ////////////////////////////////////////////
  /// Information on the event processing
  ///////////////////////////////////////////
  private:
    G4bool        endOfRun;          //> true: run will be aborted at the next emitted particle
    G4bool        nucleusIsEmitted;  //> true: next emitted particle will be the residual nucleus
    G4bool        firstLine;         //> true: the first line describing a new event is being decoded
    G4bool        beginOfEvents;     //> true: the begin-of-event tag has been reached decoding the input file

    G4bool        Emitterdecoded;    //> true: the source has been decoded from the input file
    G4bool        nextentry;

    G4bool        openedFile;        //> true: event file has been opened
    G4bool        emitNuclei;        //> true: emission of residual nuclei is enabled
    G4bool        showStatus;        //> true: status is printed on-screen (disabled at class construction)
    G4bool        isBetaDecay;       //> true: momentum conservation is disabled

  private:
    G4int         readEvents;        //> number of events already read
    G4int         skipEvents;        //> number of events to be skipped (at the beginning of file)
    G4int         processedEvents;   //> number of events already processed
    G4int         targetEvents;      //> number of events which should be processed
    

  ////////////////////////////////////////////
  /// Information on the emitted objects
  ///////////////////////////////////////////
  private:
    G4int         nEmitted;         //> number of emitted objects
    G4int        *emittedLUT;       //> lookup table for the emitted objects (particle type --> index)
    G4int         maxIndex;         //> maximum index for the emitted objects

  ////////////////////////////////
  /// Information on the beam
  ////////////////////////////////
  private:
    G4int         aBeam;            //> mass number
    G4int         zBeam;            //> charge number
    G4double      eBeam;            //> energy

  ////////////////////////////////
  /// Information on the target
  ////////////////////////////////
  private:
    G4int         aTarg;            //> mass number
    G4int         zTarg;            //> charge number
    
  //////////////////////////////////////////////////////////////////
  /// Information on the emitted particle (changes event-by-event)
  //////////////////////////////////////////////////////////////////
  private:
    G4int         emittedAtomNum;   //> mass number    
    G4int         emittedMassNum;   //> charge number  
    G4ThreeVector emittedMomentum;  //> momentum         
    
  //////////////////////////////////////////////////////////////////
  /// Information on the residual nucleus (changes event-by-event)
  //////////////////////////////////////////////////////////////////
  private:
    G4int         emitterAtomNum;   //> mass number  
    G4int         emitterMassNum;   //> charge number
    
  private:
    G4bool        createdEmitted;

  //////////////////////////
  /// Command directory  ///
  //////////////////////////
  private:
    G4String      directoryName;     //> directory name


  ////////////////////////
  /// Private methods ///
  ///////////////////////
  
  private:
    void      ReadFileHeader  ();   //> decodes the input file header
    void      InitEventTree   ();
  
  private:
    G4String  FetchEmittedName ( G4int  );   //> lookup table particle code --> particle name
    void      DecodeEmitted    ();   //> decodes a line containing information on an emitted particle
    void      DecodeEmitter    ();   //> decodes a line containing information on the source
    void      FetchEmitter     ();           //> sets the residual nucleus as the next emitted particle
    G4bool    IsHadron         ( G4int  );   //> true: particle corresponding to the argument is a hadron

  ////////////////////////
  /// Public methods ///
  ///////////////////////

  ////////////////////////
  /// Begin/end of run
  ///////////////////////
  public:
    void      BeginOfRun();
    void      EndOfRun();

  ////////////////////////
  /// Begin/end of event
  ///////////////////////
  public:
    void      BeginOfEvent();
    void      EndOfEvent();
    
    
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// Stores name, energy, direction, polarizatin, time of emission of the next emitted particle
  ///////////////////////////////////////////////////////////////////////////////////////////////
  public:
    void      NextParticle();  
        
  public:
    void      GetStatus();
    void      PrintToFile( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV );
    void      PrintToFile( G4String *sheader, G4double=1.*mm, G4double=1.*keV );

  public:
    inline G4String GetBeginOfEventTag();

  public:
    G4String GetEventHeader    ( G4double=1.*mm );
    G4String GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
        
  ///////////////////////////////////
  /// Methods for the messenger
  ///////////////////////////////////
  public:
    void      SetEventFile   ( G4String );
    void      SetEmitNuclei  ( G4bool   );
    void      SetBetaDecay   ( G4bool   );
    void      SetSkipEvents  ( G4int    );
    void      SetTargetEvents( G4int    );
    void      SetVerbose     ( G4bool   );
   
  /////////////////////////////////////////////
  /// Inline "get" methods: recoil nucleus
  /////////////////////////////////////////////
  public:
    inline G4int         GetEmitterAtomNum()      { return emitterAtomNum;   };
    inline G4int         GetEmitterMassNum()      { return emitterMassNum;   };

  // AGATA-PRISMA
  public:
    inline G4int GetEmitterIonicCharge() {return emitterIonicCharge;}
  private:
    G4int emitterIonicCharge;

  /////////////////////////////////////////////
  /// Inline "get" methods: emitted particle
  /////////////////////////////////////////////
  public:
    inline G4ThreeVector GetEmittedMomentum()     { return emittedMomentum;  };
    inline G4int         GetEmittedAtomNum()      { return emittedAtomNum;   };
    inline G4int         GetEmittedMassNum()      { return emittedMassNum;   };
  
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;
class G4UIcmdWithAnInteger;

class AgataExternalEmissionRMessenger: public G4UImessenger
{
  public:
    AgataExternalEmissionRMessenger(AgataExternalEmissionR*, G4bool, G4String);
   ~AgataExternalEmissionRMessenger();

  private:
    G4bool                     hadrons;
    
  private:
    AgataExternalEmissionR*    myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             mySubDirectory;
    G4UIcmdWithAString*        SetEventFileCmd;
    G4UIcmdWithABool*          EnableBetaCmd;
    G4UIcmdWithABool*          DisableBetaCmd;
    G4UIcmdWithABool*          EnableNucleiCmd;
    G4UIcmdWithABool*          DisableNucleiCmd;
    G4UIcmdWithABool*          VerboseOutputCmd;
    G4UIcmdWithABool*          ReducedOutputCmd;
    G4UIcmdWithAnInteger*      SetSkipEventsCmd;
    G4UIcmdWithAnInteger*      SetTargetEventsCmd;

  public:
    void SetNewValue(G4UIcommand*, G4String);

};


#endif
