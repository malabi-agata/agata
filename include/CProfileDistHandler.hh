
#ifndef ProfileDistHandler_h
#define ProfileDistHandler_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>
//#include <ios>
//#include <iostream>
//#include <istream>
#include <fstream>

//using namespace std;

#ifdef G4V10
using namespace CLHEP;
#endif

class ProfileDistHandler
{
  public:
    ProfileDistHandler( G4String );
    ~ProfileDistHandler();
    
  private:
    void ReadDistribution(std::ifstream& inFile);  
    
  public:
    void ReNew           (G4String);  
    
  public:
    G4double      GetDepth   ( );
    G4ThreeVector GetDirection( G4double );
    
  public:
    void          SetPhiRange ( G4double, G4double );
    void          SetThRange  ( G4double, G4double );
    
//   private:
//     void InitData( G4double, G4double, G4double, G4double, G4double, G4double );    

  // range in energy
  private:
    double E_min;
    double E_max;
    double E_dif;
    
  // range in angle
  private:
    double th_min;
    double th_max;
    double th_dif;
    
  // phi range for emission
  private:
    double phi_min;
    double phi_dif;
    
  // theta range for emission
  private:
    double the_min;
    double the_dif;

  private:
    G4bool goodDistribution;

  private:
    G4bool fixedTheta;
    
  // angular distribution data in differential and integral form  
  private:
//     std::vector<std::vector<double> > difProfileDist;  
//     std::vector<std::vector<double> > intProfileDist; 
//     std::vector<double>               eneDifDist; 
//     std::vector<double>               eneIntDist; 
  std::vector<double>               thickVal;
  std::vector<double>               thickPb;
  std::vector<double>               thickPbNorm;
  std::vector<double>               intProfileDist;

  public:
    inline G4bool IsGoodDistribution() { return goodDistribution; };   


};

#endif
