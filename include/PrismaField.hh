#ifndef PrismaField_h
#define PrismaField_h 1

#include "globals.hh"
#include "G4MagneticField.hh"
#include "G4ios.hh"
#include "G4ThreeVector.hh"
#include <vector> // for geant4 10.7

using namespace CLHEP;
using namespace std;

class PrismaField : public G4MagneticField
{  
  public:

    PrismaField();
    ~PrismaField();
  
    void  GetFieldValue(const G4double Point[3], G4double *Bfield) const;

  private:

    G4double fFieldComponents[3];

    G4int nbX, nbY, nbZ;
//    std::vector<G4double> posX, posY, posZ; // 
    vector< G4double > posX, posY, posZ;  // for geant4 10.7

    vector< vector< vector< G4double > > > fieldX;
    vector< vector< vector< G4double > > > fieldY;
    vector< vector< vector< G4double > > > fieldZ;
    
    // Limits of the quandrupole magnetic field
    G4double minPosX, maxPosX, minPosY, maxPosY, minPosZ, maxPosZ;
    G4double sizeX, sizeY, sizeZ;

    // Dipole geometry
    G4double curvature_outerRadius; 
    G4double curvature_innerRadius; 
    G4double dipole_halfHeight;
};

#endif
