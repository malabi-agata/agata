//////////////////////////////////////////////////////////////
/// This class is used only as an example of "non-standard"
/// event generator (opposite to the "standard" one for
/// the Agata code). User should provide his own code!!!
//////////////////////////////////////////////////////////////

#ifndef AgataTestGenerator_h
#define AgataTestGenerator_h 1

#include "AgataGeneratorAction.hh"
#include "globals.hh"

using namespace std;

class G4ParticleGun;
class G4Event;
class AgataTestMessenger;

class AgataTestGenerator : public AgataGeneration
{
  public:
    AgataTestGenerator();    
   ~AgataTestGenerator();

  private:
    G4ParticleGun*              particleGun;
//Added by Joa Ljungvall 9/2012 to allow sending particles in specific directions
  AgataTestMessenger*    myMessenger;
  bool ShootIsotropic;
  public:
    void BeginOfRun   () {};
    void EndOfRun     () {};
//Added by Joa Ljungvall 9/2012 to allow sending particles in specific directions
  void SetShootIsotropic(bool val) {ShootIsotropic=val;}
  public:
    void BeginOfEvent () {};
    void EndOfEvent   () {};
    
  public:
  void GetEventInfo(std::ostream &eventinfo){eventinfo.clear();}
    void GetStatus    () {};
    
  public:
    void PrintToFile  ( std::ofstream &/*outFileLMD*/, G4double=1.*mm, G4double=1.*keV ) {};
    void PrintToFile  ( G4String */*sheader*/, G4double=1.*mm, G4double=1.*keV ) {};  //Chen

    
  public:
    inline G4String GetBeginOfEventTag ()                   { return G4String(""); };
    inline G4String GetEventHeader     ( G4double = 1.*mm ) { return G4String(""); };
    
  public:
           G4String GetParticleHeader  ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
    
  public:
    inline G4bool   IsStartOfEvent () { return true;  };
    inline G4bool   IsEndOfEvent   () { return true;  };
    inline G4bool   IsAbortRun     () { return false; }; 
    
   public:
    inline G4ThreeVector GetEmitterVelocity () { return G4ThreeVector(0,0,1); };
    inline G4ThreeVector GetEmitterPosition () { return G4ThreeVector(0,0,0); };
    inline G4double      GetEmitterBeta     () { return 0; };
    inline G4int         GetCascadeOrder    () { return 0; };
    inline G4bool        IsSourceLongLived  () { return false; };
    inline G4double      GetCascadeDeltaTime() { return 0; };
    inline G4double      GetCascadeTime     () { return 0; };


  public:
    inline G4String GetAbortMessage() { return G4String(""); };
    
  public:
    inline G4int    GetCascadeMult () { return 1; };
    
  public:
    void     GeneratePrimaries ( G4Event* );
    
};


//Added by Joa Ljungvall 9/2012 to allow sending particles in specific directions
#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class AgataTestMessenger: public G4UImessenger
{
  public:
    AgataTestMessenger(AgataTestGenerator*, G4String);
   ~AgataTestMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataTestGenerator*     myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithABool*      SetTargetIsotropic;

};

#endif
